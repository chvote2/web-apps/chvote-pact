# CHVote: PACT 
[![pipeline status](https://gitlab.com/chvote2/web-apps/chvote-pact/badges/master/pipeline.svg)](https://gitlab.com/chvote2/web-apps/chvote-pact/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chvote-pact&metric=alert_status)](https://sonarcloud.io/dashboard?id=chvote-pact)

The Privileged Actions Confirmation Tool (PACT) is an application at the centre of the CHVote platform, as the 
orchestrator of vote / election operations configuration and deployment.

It provides an **API** to submit operations configurations, request their statuses and trigger certain events, as well
as a **User Interface** to visualize, validate and execute the critical actions following the _four eyes principle_,
in a traceable, secured - and trusted - environment.

# Components

- [pact-back](pact-back): The server side application component.
- [pact-front](pact-front): The web client component.
- [pact-reverse-proxy](pact-reverse-proxy): The NGINX configuration files of the reverse proxy component.
- [pact-docker-compose](pact-docker-compose): The set of docker compose description files to create a working environment.
- [docs](docs): A module to build the design documentation.
- [deploy](deploy): The Kubernetes deployment configuration files.

# Build and run

There are 2 components to build and run: the [backend server](pact-back) and the [frontend application](pact-front).

# Documentation

- [Application design documentation](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/docs/target/generated-docs/pdf/pact-application-design.pdf?job=artifact%3Adocs)
- [PACT REST API contract](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-back/pact-back-rest/target/site/pact-swagger.html?job=api%3Aswagger)
- [PACT Scheduler REST API contract](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-back/pact-back-rest/target/site/pact-swagger.html?job=api%3Aswagger)
- [E2E tests report](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-front/test_reports/protractor/index.html?job=check%3Ae2e)
- Backend code coverage reports:
    - [pact-b2b-client](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-back/pact-b2b-client/target/site/jacoco/jacoco.xml?job=artifact%3Aback)
    - [pact-back-contract](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-back/pact-back-contract/target/site/jacoco/jacoco.xml?job=artifact%3Aback)
    - [pact-back-fixtures](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-back/pact-back-fixtures/target/site/jacoco/jacoco.xml?job=artifact%3Aback)
    - [pact-back-repository](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-back/pact-back-repository/target/site/jacoco/jacoco.xml?job=artifact%3Aback)
    - [pact-back-rest](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-back/pact-back-rest/target/site/jacoco/jacoco.xml?job=artifact%3Aback)
    - [pact-back-scheduler](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-back/pact-back-scheduler/target/site/jacoco/jacoco.xml?job=artifact%3Aback)
    - [pact-back-service](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-back/pact-back-service/target/site/jacoco/jacoco.xml?job=artifact%3Aback)
    - [pact-notification-events](https://gitlab.com/chvote2/web-apps/chvote-pact/builds/artifacts/master/raw/pact-back/pact-notification-events/target/site/jacoco/jacoco.xml?job=artifact%3Aback)

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software, released under the [Affero General Public License 3.0](LICENSE) license.
