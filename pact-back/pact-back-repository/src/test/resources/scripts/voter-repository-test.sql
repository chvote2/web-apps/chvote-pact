-- 1 voter associated to protocol instance #1401 and 2 to protocol instance #2401

INSERT INTO PACT_T_VOTER(VOTER_N_ID, VOTER_N_PROTOCOL_VOTER_INDEX, VOTER_V_LOCAL_PERSON_ID, VOTER_N_DATE_OF_BIRTH_YEAR, VOTER_N_COUNTING_CIRCLE_INDEX, VOTER_V_COUNTING_CIRCLE_BUSINESS_ID, VOTER_V_COUNTING_CIRCLE_NAME, VOTER_V_PRINTING_AUTHORITY, VOTER_N_PROTOCOL_INSTANCE_ID) VALUES (1, 1, '1.1', 1984, 1, 'GE', 'Geneve', 'printer1', 1401);
INSERT INTO PACT_T_VOTER(VOTER_N_ID, VOTER_N_PROTOCOL_VOTER_INDEX, VOTER_V_LOCAL_PERSON_ID, VOTER_N_DATE_OF_BIRTH_YEAR, VOTER_N_COUNTING_CIRCLE_INDEX, VOTER_V_COUNTING_CIRCLE_BUSINESS_ID, VOTER_V_COUNTING_CIRCLE_NAME, VOTER_V_PRINTING_AUTHORITY, VOTER_N_PROTOCOL_INSTANCE_ID) VALUES (2, 1, '2.1', 1970, 1, 'GE', 'Geneve', 'printer1', 2401);
INSERT INTO PACT_T_VOTER(VOTER_N_ID, VOTER_N_PROTOCOL_VOTER_INDEX, VOTER_V_LOCAL_PERSON_ID, VOTER_N_DATE_OF_BIRTH_YEAR, VOTER_N_COUNTING_CIRCLE_INDEX, VOTER_V_COUNTING_CIRCLE_BUSINESS_ID, VOTER_V_COUNTING_CIRCLE_NAME, VOTER_V_PRINTING_AUTHORITY, VOTER_N_PROTOCOL_INSTANCE_ID) VALUES (3, 2, '2.2', 1978, 1, 'VD', 'Vaud', 'printer2', 2401);

INSERT INTO PACT_T_VOTER_DOIS(VOTER_ID, DOI_ID) VALUES (1, 'CH');
INSERT INTO PACT_T_VOTER_DOIS(VOTER_ID, DOI_ID) VALUES (1, 'GE');
INSERT INTO PACT_T_VOTER_DOIS(VOTER_ID, DOI_ID) VALUES (2, 'GE');
INSERT INTO PACT_T_VOTER_DOIS(VOTER_ID, DOI_ID) VALUES (3, 'VD');