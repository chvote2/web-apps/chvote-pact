/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol

import ch.ge.ve.chvote.pactback.repository.PactRepositoryTest
import java.time.LocalDateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.jdbc.Sql

class DeadLetterEventRepositoryTest extends PactRepositoryTest {

  @Autowired
  DeadLetterEventRepository repository

  @Sql(scripts = "classpath:scripts/protocol-message-repository-test.sql")
  def "deleteByProtocolId should remove the associated messages"() {
    when:
    def number = repository.deleteByProtocolId("1")
    repository.flush()

    then: "4 messages should have been deleted"
    number == 4L

    and: "the messages should not be in the database anymore"
    table("PACT_T_DLX_EVENT").collect { it.get("DLXEVENT_N_ID") } == [4L, 5L]
  }

  @Sql(scripts = "classpath:scripts/protocol-message-repository-test.sql")
  def "existsByProtocolIdAndTypeAndCreationDateAndContent should return whether an event is already stored"() {
    def creationDate = LocalDateTime.of(2018, 5, 25, 9, 0, 0)

    expect:
    repository.existsByProtocolIdAndTypeAndCreationDateAndContent("1", "Event", creationDate, "message body")
    !repository.existsByProtocolIdAndTypeAndCreationDateAndContent("1", "Event", creationDate, "hello world!")
    !repository.existsByProtocolIdAndTypeAndCreationDateAndContent("1", "Event!", creationDate, "hello world")
    !repository.existsByProtocolIdAndTypeAndCreationDateAndContent("1!", "Event", creationDate, "hello world")
  }
}
