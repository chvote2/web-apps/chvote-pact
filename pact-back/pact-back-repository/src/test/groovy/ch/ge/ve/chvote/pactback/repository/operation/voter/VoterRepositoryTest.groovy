/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.voter

import ch.ge.ve.chvote.pactback.repository.PactRepositoryTest
import java.util.stream.Collectors
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.test.context.jdbc.Sql

class VoterRepositoryTest extends PactRepositoryTest {

  @Autowired
  VoterRepository repository

  @Sql(scripts = "classpath:scripts/voter-repository-test.sql")
  def "findByProtocolInstance_Id(long) should return the associated voters"() {
    expect:
    repository.findByProtocolInstance_ProtocolId("1").map { it.id }.collect(Collectors.toSet()) == [1L] as Set
    repository.findByProtocolInstance_ProtocolId("2").map { it.id }.collect(Collectors.toSet()) == [2L, 3L] as Set
  }

  @Sql(scripts = "classpath:scripts/voter-repository-test.sql")
  def "findByProtocolInstance_Id(long, Pageable) should return the associated voters by page"() {
    expect:
    repository.findByProtocolInstance_Id(2401L, new PageRequest(0, 1)).totalElements == 2
    repository.findByProtocolInstance_Id(2401L, new PageRequest(0, 1)).totalPages == 2
    repository.findByProtocolInstance_Id(2401L, new PageRequest(0, 1)).size == 1
    repository.findByProtocolInstance_Id(2401L, new PageRequest(0, 1, Sort.by(Sort.Order.asc("id")))).content[0].id == 2L
  }

  @Sql(scripts = "classpath:scripts/voter-repository-test.sql")
  def "deleteByProtocolInstanceId should remove the associated voters"() {
    when:
    repository.deleteByProtocolInstance_Id(2401L)
    repository.flush()

    then: "2 voters should have been deleted"
    table("PACT_T_VOTER").collect { it.get("VOTER_N_ID") } == [1L]
  }
}
