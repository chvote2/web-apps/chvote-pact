/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.user

import ch.ge.ve.chvote.pactback.repository.PactRepositoryTest
import org.springframework.beans.factory.annotation.Autowired

class UserRepositoryTest extends PactRepositoryTest {
  @Autowired
  UserRepository repository

  def "findByUsername should work as expected"() {
    given:
    // the data in import.sql

    when:
    def user = repository.findByUsername(username)

    then:
    user.isPresent()
    user.get().id == id

    where:
    username || id
    'user1'  || 1
    'user2'  || 2
    'user3'  || 3
  }
}
