/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository

import ch.ge.ve.chvote.pactback.repository.conf.TestConfig
import javax.sql.DataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.jdbc.SqlScriptsTestExecutionListener
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener
import org.springframework.test.context.support.DirtiesContextTestExecutionListener
import org.springframework.test.context.transaction.TransactionalTestExecutionListener
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification

@Transactional
@ContextConfiguration(classes = TestConfig.class)
@TestExecutionListeners([
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        SqlScriptsTestExecutionListener.class
])
abstract class PactRepositoryTest extends Specification {

  @Autowired
  DataSource dataSource

  /**
   * Fetch rows from the database, inside the test transaction
   *
   * @param sqlQuery the query to execute to get the rows
   * @return a list where each row is represented as a map : [ column_1: value_2, column_2: value_2 ]
   */
  List<Map> rows(String sqlQuery) {
    return new JdbcTemplate(dataSource).queryForList(sqlQuery)
  }

  /**
   * Fetch rows - with a subset of columns - from a table in the database, inside the test transaction
   *
   * @param tableName the table to get the rows from
   * @param columns the columns to get
   * @return a list where each row is represented as a map : [ column_1: value_2, column_2: value_2 ]
   */
  List<Map> table(String tableName, List<String> columns) {
    String sql = "SELECT ${columns.join(',')} FROM $tableName"
    return rows(sql)
  }

  /**
   * Fetch all rows from a table in the database, inside the test transaction
   *
   * @param tableName the table to get the rows from
   * @return a list where each row is represented as a map : [ column_1: value_2, column_2: value_2 ]
   */
  List<Map> table(String tableName) {
    String sql = "SELECT * FROM $tableName"
    return rows(sql)
  }

}