/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.action.deploy

import ch.ge.ve.chvote.pactback.repository.PactRepositoryTest
import ch.ge.ve.chvote.pactback.repository.action.materials.VotingMaterialsCreationActionRepository
import javax.annotation.Resource

class VotingMaterialsCreationActionRepositoryTest extends PactRepositoryTest {

  @Resource
  VotingMaterialsCreationActionRepository repository

  def "existsByVotingMaterialsConfiguration_Id should find existence by id"() {
    given:
    // the data in import.sql

    when:
    def exists = repository.existsByVotingMaterialsConfiguration_Id(id)

    then:
    exists == expectedResult

    where:
    id    || expectedResult
    5601L || true
    9601L || false
    27L   || false
  }

}
