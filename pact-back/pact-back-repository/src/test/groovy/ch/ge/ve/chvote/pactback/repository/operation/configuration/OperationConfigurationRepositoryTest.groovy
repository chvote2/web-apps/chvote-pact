/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration

import ch.ge.ve.chvote.pactback.repository.PactRepositoryTest
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import java.util.stream.Collectors
import java.util.stream.LongStream
import javax.annotation.Resource

class OperationConfigurationRepositoryTest extends PactRepositoryTest {

  @Resource
  OperationConfigurationRepository repository

  def "getOne should retrieve a operation configuration together with its privileged action"() {
    given:
    // The data in import.sql

    when:
    def operationConfiguration = repository.getOne(3201L)

    then:
    operationConfiguration.action.get().id == 3501L
  }

  def "getOne should retrieve a operation configuration together with its operationReferenceFiles"() {
    given:
    // The data in import.sql

    when:
    def operationConfiguration = repository.getOne(1201L)

    then:
    operationConfiguration.operationReferenceFiles.size() == 2
  }

  def "getOne should retrieve an operation configuration with its election site configurations"() {
    given:
    // The data in import.sql

    when:
    def operationConfiguration = repository.getOne(1201L)

    then:
    operationConfiguration.electionSiteConfigurations.size() == 1
  }

  def "findAllDeployableOrPendingConfigurations should find all configuration that have no action or are not approved"() {
    given:
    // The data in import.sql

    when:
    def operationConfigurations = repository.findAllConfigurationsDeployableOrWithDeploymentInStatus(PrivilegedAction.Status.PENDING)
    def operationConfigurationIds = operationConfigurations.map() { c -> c.id }.collect(Collectors.toSet())

    then:
    operationConfigurationIds == LongStream.of(1201L, 2201L, 3201L, 4201L, 10201).boxed().collect(Collectors.toSet())
  }

}
