/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration

import ch.ge.ve.chvote.pactback.repository.PactRepositoryTest
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository
import java.util.stream.Collectors
import javax.annotation.Resource

class VotingMaterialsConfigurationRepositoryTest extends PactRepositoryTest {

  @Resource
  VotingMaterialsConfigurationRepository repository

  def "findAllByCreationActionIsNullOrCreationAction_Status should find all configurations that have no action or are not approved"() {
    given:
    // The data in import.sql

    when:
    def votingMaterialsConfigurations = repository.findAllByCreationActionIsNullOrCreationActionStatus(PrivilegedAction.Status.PENDING)
    def votingMaterialsConfigurationIds = votingMaterialsConfigurations.map( { c -> c.id }).collect(Collectors.toSet())

    then:
    votingMaterialsConfigurationIds == [5601L, 9601L] as Set
  }

}
