/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol

import ch.ge.ve.chvote.pactback.repository.PactRepositoryTest
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats
import javax.persistence.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.jdbc.Sql

class ProtocolInstanceRepositoryTest extends PactRepositoryTest {

  @Autowired
  ProtocolInstanceRepository repository

  // static factory method to create VotersCreationStats objects
  static newStatistics(String printer, int countingCircle, long number, String businessId) {
    def stats = new VotersCreationStats()
    stats.printerAuthorityName = printer
    stats.countingCircleId = countingCircle
    stats.countingCircleName = "cc" + countingCircle
    stats.numberOfVoters = number
    stats.countingCircleBusinessId = businessId

    return stats
  }

  def "mapping - VotersCreationStats should be persisted in cascade"() {
    given:
    def instance = repository.findById(1401L).orElseThrow({ _ -> new EntityNotFoundException() })
    instance.statistics.addAll([
            newStatistics("ABCD", 1, 50, "1001"),
            newStatistics("EFGH", 2, 48, "1002")
    ])

    when:
    repository.flush()   // to actually put them in database

    then:
    table("PACT_T_VOT_CREATION_STATS",
            ["VOSTAT_N_PINSTANCE_ID", "VOSTAT_V_PRINTER", "VOSTAT_N_COUNTING_CIRCLE_ID", "VOSTAT_N_COUNTING_CIRCLE_NAME",
             "VOSTAT_N_VOTERS", "VOSTAT_V_COUNTING_CIRCLE_B_ID"]) == [
            [VOSTAT_N_PINSTANCE_ID        : 1401L, VOSTAT_V_PRINTER: "ABCD", VOSTAT_N_COUNTING_CIRCLE_ID: 1,
             VOSTAT_N_COUNTING_CIRCLE_NAME: "cc1", VOSTAT_N_VOTERS: 50L, VOSTAT_V_COUNTING_CIRCLE_B_ID: "1001"],
            [VOSTAT_N_PINSTANCE_ID        : 1401L, VOSTAT_V_PRINTER: "EFGH", VOSTAT_N_COUNTING_CIRCLE_ID: 2,
             VOSTAT_N_COUNTING_CIRCLE_NAME: "cc2", VOSTAT_N_VOTERS: 48L, VOSTAT_V_COUNTING_CIRCLE_B_ID: "1002"]
    ]
  }

  def "mapping - VotersCreationStats should be unique per PIid, printer and counting circle"() {
    given:
    def instance = repository.findById(1401L).orElseThrow({ _ -> new EntityNotFoundException() })
    instance.statistics.addAll([
            newStatistics("Printer Solo", 1, 50, "1001"),
            newStatistics("Printer Solo", 1, 48, "1002")
    ])

    when:
    repository.flush()

    then:
    thrown DataIntegrityViolationException
  }

  @Sql(scripts = "classpath:scripts/voters-creation-stats.sql")
  def "mapping - ProtocolInstance should permit navigation to the stats"() {
    given:
    def instance = repository.findById(1401L).orElseThrow({ _ -> new EntityNotFoundException() })

    when:
    def stats = instance.statistics

    then:
    stats.collect {
      [it.printerAuthorityName, it.countingCircleId, it.countingCircleName,
       it.numberOfVoters, it.countingCircleBusinessId]
    } == [
            ["PRINTER 1", 1, "cc1", 350L, "1001"],
            ["PRINTER 1", 2, "cc2", 47L, "1002"],
            ["PRINTER 2", 1, "cc1", 122L, "1001"]
    ]
  }

  @Sql(scripts = "classpath:scripts/voters-creation-stats.sql")
  def "mapping - VotersCreationStats should be cascade-deleted"() {
    given:
    def instance = repository.findById(1401L).orElseThrow({ _ -> new EntityNotFoundException() })
    def config = instance.configuration

    when:
    config.setProtocolInstance(null) // necessary to allow ProtocolInstance's deletion
    repository.deleteById(1401L)
    repository.flush()

    then:
    rows("SELECT * FROM PACT_T_PROTOCOL_INSTANCE WHERE PINSTANCE_N_ID = 1401").isEmpty()
    table("PACT_T_VOT_CREATION_STATS").isEmpty()
  }

  @Sql(statements = [
          "UPDATE PACT_T_PROTOCOL_INSTANCE SET PINSTANCE_V_STATUS = 'CREDENTIALS_GENERATED' WHERE PINSTANCE_N_ID IN (3401, 6401)",
          "UPDATE PACT_T_PROTOCOL_INSTANCE SET PINSTANCE_V_NODE_KEY = 'OtherNode' WHERE PINSTANCE_N_ID = 6401"
  ])
  def "findByNodeRepartitionKeyAndStatusIn filters as expected"() {
    given:
    def dataNodeKey = "TestNode"
    def statusList = [ProtocolInstanceStatus.CREDENTIALS_GENERATED, ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES]

    when:
    repository.findAll().collect { [it.id, it.nodeRepartitionKey, it.status] }.each System.&println
    def all = repository.findByNodeRepartitionKeyAndStatusIn(dataNodeKey, statusList)

    then: "3 match the statuses, only 2 match the nodeRepartitionKey as well"
    all*.id == [3401L, 7401L]
  }


  def "get by protocol id should return an expected instance"() {
    when:
    def result = repository.getByProtocolId("3")

    then:
    result.isPresent()
    result.get().id == 3401
  }

  def "findByStatus should filter as expected"() {
    when:
    def values = repository.findByStatus(ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES)

    then:
    values*.id == [7401L]
  }
}
