/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.action.deploy.entity;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * A specialised {@link PrivilegedAction} linked to a {@link OperationConfiguration} entity.
 */
@Entity
@Table(name = "PACT_T_CONFIGURATION_DEPLOYMENT")
public class ConfigurationDeployment extends PrivilegedAction {
  public static final String TYPE = "CONFIGURATION_DEPLOYMENT";

  @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
  @JoinColumn(name = "CD_N_OPERATION_CONFIGURATION_ID")
  private OperationConfiguration operationConfiguration;

  public ConfigurationDeployment() {
    super(TYPE);
  }

  public OperationConfiguration getOperationConfiguration() {
    return operationConfiguration;
  }

  public void setOperationConfiguration(OperationConfiguration operationConfiguration) {
    this.operationConfiguration = operationConfiguration;
  }

}
