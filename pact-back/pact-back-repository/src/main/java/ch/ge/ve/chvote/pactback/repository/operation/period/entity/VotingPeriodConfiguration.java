/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.chvote.pactback.repository.operation.period.entity;

import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.entity.VotingPeriodInitialization;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.PublicKeyFile;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * An entity that represents the voting period configuration for an operation.
 */
@Entity
@Table(name = "PACT_T_VOT_PER_CONF")
public class VotingPeriodConfiguration {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "VPC_N_ID")
  private Long id;

  @Column(name = "VPC_V_AUTHOR", nullable = false)
  private String author;

  @Column(name = "VPC_D_SUBMISSION_DATE", nullable = false)
  private LocalDateTime submissionDate;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
  @JoinColumn(name = "VPC_V_ELECTORAL_AUTHORITY_KEY")
  private PublicKeyFile electoralAuthorityKey;

  @Column(name = "VPC_V_TARGET")
  @Enumerated(EnumType.STRING)
  private DeploymentTarget target;

  @Column(name = "VPC_V_SIMULATION_NAME")
  private String simulationName;

  @Column(name = "VPC_D_SITE_OPENING_DATE", nullable = false)
  private LocalDateTime siteOpeningDate;

  @Column(name = "VPC_D_SITE_CLOSING_DATE", nullable = false)
  private LocalDateTime siteClosingDate;

  @Column(name = "VPC_N_GRACE_PERIOD", nullable = false)
  private Integer gracePeriod;

  @OneToOne(cascade = CascadeType.ALL, mappedBy = "votingPeriodConfiguration", fetch = FetchType.EAGER,
            orphanRemoval = true)
  private VotingPeriodInitialization action;

  public Long getId() {
    return id;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public LocalDateTime getSubmissionDate() {
    return submissionDate;
  }

  public void setSubmissionDate(LocalDateTime submissionDate) {
    this.submissionDate = submissionDate;
  }

  public PublicKeyFile getElectoralAuthorityKey() {
    return electoralAuthorityKey;
  }

  public void setElectoralAuthorityKey(PublicKeyFile electoralAuthorityKey) {
    this.electoralAuthorityKey = electoralAuthorityKey;
  }

  public DeploymentTarget getTarget() {
    return target;
  }

  public void setTarget(DeploymentTarget target) {
    this.target = target;
  }

  public String getSimulationName() {
    return simulationName;
  }

  public void setSimulationName(String simulationName) {
    this.simulationName = simulationName;
  }

  public LocalDateTime getSiteOpeningDate() {
    return siteOpeningDate;
  }

  public void setSiteOpeningDate(LocalDateTime siteOpeningDate) {
    this.siteOpeningDate = siteOpeningDate;
  }

  public LocalDateTime getSiteClosingDate() {
    return siteClosingDate;
  }

  public void setSiteClosingDate(LocalDateTime siteClosingDate) {
    this.siteClosingDate = siteClosingDate;
  }

  public Integer getGracePeriod() {
    return gracePeriod;
  }

  public void setGracePeriod(Integer gracePeriod) {
    this.gracePeriod = gracePeriod;
  }

  public Optional<VotingPeriodInitialization> getAction() {
    return Optional.ofNullable(action);
  }

  public void setAction(VotingPeriodInitialization action) {
    this.action = action;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    VotingPeriodConfiguration that = (VotingPeriodConfiguration) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
