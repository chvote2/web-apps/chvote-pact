/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * An entity to persist the number of voting cards transmitted to the Protocol per printer and counting circle.
 */
@Entity
@Table(name = "PACT_T_VOT_CREATION_STATS",
       uniqueConstraints = @UniqueConstraint(
           columnNames = {"VOSTAT_N_PINSTANCE_ID", "VOSTAT_V_PRINTER", "VOSTAT_N_COUNTING_CIRCLE_ID"}))
public class VotersCreationStats {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "VOSTAT_N_ID")
  private Long id;

  @Column(name = "VOSTAT_V_PRINTER", nullable = false)
  private String printerAuthorityName;

  @Column(name = "VOSTAT_N_COUNTING_CIRCLE_ID", nullable = false)
  private int countingCircleId;

  @Column(name = "VOSTAT_N_VOTERS", nullable = false)
  private long numberOfVoters;

  @Column(name = "VOSTAT_N_COUNTING_CIRCLE_NAME", nullable = false)
  private String countingCircleName;

  @Column(name = "VOSTAT_V_COUNTING_CIRCLE_B_ID", nullable = false)
  private String countingCircleBusinessId;

  public Long getId() {
    return id;
  }

  public String getPrinterAuthorityName() {
    return printerAuthorityName;
  }

  public void setPrinterAuthorityName(String printerAuthorityName) {
    this.printerAuthorityName = printerAuthorityName;
  }

  public int getCountingCircleId() {
    return countingCircleId;
  }

  public void setCountingCircleId(int countingCircleId) {
    this.countingCircleId = countingCircleId;
  }

  public long getNumberOfVoters() {
    return numberOfVoters;
  }

  public void setNumberOfVoters(long numberOfVoters) {
    this.numberOfVoters = numberOfVoters;
  }

  public void setCountingCircleName(String countingCircleName) {
    this.countingCircleName = countingCircleName;
  }

  public String getCountingCircleName() {
    return countingCircleName;
  }

  public String getCountingCircleBusinessId() {
    return countingCircleBusinessId;
  }

  public void setCountingCircleBusinessId(String countingCircleBusinessId) {
    this.countingCircleBusinessId = countingCircleBusinessId;
  }
}
