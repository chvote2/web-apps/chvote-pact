/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation;

import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The {@link Operation} environment.
 */
public interface OperationRepository extends JpaRepository<Operation, Long> {

  /**
   * Finds an {@link Operation} by client Id.
   */
  Optional<Operation> findByClientId(String clientId);

  /**
   * Finds an {@link Operation} by ID of its voting materials configuration.
   */
  Optional<Operation> findByVotingMaterialsConfiguration_Id(Long vmcId);

  /**
   * Finds an {@link Operation} by ID of its test operation configuration.
   */
  Optional<Operation> findByConfigurationInTest_Id(Long operationConfigurationId);

  /**
   * Finds an {@link Operation} by ID of its deployed operation configuration.
   */
  Optional<Operation> findByDeployedConfiguration_Id(Long operationConfigurationId);


  /**
   * Finds an {@link Operation} by ID of its voting period configuration.
   */
  Optional<Operation> findByVotingPeriodConfiguration_Id(Long vpcId);

}
