/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * An entity that represents a printer's configuration.
 */
@Entity
@Table(name = "PACT_T_PRINTER_CONF")
public class PrinterConfiguration {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "PCONF_N_ID")
  private Long id;

  @Column(name = "PCONF_V_BUSINESS_ID")
  private String businessIdentifier;

  @Column(name = "PCONF_V_NAME")
  private String printerName;

  @Lob
  @Column(name = "PCONF_X_PUBLIC_KEY", columnDefinition = "BLOB NOT NULL")
  private byte[] publicKey;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
  @JoinColumn(name = "MUNICIP_N_PRINTER_CONF_ID")
  private List<Municipality> municipalities = new ArrayList<>();

  @Column(name = "PCONF_B_SWISS_ABROAD")
  private boolean swissAbroadWithoutMunicipality;

  public Long getId() {
    return id;
  }

  public String getBusinessIdentifier() {
    return businessIdentifier;
  }

  public void setBusinessIdentifier(String businessIdentifier) {
    this.businessIdentifier = businessIdentifier;
  }

  public String getPrinterName() {
    return printerName;
  }

  public void setPrinterName(String printerName) {
    this.printerName = printerName;
  }

  public byte[] getPublicKey() {
    return publicKey;
  }

  public void setPublicKey(byte[] publicKey) {
    this.publicKey = publicKey;
  }

  public List<Municipality> getMunicipalities() {
    return municipalities;
  }

  public void setMunicipalities(List<Municipality> municipalities) {
    this.municipalities = municipalities;
  }

  public boolean isSwissAbroadWithoutMunicipality() {
    return swissAbroadWithoutMunicipality;
  }

  public void setSwissAbroadWithoutMunicipality(boolean swissAbroadWithoutMunicipality) {
    this.swissAbroadWithoutMunicipality = swissAbroadWithoutMunicipality;
  }


}
