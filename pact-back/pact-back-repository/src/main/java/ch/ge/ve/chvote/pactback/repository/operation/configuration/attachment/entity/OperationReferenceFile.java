/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * An entity that represents and attachment of type {@link AttachmentType#OPERATION_REPOSITORY_ELECTION } or
 * {@link AttachmentType#OPERATION_REPOSITORY_VOTATION } or {@link
 * AttachmentType#DOMAIN_OF_INFLUENCE}.
 */
@Entity
@Table(name = "PACT_T_OPERATION_REF_FILE")
public class OperationReferenceFile extends Attachment {
  @Column(name = "ORF_V_MESSAGE_ID")
  private String messageId;

  @Column(name = "ORF_D_GENERATION_DATE")
  private LocalDateTime generationDate;

  @Column(name = "ORF_V_SRC_APP_MANUFACTURER")
  private String sourceApplicationManufacturer;

  @Column(name = "ORF_V_SRC_APP_PRODUCT")
  private String sourceApplicationProduct;

  @Column(name = "ORF_V_SRC_APP_PRODUCT_VERSION")
  private String sourceApplicationProductVersion;

  @Column(name = "ORF_V_SIGNATURE_AUTHOR")
  private String signatureAuthor;

  @Column(name = "ORF_D_SIGNATURE_EXPIRY_DATE")
  private LocalDateTime signatureExpiryDate;

  @Column(name = "ORF_V_SIGNATURE_STATUS")
  @Enumerated(EnumType.STRING)
  private SignatureStatus signatureStatus;

  public String getMessageId() {
    return messageId;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }

  public LocalDateTime getGenerationDate() {
    return generationDate;
  }

  public void setGenerationDate(LocalDateTime generationDate) {
    this.generationDate = generationDate;
  }

  public String getSourceApplicationManufacturer() {
    return sourceApplicationManufacturer;
  }

  public void setSourceApplicationManufacturer(String sourceApplicationManufacturer) {
    this.sourceApplicationManufacturer = sourceApplicationManufacturer;
  }

  public String getSourceApplicationProduct() {
    return sourceApplicationProduct;
  }

  public void setSourceApplicationProduct(String sourceApplicationProduct) {
    this.sourceApplicationProduct = sourceApplicationProduct;
  }

  public String getSourceApplicationProductVersion() {
    return sourceApplicationProductVersion;
  }

  public void setSourceApplicationProductVersion(String sourceApplicationProductVersion) {
    this.sourceApplicationProductVersion = sourceApplicationProductVersion;
  }

  public String getSignatureAuthor() {
    return signatureAuthor;
  }

  public void setSignatureAuthor(String signatureAuthor) {
    this.signatureAuthor = signatureAuthor;
  }

  public LocalDateTime getSignatureExpiryDate() {
    return signatureExpiryDate;
  }

  public void setSignatureExpiryDate(LocalDateTime signatureExpiryDate) {
    this.signatureExpiryDate = signatureExpiryDate;
  }

  public SignatureStatus getSignatureStatus() {
    return signatureStatus;
  }

  public void setSignatureStatus(SignatureStatus signatureStatus) {
    this.signatureStatus = signatureStatus;
  }

  /**
   * The possible status of the signature of an attachment.
   */
  public enum SignatureStatus {

    /**
     * No problems detected with this signature.
     */
    OK,

    /**
     * The certificate for this signature has expired.
     */
    EXPIRED_CERTIFICATE,

    /**
     * The certificate for this signature was revoked.
     */
    REVOKED_CERTIFICATE,

    /**
     * The signature is invalid.
     */
    INVALID_SIGNATURE,

    /**
     * The certificate for this signature is invalid.
     */
    INVALID_CERTIFICATE

  }
}
