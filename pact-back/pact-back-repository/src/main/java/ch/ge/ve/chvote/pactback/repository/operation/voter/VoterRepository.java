/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.voter;

import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterEntity;
import java.util.stream.Stream;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The {@link VoterEntity} repository.
 */
public interface VoterRepository extends JpaRepository<VoterEntity, Long> {

  /**
   * Find all voters by their associated protocol unique identifier.
   *
   * @param protocolId the protocol identifier.
   *
   * @return a stream containing all the voters of the given protocol identifier.
   */
  Stream<VoterEntity> findByProtocolInstance_ProtocolId(String protocolId);

  /**
   * Find all the voter by their associated protocol instance id. This method allows to retrieve the list of voters with
   * pagination.
   *
   * @param protocolInstanceId the protocol instance id.
   * @param pageable           the page request.
   *
   * @return the requested page.
   */
  Page<VoterEntity> findByProtocolInstance_Id(long protocolInstanceId, Pageable pageable);

  /**
   * Delete all the voter by their associated protocol instance id.
   *
   * @param protocolInstanceId the protocol instance id.
   */
  void deleteByProtocolInstance_Id(long protocolInstanceId);
}
