/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.voter.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DateOfBirth {

  @Column(name = "VOTER_N_DATE_OF_BIRTH_YEAR", nullable = false)
  private Integer year;

  @Column(name = "VOTER_N_DATE_OF_BIRTH_MONTH")
  private Integer monthOfYear;

  @Column(name = "VOTER_N_DATE_OF_BIRTH_DAY")
  private Integer dayOfMonth;

  public DateOfBirth() {
    /* Empty constructor required by JPA */
  }

  public DateOfBirth(Integer year, Integer monthOfYear, Integer dayOfMonth) {
    this.year = year;
    this.monthOfYear = monthOfYear;
    this.dayOfMonth = dayOfMonth;
  }

  public Integer getYear() {
    return year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public Integer getMonthOfYear() {
    return monthOfYear;
  }

  public void setMonthOfYear(Integer monthOfYear) {
    this.monthOfYear = monthOfYear;
  }

  public Integer getDayOfMonth() {
    return dayOfMonth;
  }

  public void setDayOfMonth(Integer dayOfMonth) {
    this.dayOfMonth = dayOfMonth;
  }
}
