/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol.entity;

import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * This entity persists a dead letter events from the Protocol.
 */
@Entity
@Table(name = "PACT_T_DLX_EVENT",
       indexes = {
           @Index(columnList = "DLXEVENT_N_PROTOCOL_ID"),
           @Index(columnList = "DLXEVENT_N_PROTOCOL_ID, DLXEVENT_V_TYPE, DLXEVENT_D_CREATION, DLXEVENT_V_CONTENT",
                  unique = true)
       })
public class DeadLetterEvent {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "DLXEVENT_N_ID")
  private Long id;

  @Column(name = "DLXEVENT_N_PROTOCOL_ID", nullable = false)
  private String protocolId;

  @Column(name = "DLXEVENT_V_CHANNEL", nullable = false)
  private String originalChannel;

  @Column(name = "DLXEVENT_V_ROUTE_KEY")
  private String originalRoutingKey;

  @Column(name = "DLXEVENT_V_TYPE", nullable = false)
  private String type;

  @Column(name = "DLXEVENT_V_EXCEPTION")
  private String exceptionMessage;

  @Lob
  @Basic(fetch = FetchType.LAZY)
  @Column(name = "DLXEVENT_V_STACKTRACE")
  private String exceptionStacktrace;

  @Column(name = "DLXEVENT_D_CREATION", nullable = false)
  private LocalDateTime creationDate;

  @Column(name = "DLXEVENT_D_PICKUP", nullable = false)
  private LocalDateTime pickupDate;

  @Lob
  @Basic(fetch = FetchType.LAZY)
  @Column(name = "DLXEVENT_V_CONTENT", nullable = false)
  private String content;

  @Column(name = "DLXEVENT_V_CONTENT_TYPE")
  private String contentType;

  public DeadLetterEvent() {
    /* empty constructor */
  }

  public DeadLetterEvent(Long id) {
    this();
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public String getProtocolId() {
    return protocolId;
  }

  public void setProtocolId(String protocolId) {
    this.protocolId = protocolId;
  }

  public String getOriginalChannel() {
    return originalChannel;
  }

  public void setOriginalChannel(String originalChannel) {
    this.originalChannel = originalChannel;
  }

  public String getOriginalRoutingKey() {
    return originalRoutingKey;
  }

  public void setOriginalRoutingKey(String originalRoutingKey) {
    this.originalRoutingKey = originalRoutingKey;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public void setExceptionMessage(String exceptionMessage) {
    this.exceptionMessage = exceptionMessage;
  }

  public String getExceptionStacktrace() {
    return exceptionStacktrace;
  }

  public void setExceptionStacktrace(String exceptionStacktrace) {
    this.exceptionStacktrace = exceptionStacktrace;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public LocalDateTime getPickupDate() {
    return pickupDate;
  }

  public void setPickupDate(LocalDateTime pickupDate) {
    this.pickupDate = pickupDate;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeadLetterEvent message = (DeadLetterEvent) o;
    return Objects.equals(id, message.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
