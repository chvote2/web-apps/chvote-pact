/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol.entity;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgress;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * An entity that represents a protocol instance initialized by the PACT.
 */
@Entity
@Table(
    name = "PACT_T_PROTOCOL_INSTANCE",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = "PINSTANCE_N_UUID")
    }
)
public class ProtocolInstance {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "PINSTANCE_N_ID")
  private Long id;

  /**
   * Tag to identify the PACT node responsible for the instance in case of a recovery
   */
  @Column(name = "PINSTANCE_V_NODE_KEY", nullable = false)
  private String nodeRepartitionKey;

  @Column(name = "PINSTANCE_N_UUID")
  private String protocolId;

  @Column(name = "PINSTANCE_V_ENVIRONMENT")
  @Enumerated(EnumType.STRING)
  private ProtocolEnvironment environment;

  @Column(name = "PINSTANCE_V_STATUS")
  @Enumerated(EnumType.STRING)
  private ProtocolInstanceStatus status;

  @Column(name = "PINSTANCE_D_STATUS_DATE")
  private LocalDateTime statusDate;

  @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
  @JoinColumn(name = "PINSTANCE_N_OPERATION_CONFIGURATION_ID")
  private OperationConfiguration configuration;

  @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
  @JoinColumn(name = "PINSTANCE_N_OPERATION_ID")
  private Operation operation;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "PSP_N_PINSTANCE_ID")
  private List<ProtocolStageProgress> progress = new ArrayList<>();

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "VOSTAT_N_PINSTANCE_ID", nullable = false)
  private List<VotersCreationStats> statistics = new ArrayList<>();

  @Lob
  @Basic(fetch = FetchType.EAGER)
  @Column(name = "PINSTANCE_X_PUBLIC_PARAMS")
  private String publicParameters;

  @Column(name = "PINSTANCE_D_PRINTER_ARCHIVE_DATE")
  private LocalDateTime printerArchiveCreationDate;

  @Column(name = "PINSTANCE_N_TALLY_ARCHIVE_PATH")
  private String tallyArchivePath;

  public Long getId() {
    return id;
  }

  public String getNodeRepartitionKey() {
    return nodeRepartitionKey;
  }

  public void setNodeRepartitionKey(String nodeRepartitionKey) {
    this.nodeRepartitionKey = nodeRepartitionKey;
  }

  public String getProtocolId() {
    return protocolId;
  }

  public void setProtocolId(String protocolId) {
    this.protocolId = protocolId;
  }

  public ProtocolEnvironment getEnvironment() {
    return environment;
  }

  public void setEnvironment(ProtocolEnvironment environment) {
    this.environment = environment;
  }

  public ProtocolInstanceStatus getStatus() {
    return status;
  }

  public void setStatus(ProtocolInstanceStatus status) {
    this.status = status;
  }

  public LocalDateTime getStatusDate() {
    return statusDate;
  }

  public void setStatusDate(LocalDateTime statusDate) {
    this.statusDate = statusDate;
  }

  public OperationConfiguration getConfiguration() {
    return configuration;
  }

  public void setConfiguration(OperationConfiguration configuration) {
    this.configuration = configuration;
  }

  public Operation getOperation() {
    return operation;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public String getPublicParameters() {
    return publicParameters;
  }

  public void setPublicParameters(String publicParameters) {
    this.publicParameters = publicParameters;
  }

  public LocalDateTime getPrinterArchiveCreationDate() {
    return printerArchiveCreationDate;
  }

  public void setPrinterArchiveCreationDate(LocalDateTime printerArchiveCreationDate) {
    this.printerArchiveCreationDate = printerArchiveCreationDate;
  }

  public String getTallyArchivePath() {
    return tallyArchivePath;
  }

  public void setTallyArchivePath(String tallyArchivePath) {
    this.tallyArchivePath = tallyArchivePath;
  }

  public List<ProtocolStageProgress> getProgress() {
    return progress;
  }

  public void setProgress(List<ProtocolStageProgress> progress) {
    this.progress = progress;
  }

  public List<VotersCreationStats> getStatistics() {
    return statistics;
  }

  public void setStatistics(List<VotersCreationStats> statistics) {
    this.statistics = statistics;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ProtocolInstance that = (ProtocolInstance) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
