/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 * Election site configuration entity.
 */
@Entity
@Table(name = "PACT_T_SITE_CONFIGURATION")
public class ElectionSiteConfiguration {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "SC_N_ID")
  private Long id;

  /**
   * Related ballot.
   */
  @Column(name = "SC_V_BALLOT", nullable = false)
  private String ballot;

  /**
   * Should display candidate search form on the voting site.
   */
  @Column(name = "SC_B_DISPLAY_CANDIDATE_SEARCH_FORM", nullable = false)
  private boolean displayCandidateSearchForm;

  /**
   * Should allow change of electoral list on the voting site.
   */
  @Column(name = "SC_B_ALLOW_LIST_CHANGE", nullable = false)
  private boolean allowChangeOfElectoralList;

  /**
   * Should display empty position on the voting site.
   */
  @Column(name = "SC_B_DISPLAY_EMPTY_POSITION", nullable = false)
  private boolean displayEmptyPosition;

  /**
   * Should display candidate position on a compact ballot paper.
   */
  @Column(name = "SC_B_DISPLAY_CANDIDATE_POSITION_ON_COMPACT_BALLOT", nullable = false)
  private boolean displayCandidatePositionOnACompactBallotPaper;

  /**
   * Should display candidate position on a modified ballot paper.
   */
  @Column(name = "SC_B_DISPLAY_CANDIDATE_POSITION_ON_MODIFIED_BALLOT", nullable = false)
  private boolean displayCandidatePositionOnAModifiedBallotPaper;

  /**
   * Should display suffrage count.
   */
  @Column(name = "SC_B_DISPLAY_SUFFRAGE_COUNT", nullable = false)
  private boolean displaySuffrageCount;

  /**
   * Should display verification code on election list.
   */
  @Column(name = "SC_B_DISPLAY_LIST_VERIFICATION_CODE", nullable = false)
  private boolean displayListVerificationCode;

  /**
   * Should allow open candidature.
   */
  @Column(name = "SC_B_ALLOW_OPEN_CANDIDATURE", nullable = false)
  private boolean allowOpenCandidature;

  /**
   * Should allow multiple mandates.
   */
  @Column(name = "SC_B_ALLOW_MULTIPLE_MANDATES", nullable = false)
  private boolean allowMultipleMandates;

  /**
   * Should display void ballot paper when there is no candidate or no name of the party.
   */
  @Column(name = "SC_B_DISPLAY_VOID_ON_EMPTY_BALLOT", nullable = false)
  private boolean displayVoidOnEmptyBallotPaper;

  /**
   * Name of the display model used to display candidate information.
   */
  @Column(name = "SC_V_CANDIDATE_INFO_DISPLAY_MODEL", nullable = false)
  private String candidateInformationDisplayModel;

  /**
   * Columns to be displayed on the verification table.
   */
  @ElementCollection
  @CollectionTable(name = "PACT_T_SC_VERIFICATION_TABLE_COLUMNS", joinColumns = @JoinColumn(name = "SC_N_ID"))
  @Column(name = "SC_V_VERIFICATION_TABLE_COLUMNS", nullable = false)
  private List<String> displayedColumnsOnVerificationTable;

  /**
   * Columns sort order on the verification table.
   */
  @ElementCollection
  @CollectionTable(name = "PACT_T_SC_VERIFICATION_TABLE_COLUMNS_ORDER", joinColumns = @JoinColumn(name = "SC_N_ID"))
  @Column(name = "SC_V_VERIFICATION_TABLE_COLUMNS_ORDER", nullable = false)
  private List<String> columnsOrderOnVerificationTable;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBallot() {
    return ballot;
  }

  public void setBallot(String ballot) {
    this.ballot = ballot;
  }

  public boolean isDisplayCandidateSearchForm() {
    return displayCandidateSearchForm;
  }

  public void setDisplayCandidateSearchForm(boolean displayCandidateSearchForm) {
    this.displayCandidateSearchForm = displayCandidateSearchForm;
  }

  public boolean isAllowChangeOfElectoralList() {
    return allowChangeOfElectoralList;
  }

  public void setAllowChangeOfElectoralList(boolean allowChangeOfElectoralList) {
    this.allowChangeOfElectoralList = allowChangeOfElectoralList;
  }

  public boolean isDisplayEmptyPosition() {
    return displayEmptyPosition;
  }

  public void setDisplayEmptyPosition(boolean displayEmptyPosition) {
    this.displayEmptyPosition = displayEmptyPosition;
  }

  public boolean isDisplayCandidatePositionOnACompactBallotPaper() {
    return displayCandidatePositionOnACompactBallotPaper;
  }

  public void setDisplayCandidatePositionOnACompactBallotPaper(boolean displayCandidatePositionOnACompactBallotPaper) {
    this.displayCandidatePositionOnACompactBallotPaper = displayCandidatePositionOnACompactBallotPaper;
  }

  public boolean isDisplayCandidatePositionOnAModifiedBallotPaper() {
    return displayCandidatePositionOnAModifiedBallotPaper;
  }

  public void setDisplayCandidatePositionOnAModifiedBallotPaper(boolean
                                                                    displayCandidatePositionOnAModifiedBallotPaper) {
    this.displayCandidatePositionOnAModifiedBallotPaper = displayCandidatePositionOnAModifiedBallotPaper;
  }

  public boolean isDisplaySuffrageCount() {
    return displaySuffrageCount;
  }

  public void setDisplaySuffrageCount(boolean displaySuffrageCount) {
    this.displaySuffrageCount = displaySuffrageCount;
  }

  public boolean isDisplayListVerificationCode() {
    return displayListVerificationCode;
  }

  public void setDisplayListVerificationCode(boolean displayListVerificationCode) {
    this.displayListVerificationCode = displayListVerificationCode;
  }

  public boolean isAllowOpenCandidature() {
    return allowOpenCandidature;
  }

  public void setAllowOpenCandidature(boolean allowOpenCandidature) {
    this.allowOpenCandidature = allowOpenCandidature;
  }

  public boolean isAllowMultipleMandates() {
    return allowMultipleMandates;
  }

  public void setAllowMultipleMandates(boolean allowMultipleMandates) {
    this.allowMultipleMandates = allowMultipleMandates;
  }

  public boolean isDisplayVoidOnEmptyBallotPaper() {
    return displayVoidOnEmptyBallotPaper;
  }

  public void setDisplayVoidOnEmptyBallotPaper(boolean displayVoidOnEmptyBallotPaper) {
    this.displayVoidOnEmptyBallotPaper = displayVoidOnEmptyBallotPaper;
  }

  public String getCandidateInformationDisplayModel() {
    return candidateInformationDisplayModel;
  }

  public void setCandidateInformationDisplayModel(String candidateInformationDisplayModel) {
    this.candidateInformationDisplayModel = candidateInformationDisplayModel;
  }

  public List<String> getDisplayedColumnsOnVerificationTable() {
    return displayedColumnsOnVerificationTable;
  }

  public void setDisplayedColumnsOnVerificationTable(List<String> displayedColumnsOnVerificationTable) {
    this.displayedColumnsOnVerificationTable = displayedColumnsOnVerificationTable;
  }

  public List<String> getColumnsOrderOnVerificationTable() {
    return columnsOrderOnVerificationTable;
  }

  public void setColumnsOrderOnVerificationTable(List<String> columnsOrderOnVerificationTable) {
    this.columnsOrderOnVerificationTable = columnsOrderOnVerificationTable;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    return Objects.equals(id, ((ElectionSiteConfiguration) o).id);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id);
  }
}
