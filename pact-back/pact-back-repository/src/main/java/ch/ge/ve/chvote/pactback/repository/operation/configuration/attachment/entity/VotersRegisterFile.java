/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * An entity that represents and attachment of type {@link AttachmentType#OPERATION_REGISTER}.
 */
@Entity
@Table(name = "PACT_T_VOTERS_REGISTER_FILE")
public class VotersRegisterFile extends OperationReferenceFile {

  @Column(name = "VRF_N_NB_OF_VOTERS")
  private Integer nbOfVoters;

  public Integer getNbOfVoters() {
    return nbOfVoters;
  }

  public void setNbOfVoters(Integer nbOfVoters) {
    this.nbOfVoters = nbOfVoters;
  }
}
