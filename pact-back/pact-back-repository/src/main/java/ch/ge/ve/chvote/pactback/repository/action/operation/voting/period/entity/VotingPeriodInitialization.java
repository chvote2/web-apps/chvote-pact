/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.entity;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * A specialised {@link PrivilegedAction} linked to a {@link VotingPeriodInitialization} entity.
 */
@Entity
@Table(name = "PACT_T_VOTING_PERIOD_INIT")
public class VotingPeriodInitialization extends PrivilegedAction {

  public static final String TYPE = "VOTING_PERIOD_INITIALIZATION";


  @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
  @JoinColumn(name = "VPINIT_N_VPC_ID")
  private VotingPeriodConfiguration votingPeriodConfiguration;

  public VotingPeriodInitialization() {
    super(TYPE);
  }

  public VotingPeriodConfiguration getVotingPeriodConfiguration() {
    return votingPeriodConfiguration;
  }

  public void setVotingPeriodConfiguration(VotingPeriodConfiguration votingPeriodConfiguration) {
    this.votingPeriodConfiguration = votingPeriodConfiguration;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    VotingPeriodInitialization that = (VotingPeriodInitialization) o;
    return Objects.equals(votingPeriodConfiguration, that.votingPeriodConfiguration);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), votingPeriodConfiguration);
  }
}
