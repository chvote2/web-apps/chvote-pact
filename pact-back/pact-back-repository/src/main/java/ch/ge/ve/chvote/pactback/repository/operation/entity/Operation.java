/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.entity;

import ch.ge.ve.chvote.pactback.repository.exception.OperationStateException;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * An entity that represents an operation with its {@link OperationConfiguration}s and its
 * {@link VotingMaterialsConfiguration}.
 */
@Entity
@Table(name = "PACT_T_OPERATION")
public class Operation {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "OP_N_ID")
  private Long id;

  @Column(name = "OP_V_CLIENT_ID", unique = true)
  private String clientId;

  @Column(name = "OP_D_CREATION_DATE")
  private LocalDateTime creationDate;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "OP_OPC_N_ID_TEST")
  private OperationConfiguration configurationInTest;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "OP_OPC_N_ID_DEPLOYED")
  private OperationConfiguration deployedConfiguration;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "OP_VMC_N_ID")
  private VotingMaterialsConfiguration votingMaterialsConfiguration;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "OP_VPC_N_ID")
  private VotingPeriodConfiguration votingPeriodConfiguration;

  public Long getId() {
    return id;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public Optional<OperationConfiguration> getConfigurationInTest() {
    return Optional.ofNullable(configurationInTest);
  }

  public void setConfigurationInTest(OperationConfiguration configurationInTest) {
    this.configurationInTest = configurationInTest;
  }

  public OperationConfiguration fetchConfigurationInTest() {
    return getConfigurationInTest().orElseThrow(getFetchExceptionSupplier("test configuration"));
  }

  public Optional<OperationConfiguration> getDeployedConfiguration() {
    return Optional.ofNullable(deployedConfiguration);
  }

  public void setDeployedConfiguration(OperationConfiguration deployedConfiguration) {
    this.deployedConfiguration = deployedConfiguration;
  }

  public OperationConfiguration fetchDeployedConfiguration() {
    return getDeployedConfiguration().orElseThrow(getFetchExceptionSupplier("deployed configuration"));
  }

  public Optional<VotingMaterialsConfiguration> getVotingMaterialsConfiguration() {
    return Optional.ofNullable(votingMaterialsConfiguration);
  }

  public void setVotingMaterialsConfiguration(VotingMaterialsConfiguration votingMaterialsConfiguration) {
    this.votingMaterialsConfiguration = votingMaterialsConfiguration;
  }

  public VotingMaterialsConfiguration fetchVotingMaterialsConfiguration() {
    return getVotingMaterialsConfiguration().orElseThrow(getFetchExceptionSupplier("voting material configuration"));
  }


  public Optional<VotingPeriodConfiguration> getVotingPeriodConfiguration() {
    return Optional.ofNullable(votingPeriodConfiguration);
  }

  public void setVotingPeriodConfiguration(VotingPeriodConfiguration votingPeriodConfiguration) {
    this.votingPeriodConfiguration = votingPeriodConfiguration;
  }

  public VotingPeriodConfiguration fetchVotingPeriodConfiguration() {
    return getVotingPeriodConfiguration().orElseThrow(getFetchExceptionSupplier("voting period configuration"));
  }

  private Supplier<OperationStateException> getFetchExceptionSupplier(String nullProperty) {
    return () -> new OperationStateException(String.format("Operation [%s] has no %s", getClientId(), nullProperty));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Operation that = (Operation) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
