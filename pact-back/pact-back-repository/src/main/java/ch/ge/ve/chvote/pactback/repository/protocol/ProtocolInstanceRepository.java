/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * the {@link ProtocolInstance} repository.
 */
public interface ProtocolInstanceRepository extends JpaRepository<ProtocolInstance, Long> {

  /**
   * Whether a protocol instance exists for the given {@link ProtocolEnvironment} and operation client id.
   *
   * @param environment the {@link ProtocolEnvironment} of the protocol instance.
   * @param clientId    the client id of the operation associated to the protocol instance.
   *
   * @return true if it exists, false otherwise.
   */
  boolean existsByEnvironmentAndOperation_ClientId(ProtocolEnvironment environment,
                                                   String clientId);

  /**
   * Find all protocol instances associated to a given "repartition key" where the status is one of the given ones.
   */
  List<ProtocolInstance> findByNodeRepartitionKeyAndStatusIn(String nodeRepartitionKey,
                                                             Collection<ProtocolInstanceStatus> statuses);

  /**
   * Find all protocol instances having a given status.
   */
  List<ProtocolInstance> findByStatus(ProtocolInstanceStatus status);

  /**
   * Find an instance by protocol instance id
   */
  Optional<ProtocolInstance> getByProtocolId(String protocolId);

}
