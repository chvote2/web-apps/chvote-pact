/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.action;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * A base repository for {@link PrivilegedAction}s containing common functionality.
 *
 * @param <T> A subtype of {@link PrivilegedAction}.
 */
public interface BasePrivilegedActionRepository<T extends PrivilegedAction> extends JpaRepository<T, Long> {

  /**
   * Retrieve a {@link PrivilegedAction} by id and {@link PrivilegedAction.Status}.
   *
   * @param id     The id of the {@link PrivilegedAction} to retrieve.
   * @param status The expected {@link PrivilegedAction.Status} for this action.
   *
   * @return The {@link PrivilegedAction}.
   */
  Optional<T> findByIdAndStatus(Long id, PrivilegedAction.Status status);

}
