/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * An entity that represents a municipality associated to a printer's configuration.
 */
@Entity
@Table(name = "PACT_T_MUNICIPALITY")
public class Municipality {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "MUNICIP_N_ID")
  private Long id;

  @Column(name = "MUNICIP_N_OFS_ID")
  private Integer ofsId;

  @Column(name = "MUNICIP_V_NAME")
  private String name;

  @Column(name = "MUNICIP_B_VIRTUAL")
  private boolean virtual;

  public Long getId() {
    return id;
  }

  public Integer getOfsId() {
    return ofsId;
  }

  public void setOfsId(Integer ofsId) {
    this.ofsId = ofsId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isVirtual() {
    return virtual;
  }

  public void setVirtual(boolean virtual) {
    this.virtual = virtual;
  }

}
