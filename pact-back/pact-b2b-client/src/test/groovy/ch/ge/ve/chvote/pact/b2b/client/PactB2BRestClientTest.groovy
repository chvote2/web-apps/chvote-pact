/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client

import ch.ge.ve.chvote.pact.b2b.client.exceptions.AttachmentNotFoundException
import ch.ge.ve.chvote.pact.b2b.client.exceptions.PactB2BRestClientException
import ch.ge.ve.chvote.pact.b2b.client.exceptions.ProtocolInstanceNotFoundException
import ch.ge.ve.chvote.pact.b2b.client.model.OperationBaseConfiguration
import ch.ge.ve.chvote.pact.b2b.client.model.Voter
import ch.ge.ve.chvote.pact.b2b.client.model.VotingSiteConfiguration
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

class PactB2BRestClientTest extends Specification {

  private RestTemplate restTemplate
  private PactB2BRestClient client

  void setup() {
    def config = Mock(PactB2BRestClientConfiguration)
    def restTemplateBuilder = Mock(RestTemplateBuilder)

    config.username >> "username"
    config.password >> "password"
    config.rootURI >> "http://localhost:8080/b2b/"

    restTemplate = Mock(RestTemplate)
    restTemplateBuilder.rootUri("http://localhost:8080/b2b/") >> restTemplateBuilder
    restTemplateBuilder.basicAuthorization("username", "password") >> restTemplateBuilder
    restTemplateBuilder.build() >> restTemplate

    client = new PactB2BRestClient(config, restTemplateBuilder)
  }

  def "getVoters should throw ProtocolInstanceNotFoundException if no protocol instance was found"() {
    given:
    restTemplate.exchange(
            "/protocol/{protocolId}/voters",
            HttpMethod.GET,
            _ as HttpEntity,
            _ as ParameterizedTypeReference<List<Voter>>,
            ["protocolId": "pid"]) >> { throw new HttpClientErrorException(HttpStatus.NOT_FOUND) }

    when:
    client.getVoters("pid")

    then:
    def ex = thrown(ProtocolInstanceNotFoundException)
    ex.message == "No protocol instance was found for protocol id: [pid]"
  }

  def "getVoters should throw PactB2BRestClientException if an http error other than 404 is returned"() {
    given:
    restTemplate.exchange(
            "/protocol/{protocolId}/voters",
            HttpMethod.GET,
            _ as HttpEntity,
            _ as ParameterizedTypeReference<List<Voter>>,
            ["protocolId": "pid"]) >> { throw new HttpClientErrorException(HttpStatus.BAD_REQUEST) }

    when:
    client.getVoters("pid")

    then:
    def ex = thrown(PactB2BRestClientException)
    ex.message == "Bad Request (400)"
  }

  def "getVoters should call the expected endpoint and return a list of voters for a valid protocol id"() {
    when:
    def result = client.getVoters("pid")

    then:
    result.size() == 3
    1 * restTemplate.exchange(
            "/protocol/{protocolId}/voters",
            HttpMethod.GET,
            _ as HttpEntity,
            _ as ParameterizedTypeReference<List<Voter>>,
            ["protocolId": "pid"]) >> ResponseEntity.ok([Mock(Voter), Mock(Voter), Mock(Voter)] as List<Voter>)
  }

  def "getOperationConfiguration should call the expected endpoint and return an operation configuration object for a valid protocol id"() {
    when:
    def result = client.getOperationConfiguration("pid")

    then:
    1 * restTemplate.exchange(
            "/protocol/{protocolId}/operation",
            HttpMethod.GET,
            _ as HttpEntity,
            _ as ParameterizedTypeReference<OperationBaseConfiguration>,
            ["protocolId": "pid"]) >>
            ResponseEntity.ok(Mock(OperationBaseConfiguration))
    result != null

  }

  def "getVotingSiteConfiguration should call the expected endpoint and return a voting site configuration for a valid protocol id"() {
    when:
    def result = client.getVotingSiteConfiguration("pid")

    then:
    1 * restTemplate.exchange(
            "/protocol/{protocolId}/voting-site-configuration",
            HttpMethod.GET,
            _ as HttpEntity,
            _ as ParameterizedTypeReference<byte[]>,
            ["protocolId": "pid"]) >>
            ResponseEntity.ok(Mock(VotingSiteConfiguration))
    result != null
  }

  def "getAttachment return a bad response"() {
    given:
    restTemplate.exchange(
            "/protocol/{protocolId}/attachment/{attachmentId}",
            HttpMethod.GET,
            _ as HttpEntity,
            byte[].class,
            [protocolId: "pid", attachmentId: 1]) >>
            { throw new HttpClientErrorException(HttpStatus.I_AM_A_TEAPOT) }

    when:
    client.getAttachment("pid", 1)

    then:
    def ex = thrown(PactB2BRestClientException)
    ex.message == "I'm a teapot (418)"
  }

  def "getAttachment cannot find resources"() {
    given:
    restTemplate.exchange(
            "/protocol/{protocolId}/attachment/{attachmentId}",
            HttpMethod.GET,
            _ as HttpEntity,
            byte[].class,
            [protocolId: "pid", attachmentId: 1]) >>
            { throw new HttpClientErrorException(HttpStatus.NOT_FOUND) }

    when:
    client.getAttachment("pid", 1)

    then:
    def ex = thrown(AttachmentNotFoundException)
    ex.message == "No resource found for protocolId: [pid] and attachmentId [1]"
  }

  def "getAttachment return attachment"() {
    when:
    def result = client.getAttachment("pid", 1)

    then:
    1 * restTemplate.exchange("/protocol/{protocolId}/attachment/{attachmentId}", HttpMethod.GET, _ as HttpEntity,
            byte[].class, [protocolId: "pid", attachmentId: 1]) >> ResponseEntity.ok("test".bytes)
    new String(result.content) == "test"
  }

}
