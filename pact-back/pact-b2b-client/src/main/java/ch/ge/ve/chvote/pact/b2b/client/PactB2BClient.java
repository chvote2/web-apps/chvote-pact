/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client;

import ch.ge.ve.chvote.pact.b2b.client.exceptions.AttachmentNotFoundException;
import ch.ge.ve.chvote.pact.b2b.client.exceptions.ProtocolInstanceNotFoundException;
import ch.ge.ve.chvote.pact.b2b.client.model.Attachment;
import ch.ge.ve.chvote.pact.b2b.client.model.OperationBaseConfiguration;
import ch.ge.ve.chvote.pact.b2b.client.model.Voter;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingSiteConfiguration;
import java.util.List;

/**
 * PACT B2B client API, for use by CHVote applications
 */
public interface PactB2BClient {

  /**
   * Retrieve all the voters associated with the given protocol resource.
   *
   * @param protocolId the protocol resource identifier.
   *
   * @return the list of {@link Voter}s.
   *
   * @throws ProtocolInstanceNotFoundException if the given protocol id does not exists.
   */
  List<Voter> getVoters(String protocolId);

  /**
   * Retrieves information about the operation associated with the given protocol id.
   *
   * @param protocolId the protocol resource identifier.
   *
   * @return an object containing information about the operation.
   *
   * @throws ProtocolInstanceNotFoundException if the given protocol id does not exists.
   */
  OperationBaseConfiguration getOperationConfiguration(String protocolId);

  /**
   * Retrieves the voting site configuration for the operation denoted by the given protocol id.
   *
   * @param protocolId the protocol resource identifier.
   *
   * @return an object containing the voting site configuration.
   *
   * @throws ProtocolInstanceNotFoundException if the given protocol id does not exists.
   */
  VotingSiteConfiguration getVotingSiteConfiguration(String protocolId);


  /**
   * Retrieves an attachment from for a given protocol id and attachment id.
   *
   * @param protocolId the protocol identifier.
   * @param attachmentId the attachment identifier.
   *
   * @return the attachment
   *
   * @throws AttachmentNotFoundException if the given attachment does not exists.
   */
  Attachment getAttachment(String protocolId, long attachmentId);

}
