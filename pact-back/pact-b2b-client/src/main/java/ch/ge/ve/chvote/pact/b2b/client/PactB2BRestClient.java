/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client;

import ch.ge.ve.chvote.pact.b2b.client.exceptions.AttachmentNotFoundException;
import ch.ge.ve.chvote.pact.b2b.client.exceptions.PactB2BRestClientException;
import ch.ge.ve.chvote.pact.b2b.client.exceptions.ProtocolInstanceNotFoundException;
import ch.ge.ve.chvote.pact.b2b.client.model.Attachment;
import ch.ge.ve.chvote.pact.b2b.client.model.OperationBaseConfiguration;
import ch.ge.ve.chvote.pact.b2b.client.model.Voter;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingSiteConfiguration;
import com.google.common.collect.ImmutableMap;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

/**
 * The default PACT B2B client implementation.
 */
@Component
public class PactB2BRestClient implements PactB2BClient {
  private final RestTemplate restTemplate;

  /**
   * Create a new pact b2b rest client.
   *
   * @param config              an object containing the required parameters to establish the connection.
   * @param restTemplateBuilder a {@link RestTemplate} builder.
   */
  @Autowired
  public PactB2BRestClient(PactB2BRestClientConfiguration config,
                           RestTemplateBuilder restTemplateBuilder) {
    this.restTemplate = restTemplateBuilder.rootUri(config.getRootURI())
                                           .basicAuthorization(config.getUsername(),
                                                               config.getPassword())
                                           .build();
  }

  private <T> T getRequestForProtocol(ParameterizedTypeReference<T> typeReference,
                                      String url,
                                      String protocolId) {

    try {
      return restTemplate.exchange(url,
                                   HttpMethod.GET,
                                   new HttpEntity<>(createXRequestedWithHeaders()),
                                   typeReference,
                                   Collections.singletonMap("protocolId", protocolId))
                         .getBody();
    } catch (HttpStatusCodeException exception) {
      if (exception.getStatusCode() == org.springframework.http.HttpStatus.NOT_FOUND) {
        throw new ProtocolInstanceNotFoundException(protocolId, exception);
      } else {
        throw new PactB2BRestClientException(exception.getStatusCode(), exception);
      }
    }
  }


  private HttpHeaders createXRequestedWithHeaders() {
    HttpHeaders headers = new HttpHeaders();
    headers.set("X-Requested-With", "XMLHttpRequest");
    return headers;
  }

  @Override
  public List<Voter> getVoters(String protocolId) {
    return getRequestForProtocol(new ParameterizedTypeReference<List<Voter>>() {
    }, "/protocol/{protocolId}/voters", protocolId);
  }

  @Override
  public OperationBaseConfiguration getOperationConfiguration(String protocolId) {
    return getRequestForProtocol(new ParameterizedTypeReference<OperationBaseConfiguration>() {
    }, "/protocol/{protocolId}/operation", protocolId);
  }

  @Override
  public VotingSiteConfiguration getVotingSiteConfiguration(String protocolId) {
    return getRequestForProtocol(new ParameterizedTypeReference<VotingSiteConfiguration>() {
    }, "/protocol/{protocolId}/voting-site-configuration", protocolId);
  }

  @Override
  public Attachment getAttachment(String protocolId, long attachmentId) {
    HttpHeaders headers = createXRequestedWithHeaders();
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));


    try {
      return new Attachment(restTemplate.exchange("/protocol/{protocolId}/attachment/{attachmentId}",
                                                  HttpMethod.GET,
                                                  new HttpEntity<>(headers),
                                                  byte[].class,
                                                  ImmutableMap.of("protocolId", protocolId,
                                                                  "attachmentId", attachmentId)
      ));
    } catch (HttpStatusCodeException exception) {
      if (exception.getStatusCode() == org.springframework.http.HttpStatus.NOT_FOUND) {
        throw new AttachmentNotFoundException(protocolId, attachmentId, exception);
      } else {
        throw new PactB2BRestClientException(exception.getStatusCode(), exception);
      }
    }

  }
}
