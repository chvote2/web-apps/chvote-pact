/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client.model;

import java.util.Arrays;
import java.util.Locale;

/**
 * An enumeration of all the official languages of Switzerland.
 */
public enum Lang {
  /**
   * French, see {@link Locale#FRENCH}.
   */
  FR(Locale.FRENCH),
  /**
   * Italian, see {@link Locale#ITALIAN}.
   */
  IT(Locale.ITALIAN),
  /**
   * German, see {@link Locale#GERMAN}.
   */
  DE(Locale.GERMAN),
  /**
   * Romansh (its {@link Locale} is the result fo calling: <code>Locale.forLanguageTag("rm")</code>).
   */
  RM(Locale.forLanguageTag("rm"));

  private final Locale locale;

  Lang(Locale locale) {
    this.locale = locale;
  }

  public Locale getLocale() {
    return locale;
  }

  /**
   * Retrieve a language by its associated {@link Locale}.
   *
   * @param locale the {@link Locale}.
   *
   * @return the language that matches the given locale.
   */
  public static Lang getLanguageByLocale(Locale locale) {
    return Arrays.stream(values())
                 .filter(lang -> lang.locale.equals(locale))
                 .findFirst()
                 .orElse(null);
  }

  /**
   * Retrieve the language value by its language tag.
   *
   * @param languageTag the language tag.
   *
   * @return the language that matches the given language tag.
   *
   * @see Locale#forLanguageTag(String)
   */
  public static Lang getLanguageByLanguageTag(String languageTag) {
    return getLanguageByLocale(Locale.forLanguageTag(languageTag));
  }
}
