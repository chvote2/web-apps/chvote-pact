/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client.model;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public class Attachment {
  private final byte[]    content;
  private final MediaType mediaType;
  private final String    fileName;

  public Attachment(ResponseEntity<byte[]> response) {
    this.content = response.getBody();
    this.mediaType = response.getHeaders().getContentType();
    this.fileName = response.getHeaders().getContentDisposition().getFilename();
  }

  public byte[] getContent() {
    return content;
  }

  public MediaType getMediaType() {
    return mediaType;
  }

  public String getFileName() {
    return fileName;
  }
}
