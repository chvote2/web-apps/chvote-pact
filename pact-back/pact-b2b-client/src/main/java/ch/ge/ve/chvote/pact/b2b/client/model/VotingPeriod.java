/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * An exchange object that represents a voting period.
 */
public class VotingPeriod {
  @NotNull
  private final LocalDateTime openingDate;

  @NotNull
  private final LocalDateTime closingDate;

  @NotNull
  @Min(0)
  @Max(50)
  private final Integer gracePeriod;

  @JsonCreator
  public VotingPeriod(
      @JsonProperty("openingDate") LocalDateTime openingDate,
      @JsonProperty("closingDate") LocalDateTime closingDate,
      @JsonProperty("gracePeriod") Integer gracePeriod) {
    this.openingDate = openingDate;
    this.closingDate = closingDate;
    this.gracePeriod = gracePeriod;
  }

  /**
   * Voting period opening date
   */
  public LocalDateTime getOpeningDate() {
    return this.openingDate;
  }

  /**
   * Voting period closing date
   */
  public LocalDateTime getClosingDate() {
    return this.closingDate;
  }

  /**
   * Grace period during which started sessions will continue voting process
   */
  public Integer getGracePeriod() {
    return this.gracePeriod;
  }
}
