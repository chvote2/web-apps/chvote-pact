/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Configuration of an election group
 */
public class ElectionGroupBallotConfig {

  private final List<Documentation> ballotDocumentations;
  private final boolean             displayCandidateSearchForm;
  private final boolean             allowChangeOfElectoralList;
  private final boolean             displayEmptyPosition;
  private final boolean             displayCandidatePositionOnACompactBallotPaper;
  private final boolean             displayCandidatePositionOnAModifiedBallotPaper;
  private final boolean             displaySuffrageCount;
  private final boolean             displayListVerificationCode;
  private final String              candidateInformationDisplayModel;
  private final boolean             allowOpenCandidature;
  private final boolean             allowMultipleMandates;
  private final boolean             displayVoidOnEmptyBallotPaper;
  private final String[]            displayedColumnsOnVerificationTable;
  private final String[]            columnsOrderOnVerificationTable;

  @JsonCreator
  public ElectionGroupBallotConfig(
      @JsonProperty("ballotDocumentations") List<Documentation> ballotDocumentations,
      @JsonProperty("displayCandidateSearchForm") boolean displayCandidateSearchForm,
      @JsonProperty("allowChangeOfElectoralList") boolean allowChangeOfElectoralList,
      @JsonProperty("displayEmptyPosition") boolean displayEmptyPosition,
      @JsonProperty("displayCandidatePositionOnACompactBallotPaper")
          boolean displayCandidatePositionOnACompactBallotPaper,
      @JsonProperty("displayCandidatePositionOnAModifiedBallotPaper")
          boolean displayCandidatePositionOnAModifiedBallotPaper,
      @JsonProperty("displaySuffrageCount") boolean displaySuffrageCount,
      @JsonProperty("displayListVerificationCode") boolean displayListVerificationCode,
      @JsonProperty("candidateInformationDisplayModel") String candidateInformationDisplayModel,
      @JsonProperty("allowOpenCandidature") boolean allowOpenCandidature,
      @JsonProperty("allowMultipleMandates") boolean allowMultipleMandates,
      @JsonProperty("displayVoidOnEmptyBallotPaper") boolean displayVoidOnEmptyBallotPaper,
      @JsonProperty("displayedColumnsOnVerificationTable") String[] displayedColumnsOnVerificationTable,
      @JsonProperty("columnsOrderOnVerificationTable") String[] columnsOrderOnVerificationTable) {
    this.ballotDocumentations = ballotDocumentations;
    this.displayCandidateSearchForm = displayCandidateSearchForm;
    this.allowChangeOfElectoralList = allowChangeOfElectoralList;
    this.displayEmptyPosition = displayEmptyPosition;
    this.displayCandidatePositionOnACompactBallotPaper = displayCandidatePositionOnACompactBallotPaper;
    this.displayCandidatePositionOnAModifiedBallotPaper = displayCandidatePositionOnAModifiedBallotPaper;
    this.displaySuffrageCount = displaySuffrageCount;
    this.displayListVerificationCode = displayListVerificationCode;
    this.candidateInformationDisplayModel = candidateInformationDisplayModel;
    this.allowOpenCandidature = allowOpenCandidature;
    this.allowMultipleMandates = allowMultipleMandates;
    this.displayVoidOnEmptyBallotPaper = displayVoidOnEmptyBallotPaper;
    this.displayedColumnsOnVerificationTable = displayedColumnsOnVerificationTable;
    this.columnsOrderOnVerificationTable = columnsOrderOnVerificationTable;
  }

  public List<Documentation> getBallotDocumentations() {
    return ballotDocumentations;
  }

  public boolean isDisplayCandidateSearchForm() {
    return displayCandidateSearchForm;
  }

  public boolean isAllowChangeOfElectoralList() {
    return allowChangeOfElectoralList;
  }

  public boolean isDisplayEmptyPosition() {
    return displayEmptyPosition;
  }

  public boolean isDisplayCandidatePositionOnACompactBallotPaper() {
    return displayCandidatePositionOnACompactBallotPaper;
  }

  public boolean isDisplayCandidatePositionOnAModifiedBallotPaper() {
    return displayCandidatePositionOnAModifiedBallotPaper;
  }

  public boolean isDisplaySuffrageCount() {
    return displaySuffrageCount;
  }

  public boolean isDisplayListVerificationCode() {
    return displayListVerificationCode;
  }

  public String getCandidateInformationDisplayModel() {
    return candidateInformationDisplayModel;
  }

  public boolean isAllowOpenCandidature() {
    return allowOpenCandidature;
  }

  public boolean isAllowMultipleMandates() {
    return allowMultipleMandates;
  }

  public boolean isDisplayVoidOnEmptyBallotPaper() {
    return displayVoidOnEmptyBallotPaper;
  }

  public String[] getDisplayedColumnsOnVerificationTable() {
    return displayedColumnsOnVerificationTable;
  }

  public String[] getColumnsOrderOnVerificationTable() {
    return columnsOrderOnVerificationTable;
  }
}
