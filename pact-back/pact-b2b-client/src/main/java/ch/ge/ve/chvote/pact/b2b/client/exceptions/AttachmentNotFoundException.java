/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client.exceptions;

/**
 * Thrown when a resource is not found.
 */
public class AttachmentNotFoundException extends RuntimeException {

  /**
   * Creates a new attachment not found exception with a message and a root cause.
   *
   * @param protocolId   protocol id (used to format the {@link Exception#getMessage()} of this exception).
   * @param attachmentId attachment id (used to format the {@link Exception#getMessage()} of this exception).
   * @param cause        the original cause.
   *
   * @see RuntimeException#RuntimeException(String, Throwable)
   */
  public AttachmentNotFoundException(String protocolId, long attachmentId, Throwable cause) {
    super(String.format("No resource found for protocolId: [%s] and attachmentId [%s]", protocolId, attachmentId),
          cause);
  }
}
