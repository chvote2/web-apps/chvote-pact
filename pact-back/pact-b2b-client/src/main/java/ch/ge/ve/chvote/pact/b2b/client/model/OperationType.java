/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client.model;

/**
 * Type of operation
 */
public enum OperationType {
  /**
   * This Type Is to test voting site without other components (BAM, VRUM, etc.) but only with Protocol. Also all data
   * like register are mocked. For this operation type no tally archive is generated.
   */
  TEST,

  /**
   * Used to execute a whole workflow of an operation as it will be in a production mode. The unique difference between
   * simulation and real is that that it's not visible in a public mode.
   */
  SIMULATION,

  /**
   * This type is for operation that is in a production mode with real voters and no component is mocked
   */
  REAL
}