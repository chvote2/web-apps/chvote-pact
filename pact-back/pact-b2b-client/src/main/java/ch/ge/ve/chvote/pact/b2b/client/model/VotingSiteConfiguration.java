/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Set;

/**
 * Configuration of the voting site
 */
public class VotingSiteConfiguration {

  /**
   * global documentations
   */
  private final Set<Documentation> documentations;

  /**
   * Translation file id in PACT
   */
  private final Long translationsId;

  /**
   * Identify if votation ballots should be grouped ?
   */
  private final boolean groupVotation;

  /**
   * List of votation ballot configuration
   */
  private final List<VotationBallotConfig> votationBallotConfigList;

  /**
   * List of election group ballot configuration
   */
  private final List<ElectionGroupBallotConfig> electionGroupBallotConfigList;

  /**
   * List of Highlighted Question configuration
   */
  private final List<Documentation> highlightedQuestions;


  @JsonCreator
  public VotingSiteConfiguration(
      @JsonProperty("documentations") Set<Documentation> documentations,
      @JsonProperty("translationsId") Long translationsId,
      @JsonProperty("groupVotation") boolean groupVotation,
      @JsonProperty("votationBallotConfigList") List<VotationBallotConfig> votationBallotConfigList,
      @JsonProperty("electionGroupBallotConfigList") List<ElectionGroupBallotConfig> electionGroupBallotConfigList,
      @JsonProperty("highlightedQuestions") List<Documentation> highlightedQuestions) {
    this.documentations = documentations;
    this.translationsId = translationsId;
    this.groupVotation = groupVotation;
    this.votationBallotConfigList = votationBallotConfigList;
    this.electionGroupBallotConfigList = electionGroupBallotConfigList;
    this.highlightedQuestions = highlightedQuestions;
  }

  public Set<Documentation> getDocumentations() {
    return documentations;
  }

  public Long getTranslationsId() {
    return translationsId;
  }

  public boolean isGroupVotation() {
    return groupVotation;
  }

  public List<VotationBallotConfig> getVotationBallotConfigList() {
    return votationBallotConfigList;
  }

  public List<ElectionGroupBallotConfig> getElectionGroupBallotConfigList() {
    return electionGroupBallotConfigList;
  }

  public List<Documentation> getHighlightedQuestions() {
    return highlightedQuestions;
  }
}
