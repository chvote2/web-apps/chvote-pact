/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/


package ch.ge.ve.chvote.pact.b2b.client.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Documentation of an operation.
 */
public class Documentation {
  private final String            label;
  private final Lang              lang;
  private final Long              attachmentId;
  private final DocumentationType type;

  /**
   * Create a new documentation instance.
   *
   * @param type         the type of documentation.
   * @param label        the document label.
   * @param lang         the target language of the document.
   * @param attachmentId the PACT attachment id for the document.
   */
  @JsonCreator
  public Documentation(
      @JsonProperty("type") DocumentationType type,
      @JsonProperty("label") String label,
      @JsonProperty("lang") Lang lang,
      @JsonProperty("attachmentId") Long attachmentId) {
    this.label = label;
    this.lang = lang;
    this.attachmentId = attachmentId;
    this.type = type;
  }

  /**
   * Document label
   */
  public String getLabel() {
    return label;
  }

  /**
   * The target language of the document
   */
  public Lang getLang() {
    return lang;
  }

  /**
   * The attachment id in PACT
   */
  public Long getAttachmentId() {
    return attachmentId;
  }

  public DocumentationType getType() {
    return this.type;
  }

}
