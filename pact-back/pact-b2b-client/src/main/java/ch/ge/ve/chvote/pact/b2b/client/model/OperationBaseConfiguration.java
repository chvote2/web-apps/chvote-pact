/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * An exchange object that represents an operation configuration.
 */
public class OperationBaseConfiguration {

  @NotNull
  @Pattern(regexp = "[\\p{L}\\d \\-',()/._]+")
  @Size(min = 1, max = 50)
  private final String shortLabel;

  @NotNull
  @Pattern(regexp = "[\\p{L}\\d \\-',()/._]+")
  @Size(min = 1, max = 200)
  private final String longLabel;

  @NotNull
  private final Canton canton;

  @NotNull
  @Pattern(regexp = "[\\p{L}\\d \\-',()/._]+")
  @Size(min = 1, max = 500)
  private final String votingCardLabel;

  @Size(min = 1)
  @Pattern(regexp = "[\\p{L}\\d \\-',()/._]+")
  @Size(min = 1, max = 200)
  private final String simulationName;

  @NotNull
  private final List<Long> votationRepositoryIds;

  @NotNull
  private final List<Long> electionRepositoryIds;

  @NotNull
  private final Lang defaultLanguage;

  @NotNull
  private final OperationType type;

  @NotNull
  private final VotingPeriod votingPeriod;

  @NotNull
  private final LocalDateTime operationDate;

  @JsonCreator
  public OperationBaseConfiguration(@JsonProperty("shortLabel") String shortLabel,
                                    @JsonProperty("longLabel") String longLabel,
                                    @JsonProperty("canton") Canton canton,
                                    @JsonProperty("votingCardLabel") String votingCardLabel,
                                    @JsonProperty("simulationName") String simulationName,
                                    @JsonProperty("defaultLanguage") Lang defaultLanguage,
                                    @JsonProperty("type") OperationType type,
                                    @JsonProperty("votingPeriod") VotingPeriod votingPeriod,
                                    @JsonProperty("operationDate") LocalDateTime operationDate,
                                    @JsonProperty("votationRepositoryIds") List<Long> votationRepositoryIds,
                                    @JsonProperty("electionRepositoryIds") List<Long> electionRepositoryIds) {
    this.shortLabel = shortLabel;
    this.longLabel = longLabel;
    this.canton = canton;
    this.votingCardLabel = votingCardLabel;
    this.simulationName = simulationName;
    this.defaultLanguage = defaultLanguage;
    this.type = type;
    this.votingPeriod = votingPeriod;
    this.operationDate = operationDate;
    this.votationRepositoryIds = votationRepositoryIds;
    this.electionRepositoryIds = electionRepositoryIds;
  }

  public String getShortLabel() {
    return shortLabel;
  }

  public String getLongLabel() {
    return longLabel;
  }

  public Canton getCanton() {
    return canton;
  }

  public String getVotingCardLabel() {
    return votingCardLabel;
  }

  public String getSimulationName() {
    return simulationName;
  }

  public Lang getDefaultLanguage() {
    return defaultLanguage;
  }

  public OperationType getType() {
    return type;
  }

  public VotingPeriod getVotingPeriod() {
    return votingPeriod;
  }

  public LocalDateTime getOperationDate() {
    return operationDate;
  }

  public List<Long> getVotationRepositoryIds() {
    return votationRepositoryIds;
  }

  public List<Long> getElectionRepositoryIds() {
    return electionRepositoryIds;
  }
}

