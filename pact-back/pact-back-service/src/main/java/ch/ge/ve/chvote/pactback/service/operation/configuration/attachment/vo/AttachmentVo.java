/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration.attachment.vo;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.Attachment;
import java.time.LocalDateTime;

/**
 * An {@link Attachment} value object.
 */
public class AttachmentVo {

  private final Long          id;
  private final String        name;
  private final String        type;
  private final LocalDateTime importDate;
  private final String        fileHash;

  /**
   * Creates a new attachment value object based on the given attachment entity.
   *
   * @param attachment the attachment entity.
   */
  public AttachmentVo(Attachment attachment) {
    this.id = attachment.getId();
    this.name = attachment.getName();
    this.type = attachment.getType().name();
    this.importDate = attachment.getImportDate();
    this.fileHash = attachment.getFileHash();
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getType() {
    return type;
  }

  public LocalDateTime getImportDate() {
    return importDate;
  }

  public String getFileHash() {
    return fileHash;
  }

}
