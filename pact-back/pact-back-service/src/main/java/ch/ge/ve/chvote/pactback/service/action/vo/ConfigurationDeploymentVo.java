/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action.vo;

import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeployment;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.service.operation.configuration.vo.OperationConfigurationVo;

/**
 * A configuration deployment value object.
 */
public class ConfigurationDeploymentVo extends PrivilegedActionVo {

  private final OperationConfigurationVo operationConfiguration;

  /**
   * Creates a new configuration deployment value object based on the given configuration deployment entity.
   *
   * @param action The configuration deployment entity.
   */
  public ConfigurationDeploymentVo(ConfigurationDeployment action) {
    super(action);
    this.operationConfiguration = new OperationConfigurationVo(action.getOperationConfiguration());
  }

  /**
   * Creates a dummy configuration deployment value object based on the given {@link OperationConfiguration}. An
   * instance created with this constructor will always return "NOT_CREATED" when calling {@link #getType()}.
   *
   * @param operationConfiguration the operation configuration entity.
   */
  public ConfigurationDeploymentVo(OperationConfiguration operationConfiguration) {
    super(ConfigurationDeployment.TYPE,
          operationConfiguration.getOperationName(),
          operationConfiguration.getOperationDate(),
          operationConfiguration.getLastChangeUser(),
          operationConfiguration.getLastChangeDate());
    this.operationConfiguration = new OperationConfigurationVo(operationConfiguration);
  }

  public OperationConfigurationVo getOperationConfiguration() {
    return operationConfiguration;
  }

}