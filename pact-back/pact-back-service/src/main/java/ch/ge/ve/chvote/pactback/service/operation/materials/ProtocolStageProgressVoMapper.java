/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.materials;

import ch.ge.ve.chvote.pactback.contract.operation.status.ProtocolStageProgressVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialCreationStage;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgress;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A utility for mapping {@link ProtocolInstance} to a list of {@link ProtocolStageProgressVo}.
 */
public class ProtocolStageProgressVoMapper {

  /**
   * Creates a new list of {@link ProtocolStageProgressVo} for the given {@link ProtocolInstance}.
   *
   * @param instance the {@link ProtocolInstance} that holds the progress of the material generation task.
   *
   * @return the mapped list of 1{@link ProtocolStageProgressVo}.
   */
  public static List<ProtocolStageProgressVo<VotingMaterialCreationStage>> mapProgress(ProtocolInstance instance) {
    List<ProtocolStageProgressVo<VotingMaterialCreationStage>> result = new ArrayList<>();
    result.add(mapInitStage(instance));
    result.addAll(
        instance.getProgress()
                .stream()
                .filter(ProtocolStageProgressVoMapper::isVotingMaterialCreationStage)
                .map(ProtocolStageProgressVoMapper::mapProtocolStageProgress).collect(Collectors.toList())
    );

    return result;
  }

  private static ProtocolStageProgressVo<VotingMaterialCreationStage> mapInitStage(ProtocolInstance instance) {
    double initRatio =
        instance.getProgress()
                .stream()
                .filter(ProtocolStageProgressVoMapper::isInitializationStage)
                .filter(progress -> progress.getDone().equals(progress.getTotal()))
                .count() / 5.0;

    LocalDateTime startDate =
        instance.getProgress()
                .stream()
                .filter(ProtocolStageProgressVoMapper::isInitializationStage)
                .sorted(Comparator.comparing(ProtocolStageProgress::getStartDate))
                .map(ProtocolStageProgress::getStartDate)
                .findFirst().orElse(null);

    LocalDateTime lastModificationDate =
        instance.getProgress()
                .stream()
                .filter(ProtocolStageProgressVoMapper::isInitializationStage)
                .sorted(Comparator.comparing(ProtocolStageProgress::getLastModificationDate).reversed())
                .map(ProtocolStageProgress::getLastModificationDate)
                .findFirst().orElse(null);

    return new ProtocolStageProgressVo<>(
        VotingMaterialCreationStage.INITIALIZE_PARAMETERS,
        -1,
        initRatio,
        startDate,
        lastModificationDate);
  }

  private static boolean isInitializationStage(ProtocolStageProgress progress) {
    ProtocolStage stage = progress.getStage();
    return ProtocolStage.SEND_PUBLIC_PARAMETERS.equals(stage) ||
           ProtocolStage.PUBLISH_PUBLIC_KEY_PARTS.equals(stage) ||
           ProtocolStage.STORE_PUBLIC_KEY_PARTS.equals(stage) ||
           ProtocolStage.SEND_ELECTION_SET.equals(stage) ||
           ProtocolStage.PUBLISH_PRIMES.equals(stage);
  }

  private static boolean isVotingMaterialCreationStage(ProtocolStageProgress progress) {
    ProtocolStage stage = progress.getStage();
    return stage == ProtocolStage.SEND_VOTERS
           || stage == ProtocolStage.BUILD_PUBLIC_CREDENTIALS
           || stage == ProtocolStage.REQUEST_PRIVATE_CREDENTIALS;
  }

  private static ProtocolStageProgressVo<VotingMaterialCreationStage> mapProtocolStageProgress(
      ProtocolStageProgress progress) {
    return new ProtocolStageProgressVo<>(
        VotingMaterialCreationStage.valueOf(progress.getStage().name()),
        progress.getCcIndex(),
        progress.getDone() / (double) progress.getTotal(),
        progress.getStartDate(),
        progress.getLastModificationDate()
    );
  }

  /**
   * Hide utility class constructor.
   */
  private ProtocolStageProgressVoMapper() {
    throw new AssertionError("Not instantiable");
  }
}
