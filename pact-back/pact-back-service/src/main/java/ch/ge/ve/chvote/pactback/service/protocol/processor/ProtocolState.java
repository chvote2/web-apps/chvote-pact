/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService;
import ch.ge.ve.chvote.pactback.service.protocol.progress.DefaultProgressTracker;
import ch.ge.ve.chvote.pactback.service.protocol.progress.DefaultProgressTrackerByCC;
import ch.ge.ve.protocol.client.ProtocolClient;
import ch.ge.ve.protocol.client.progress.api.ProgressTracker;
import ch.ge.ve.protocol.client.progress.api.ProgressTrackerByCC;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.function.Function;

/**
 * A class containing the current state of a {@link ProtocolInstance}.
 */
public class ProtocolState {
  private final ProtocolInstanceService protocolInstanceService;
  private final ProtocolClient          client;
  private final PublicParametersFactory publicParametersFactory;
  private       ProtocolInstance        instance;

  /**
   * Create a new {@link ProtocolState}.
   *
   * @param instance                the {@link ProtocolInstance}.
   * @param protocolInstanceService the {@link ProtocolInstanceService}.
   * @param publicParametersFactory the {@link PublicParametersFactory}.
   * @param client                  the {@link ProtocolClient}-
   */
  public ProtocolState(ProtocolInstance instance,
                       ProtocolInstanceService protocolInstanceService,
                       PublicParametersFactory publicParametersFactory,
                       ProtocolClient client) {
    this.protocolInstanceService = protocolInstanceService;
    this.instance = instance;
    this.publicParametersFactory = publicParametersFactory;
    this.client = client;
  }

  public Operation getOperation() {
    return instance.getOperation();
  }

  public OperationConfiguration getConfiguration() {
    return instance.getConfiguration();
  }

  public ProtocolInstance getInstance() {
    return instance;
  }

  /**
   * Execute an action on the {@code ProtocolInstance} within a transaction.
   *
   * @param action the action to perform
   *
   * @return the object returned by the action
   *
   * @see ProtocolInstanceService#withProtocolInstance(Long, Function)
   */
  public <T> T withInstanceInTransaction(Function<ProtocolInstance, T> action) {
    return protocolInstanceService.withProtocolInstance(instance.getId(), action);
  }

  public ProtocolClient getClient() {
    return client;
  }

  public PublicParametersFactory getPublicParametersFactory() {
    return publicParametersFactory;
  }

  /**
   * Update the status of the {@link ProtocolInstance} of this {@link ProtocolState}.
   *
   * @param status the new {@link ProtocolInstanceStatus} to set.
   */
  public void updateProtocolStatus(ProtocolInstanceStatus status) {
    instance = protocolInstanceService.updateProtocolStatus(instance.getId(), status);
  }

  /**
   * Update the Voters creation statistics associated to the underlying {@code ProtocolInstance}
   *
   * @param statistics the new statistics to associate
   */
  public void updateCreationStatistics(Collection<VotersCreationStats> statistics) {
    instance = protocolInstanceService.saveCreationStatistics(instance.getId(), statistics);
  }

  public void updateProtocolPublicParameters(PublicParameters publicParameters) {
    instance = protocolInstanceService.setPublicParameters(instance.getId(), publicParameters);
  }

  public ProgressTracker createProgressTracker(ProtocolStage stage) {
    return new DefaultProgressTracker(instance.getId(), stage, protocolInstanceService);
  }

  public ProgressTrackerByCC createProgressTrackerByCC(ProtocolStage stage) {
    return new DefaultProgressTrackerByCC(instance.getId(), stage, protocolInstanceService);
  }

  /**
   * Update the creation date of the printer archive associated to the underlying {@code ProtocolInstance}
   *
   * @param creationDate the new statistics to associate
   */
  public void updatePrinterArchiveCreationDate(LocalDateTime creationDate) {
    instance = protocolInstanceService.updatePrinterArchiveCreationDate(instance.getId(), creationDate);
  }

  /**
   * Update the location of the generated tally archive associated to the underlying {@code ProtocolInstance}
   *
   * @param tallyArchiveLocation the new location of the tally archive
   */
  public void updateTallyArchiveLocation(Path tallyArchiveLocation) {
    instance = protocolInstanceService.updateTallyArchiveLocation(instance.getId(), tallyArchiveLocation);
  }

  /**
   * Check whether the {@code ProtocolInstance} "has reached" the given status, ie if its status is
   * at least at the given status.
   *
   * @param status the minimal status expected
   *
   * @return {@code true} if the status of this instance is at the given status or after
   */
  public boolean isProtocolInstanceStatusAtOrAfter(ProtocolInstanceStatus status) {
    ProtocolInstanceStatus instanceStatus = instance.getStatus();
    return instanceStatus != null && instanceStatus.compareTo(status) >= 0;
  }
}
