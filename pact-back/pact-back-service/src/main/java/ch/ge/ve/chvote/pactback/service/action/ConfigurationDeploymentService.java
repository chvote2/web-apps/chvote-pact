/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action;

import ch.ge.ve.chvote.pactback.repository.action.deploy.ConfigurationDeploymentRepository;
import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeployment;
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.OperationConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.user.UserRepository;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import ch.ge.ve.chvote.pactback.service.action.vo.ConfigurationDeploymentVo;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyExistsException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException;
import ch.ge.ve.chvote.pactback.service.operation.OperationService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A specialised {@link BaseActionService} for {@link ConfigurationDeploymentVo}s.
 */
@Service
public class ConfigurationDeploymentService extends ActionCreationService<ConfigurationDeploymentVo> {

  private final ConfigurationDeploymentRepository                                          actionRepository;
  private final OperationConfigurationRepository                                           configurationRepository;
  private final OperationService                                                           operationService;
  private final PendingActionRetriever<ConfigurationDeployment, ConfigurationDeploymentVo> pendingActionRetriever;

  /**
   * Create a new configuraton deployment service with the given repositories.
   *
   * @param userRepository          the {@link User} repository.
   * @param actionRepository        the {@link ConfigurationDeployment} repository.
   * @param configurationRepository the {@link OperationConfiguration} repository.
   * @param operationService        the {@link OperationService}.
   */
  @Autowired
  public ConfigurationDeploymentService(UserRepository userRepository,
                                        ConfigurationDeploymentRepository actionRepository,
                                        OperationConfigurationRepository configurationRepository,
                                        OperationService operationService) {
    super(userRepository);
    this.actionRepository = actionRepository;
    this.configurationRepository = configurationRepository;
    this.operationService = operationService;
    this.pendingActionRetriever = new PendingActionRetriever<>(actionRepository, ConfigurationDeploymentVo::new);
  }

  private ConfigurationDeploymentVo mapToConfigurationDeploymentVo(OperationConfiguration operationConfiguration) {
    return operationConfiguration.getAction()
                                 .map(ConfigurationDeploymentVo::new)
                                 .orElseGet(() -> new ConfigurationDeploymentVo(operationConfiguration));
  }

  @Override
  @Transactional(readOnly = true)
  public List<ConfigurationDeploymentVo> getNewOrPendingActions() {
    return configurationRepository
        .findAllConfigurationsDeployableOrWithDeploymentInStatus(PrivilegedAction.Status.PENDING)
        .map(this::mapToConfigurationDeploymentVo)
        .collect(Collectors.toList());
  }

  @Override
  @Transactional(readOnly = true)
  public ConfigurationDeploymentVo getByBusinessId(Long businessId) {
    OperationConfiguration configuration = getOperationConfigurationById(businessId);

    configuration.getAction().ifPresent(action -> {
      if (!action.getStatus().canUpdate()) {
        throw new PrivilegedActionAlreadyProcessedException(action.getId());
      }
    });
    return mapToConfigurationDeploymentVo(configuration);
  }

  @Override
  @Transactional
  public void createAction(Long businessId, String requesterName) {
    if (actionRepository.existsByOperationConfiguration_Id(businessId)) {
      throw new PrivilegedActionAlreadyExistsException(
          String.format("An action was already created for the OperationConfiguration object with ID %d", businessId));
    }

    OperationConfiguration operationConfiguration = getOperationConfigurationById(businessId);

    ConfigurationDeployment action = new ConfigurationDeployment();
    action.setOperationConfiguration(operationConfiguration);

    actionRepository.save(populateNewAction(action, requesterName, operationConfiguration.getOperationName(),
                                            operationConfiguration.getOperationDate()));
  }

  @Override
  @Transactional
  public void approveAction(long actionId, String validatorName) {
    User validator = getUserByName(validatorName);

    ConfigurationDeployment action = pendingActionRetriever.getPendingPrivilegedAction(actionId);
    action = updatePrivilegedAction(action, validator, PrivilegedAction.Status.APPROVED);

    operationService.deployConfigurationInTest(action.getOperationConfiguration().getId());
    actionRepository.save(action);
  }

  @Override
  @Transactional
  public void rejectAction(long actionId, String validatorName, String reason) {
    User validator = getUserByName(validatorName);

    ConfigurationDeployment action = pendingActionRetriever.getPendingPrivilegedAction(actionId);
    actionRepository.save(updatePrivilegedAction(action, validator, PrivilegedAction.Status.REJECTED, reason));
  }

  @Override
  public ConfigurationDeploymentVo getPendingPrivilegedAction(long id) {
    return pendingActionRetriever.getPendingPrivilegedActionVo(id);
  }

  private OperationConfiguration getOperationConfigurationById(long id) {
    return configurationRepository.findById(id)
                                  .orElseThrow(() -> new EntityNotFoundException(
                                      String.format("No operation configuration found for id %d", id)));
  }

}
