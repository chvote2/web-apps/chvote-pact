/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration.printer.vo;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.Municipality;

/**
 * A {@link Municipality} value object.
 */
public class MunicipalityVo {
  private final Long   id;
  private final Integer ofsId;
  private final String name;

  /**
   * Creates a new municipality value object based on the given {@link Municipality} entity.
   *
   * @param municipality the {@link Municipality} entity.
   */
  public MunicipalityVo(Municipality municipality) {
    this.id = municipality.getId();
    this.ofsId = municipality.getOfsId();
    this.name = municipality.getName();
  }

  public Long getId() {
    return id;
  }

  public Integer getOfsId() {
    return ofsId;
  }

  public String getName() {
    return name;
  }
}
