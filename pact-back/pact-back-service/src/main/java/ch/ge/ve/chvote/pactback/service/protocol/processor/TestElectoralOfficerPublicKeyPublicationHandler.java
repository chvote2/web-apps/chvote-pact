/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.ByteStreams;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

/**
 * A {@link ProtocolProcessorHandler} that sends, for test, one valid hard-coded electoral officer's public key to the
 * protocol.
 */
@SuppressWarnings("UnstableApiUsage")
@Component
public class TestElectoralOfficerPublicKeyPublicationHandler extends ElectoralOfficerPublicKeyPublicationHandler {

  private static final String PATTERN_ELECTORAL_AUTHORITY_KEYS =
      "/electoral-authority-keys-for-test-modes/level-%d-authority-keys.json";
  private final        int    securityLevel;

  /**
   * Creates a new electoral officer public key publication handler for a protocol instance in test..
   *
   * @param objectMapper  the {@link ObjectMapper} to be used
   * @param securityLevel the security level
   */
  @Autowired
  public TestElectoralOfficerPublicKeyPublicationHandler(ObjectMapper objectMapper,
                                                         @Value("${ch.ge.ve.security-level}") int securityLevel) {
    super(objectMapper);
    this.securityLevel = securityLevel;
  }

  @Override
  protected byte[] getEncryptionPublicKeyStream(ProtocolState state) throws IOException {
    return ByteStreams.toByteArray(
        new ClassPathResource(String.format(PATTERN_ELECTORAL_AUTHORITY_KEYS, securityLevel))
            .getInputStream());
  }

}
