/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import org.springframework.stereotype.Component;

/**
 * A {@link ProtocolProcessorHandler} that requests the generation of keys.
 */
@Component
public class KeyGenerationHandler extends ProtocolProcessorHandler {
  @Override
  protected void process(ProtocolState state) {
    state.getClient().requestKeyGeneration();
    state.updateProtocolStatus(ProtocolInstanceStatus.GENERATING_KEYS);
    state.getClient()
         .ensurePublicKeyPartsPublishedToBB(state.createProgressTracker(ProtocolStage.PUBLISH_PUBLIC_KEY_PARTS));
    state.getClient()
         .ensurePublicKeyPartsForwardedToCCs(state.createProgressTracker(ProtocolStage.STORE_PUBLIC_KEY_PARTS));
    state.updateProtocolStatus(ProtocolInstanceStatus.KEYS_GENERATED);
  }

  @Override
  public boolean hasBeenProcessed(ProtocolState state) {
    return state.isProtocolInstanceStatusAtOrAfter(ProtocolInstanceStatus.KEYS_GENERATED);
  }
}
