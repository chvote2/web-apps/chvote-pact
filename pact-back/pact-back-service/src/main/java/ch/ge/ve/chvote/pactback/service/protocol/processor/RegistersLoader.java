/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.VotersRegisterFile;
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class to load the registers from the database
 */
@Service
public class RegistersLoader {
  private final VotingMaterialsConfigurationRepository repository;

  public RegistersLoader(VotingMaterialsConfigurationRepository repository) {
    this.repository = repository;
  }

  /**
   * Loads the registers in memory from the database.
   *
   * @param id id of the voting material configuration
   *
   * @return a {@link ByteArrayInputStream} for each register file
   */
  @Transactional
  public InputStream[] loadRegistersAsInputStreams(Long id) {
    // need to reload the entity to be able to resolve the lazy links (this would not work with a detached entity)
    VotingMaterialsConfiguration votingMaterialsConfiguration = repository.getOne(id);

    List<VotersRegisterFile> registers = votingMaterialsConfiguration.getRegisters();
    return registers.stream()
                    .map(OperationReferenceFile::getFile)
                    .map(ByteArrayInputStream::new)
                    .toArray(InputStream[]::new);
  }
}
