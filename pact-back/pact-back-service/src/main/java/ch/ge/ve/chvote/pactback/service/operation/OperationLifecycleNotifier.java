/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation;

import ch.ge.ve.chvote.pactback.notification.event.VotingMaterialsGeneratedEvent;
import ch.ge.ve.chvote.pactback.notification.publisher.NotificationEventsPublisher;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolStatusChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Listens to the changes of {@link ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance}s and publishes
 * events to the {@link NotificationEventsPublisher}.
 */
@Component
public class OperationLifecycleNotifier {
  private static final Logger logger = LoggerFactory.getLogger(OperationLifecycleNotifier.class);

  private final NotificationEventsPublisher notificationEventsPublisher;

  /**
   * Create a new operation lifecycle notifier.
   *
   * @param notificationEventsPublisher the notification event publisher where the events will be published.
   */
  @Autowired
  public OperationLifecycleNotifier(NotificationEventsPublisher notificationEventsPublisher) {
    this.notificationEventsPublisher = notificationEventsPublisher;
  }

  /**
   * Publish a {@link VotingMaterialsGeneratedEvent} to the {@link NotificationEventsPublisher} whenever a {@link
   * ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance} status transitions to: {@link
   * ProtocolInstanceStatus#CREDENTIALS_GENERATED}, otherwise does nothing.
   *
   * @param event raised when the status changed - will be checked to have transitioned to {@link
   *              ProtocolInstanceStatus#CREDENTIALS_GENERATED}
   */
  @EventListener
  public void notifyVotingMaterialsReady(ProtocolStatusChangeEvent event) {
    if (event.getStatusAfter() == ProtocolInstanceStatus.CREDENTIALS_GENERATED) {
      String protocolId = event.getProtocolInstance().getProtocolId();

      logger.info("Send VotingMaterialsGeneratedEvent for protocol id = {}", protocolId);

      notificationEventsPublisher.publish(
          new VotingMaterialsGeneratedEvent(protocolId)
      );
    }
  }
}
