/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.voter.vo;

import java.time.Month;
import java.time.Year;
import java.util.Objects;
import java.util.Optional;

public final class DateOfBirthVo {

  private final Year    year;
  private final Month   monthOfYear;
  private final Integer dayOfMonth;

  public DateOfBirthVo(Year year, Month monthOfYear, Integer dayOfMonth) {
    this.year = Objects.requireNonNull(year);
    this.monthOfYear = monthOfYear;
    this.dayOfMonth = dayOfMonth;
  }

  public Year getYear() {
    return year;
  }

  public Optional<Month> getMonthOfYear() {
    return Optional.ofNullable(monthOfYear);
  }

  public Optional<Integer> getDayOfMonth() {
    return Optional.ofNullable(dayOfMonth);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DateOfBirthVo that = (DateOfBirthVo) o;
    return Objects.equals(year, that.year)
           && monthOfYear == that.monthOfYear
           && Objects.equals(dayOfMonth, that.dayOfMonth);
  }

  @Override
  public int hashCode() {
    return Objects.hash(year, monthOfYear, dayOfMonth);
  }
}
