/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation;

import com.google.common.collect.ImmutableMap;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

/**
 * Implementation of {@link PactSchedulerApi} as a REST client.
 */
public class PactSchedulerRestClient implements PactSchedulerApi {

  private static final Logger logger = LoggerFactory.getLogger(PactSchedulerRestClient.class);

  private final RestTemplate restTemplate;

  /**
   * Instantiates a REST client for the {@code PactSchedulerApi}.
   *
   * @param rootUri URI of the server hosting the endpoints - <i>eg</i> "http://localhost:8080"
   * @param restTemplateBuilder SpringBoot's builder for a {@link RestTemplate}
   */
  public PactSchedulerRestClient(String rootUri, RestTemplateBuilder restTemplateBuilder) {
    this.restTemplate = restTemplateBuilder.rootUri(rootUri)
                                           .basicAuthorization("test-user", "password")
                                           .build();
  }

  @Override
  public void register(String clientId, LocalDateTime scheduledDate) {
    final String endpointUrl = "/tasks/operations/{clientId}?date={date}";
    final Map<String, Object> params = ImmutableMap.of("clientId", clientId, "date", scheduledDate);
    logger.debug("Register operation to {} with {}", endpointUrl, params);
    restTemplate.put(endpointUrl, null, params);
  }

  @Override
  public void unregister(String clientId) {
    final String endpointUrl = "/tasks/operations/{clientId}";
    logger.debug("Unregister operation {} at {}", clientId, endpointUrl);
    try {
      restTemplate.delete(endpointUrl, Collections.singletonMap("clientId", Objects.requireNonNull(clientId)));
    } catch (HttpStatusCodeException e) {
      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
        logger.warn("Endpoint indicates that the operation [{}] has never been registered", clientId);
      } else {
        throw e;
      }
    }
  }

}
