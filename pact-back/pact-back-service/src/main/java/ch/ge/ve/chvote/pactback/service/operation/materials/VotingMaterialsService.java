/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.materials;

import ch.ge.ve.chvote.pactback.contract.operation.MunicipalityToPrinterLinkVo;
import ch.ge.ve.chvote.pactback.contract.operation.VotingMaterialsConfigurationSubmissionVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotersCreationStatisticsVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.VotersRegisterFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.Municipality;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.exception.InvalidVotingMaterialsConfigurationException;
import ch.ge.ve.chvote.pactback.service.operation.TimeService;
import ch.ge.ve.chvote.pactback.service.operation.configuration.printer.MunicipalityVoMapper;
import ch.ge.ve.chvote.pactback.service.operation.configuration.printer.PrinterConfigurationVoMapper;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * A service for managing the voting materials.
 */
@Service
public class VotingMaterialsService {

  private final VotingMaterialsAttachmentService attachmentService;

  private final OperationRepository operationRepository;

  private final VotingMaterialsStatusVoMapper votingMaterialsStatusVoMapper;

  private final TimeService timeService;

  /**
   * Create a new {@link VotingMaterialsService} with the given repository and the given mapper.
   */
  @Autowired
  public VotingMaterialsService(
      VotingMaterialsAttachmentService attachmentService,
      OperationRepository operationRepository,
      VotingMaterialsStatusVoMapper votingMaterialsStatusVoMapper,
      TimeService timeService) {
    this.attachmentService = attachmentService;
    this.operationRepository = operationRepository;
    this.votingMaterialsStatusVoMapper = votingMaterialsStatusVoMapper;
    this.timeService = timeService;
  }

  /**
   * Creates a new {@link VotingMaterialsConfiguration} for an existing {@link Operation}.
   * <p>Can't be invoked outside of a parent transaction.</p>
   *
   * @param submissionVo data for the voting materials configuration
   * @param zipFiles     ZIP files, each containing one XML voters register file
   *
   * @return a new {@link VotingMaterialsConfiguration}
   */
  @Transactional(propagation = Propagation.REQUIRED)
  public VotingMaterialsConfiguration createVotingMaterialsConfiguration(
      VotingMaterialsConfigurationSubmissionVo submissionVo,
      List<MultipartFile> zipFiles
  ) {

    VotingMaterialsConfiguration votingMaterials = new VotingMaterialsConfiguration();

    // main data
    votingMaterials.setVotingCardLabel(submissionVo.getVotingCardLabel());
    votingMaterials.setTarget(DeploymentTarget.valueOf(submissionVo.getTarget().name()));
    votingMaterials.setSimulationName(submissionVo.getSimulationName());
    votingMaterials.setAuthor(submissionVo.getUser());
    votingMaterials.setSubmissionDate(timeService.now());
    votingMaterials.setValidationStatus(ValidationStatus.NOT_YET_VALIDATED);

    // voters register files
    List<VotersRegisterFile> registers =
        attachmentService.createVotersRegisterFiles(submissionVo.getRegisterFilesCatalog(), zipFiles);
    votingMaterials.setRegisters(registers);

    // printerConfigurations
    List<PrinterConfiguration> printerConfigurations =
        submissionVo.getPrinters().stream()
                    .map(PrinterConfigurationVoMapper::mapToPrinter)
                    .collect(Collectors.toList());

    if (submissionVo.getSwissAbroadWithoutMunicipalityPrinter() != null) {
      PrinterConfiguration swissAbroadPrinterConfiguration =
          PrinterConfigurationVoMapper.mapToPrinter(submissionVo.getSwissAbroadWithoutMunicipalityPrinter(), true);
      printerConfigurations.add(
          swissAbroadPrinterConfiguration
      );
      // TODO remove this relation to keep only printers relations
      // TODO also rename the printers relation to printerConfigurations
      votingMaterials.setSwissAbroadPrinter(swissAbroadPrinterConfiguration);
    }

    checkPrinterConfigurations(printerConfigurations);

    votingMaterials.setPrinters(printerConfigurations);

    // real municipalities
    Map<Integer, Municipality> municipalities =
        submissionVo.getMunicipalities().stream()
                    .map(MunicipalityVoMapper::mapToRealMunicipality)
                    .collect(Collectors.toMap(Municipality::getOfsId, Function.identity()));

    // virtual municipalities
    municipalities.putAll(
        submissionVo.getVirtualMunicipalities().stream()
                    .map(MunicipalityVoMapper::mapToVirtualMunicipality)
                    .collect(Collectors.toMap(Municipality::getOfsId, Function.identity()))
    );

    // municipalities to printerConfigurations
    checkSegmentation(submissionVo.getMunicipalitiesToPrintersLinks());

    Map<String, PrinterConfiguration> printersConfigurationsWithMunicipalitiesMap = printerConfigurations
        .stream()
        .filter(printerConfiguration -> !printerConfiguration.isSwissAbroadWithoutMunicipality())
        .collect(Collectors.toMap(PrinterConfiguration::getBusinessIdentifier, p -> p));

    submissionVo.getMunicipalitiesToPrintersLinks()
                .forEach(link -> associateMunicipalityToPrinterConfiguration(
                    link,
                    printersConfigurationsWithMunicipalitiesMap,
                    municipalities));

    return votingMaterials;
  }

  private void checkPrinterConfigurations(List<PrinterConfiguration> printerConfigurations) {
    printerConfigurations.stream()
                         .collect(Collectors.groupingBy(PrinterConfiguration::getBusinessIdentifier))
                         .forEach((key, value) -> {
                           long differentPublicKeysCount = value.stream()
                                                                .map(PrinterConfiguration::getPublicKey)
                                                                .map(BigInteger::new)
                                                                .distinct()
                                                                .count();
                           if (differentPublicKeysCount > 1) {
                             throw new InvalidPrinterConfigurationException(
                                 String.format(
                                     "Printer [%s] configurations reference [%d] distinct public keys",
                                     key, differentPublicKeysCount));
                           }
                         });
  }

  /**
   * Checks that no municipality is associated to more than 1 printer.
   */
  private void checkSegmentation(Set<MunicipalityToPrinterLinkVo> links) {
    try {
      links.stream()
           .collect(Collectors.toMap(MunicipalityToPrinterLinkVo::getMunicipalityOfsId,
                                     MunicipalityToPrinterLinkVo::getPrinterId));
    } catch (IllegalStateException e) {
      throw new InvalidVotingMaterialsConfigurationException(
          "The list of links municipalities/printers associates a municipality to more than one printer.", e);
    }
  }

  /**
   * Get an {@link VotingMaterialsStatusVo} given an operation client identifier.
   */
  @Transactional(readOnly = true)
  public VotingMaterialsStatusVo getVotingMaterialsStatusByClientId(String clientId) {
    Operation operation = operationRepository.findByClientId(clientId)
                                             .orElseThrow(() -> new EntityNotFoundException(
                                                 String.format("No operation found for clientId [%s]", clientId)));

    if (!operation.getVotingMaterialsConfiguration().isPresent()) {
      throw new EntityNotFoundException(
          String.format("No voting materials configuration found for clientId [%s]", clientId));
    }

    return votingMaterialsStatusVoMapper.map(operation);
  }

  private void associateMunicipalityToPrinterConfiguration(
      MunicipalityToPrinterLinkVo linkVo,
      Map<String, PrinterConfiguration> printers,
      Map<Integer, Municipality> municipalities) {

    PrinterConfiguration printer = Optional.ofNullable(printers.get(linkVo.getPrinterId()))
                                           .orElseThrow(
                                               getItemNotFoundExceptionSupplier("printer", linkVo.getPrinterId()));

    Municipality municipality = Optional.ofNullable(municipalities.get(linkVo.getMunicipalityOfsId()))
                                        .orElseThrow(getItemNotFoundExceptionSupplier("municipality",
                                                                                      linkVo.getMunicipalityOfsId()));

    printer.getMunicipalities().add(municipality);
  }

  private Supplier<RuntimeException> getItemNotFoundExceptionSupplier(String type, Object value) {
    return () -> new InvalidVotingMaterialsConfigurationException(
        "MunicipalityToPrinterLinkVo points to non-existing " + type + " [" + value + "]");
  }

  /**
   * Get the statistics of Voters creation for the deployed configuration of an operation.
   *
   * @param clientId client identifier of the operation
   *
   * @return the statistics associated to this operation
   *
   * @throws EntityNotFoundException if no operation matches the clientId, or it has no associated statistics
   */
  @Transactional(readOnly = true)
  public List<VotersCreationStatisticsVo> getDeployedMaterialsCreationStatistics(String clientId) {
    List<VotersCreationStats> statistics = operationRepository.findByClientId(clientId)
                                                              .flatMap(Operation::getDeployedConfiguration)
                                                              .flatMap(OperationConfiguration::getProtocolInstance)
                                                              .map(ProtocolInstance::getStatistics)
                                                              .orElse(Collections.emptyList());

    if (statistics.isEmpty()) {
      throw new EntityNotFoundException("No statistics for deployed Operation where clientId = " + clientId);
    }
    return statistics.stream()
                     .map(VotingMaterialsService::mapStatistic)
                     .collect(Collectors.toList());

  }

  private static VotersCreationStatisticsVo mapStatistic(VotersCreationStats entity) {
    return new VotersCreationStatisticsVo(entity.getPrinterAuthorityName(),
                                          entity.getCountingCircleBusinessId(),
                                          entity.getCountingCircleName(),
                                          entity.getNumberOfVoters());
  }


  /**
   * Validates the {@link VotingMaterialsConfiguration} after voting material has been created for the given {@link
   * Operation}.
   *
   * @param clientId client identifier of the operation
   * @param user     the username of the user performing the validation.
   */
  @Transactional
  public void validateConfigurationByClientId(String clientId, String user) {
    Operation operation = operationRepository.findByClientId(clientId)
                                             .orElseThrow(() -> new EntityNotFoundException(
                                                 "No operation where clientId = " + clientId));


    VotingMaterialsStatusVo votingMaterialsStatusVo = votingMaterialsStatusVoMapper.map(operation);

    if ((votingMaterialsStatusVo.getState() != VotingMaterialsStatusVo.State.CREATED) &&
        (votingMaterialsStatusVo.getState() != VotingMaterialsStatusVo.State.INVALIDATION_REJECTED)) {
      throw new IllegalStateException(
          "Current state of voting materials [" + votingMaterialsStatusVo.getState() + "] does not allow validation");
    }

    operation
        .getVotingMaterialsConfiguration()
        .ifPresent(votingMaterialsConfiguration -> {
          votingMaterialsConfiguration.setValidationStatus(ValidationStatus.VALIDATED);
          votingMaterialsConfiguration.setValidator(user);
          votingMaterialsConfiguration.setValidationDate(LocalDateTime.now());
          votingMaterialsConfiguration.setInvalidationAction(null);
          operationRepository.save(operation);
        });
  }
}
