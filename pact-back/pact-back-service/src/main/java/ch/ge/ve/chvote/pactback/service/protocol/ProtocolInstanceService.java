/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol;

import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZING;
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.GENERATING_CREDENTIALS;
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.GENERATING_KEYS;
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.INITIALIZING;
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.KEYS_GENERATED;
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.PUBLIC_PARAMETERS_INITIALIZED;

import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.DeadLetterEventRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgress;
import ch.ge.ve.chvote.pactback.service.exception.DataSerializationException;
import ch.ge.ve.chvote.pactback.service.exception.InvalidDeploymentException;
import ch.ge.ve.chvote.pactback.service.exception.ProtocolInstanceAlreadyExistsException;
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolDeletionEvent;
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolStatusChangeEvent;
import ch.ge.ve.protocol.model.PublicParameters;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigInteger;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A service for retrieving and modifying {@link ProtocolInstance} entities.
 */
@Service
public class ProtocolInstanceService {

  private final ObjectMapper               objectMapper;
  private final ProtocolInstanceRepository instanceRepository;
  private final DeadLetterEventRepository  deadLetterEventRepository;
  private final ProtocolCleanupService     protocolCleanupService;
  private final VoterRepository            voterRepository;
  private final ApplicationEventPublisher  eventPublisher;
  private final String                     nodeRepartitionKey;

  /**
   * Creates a new {@link ProtocolInstanceService}.
   */
  @Autowired
  public ProtocolInstanceService(ProtocolInstanceRepository instanceRepository,
                                 DeadLetterEventRepository deadLetterEventRepository,
                                 ProtocolCleanupService protocolCleanupService,
                                 VoterRepository voterRepository,
                                 ApplicationEventPublisher eventPublisher,
                                 ObjectMapper objectMapper,
                                 @Value("${chvote.pact.node.repartition-key:defaultNode}") String nodeRepartitionKey) {
    this.objectMapper = objectMapper;
    this.instanceRepository = instanceRepository;
    this.deadLetterEventRepository = deadLetterEventRepository;
    this.protocolCleanupService = protocolCleanupService;
    this.voterRepository = voterRepository;
    this.eventPublisher = eventPublisher;
    this.nodeRepartitionKey = nodeRepartitionKey;
  }

  /**
   * Get the "node repartition key" affected to this instance of the PACT application.
   * <p>
   * It's a key to help partition the {@link ProtocolInstance}s so that each one can be associated to a single PACT
   * instance. This is notably used on startup : each application instances searches if it has {@code ProtocolInstance}
   * that are stuck in an intermediate state and should be taken care of.
   * </p>
   *
   * @return the node repartition key for this instance of the PACT application
   */
  public String getNodeRepartitionKey() {
    return nodeRepartitionKey;
  }

  /**
   * Deletes the given {@link ProtocolInstance}.
   *
   * @param instance the {@link ProtocolInstance} to delete.
   */
  @Transactional
  public void deleteProtocolInstance(ProtocolInstance instance) {
    protocolCleanupService.cleanUpProtocol(instance);
    deadLetterEventRepository.deleteByProtocolId(instance.getProtocolId());
    voterRepository.deleteByProtocolInstance_Id(instance.getId());
    instanceRepository.delete(instance);
    // raises a Spring Event Listener notification. At transaction commit, the event will be propagated to a
    // consumer which will delete the output directory of the protocol instance.
    // Using an event listener mechanism will make sure that the directory deletion will be performed only if
    // the DB transaction of this method (and the calling method) successfully commits
    eventPublisher.publishEvent(new ProtocolDeletionEvent(instance));
  }

  private void checkInstanceCreation(Operation operation,
                                     OperationConfiguration configuration,
                                     ProtocolEnvironment environment) {

    if (ValidationStatus.INVALIDATED == configuration.getValidationStatus()) {
      throw new InvalidDeploymentException(String.format(
          "Configuration with id: '%s' has been invalidated and can no longer be deployed",
          configuration.getId()));
    }

    Optional<ProtocolInstance> instanceOptional = configuration.getProtocolInstance();

    if (instanceOptional.isPresent()) {
      ProtocolInstance instance = instanceOptional.get();

      if (ProtocolInstanceStatus.CREDENTIALS_GENERATION_FAILURE.equals(instance.getStatus())) {
        this.deleteProtocolInstance(instance);
        configuration.setProtocolInstance(null);
      } else {
        throw new ProtocolInstanceAlreadyExistsException(String.format(
            "Configuration with id: '%s' has already a protocol instance that is: '%s' in: '%s'",
            configuration.getId(), instance.getStatus(), instance.getEnvironment().name()));
      }

    } else if (instanceRepository.existsByEnvironmentAndOperation_ClientId(environment, operation.getClientId())) {
      throw new ProtocolInstanceAlreadyExistsException(String.format(
          "Another configuration for the operation with id: '%s' has already a protocol instance in: '%s'",
          operation.getId(), environment.name()));
    }
  }

  /**
   * Creates and persists a new {@link ProtocolInstance} for the given {@link OperationConfiguration} id and {@link
   * ProtocolEnvironment}.<br> If the configuration has already an associated instance or a protocol instance already
   * exists for the same environment and
   * {@link ch.ge.ve.chvote.pactback.repository.operation.entity.Operation#getClientId()}
   * then calling this function will throw: {@link ProtocolInstanceAlreadyExistsException}.<br>If the given {@link
   * OperationConfiguration} is {@link ValidationStatus#INVALIDATED} then calling this function will throw: {@link
   * InvalidDeploymentException}.<br>This function will not initialize the protocol instance, to initialize it call
   * {@link ProtocolInitializationService#startProtocol(ProtocolInstance)} with the resulting {@link ProtocolInstance}
   * entity.
   *
   * @param operation                      the {@link Operation}.
   * @param operationConfigurationSupplier a function that provides an {@link OperationConfiguration}
   * @param environment                    the {@link ProtocolEnvironment}.
   *
   * @return The persisted {@link ProtocolInstance} entity.
   */
  @Transactional
  public ProtocolInstance createProtocolInstance(
      Operation operation,
      Supplier<OperationConfiguration> operationConfigurationSupplier,
      ProtocolEnvironment environment) {

    OperationConfiguration configuration = operationConfigurationSupplier.get();
    checkInstanceCreation(operation, configuration, environment);

    ProtocolInstance instance = new ProtocolInstance();
    instance.setNodeRepartitionKey(nodeRepartitionKey);
    instance.setProtocolId(generateProtocolId());
    instance.setConfiguration(configuration);
    instance.setOperation(operation);
    instance.setEnvironment(environment);
    instance.setStatus(INITIALIZING);
    instance.setStatusDate(LocalDateTime.now());

    return instanceRepository.save(instance);
  }

  private String generateProtocolId() {
    MessageDigest digest;
    try {
      digest = MessageDigest.getInstance("SHA-256", "SUN");
    } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
      throw new IllegalStateException("The SUN provider is required", e);
    }
    byte[] hash = digest.digest(BigInteger.valueOf(Instant.now().getNano()).toByteArray());
    return Base64.getUrlEncoder().encodeToString(hash) + "-" + UUID.randomUUID().toString();
  }

  /**
   * Updates the given {@link ProtocolInstance} status with the given {@link ProtocolInstanceStatus}.
   *
   * @param instanceId the id of the {@link ProtocolInstance}to update.
   * @param newStatus  the {@link ProtocolInstanceStatus} to set.
   *
   * @return the resulting {@link ProtocolInstance}.
   */
  @Transactional
  public ProtocolInstance updateProtocolStatus(Long instanceId, ProtocolInstanceStatus newStatus) {
    ProtocolInstance protocolInstance = instanceRepository.getOne(instanceId);
    ProtocolInstanceStatus currentStatus = protocolInstance.getStatus();

    protocolInstance.setStatus(newStatus);
    protocolInstance.setStatusDate(LocalDateTime.now());

    eventPublisher.publishEvent(new ProtocolStatusChangeEvent(protocolInstance, currentStatus, newStatus));
    return instanceRepository.save(protocolInstance);
  }


  /**
   * Update the given {@link ProtocolInstance} progress with the given parameters.
   *
   * @param instanceId the id of the {@link ProtocolInstance}to update.
   * @param stage      the stage where the progress will be registered.
   * @param ccIndex    the control component index (-1 one if this stage is not tracked by control component index)
   * @param done       the amount of done units.
   * @param total      the total amount of units.
   *
   * @return the resulting {@link ProtocolInstance}.
   */
  @Transactional
  public ProtocolInstance updateProtocolProgress(Long instanceId, ProtocolStage stage,
                                                 int ccIndex, long done, long total) {
    ProtocolInstance instance = instanceRepository.getOne(instanceId);

    ProtocolStageProgress stageProgress = findProgressByStageAndCCIndex(instance, stage, ccIndex);

    if (stageProgress == null) {
      stageProgress = new ProtocolStageProgress();
      stageProgress.setCcIndex(ccIndex);
      stageProgress.setStage(stage);
      stageProgress.setStartDate(LocalDateTime.now());
      instance.getProgress().add(stageProgress);
    }

    stageProgress.setLastModificationDate(LocalDateTime.now());
    stageProgress.setDone(done);
    stageProgress.setTotal(total);

    return instanceRepository.save(instance);
  }

  private ProtocolStageProgress findProgressByStageAndCCIndex(ProtocolInstance instance,
                                                              ProtocolStage stage, int ccIndex) {
    Optional<ProtocolStageProgress> stageProgress =
        instance.getProgress().stream()
                .filter(sp -> sp.getStage().equals(stage) && sp.getCcIndex().equals(ccIndex))
                .findFirst();

    return stageProgress.orElse(null);
  }

  /**
   * Affects Voters creation statistics to a ProtocolInstance, replacing existing values if any.
   *
   * @param instanceId identifier of the {@code ProtocolInstance}
   * @param statistics the statistics to persist
   *
   * @return the saved ProtocolInstance object
   */
  @Transactional
  public ProtocolInstance saveCreationStatistics(Long instanceId, Collection<VotersCreationStats> statistics) {
    ProtocolInstance protocolInstance = instanceRepository.getOne(instanceId);
    List<VotersCreationStats> statsList = protocolInstance.getStatistics();
    if (!statsList.isEmpty()) {
      statsList.clear();
    }
    statsList.addAll(statistics);
    return instanceRepository.save(protocolInstance);
  }

  @Transactional
  public ProtocolInstance setPublicParameters(Long instanceId, PublicParameters publicParameters) {
    ProtocolInstance protocolInstance = instanceRepository.getOne(instanceId);
    try {
      protocolInstance.setPublicParameters(objectMapper.writeValueAsString(publicParameters));
    } catch (JsonProcessingException e) {
      throw new DataSerializationException("Couldn't serialize public parameters", e);
    }
    return instanceRepository.save(protocolInstance);
  }

  @Transactional
  public ProtocolInstance updatePrinterArchiveCreationDate(Long instanceId, LocalDateTime printerArchiveCreationDate) {
    ProtocolInstance protocolInstance = instanceRepository.getOne(instanceId);
    protocolInstance.setPrinterArchiveCreationDate(printerArchiveCreationDate);
    return instanceRepository.save(protocolInstance);
  }

  @Transactional
  public ProtocolInstance updateTallyArchiveLocation(Long instanceId, Path tallyArchiveLocation) {
    ProtocolInstance protocolInstance = instanceRepository.getOne(instanceId);
    protocolInstance.setTallyArchivePath(tallyArchiveLocation.toString());
    return instanceRepository.save(protocolInstance);
  }

  @Transactional
  public List<ProtocolInstance> getProtocolInstancesStuckInGeneration() {
    return instanceRepository.findByNodeRepartitionKeyAndStatusIn(nodeRepartitionKey, EnumSet.of(
        INITIALIZING, PUBLIC_PARAMETERS_INITIALIZED, GENERATING_KEYS, KEYS_GENERATED, GENERATING_CREDENTIALS,
        ELECTION_OFFICER_KEY_INITIALIZING));
  }

  /**
   * Fetches all {@code ProtocolInstance}s whose treatment failed during the results generation phase.
   *
   * @return matching protocol instances
   */
  @Transactional(readOnly = true)
  public List<ProtocolInstance> getProtocolInstancesWhereTallyArchiveGenerationFailed() {
    return instanceRepository.findByStatus(ProtocolInstanceStatus.TALLY_ARCHIVE_GENERATION_FAILURE);
  }

  /**
   * Executes some action on a {@code ProtocolInstance} in a <em>read-only</em> transactional context.
   * <p>
   * Useful when access to deeper lazy-initialized dependencies is needed in a non-transactional context. This class is
   * designed to offer modification services : in this regards, this method should only be used for read access, thus
   * the transaction is enforced as read-only.
   * </p>
   *
   * @param instanceId identifier of the protocol instance entity
   * @param action     the action to perform
   * @param <T>        type of returned object
   *
   * @return the object returned by the action
   */
  @Transactional(readOnly = true)
  public <T> T withProtocolInstance(Long instanceId, Function<ProtocolInstance, T> action) {
    ProtocolInstance protocolInstance = instanceRepository.getOne(instanceId);
    return action.apply(protocolInstance);
  }
}
