/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol;

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.exception.ProtocolInitializationException;
import ch.ge.ve.chvote.pactback.service.protocol.processor.InitPublicParametersHandler;
import ch.ge.ve.chvote.pactback.service.protocol.processor.KeyGenerationHandler;
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolHandlerChain;
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolProcessorHandler;
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolState;
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolStateFactory;
import ch.ge.ve.chvote.pactback.service.protocol.processor.RealCredentialsGenerationHandler;
import ch.ge.ve.chvote.pactback.service.protocol.processor.TestCredentialsGenerationHandler;
import ch.ge.ve.chvote.pactback.service.protocol.processor.TestElectoralOfficerPublicKeyPublicationHandler;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * A service to initialize a {@link ProtocolInstance}.
 */
@Service
public class ProtocolInitializationService {
  private static final Logger log = LoggerFactory.getLogger(ProtocolInitializationService.class);

  private final InitPublicParametersHandler                     initPublicParametersHandler;
  private final KeyGenerationHandler                            keyGenerationHandler;
  private final RealCredentialsGenerationHandler                realCredentialsGenerationHandler;
  private final TestCredentialsGenerationHandler                testCredentialsGenerationHandler;
  private final ProtocolStateFactory                            protocolStateFactory;
  private final TestElectoralOfficerPublicKeyPublicationHandler testSendElectoralOfficerPublicKeyHandler;

  /**
   * Create a new {@link ProtocolInitializationService}.
   *
   * @param initPublicParametersHandler                 the {@link InitPublicParametersHandler}.
   * @param keyGenerationHandler                        the {@link KeyGenerationHandler}.
   * @param realCredentialsGenerationHandler            the {@link RealCredentialsGenerationHandler}.
   * @param testCredentialsGenerationHandler            the {@link TestCredentialsGenerationHandler}.
   * @param protocolStateFactory                        the {@link ProtocolStateFactory}.
   * @param testSendElectoralOfficerPublicKeyHandler    the {@link TestElectoralOfficerPublicKeyPublicationHandler}
   */
  @Autowired
  public ProtocolInitializationService(InitPublicParametersHandler initPublicParametersHandler,
                                       KeyGenerationHandler keyGenerationHandler,
                                       RealCredentialsGenerationHandler realCredentialsGenerationHandler,
                                       TestCredentialsGenerationHandler testCredentialsGenerationHandler,
                                       ProtocolStateFactory protocolStateFactory,
                                       TestElectoralOfficerPublicKeyPublicationHandler testSendElectoralOfficerPublicKeyHandler) {
    this.initPublicParametersHandler = initPublicParametersHandler;
    this.keyGenerationHandler = keyGenerationHandler;
    this.realCredentialsGenerationHandler = realCredentialsGenerationHandler;
    this.testCredentialsGenerationHandler = testCredentialsGenerationHandler;
    this.protocolStateFactory = protocolStateFactory;
    this.testSendElectoralOfficerPublicKeyHandler = testSendElectoralOfficerPublicKeyHandler;
  }

  private List<ProtocolProcessorHandler> createChain(ProtocolEnvironment env) {
    List<ProtocolProcessorHandler> handlers = new ArrayList<>();
    handlers.add(initPublicParametersHandler);
    handlers.add(keyGenerationHandler);

    if (ProtocolEnvironment.TEST.equals(env)) {
      handlers.add(testCredentialsGenerationHandler);
      handlers.add(testSendElectoralOfficerPublicKeyHandler);
    } else if (ProtocolEnvironment.PRODUCTION.equals(env)) {
      handlers.add(realCredentialsGenerationHandler);
    }

    return handlers;
  }

  private void execute(ProtocolHandlerChain chain, ProtocolState state) {
    try {
      chain.proceed(state);
    } catch (Exception e) {
      log.error("Failed to initialize protocol", e);
      state.updateProtocolStatus(ProtocolInstanceStatus.CREDENTIALS_GENERATION_FAILURE);
      throw new ProtocolInitializationException(
          String.format("Unable to initialize protocol for operation [%s]", state.getOperation().getClientId()), e);
    }
  }

  /**
   * Start a new {@link ProtocolInstance} asynchronously. The configuration for this {@link ProtocolInstance} will be
   * retrieved from {@link ProtocolInstance#getConfiguration()}.
   *
   * @param instance the {@link ProtocolInstance} to start.
   */
  @Async
  public void startProtocol(ProtocolInstance instance) {
    ProtocolState state = protocolStateFactory.initProtocolState(instance);
    ProtocolHandlerChain chain = new ProtocolHandlerChain(createChain(instance.getEnvironment()));

    execute(chain, state);
  }

  /**
   * Takes the given {@code ProtocolInstance} from its currently persisted state down the chain,
   * <i>ie</i> until the status {@code CREDENTIALS_GENERATED}.
   * <p>
   * This method has been conceived for recovery measures, if the method {@link #startProtocol(ProtocolInstance)}
   * has failed during its execution.
   * </p>
   *
   * @param instance the {@code ProtocolInstance} to continue with
   */
  @Async
  public void continueProtocol(ProtocolInstance instance) {
    // "initProtocolState" can also be used here to wrap the ProtocolInstance into a ProtocolState
    ProtocolState state = protocolStateFactory.initProtocolState(instance);
    List<ProtocolProcessorHandler> handlers = createChain(instance.getEnvironment());
    handlers.removeIf(h -> h.hasBeenProcessed(state));

    if (!handlers.isEmpty()) {
      execute(new ProtocolHandlerChain(handlers), state);
    }
  }
}
