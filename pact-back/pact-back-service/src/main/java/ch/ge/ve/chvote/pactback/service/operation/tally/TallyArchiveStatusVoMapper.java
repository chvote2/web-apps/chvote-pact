/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.tally;

import ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import org.springframework.stereotype.Service;

/**
 * A utility class for creating a {@link TallyArchiveStatusVo} from a {@link ProtocolInstance}.
 */
@Service
public class TallyArchiveStatusVoMapper {

  /**
   * Creates a new {@link TallyArchiveStatusVo} for the given {@link ProtocolInstance}.
   * <p>
   * May not be invoked outside a parent transaction.
   *
   * @param instance the {@link ProtocolInstance} to map.
   *
   * @return the mapped {@link TallyArchiveStatusVo}.
   */
  public TallyArchiveStatusVo map(ProtocolInstance instance) {
    return new TallyArchiveStatusVo(
        mapState(instance),
        instance.getTallyArchivePath()
    );
  }

  private TallyArchiveStatusVo.State mapState(ProtocolInstance instance) {
    TallyArchiveStatusVo.State state;
    switch (instance.getStatus()) {
      case SHUFFLING:
      case DECRYPTING:
      case GENERATING_RESULTS:
        state = TallyArchiveStatusVo.State.CREATION_IN_PROGRESS;
        break;
      case RESULTS_AVAILABLE:
        state = TallyArchiveStatusVo.State.CREATED;
        break;
      case NOT_ENOUGH_VOTES_TO_INITIATE_TALLY:
        state = TallyArchiveStatusVo.State.NOT_ENOUGH_VOTES_CAST;
        break;
      case TALLY_ARCHIVE_GENERATION_FAILURE:
        state = TallyArchiveStatusVo.State.CREATION_FAILED;
        break;
      default:
        state = TallyArchiveStatusVo.State.NOT_REQUESTED;
    }
    return state;
  }

}
