/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol;

import ch.ge.ve.chvote.pactback.repository.protocol.DeadLetterEventRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.DeadLetterEvent;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.protocol.vo.DeadLetterEventVo;
import ch.ge.ve.event.AcknowledgeableEvent;
import ch.ge.ve.event.Event;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventProcessingRuntimeException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A service to manage dead letter events.
 */
@Service
@Transactional
public class DeadLetterEventService {

  private static final Logger logger = LoggerFactory.getLogger(DeadLetterEventService.class);

  private final DeadLetterEventRepository repository;
  private final ObjectMapper              mapper;
  private final EventBus                  eventBus;

  @Autowired
  public DeadLetterEventService(DeadLetterEventRepository repository, ObjectMapper mapper, EventBus eventBus) {
    this.repository = repository;
    this.mapper = mapper;
    this.eventBus = eventBus;
  }

  /**
   * Persists the given event if it is not already stored.
   *
   * @param event the event to persist.
   */
  public void save(DeadLetterEventVo event) {
    if (event.getId() != null || !isAlreadyStored(event)) {
      logger.info("Storing event {}", event);
      DeadLetterEvent entity = new DeadLetterEvent(event.getId());
      entity.setContentType(event.getContentType());
      entity.setContent(event.getContent());
      entity.setCreationDate(event.getCreationDate());
      entity.setPickupDate(event.getPickupDate());
      entity.setExceptionMessage(event.getExceptionMessage());
      entity.setExceptionStacktrace(event.getExceptionStacktrace());
      entity.setType(event.getType());
      entity.setProtocolId(event.getProtocolId());
      entity.setOriginalChannel(event.getOriginalChannel());
      entity.setOriginalRoutingKey(event.getOriginalRoutingKey());
      repository.save(entity);
    } else {
      logger.info("Event {} is already stored...", event);
    }
  }

  private boolean isAlreadyStored(DeadLetterEventVo event) {
    return repository.existsByProtocolIdAndTypeAndCreationDateAndContent(event.getProtocolId(), event.getType(),
                                                                         event.getCreationDate(), event.getContent());
  }

  /**
   * Returns all persisted dead letter events.
   *
   * @return all persisted dead letter events.
   */
  @Transactional(readOnly = true)
  public List<DeadLetterEventVo> findAll() {
    return repository.findAll().stream().map(DeadLetterEventVo::fromEntity).collect(Collectors.toList());
  }

  /**
   * Finds an event given its id.
   *
   * @return the corresponding event.
   */
  @Transactional(readOnly = true)
  public DeadLetterEventVo findById(long id) {
    return repository.findById(id)
                     .map(DeadLetterEventVo::fromEntity)
                     .orElseThrow(() -> new EntityNotFoundException("Event [" + id + "] not found"));
  }

  /**
   * Republishes the specified event then deletes it.
   *
   * @param id the id of the event to republish.
   */
  public void republish(long id) {
    DeadLetterEventVo eventValueObject = findById(id);
    logger.info("Republishing event {}", eventValueObject);
    Event event = deserialize(eventValueObject.getContent(), eventValueObject.getType());
    String emitter = null;
    if (event instanceof AcknowledgeableEvent) {
      emitter = ((AcknowledgeableEvent) event).getEmitter();
    }
    eventBus.publish(eventValueObject.getOriginalChannel(), eventValueObject.getProtocolId(),
                     emitter, event, eventValueObject.getOriginalRoutingKey());
    repository.deleteById(id);
  }

  private Event deserialize(String event, String eventClass) {
    try {
      return mapper.readValue(event, readEventClass(eventClass));
    } catch (ClassNotFoundException | IOException e) {
      throw new EventProcessingRuntimeException(e);
    }
  }

  private Class<? extends Event> readEventClass(String eventClass) throws ClassNotFoundException {
    Class<?> cls = Class.forName(eventClass);
    if (Event.class.isAssignableFrom(cls)) {
      return (Class<? extends Event>) cls;
    }
    throw new EventProcessingRuntimeException("Not an event class: " + eventClass);
  }
}
