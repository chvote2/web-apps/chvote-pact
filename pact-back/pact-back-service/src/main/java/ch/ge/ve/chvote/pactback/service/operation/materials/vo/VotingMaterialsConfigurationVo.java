/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.materials.vo;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.service.operation.configuration.attachment.vo.VotersRegisterFileVo;
import ch.ge.ve.chvote.pactback.service.operation.configuration.printer.vo.PrinterConfigurationVo;
import ch.ge.ve.chvote.pactback.service.operation.configuration.vo.OperationConfigurationVo;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A {@link VotingMaterialsConfiguration} value object.
 */
public class VotingMaterialsConfigurationVo {

  private final Long                         id;
  private final String                       author;
  private final LocalDateTime                submissionDate;
  private final String                       votingCardLabel;
  private final String                       target;
  private final String                       simulationName;
  private final String                       digitalHash1;
  private final String                       digitalHash2;
  private final String                       votingSiteUrl;
  private final OperationConfigurationVo     configuration;
  private final List<VotersRegisterFileVo>   registers;
  private final List<PrinterConfigurationVo> printers;

  /**
   * Creates a new voting materials configuration value object based on the voting materials configuration of the
   * specified {@link Operation} entity.
   *
   * @param operation the {@link Operation} entity
   */
  public VotingMaterialsConfigurationVo(Operation operation) {
    VotingMaterialsConfiguration votingMaterialsConfiguration =
        operation.getVotingMaterialsConfiguration()
                 .orElseThrow(() -> new IllegalStateException(
                     String.format("Missing voting materials configuration in operation [%s]",
                                   operation.getClientId())));

    this.id = votingMaterialsConfiguration.getId();
    this.author = votingMaterialsConfiguration.getAuthor();
    this.submissionDate = votingMaterialsConfiguration.getSubmissionDate();
    this.votingCardLabel = votingMaterialsConfiguration.getVotingCardLabel();
    this.target = votingMaterialsConfiguration.getTarget().name();
    this.simulationName = votingMaterialsConfiguration.getSimulationName();
    // the following values are hard-coded, because they're not yet implemented on the caller's side
    //FIXME
    this.digitalHash1 = "A7ECDB122A5CFFA2D483F874C72DB962CD7693B318AD5259B9852498EB3CD191";
    this.digitalHash2 = "2B3990CB1394E8C80EA3C746F4DC32D1C036F54D3CD45F5934CBFC61DF4B3452";
    this.votingSiteUrl = "http://www.ge.ch/vote-electronique";

    this.configuration = new OperationConfigurationVo(operation.fetchDeployedConfiguration());
    this.registers = votingMaterialsConfiguration.getRegisters()
                                                 .stream()
                                                 .map(VotersRegisterFileVo::new)
                                                 .collect(Collectors.toList());
    this.printers =
        Stream.concat(
            Stream.concat(
                votingMaterialsConfiguration.getPrinters().stream()
                                            .filter(printer -> !printer.isSwissAbroadWithoutMunicipality())
                                            .map(PrinterConfigurationVo::buildWithRealMunicipalities),
                votingMaterialsConfiguration.getPrinters().stream()
                                            .filter(printer -> !printer.isSwissAbroadWithoutMunicipality())
                                            .map(PrinterConfigurationVo::buildWithVirtualMunicipality)),
            votingMaterialsConfiguration.getPrinters().stream()
                                        .filter(PrinterConfiguration::isSwissAbroadWithoutMunicipality)
                                        .map(PrinterConfigurationVo::buildSwissAbroadWithoutMunicipality))
              .filter(Objects::nonNull)
              .sorted(Comparator.comparing(PrinterConfigurationVo::getPrinterName))
              .collect(Collectors.toList());
  }

  public Long getId() {
    return id;
  }

  public String getAuthor() {
    return author;
  }

  public LocalDateTime getSubmissionDate() {
    return submissionDate;
  }

  public String getVotingCardLabel() {
    return votingCardLabel;
  }

  public String getTarget() {
    return target;
  }

  public String getSimulationName() {
    return simulationName;
  }

  public String getDigitalHash1() {
    return digitalHash1;
  }

  public String getDigitalHash2() {
    return digitalHash2;
  }

  public String getVotingSiteUrl() {
    return votingSiteUrl;
  }

  public OperationConfigurationVo getConfiguration() {
    return configuration;
  }

  public List<VotersRegisterFileVo> getRegisters() {
    return registers;
  }

  public List<PrinterConfigurationVo> getPrinters() {
    return printers;
  }

}
