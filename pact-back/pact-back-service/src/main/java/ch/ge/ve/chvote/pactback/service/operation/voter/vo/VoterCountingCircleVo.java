/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.voter.vo;

import java.util.Objects;

public final class VoterCountingCircleVo {

  private final int    protocolCountingCircleIndex;
  private final String businessId;
  private final String name;

  public VoterCountingCircleVo(int protocolCountingCircleIndex, String businessId, String name) {
    this.protocolCountingCircleIndex = protocolCountingCircleIndex;
    this.businessId = Objects.requireNonNull(businessId);
    this.name = Objects.requireNonNull(name);
  }

  public int getProtocolCountingCircleIndex() {
    return protocolCountingCircleIndex;
  }

  public String getBusinessId() {
    return businessId;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VoterCountingCircleVo that = (VoterCountingCircleVo) o;
    return protocolCountingCircleIndex == that.protocolCountingCircleIndex
           && Objects.equals(businessId, that.businessId)
           && Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(protocolCountingCircleIndex, businessId, name);
  }
}
