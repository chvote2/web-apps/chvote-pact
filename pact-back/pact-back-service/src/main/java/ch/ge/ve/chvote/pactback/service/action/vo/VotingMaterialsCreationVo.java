/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action.vo;

import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreation;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.service.operation.materials.vo.VotingMaterialsConfigurationVo;
import java.time.LocalDateTime;

/**
 * A {@link VotingMaterialsCreation} value object.
 */
public class VotingMaterialsCreationVo extends PrivilegedActionVo {

  private final VotingMaterialsConfigurationVo votingMaterialsConfiguration;

  /**
   * Creates a new voting materials creation value object based on the given {@link VotingMaterialsCreation} entity.
   *
   * @param operation the {@link VotingMaterialsCreation} entity. Its voting material configuration is expected to
   *                  be the voting material configuration of the action
   * @param action the {@link VotingMaterialsCreation} entity
   */
  public VotingMaterialsCreationVo(Operation operation, VotingMaterialsCreation action) {
    super(action);
    this.votingMaterialsConfiguration = new VotingMaterialsConfigurationVo(operation);
  }

  public VotingMaterialsCreationVo(Operation operation, String operationName, LocalDateTime operationDate) {
    super(VotingMaterialsCreation.TYPE,
          operationName,
          operationDate,
          operation.fetchVotingMaterialsConfiguration().getAuthor(),
          operation.fetchVotingMaterialsConfiguration().getSubmissionDate());
    this.votingMaterialsConfiguration = new VotingMaterialsConfigurationVo(operation);
  }

  public VotingMaterialsConfigurationVo getVotingMaterialsConfiguration() {
    return votingMaterialsConfiguration;
  }

}
