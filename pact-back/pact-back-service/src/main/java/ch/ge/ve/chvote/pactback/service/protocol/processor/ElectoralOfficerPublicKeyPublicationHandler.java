/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ElectoralOfficerPublicKeyPublicationHandler extends ProtocolInstanceStatusDrivenHandler {

  private final ObjectMapper objectMapper;

  /**
   * Creates a new electoral office public key publication handler.
   *
   * @param objectMapper the {@link ObjectMapper} to be used
   */
  @Autowired
  public ElectoralOfficerPublicKeyPublicationHandler(ObjectMapper objectMapper) {
    super(
        ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZED,
        ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE
    );
    this.objectMapper = objectMapper;
  }

  @Override
  protected void doProcess(ProtocolState state) throws IOException {
    state.updateProtocolStatus(ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZING);

    state.getClient().sendElectionOfficerPublicKey(
        objectMapper.readValue(getEncryptionPublicKeyStream(state), EncryptionPublicKey.class));

    state.getClient().ensureElectionOfficerPublicKeyForwardedToCCs(
        state.createProgressTracker(ProtocolStage.SEND_ELECTION_OFFICER_KEY));

  }

  protected abstract byte[] getEncryptionPublicKeyStream(ProtocolState state) throws IOException;

}
