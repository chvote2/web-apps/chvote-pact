/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.contract.tallyarchive.VotersPerCountingCircleVo;
import ch.ge.ve.filenamer.archive.TallyArchiveMaker;
import ch.ge.ve.protocol.support.ElectionVerificationDataWriterToFiles;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This component handles the generation of a tally archives.
 */
@Service
public class TallyArchiveGenerator {
  private static final String TALLY_ROOT_FOLDER_NAME         = "tallyArchive";
  private static final String OPERATION_REFERENCES_DIRECTORY = "operationReferences";
  private static final String COUNTING_CIRCLES_FILENAME      = "counting-circles.json";

  private final ObjectMapper objectMapper;

  @Autowired
  public TallyArchiveGenerator(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  /**
   * Creates a tally archive.
   *
   * @param outputDirectory               the output directory of the tally archive.
   * @param verificationDataWriterToFiles the election verification data writer that holds the incoming protocol files.
   * @param operationLabel                the label of the operation (will be used for naming the tally archive).
   * @param repositoryStreamsByName       all the operation reference files that the tally archive should include.
   *
   * @return the created tally archive path.
   *
   * @throws IOException if there was a problem writing the tally archive.
   */
  public Path createTallyArchive(Path outputDirectory,
                                 ElectionVerificationDataWriterToFiles verificationDataWriterToFiles,
                                 String operationLabel,
                                 Map<String, InputStream> repositoryStreamsByName,
                                 List<VotersPerCountingCircleVo> votersPerCountingCircles) throws IOException {
    TallyArchiveMaker tallyArchiveMaker = new TallyArchiveMaker(operationLabel);
    Path tallyArchiveRootFolder = Files.createDirectory(getTallyArchiveRootFolder(outputDirectory));
    List<Path> operationReferenceSources =
        writeOperationRepositories(repositoryStreamsByName,
                                   Files.createDirectory(outputDirectory.resolve(OPERATION_REFERENCES_DIRECTORY)));


    return tallyArchiveMaker
        .withOperationReferenceSources(operationReferenceSources)
        .withPublicParametersSource(verificationDataWriterToFiles.getPublicParametersFilePath())
        .withElectionSetForVerificationSource(verificationDataWriterToFiles.getElectionSetFilePath())
        .withPrimesSource(verificationDataWriterToFiles.getPrimesFilePath())
        .withGeneratorsSource(verificationDataWriterToFiles.getGeneratorsFilePath())
        .withPublicKeyPartsSource(verificationDataWriterToFiles.getPublicKeyPartsFilePath())
        .withPublicCredentialsSource(verificationDataWriterToFiles.getPublicCredentialsFilePath())
        .withBallotsSource(verificationDataWriterToFiles.getBallotsFilePath())
        .withConfirmationsSource(verificationDataWriterToFiles.getConfirmationsFilePath())
        .withShufflesSource(verificationDataWriterToFiles.getShufflesFilePath())
        .withShuffleProofsSource(verificationDataWriterToFiles.getShuffleProofsFilePath())
        .withPartialDecryptionsSource(verificationDataWriterToFiles.getPartialDecryptionsFilePath())
        .withPartialDecryptionProofsSource(verificationDataWriterToFiles.getDecryptionProofsFilePath())
        .withCountingCirclesSource(writeCountingCircles(votersPerCountingCircles, outputDirectory))
        .make(tallyArchiveRootFolder);
  }

  private List<Path> writeOperationRepositories(Map<String, InputStream> repositoryStreamsByName,
                                                Path outputDirectory) throws IOException {
    List<Path> allRepositories = new ArrayList<>(repositoryStreamsByName.size());
    for (Map.Entry<String, InputStream> entry : repositoryStreamsByName.entrySet()) {
      final Path outputPath = outputDirectory.resolve(entry.getKey());
      Files.copy(entry.getValue(), outputPath);

      allRepositories.add(outputPath);
    }
    return allRepositories;
  }

  private Path writeCountingCircles(List<VotersPerCountingCircleVo> votersPerCountingCircles,
                                    Path outputDirectory) throws IOException {
    Path outputFile = outputDirectory.resolve(COUNTING_CIRCLES_FILENAME);

    try (OutputStream out = Files.newOutputStream(outputFile)) {
      objectMapper.writeValue(out, votersPerCountingCircles);
    }

    return outputFile;
  }

  private Path getTallyArchiveRootFolder(Path parentPath) {
    return parentPath.resolve(TALLY_ROOT_FOLDER_NAME);
  }
}
