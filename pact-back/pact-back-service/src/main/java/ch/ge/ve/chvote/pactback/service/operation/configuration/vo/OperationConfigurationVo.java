/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration.vo;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.service.operation.configuration.attachment.vo.OperationReferenceFileVo;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@link OperationConfiguration} value object.
 */
public class OperationConfigurationVo {
  private final Long                           id;
  private final LocalDateTime                  operationDate;
  private final LocalDateTime                  lastChangeDate;
  private final LocalDateTime                  siteOpeningDate;
  private final LocalDateTime                  siteClosingDate;
  private final String                         lastChangeUser;
  private final String                         siteTextHash;
  private final Integer                        gracePeriod;
  private final String                         operationName;
  private final List<OperationReferenceFileVo> deploymentFiles;

  /**
   * Creates a new operation configuration value object based on the given {@link OperationConfiguration} entity.
   *
   * @param operationConfiguration the {@link OperationConfiguration} entity.
   */
  public OperationConfigurationVo(OperationConfiguration operationConfiguration) {
    this.id = operationConfiguration.getId();
    this.operationDate = operationConfiguration.getOperationDate();
    this.lastChangeDate = operationConfiguration.getLastChangeDate();
    this.siteOpeningDate = operationConfiguration.getSiteOpeningDate();
    this.siteClosingDate = operationConfiguration.getSiteClosingDate();
    this.lastChangeUser = operationConfiguration.getLastChangeUser();
    this.siteTextHash = operationConfiguration.getSiteTextHash();
    this.gracePeriod = operationConfiguration.getGracePeriod();
    this.operationName = operationConfiguration.getOperationName();
    this.deploymentFiles = operationConfiguration.getOperationReferenceFiles()
                                                 .stream()
                                                 .map(OperationReferenceFileVo::new)
                                                 .collect(Collectors.toList());
  }

  public Long getId() {
    return id;
  }

  public LocalDateTime getOperationDate() {
    return operationDate;
  }

  public LocalDateTime getLastChangeDate() {
    return lastChangeDate;
  }

  public LocalDateTime getSiteOpeningDate() {
    return siteOpeningDate;
  }

  public LocalDateTime getSiteClosingDate() {
    return siteClosingDate;
  }

  public String getLastChangeUser() {
    return lastChangeUser;
  }

  public String getSiteTextHash() {
    return siteTextHash;
  }

  public Integer getGracePeriod() {
    return gracePeriod;
  }

  public String getOperationName() {
    return operationName;
  }

  public List<OperationReferenceFileVo> getDeploymentFiles() {
    return deploymentFiles;
  }

}
