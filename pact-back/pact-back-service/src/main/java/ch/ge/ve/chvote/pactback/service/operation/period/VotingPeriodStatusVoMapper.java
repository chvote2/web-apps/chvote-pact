/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.period;

import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.AVAILABLE_FOR_INITIALIZATION;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.CLOSED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.INITIALIZATION_FAILED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.INITIALIZATION_IN_PROGRESS;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.INITIALIZATION_REJECTED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.INITIALIZATION_REQUESTED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.INITIALIZED;

import ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo;
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.entity.VotingPeriodInitialization;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A utility class for mapping {@link Operation} to {@link VotingPeriodStatusVo}.
 */
@Service
public class VotingPeriodStatusVoMapper {

  private final Duration votingPeriodTimeout;
  private final String   pactContextUrl;

  @Autowired
  public VotingPeriodStatusVoMapper(ChVoteConfigurationProperties configurationProperties) {
    this.votingPeriodTimeout = Duration.of(
        configurationProperties.getProtocolCore().getVotingPeriodTimeoutMinutes(),
        ChronoUnit.MINUTES
    );

    this.pactContextUrl = configurationProperties.getPactContextUrl();
  }

  /**
   * Resolve the voting period status of the given operation.
   *
   * @param operation the operation.
   *
   * @return the mapped voting period status.
   */
  public VotingPeriodStatusVo map(Operation operation) {
    return new VotingPeriodStatusVo(
        userForLastChange(operation),
        dateForLastChange(operation),
        state(operation),
        rejectionReason(operation),
        deploymentPageUrl(operation)
    );
  }


  private String rejectionReason(Operation operation) {
    return getVotingPeriodConfiguration(operation)
        .getAction()
        .map(PrivilegedAction::getRejectionReason).orElse(null);
  }


  private String userForLastChange(Operation operation) {
    VotingPeriodConfiguration configuration = getVotingPeriodConfiguration(operation);

    return configuration
        .getAction()
        .map(action -> Optional.ofNullable(action.getValidator())
                               .orElse(action.getRequester()).getUsername())
        .orElse(configuration.getAuthor());
  }

  private VotingPeriodConfiguration getVotingPeriodConfiguration(Operation operation) {
    return operation.getVotingPeriodConfiguration()
                    .orElseThrow(() -> new EntityNotFoundException(
                        String.format("No voting period configuration found for clientId [%s]",
                                      operation.getClientId())));
  }

  private LocalDateTime dateForLastChange(Operation operation) {
    VotingPeriodConfiguration configuration = getVotingPeriodConfiguration(operation);

    if (isVotingPeriodConfigurationApproved(configuration)) {
      return getProtocolInstance(operation).getStatusDate();
    } else {
      return configuration
          .getAction()
          .map(PrivilegedAction::getStatusDate)
          .orElse(configuration.getSubmissionDate());
    }
  }

  private boolean isVotingPeriodConfigurationApproved(VotingPeriodConfiguration configuration) {
    Optional<VotingPeriodInitialization> creationAction = configuration.getAction();
    return creationAction.isPresent() && PrivilegedAction.Status.APPROVED == creationAction.get().getStatus();
  }

  private ProtocolInstance getProtocolInstance(Operation operation) {
    return operation.getDeployedConfiguration()
                    .orElseThrow(() ->
                                     new IllegalStateException(
                                         String.format("No deployed configuration found for clientId [%s]",
                                                       operation.getClientId())))
                    .getProtocolInstance()
                    .orElseThrow(() -> new IllegalStateException(
                        String.format("No protocol instance found for clientId [%s]", operation.getClientId()))
                    );
  }

  private VotingPeriodStatusVo.State state(Operation operation) {
    VotingPeriodConfiguration configuration = getVotingPeriodConfiguration(operation);

    if (isVotingPeriodConfigurationApproved(configuration)) {
      return mapPeriodInitializationState(operation);
    } else {
      return configuration.getAction()
                          .map(this::mapActionState)
                          .orElse(AVAILABLE_FOR_INITIALIZATION);
    }
  }


  private VotingPeriodStatusVo.State mapActionState(VotingPeriodInitialization action) {
    switch (action.getStatus()) {
      case PENDING:
        return INITIALIZATION_REQUESTED;
      case APPROVED:
        return INITIALIZATION_IN_PROGRESS;
      case REJECTED:
        return INITIALIZATION_REJECTED;
      default:
        throw new IllegalStateException(
            String.format("Unexpected period initialization action status [%s]", action.getStatus()));
    }
  }

  private VotingPeriodStatusVo.State mapPeriodInitializationState(Operation operation) {
    ProtocolInstance protocolInstance = getProtocolInstance(operation);
    ProtocolInstanceStatus protocolInstanceStatus = protocolInstance.getStatus();

    switch (protocolInstanceStatus) {
      case INITIALIZING:
      case GENERATING_KEYS:
      case KEYS_GENERATED:
      case PUBLIC_PARAMETERS_INITIALIZED:
      case GENERATING_CREDENTIALS:
      case CREDENTIALS_GENERATED:
        return INITIALIZATION_IN_PROGRESS;
      case ELECTION_OFFICER_KEY_INITIALIZING:
        // fallthrough intended to catch all the protocol instance statuses until the initialization is done
        if (LocalDateTime.now().compareTo(protocolInstance.getStatusDate().plus(votingPeriodTimeout)) > 0) {
          return INITIALIZATION_FAILED;
        }
        return INITIALIZATION_IN_PROGRESS;
      case CREDENTIALS_GENERATION_FAILURE:
      case ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE:
        return INITIALIZATION_FAILED;
      case ELECTION_OFFICER_KEY_INITIALIZED:
        return INITIALIZATION_IN_PROGRESS;
      case READY_TO_RECEIVE_VOTES:
        return INITIALIZED;
      case GENERATING_RESULTS:
      case DECRYPTING:
      case SHUFFLING:
      case TALLY_ARCHIVE_GENERATION_FAILURE:
      case NOT_ENOUGH_VOTES_TO_INITIATE_TALLY:
      case RESULTS_AVAILABLE:
        return CLOSED;

      default:
        throw new IllegalStateException(
            String.format("Unexpected protocol instance status [%s]", protocolInstanceStatus)
        );
    }
  }

  private String deploymentPageUrl(Operation operation) {
    Long id = getVotingPeriodConfiguration(operation).getId();
    return String.format("%s/voting-period-initialization/%s", pactContextUrl, id);
  }
}
