/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.period;

import ch.ge.ve.chvote.pactback.contract.operation.VotingPeriodConfigurationSubmissionVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * A service for managing the voting period.
 */
@Service
public class VotingPeriodService {

  private final VotingPeriodAttachmentService attachmentService;
  private final OperationRepository           operationRepository;
  private final VotingPeriodStatusVoMapper    votingPeriodStatusVoMapper;


  /**
   * Create a new voting period service.
   *
   * @param attachmentService          an attachment service.
   * @param operationRepository        an operation repository.
   * @param votingPeriodStatusVoMapper a voting period status mapper.
   */
  @Autowired
  public VotingPeriodService(VotingPeriodAttachmentService attachmentService,
                             OperationRepository operationRepository,
                             VotingPeriodStatusVoMapper votingPeriodStatusVoMapper) {
    this.attachmentService = attachmentService;
    this.operationRepository = operationRepository;
    this.votingPeriodStatusVoMapper = votingPeriodStatusVoMapper;
  }

  /**
   * Convert {@link VotingPeriodConfigurationSubmissionVo} to the corresponding entity {@link
   * VotingPeriodConfiguration}
   *
   * @param submissionVo Value object to be converted
   *
   * @return entity {@link VotingPeriodConfiguration}
   */
  public VotingPeriodConfiguration convertToVotingPeriodConfiguration(
      VotingPeriodConfigurationSubmissionVo submissionVo,
      MultipartFile electionOfficerPublicKey
  ) {
    VotingPeriodConfiguration votingPeriodConfiguration = new VotingPeriodConfiguration();
    votingPeriodConfiguration.setAuthor(submissionVo.getUser());
    votingPeriodConfiguration.setElectoralAuthorityKey(
        attachmentService.createPublicKeyFile(electionOfficerPublicKey, submissionVo.getElectoralAuthorityKey()));
    votingPeriodConfiguration.setSubmissionDate(LocalDateTime.now());
    votingPeriodConfiguration.setSiteOpeningDate(submissionVo.getSiteOpeningDate());
    votingPeriodConfiguration.setSiteClosingDate(submissionVo.getSiteClosingDate());
    votingPeriodConfiguration.setGracePeriod(submissionVo.getGracePeriod());
    votingPeriodConfiguration.setSimulationName(submissionVo.getSimulationName());
    votingPeriodConfiguration.setTarget(DeploymentTarget.valueOf(submissionVo.getTarget().name()));
    return votingPeriodConfiguration;
  }

  /**
   * Retrieves the status of the voting period of the deployed configuration.
   *
   * @param clientId the operation identifier in the client's system
   *
   * @return an {@link VotingPeriodStatusVo} for the matching operation
   */
  @Transactional(readOnly = true)
  public VotingPeriodStatusVo getVotingPeriodStatusByClientId(String clientId) {
    Operation operation = operationRepository.findByClientId(clientId)
                                             .orElseThrow(() -> new EntityNotFoundException(
                                                 String.format("No operation found for clientId [%s]", clientId)));

    if (!operation.getVotingPeriodConfiguration().isPresent()) {
      throw new EntityNotFoundException(
          String.format("No voting period configuration found for clientId [%s]", clientId));
    }

    return votingPeriodStatusVoMapper.map(operation);
  }
}
