/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration.printer.vo;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.VotingCardType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.Municipality;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@link PrinterConfiguration} value object.
 */
public class PrinterConfigurationVo {
  private final Long                 id;
  private final String               printerName;
  private final String               publicKeyHash;
  private final String               votingCardType;
  private final List<MunicipalityVo> municipalities;
  private final boolean              isSwissAbroadWithoutMunicipality;

  /**
   * Creates a new printer configuration value object based on the given {@link PrinterConfiguration} entity.
   *
   * @param configuration the {@link PrinterConfiguration} entity
   * @param virtual       if true: consider only the virtual municipalities. If false: consider only the normal
   *                      municipalities
   */
  private PrinterConfigurationVo(PrinterConfiguration configuration, boolean virtual,
                                 boolean isSwissAbroadWithoutMunicipality) {
    this.id = configuration.getId();
    this.printerName = configuration.getPrinterName();

    // the following values are hard-coded, because they're not yet implemented on the caller's side
    this.publicKeyHash = "SHA256: 1E0C8B3863B6EBEE56A61DE6A3A3D59123182FD3ECB1ED7C5084108EAB3D9C41";

    this.isSwissAbroadWithoutMunicipality = isSwissAbroadWithoutMunicipality;
    if (virtual) {
      this.municipalities = configuration.getMunicipalities().stream()
                                         .filter(Municipality::isVirtual)
                                         .map(MunicipalityVo::new)
                                         .collect(Collectors.toList());
      this.votingCardType = VotingCardType.VIRTUAL.name();
    } else {
      this.municipalities = configuration.getMunicipalities().stream()
                                         .filter(m -> !m.isVirtual())
                                         .map(MunicipalityVo::new)
                                         .collect(Collectors.toList());
      this.votingCardType = VotingCardType.REAL.name();
    }
  }

  /**
   * Creates a new printer configuration value object, retaining the real municipalities and discarding the virtual
   * municipality.
   *
   * @return a new printer configuration value object, or null if the printer has no real municipalities
   */
  public static PrinterConfigurationVo buildWithRealMunicipalities(PrinterConfiguration printer) {
    boolean hasRealMunicipalities = printer.getMunicipalities().stream().anyMatch(m -> !m.isVirtual());
    return hasRealMunicipalities ? new PrinterConfigurationVo(printer, false, false) : null;
  }

  /**
   * Creates a new printer configuration value object, retaining the virtual municipality and discarding the real
   * municipalities.
   *
   * @return a new printer configuration value object, or null if the printer has no virtual municipality
   */
  public static PrinterConfigurationVo buildWithVirtualMunicipality(PrinterConfiguration printer) {
    boolean hasVirtualMunicipality = printer.getMunicipalities().stream().anyMatch(Municipality::isVirtual);

    return hasVirtualMunicipality ? new PrinterConfigurationVo(printer, true, false) : null;
  }

  /**
   * Creates a new printer configuration value object, without any linked municipality
   *
   * @return a new printer configuration value object
   */
  public static PrinterConfigurationVo buildSwissAbroadWithoutMunicipality(PrinterConfiguration printer) {
    return new PrinterConfigurationVo(printer, false, true);
  }

  public Long getId() {
    return id;
  }

  public String getPrinterName() {
    return printerName;
  }

  public String getPublicKeyHash() {
    return publicKeyHash;
  }

  public String getVotingCardType() {
    return votingCardType;
  }

  public List<MunicipalityVo> getMunicipalities() {
    return municipalities;
  }

  public boolean isSwissAbroadWithoutMunicipality() {
    return isSwissAbroadWithoutMunicipality;
  }
}
