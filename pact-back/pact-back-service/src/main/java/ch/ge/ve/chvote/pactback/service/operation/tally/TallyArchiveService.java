/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.tally;

import ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolTallyArchiveService;
import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.function.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A service for triggering the generation of the tally archive and retrieving its status.
 */
@Service
public class TallyArchiveService {
  private final OperationRepository         operationRepository;
  private final TallyArchiveStatusVoMapper  tallyArchiveStatusVoMapper;
  private final ProtocolTallyArchiveService protocolTallyArchiveService;

  /**
   * Create a new {@link TallyArchiveService}.
   *
   * @param operationRepository         the {@link OperationRepository}.
   * @param tallyArchiveStatusVoMapper  the {@link TallyArchiveStatusVoMapper}.
   * @param protocolTallyArchiveService the {@link ProtocolTallyArchiveService}.
   */
  @Autowired
  public TallyArchiveService(OperationRepository operationRepository,
                             TallyArchiveStatusVoMapper tallyArchiveStatusVoMapper,
                             ProtocolTallyArchiveService protocolTallyArchiveService) {
    this.operationRepository = operationRepository;
    this.tallyArchiveStatusVoMapper = tallyArchiveStatusVoMapper;
    this.protocolTallyArchiveService = protocolTallyArchiveService;
  }

  /**
   * Trigger the tally archive generation process in anticipation for a given operation.
   * <p>
   *   This is possible only for operations whose protocol instance is deployed (ie not in test) with a SIMULATION
   *   target.
   * </p>
   *
   * @param operationClientId the operation identifier in the client's referential
   *
   * @throws GenerationInProgressException      if the generation has already started for this operation
   * @throws CannotPublishTallyArchiveException if the operation is not in SIMULATION mode, or if the protocol
   *                                            is not in a state where the results can be generated.
   * @throws EntityNotFoundException            if the operation does not have a deployed configuration with an
   *                                            initialized protocol instance.
   */
  @Transactional
  public void triggerGenerationManually(String operationClientId) {
    ProtocolInstance instance = getDeployedProtocolInstance(operationClientId);
    checkCanPublishTally(instance,
                         conf -> conf.getTarget() == DeploymentTarget.SIMULATION,
                         "Operation [%s] is not in SIMULATION and cannot be manually closed.");

    protocolTallyArchiveService.startTallying(instance);
  }

  /**
   * Trigger the tally archive generation process for a given operation, at the end of the voting period.
   * <p>
   *   This is possible only if the voting period has actually ended.
   * </p>
   *
   * @param operationClientId the operation identifier in the client's referential
   *
   * @throws GenerationInProgressException      if the generation has already started for this operation
   * @throws CannotPublishTallyArchiveException if the operation's voting period has not ended, or if the protocol
   *                                            is not in a state where the results can be generated.
   * @throws EntityNotFoundException            if the operation does not have a deployed configuration with an
   *                                            initialized protocol instance.
   */
  @Transactional
  public void triggerScheduledGeneration(String operationClientId) {
    ProtocolInstance instance = getDeployedProtocolInstance(operationClientId);
    checkCanPublishTally(instance,
                         conf -> LocalDateTime.now().isAfter(endOfVotingPeriod(conf)),
                         "Voting period of operation [%s] has not ended yet - cannot be closed.");

    protocolTallyArchiveService.startTallying(instance);
  }

  private static LocalDateTime endOfVotingPeriod(VotingPeriodConfiguration configuration) {
    return configuration.getSiteClosingDate().plusMinutes(configuration.getGracePeriod());
  }

  /**
   * Retrieve the status of the tally archive generation process by operation client id.
   *
   * @param operationClientId the operation client id
   *
   * @return the status of the tally archive generation process.
   *
   * @throws EntityNotFoundException if the operation does not have a deployed configuration with an initialized
   *                                 protocol instance.
   */
  @Transactional
  public TallyArchiveStatusVo getTallyArchiveStatus(String operationClientId) {
    ProtocolInstance instance = getDeployedProtocolInstance(operationClientId);
    return tallyArchiveStatusVoMapper.map(instance);
  }

  private ProtocolInstance getDeployedProtocolInstance(String operationClientId) {
    return operationRepository.findByClientId(operationClientId)
                              .flatMap(Operation::getDeployedConfiguration)
                              .flatMap(OperationConfiguration::getProtocolInstance)
                              .orElseThrow(
                                  () -> new EntityNotFoundException(
                                      String.format("Operation [%s] has no deployed protocol instance",
                                                    operationClientId))
                              );
  }

  private void checkCanPublishTally(ProtocolInstance instance, Predicate<VotingPeriodConfiguration> condition,
                                    String messageTemplate) {
    Operation operation = instance.getOperation();
    if (isGenerationInProgress(instance.getStatus())) {
      throw new GenerationInProgressException("Generation already in progress for operation " + operation.getClientId());
    }
    boolean isReadyToPublish = EnumSet.of(
        ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES,
        ProtocolInstanceStatus.TALLY_ARCHIVE_GENERATION_FAILURE
    ).contains(instance.getStatus());

    if (!isReadyToPublish) {
      throw new CannotPublishTallyArchiveException(
          String.format("Operation [%s] is not ready for publishing the tally.", operation.getClientId()));
    }

    VotingPeriodConfiguration votingPeriodConfiguration =
        operation.getVotingPeriodConfiguration().orElseThrow(
            () -> new EntityNotFoundException(
                String.format("Operation [%s] has not yet a voting period configuration.", operation.getClientId())
            )
        );
    if (LocalDateTime.now().isBefore(votingPeriodConfiguration.getSiteOpeningDate())) {
      throw new CannotPublishTallyArchiveException(
          String.format("Operation [%s] is not opened yet", operation.getClientId()));
    }
    if (condition.negate().test(votingPeriodConfiguration)) {
      throw new CannotPublishTallyArchiveException(String.format(messageTemplate, operation.getClientId()));
    }
  }

  private static boolean isGenerationInProgress(ProtocolInstanceStatus status) {
    return EnumSet.of(
        ProtocolInstanceStatus.SHUFFLING, ProtocolInstanceStatus.DECRYPTING, ProtocolInstanceStatus.GENERATING_RESULTS
    ).contains(status);
  }
}
