/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.vo;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.DeadLetterEvent;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import java.time.LocalDateTime;
import java.util.Objects;

public final class DeadLetterEventVo {

  public static DeadLetterEventVoBuilder builder() {
    return new DeadLetterEventVoBuilder();
  }

  public static DeadLetterEventVo fromEntity(DeadLetterEvent entity) {
    return new DeadLetterEventVo(entity.getId(), entity.getProtocolId(), entity.getOriginalChannel(),
                                 entity.getOriginalRoutingKey(), entity.getType(), entity.getExceptionMessage(),
                                 entity.getExceptionStacktrace(), entity.getCreationDate(), entity.getPickupDate(),
                                 entity.getContent(), entity.getContentType());
  }

  private final Long          id;
  private final String        protocolId;
  private final String        originalChannel;
  private final String        originalRoutingKey;
  private final String        type;
  private final String        exceptionMessage;
  private final String        exceptionStacktrace;
  private final LocalDateTime creationDate;
  private final LocalDateTime pickupDate;
  private final String        content;
  private final String        contentType;

  DeadLetterEventVo(Long id, String protocolId, String originalChannel, String originalRoutingKey, String type,
                    String exceptionMessage, String exceptionStacktrace, LocalDateTime creationDate,
                    LocalDateTime pickupDate, String content, String contentType) {
    this.id = id;
    this.protocolId = Preconditions.checkNotNull(protocolId);
    this.originalChannel = Preconditions.checkNotNull(originalChannel);
    this.originalRoutingKey = originalRoutingKey;
    this.type = Preconditions.checkNotNull(type);
    this.exceptionMessage = exceptionMessage;
    this.exceptionStacktrace = exceptionStacktrace;
    this.creationDate = Preconditions.checkNotNull(creationDate);
    this.pickupDate = Preconditions.checkNotNull(pickupDate);
    this.content = Preconditions.checkNotNull(content);
    this.contentType = contentType;
  }

  public Long getId() {
    return id;
  }

  public String getProtocolId() {
    return protocolId;
  }

  public String getOriginalChannel() {
    return originalChannel;
  }

  public String getOriginalRoutingKey() {
    return originalRoutingKey;
  }

  public String getType() {
    return type;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public String getExceptionStacktrace() {
    return exceptionStacktrace;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public LocalDateTime getPickupDate() {
    return pickupDate;
  }

  public String getContent() {
    return content;
  }

  public String getContentType() {
    return contentType;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
                      .add("protocolId", protocolId)
                      .add("originalChannel", originalChannel)
                      .add("originalRoutingKey", originalRoutingKey)
                      .add("type", type)
                      .add("creationDate", creationDate)
                      .toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeadLetterEventVo that = (DeadLetterEventVo) o;
    return Objects.equals(id, that.id)
           && Objects.equals(protocolId, that.protocolId)
           && Objects.equals(originalChannel, that.originalChannel)
           && Objects.equals(originalRoutingKey, that.originalRoutingKey)
           && Objects.equals(type, that.type)
           && Objects.equals(exceptionMessage, that.exceptionMessage)
           && Objects.equals(exceptionStacktrace, that.exceptionStacktrace)
           && Objects.equals(creationDate, that.creationDate)
           && Objects.equals(pickupDate, that.pickupDate)
           && Objects.equals(content, that.content)
           && Objects.equals(contentType, that.contentType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, protocolId, type, creationDate);
  }
}
