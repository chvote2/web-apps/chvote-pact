/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.protocol.client.ProtocolClientFactory;
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolDeletionEvent;
import ch.ge.ve.chvote.pactback.service.protocol.processor.OutputFilesManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * A service to cleanup {@link ProtocolInstance} and keep track of their status.
 */
@Service
public class ProtocolCleanupService {

  private final ProtocolClientFactory protocolClientFactory;

  private final OutputFilesManager outputFilesManager;

  /**
   * Create a new {@link ProtocolInstance} cleanup service.
   *
   * @param protocolClientFactory the {@link ProtocolClientFactory}.
   */
  @Autowired
  public ProtocolCleanupService(ProtocolClientFactory protocolClientFactory,
                                OutputFilesManager outputFilesManager) {
    this.protocolClientFactory = protocolClientFactory;
    this.outputFilesManager = outputFilesManager;
  }

  /**
   * Request cleanup of the given {@link ProtocolInstance}.
   *
   * @param instance the {@link ProtocolInstance} to cleanup.
   */
  @Async
  public void cleanUpProtocol(ProtocolInstance instance) {
    protocolClientFactory.createClient(instance.getProtocolId()).requestCleanup();
  }

  /**
   * Deletes the files of a protocol.
   * <p>
   * This method is guaranteed to be invoked only if the transaction for the deletion of the DB protocol and the kernel
   * protocol has successfully committed. That is, we don't delete files for a protocol whose deletion has rolled back.
   *
   * @param event event which specifies the protocol instance whose files must be deleted
   */
  @TransactionalEventListener
  public void deleteProtocolFiles(ProtocolDeletionEvent event) {
    outputFilesManager.deleteAllFiles(event.getProtocolInstance());
  }

}
