/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action;

import ch.ge.ve.chvote.pactback.repository.action.BasePrivilegedActionRepository;
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.service.action.vo.PrivilegedActionVo;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionNotFoundException;
import java.util.function.Function;

/**
 * A strategy class to retrieve {@link PrivilegedAction} entities as {@link PrivilegedActionVo} to avoid exposing the
 * entity implementation detail in the output.
 *
 * @param <T> A subtype of {@link PrivilegedAction}.
 * @param <E> A subtype of {@link PrivilegedActionVo}.
 */
class PendingActionRetriever<T extends PrivilegedAction, E extends PrivilegedActionVo> {

  private final BasePrivilegedActionRepository<T> repository;
  private final Function<T, E>                    mappingFunction;

  /**
   * Create a new pending action retriever.
   *
   * @param repository      the action repository for actions of type {@link T}.
   * @param mappingFunction the mapping function from {@link T} to {@link E}.
   */
  PendingActionRetriever(BasePrivilegedActionRepository<T> repository,
                         Function<T, E> mappingFunction) {
    this.repository = repository;
    this.mappingFunction = mappingFunction;
  }


  /**
   * Retrieve a {@link PrivilegedActionVo} of type {@link E} by the given id which status is {@link
   * PrivilegedAction.Status#PENDING}.
   *
   * @param id the id of the action.
   *
   * @return the specialised {@link PrivilegedActionVo}.
   *
   * @throws PrivilegedActionNotFoundException         if the action does not exists.
   * @throws PrivilegedActionAlreadyProcessedException if the action exists but its status is not {@link
   *                                                   PrivilegedAction.Status#PENDING}.
   */
  E getPendingPrivilegedActionVo(long id) {
    return mappingFunction.apply(getPendingPrivilegedAction(id));
  }

  /**
   * Retrieve a {@link PrivilegedAction} of type {@link T} by the given id which status is {@link
   * PrivilegedAction.Status#PENDING}.
   *
   * @param id the id of the action.
   *
   * @return the specialised {@link PrivilegedAction}.
   *
   * @throws PrivilegedActionNotFoundException         if the action does not exists.
   * @throws PrivilegedActionAlreadyProcessedException if the action exists but its status is not {@link
   *                                                   PrivilegedAction.Status#PENDING}.
   */
  T getPendingPrivilegedAction(long id) {
    T action = repository.findById(id)
                         .orElseThrow(() -> new PrivilegedActionNotFoundException(
                             String.format("No privileged action found for id %d", id)));

    if (!action.getStatus().canUpdate()) {
      throw new PrivilegedActionAlreadyProcessedException(action.getId());
    }

    return action;
  }
}
