/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation;

import ch.ge.ve.chvote.pact.b2b.client.model.Canton;
import ch.ge.ve.chvote.pact.b2b.client.model.Documentation;
import ch.ge.ve.chvote.pact.b2b.client.model.DocumentationType;
import ch.ge.ve.chvote.pact.b2b.client.model.ElectionGroupBallotConfig;
import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.chvote.pact.b2b.client.model.OperationBaseConfiguration;
import ch.ge.ve.chvote.pact.b2b.client.model.OperationType;
import ch.ge.ve.chvote.pact.b2b.client.model.VotationBallotConfig;
import ch.ge.ve.chvote.pact.b2b.client.model.Voter;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingSiteConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.Attachment;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.BallotDocumentation;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.InformationAttachment;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterEntity;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery;
import ch.ge.ve.interfaces.ech.service.JAXBEchCodecImpl;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A support service that retrieves and maps {@link ProtocolInstance} and {@link Voter} information to their
 * corresponding DTOs.
 */
@Service
public class ProtocolEndpointSupportService {

  private final VoterRepository            voterRepository;
  private final ProtocolInstanceRepository protocolInstanceRepository;
  private final JAXBEchCodecImpl<Delivery> votationCodec;

  /**
   * Creates a new protocol endpoint support service.
   *
   * @param voterRepository            data repository for retrieving voter information.
   * @param protocolInstanceRepository data repository for retrieving protocol instance information.
   */
  @Autowired
  public ProtocolEndpointSupportService(VoterRepository voterRepository,
                                        ProtocolInstanceRepository protocolInstanceRepository) {
    this.voterRepository = voterRepository;
    this.protocolInstanceRepository = protocolInstanceRepository;
    this.votationCodec = new JAXBEchCodecImpl<>(Delivery.class);
  }

  /**
   * Retrieve all the voters associated to a protocol instance by protocol id.
   *
   * @param protocolId the protocol id.
   *
   * @return the list of {@link Voter}s.
   */
  @Transactional(readOnly = true)
  public List<Voter> getVotersByProtocolId(String protocolId) {
    return voterRepository.findByProtocolInstance_ProtocolId(protocolId).map(this::toVoter)
                          .collect(Collectors.toList());
  }

  private Voter toVoter(VoterEntity voterVo) {
    return new Voter(voterVo.getAllowedDoiIds().stream().map(DomainOfInfluence::new).collect(Collectors.toList()),
                     voterVo.getLocalPersonId(),
                     voterVo.getProtocolVoterIndex(),
                     voterVo.getDateOfBirth().getYear(),
                     voterVo.getDateOfBirth().getMonthOfYear(),
                     voterVo.getDateOfBirth().getDayOfMonth(),
                     "TODO", // TODO Not yet implemented
                     new CountingCircle(voterVo.getVoterCountingCircle().getProtocolCountingCircleIndex(),
                                        voterVo.getVoterCountingCircle().getBusinessId(),
                                        voterVo.getVoterCountingCircle().getName())
    );
  }

  /**
   * Retrieve the {@link OperationBaseConfiguration} associated to the protocol instance identified by the given
   * protocol id.
   *
   * @param protocolId the protocol id.
   *
   * @return the {@link OperationBaseConfiguration} associated to the given identifier.
   *
   * @throws EntityNotFoundException If no protocol instance exists for the given identifier.
   * @throws IllegalStateException   If the protocol instance is in production but the {@link
   *                                 VotingMaterialsConfiguration} is not yet available.
   */
  @Transactional(readOnly = true)
  public OperationBaseConfiguration getOperationConfigurationByProtocolId(String protocolId) {
    ProtocolInstance protocolInstance = getProtocolInstanceByProtocolId(protocolId);
    OperationConfiguration operationConfiguration = protocolInstance.getConfiguration();

    return new OperationBaseConfiguration(
        operationConfiguration.getOperationLabel(),
        operationConfiguration.getOperationName(),
        Canton.findByAbbreviation(operationConfiguration.getCanton()),
        protocolInstance.getOperation()
                        .getVotingMaterialsConfiguration()
                        .map(VotingMaterialsConfiguration::getVotingCardLabel)
                        .orElse(null),
        protocolInstance.getOperation()
                        .getVotingMaterialsConfiguration()
                        .map(VotingMaterialsConfiguration::getSimulationName)
                        .orElse(null),
        Lang.FR, // TODO Not yet implemented
        getOperationType(protocolInstance),
        getVotingPeriodOrNull(protocolInstance),
        operationConfiguration.getOperationDate(),
        getAttachmentIds(operationConfiguration.getOperationReferenceFiles(),
                         AttachmentType.OPERATION_REPOSITORY_VOTATION),
        getAttachmentIds(operationConfiguration.getOperationReferenceFiles(),
                         AttachmentType.OPERATION_REPOSITORY_ELECTION)

    );
  }

  private List<Long> getAttachmentIds(List<OperationReferenceFile> operationReferenceFiles, AttachmentType type) {
    return operationReferenceFiles.stream()
                                  .filter(file -> file.getType() == type)
                                  .map(Attachment::getId)
                                  .collect(Collectors.toList());
  }

  private VotingPeriod getVotingPeriodOrNull(ProtocolInstance protocolInstance) {
    if (getOperationType(protocolInstance) == OperationType.TEST) {
      return null;
    }

    return protocolInstance.getOperation()
                           .getVotingPeriodConfiguration()
                           .map(vpc -> new VotingPeriod(vpc.getSiteOpeningDate(),
                                                        vpc.getSiteClosingDate(),
                                                        vpc.getGracePeriod()))
                           .orElse(null);
  }

  private OperationType getOperationType(ProtocolInstance protocolInstance) {
    if (protocolInstance.getEnvironment() == ProtocolEnvironment.TEST) {
      return OperationType.TEST;
    } else {
      return protocolInstance
          .getOperation()
          .getVotingMaterialsConfiguration()
          .map(VotingMaterialsConfiguration::getTarget)
          .map(target -> {
            if (DeploymentTarget.SIMULATION.equals(target)) {
              return OperationType.SIMULATION;
            } else {
              return OperationType.REAL;
            }
          }).orElseThrow(() -> new IllegalStateException("The voting material is not yet available."));
    }
  }

  /**
   * Retrieve the {@link VotingSiteConfiguration} associated to the protocol instance identified by the given protocol
   * id.
   *
   * @param protocolId the protocol id.
   *
   * @return the {@link VotingSiteConfiguration} associated to the given identifier.
   *
   * @throws EntityNotFoundException If no protocol instance exists for the given identifier.
   * @throws IllegalStateException   If the protocol instance is in production but the {@link
   *                                 VotingMaterialsConfiguration} is not yet available.
   */
  @Transactional(readOnly = true)
  public VotingSiteConfiguration getVotingSiteConfigurationByProtocolId(String protocolId) {
    ProtocolInstance protocolInstance = getProtocolInstanceByProtocolId(protocolId);

    return new VotingSiteConfiguration(getDocumentationFromInstance(protocolInstance),
                                       getTranslationsId(protocolInstance),
                                       protocolInstance.getConfiguration().isGroupVotation(),
                                       getVotationBallotConfig(protocolInstance),
                                       getElectionGroupBallotConfig(protocolInstance),
                                       getHighlightedQuestions(protocolInstance));
  }

  private List<Documentation> getHighlightedQuestions(ProtocolInstance protocolInstance) {
    return protocolInstance.getConfiguration()
                           .getHighlightedQuestions()
                           .stream()
                           .map(question ->
                                    new Documentation(DocumentationType.HIGHLIGHTED_QUESTION,
                                                      question.getQuestion(),
                                                      Lang.valueOf(question.getLanguage()),
                                                      question.getId()))
                           .collect(Collectors.toList());
  }

  private List<ElectionGroupBallotConfig> getElectionGroupBallotConfig(ProtocolInstance protocolInstance) {
    return protocolInstance
        .getConfiguration()
        .getElectionSiteConfigurations()
        .stream()
        .map(config ->
                 new ElectionGroupBallotConfig(
                     getBallotDocumentationConfig(config.getBallot(), protocolInstance
                         .getConfiguration()
                         .getBallotDocumentations()),
                     config.isDisplayCandidateSearchForm(),
                     config.isAllowChangeOfElectoralList(),
                     config.isDisplayEmptyPosition(),
                     config.isDisplayCandidatePositionOnACompactBallotPaper(),
                     config.isDisplayCandidatePositionOnAModifiedBallotPaper(),
                     config.isDisplaySuffrageCount(),
                     config.isDisplayListVerificationCode(),
                     config.getCandidateInformationDisplayModel(),
                     config.isAllowOpenCandidature(),
                     config.isAllowMultipleMandates(),
                     config.isDisplayVoidOnEmptyBallotPaper(),
                     config.getDisplayedColumnsOnVerificationTable().toArray(new String[0]),
                     config.getColumnsOrderOnVerificationTable().toArray(new String[0])
                 )
        ).collect(Collectors.toList());
  }


  private List<VotationBallotConfig> getVotationBallotConfig(ProtocolInstance protocolInstance) {
    return protocolInstance
        .getConfiguration().getOperationReferenceFiles()
        .stream()
        .filter(file -> file.getType() == AttachmentType.OPERATION_REPOSITORY_VOTATION)
        .flatMap(file -> votationCodec
            .deserialize(new ByteArrayInputStream(file.getFile())).getInitialDelivery().getVoteInformation()
            .stream().map(voteInformation -> voteInformation.getVote().getVoteIdentification())
        )
        .map(ballotId -> new VotationBallotConfig(
            ballotId,
            getBallotDocumentationConfig(
                ballotId, protocolInstance.getConfiguration().getBallotDocumentations())))
        .collect(Collectors.toList());
  }

  private List<Documentation> getBallotDocumentationConfig(
      String ballotId, List<BallotDocumentation> ballotDocumentations) {
    return ballotDocumentations
        .stream()
        .filter(bd -> bd.getBallot().equals(ballotId))
        .map(bd -> new Documentation(DocumentationType.BALLOT_DOCUMENTATION, bd.getLabel(), Lang.valueOf(
            bd.getLanguage()), bd.getId()))
        .collect(Collectors.toList());
  }

  private Long getTranslationsId(ProtocolInstance protocolInstance) {
    return protocolInstance
        .getConfiguration().getInformationAttachments().stream()
        .filter(file -> file.getType() == AttachmentType.OPERATION_TRANSLATION_FILE)
        .findFirst()
        .map(Attachment::getId)
        .orElseThrow(() -> new IllegalStateException("Technically impossible since required from upload"));
  }


  private Set<Documentation> getDocumentationFromInstance(ProtocolInstance protocolInstance) {
    return getInformationAttachmentsFileStream(protocolInstance,
                                               AttachmentType.OPERATION_TERMS_OF_USAGE,
                                               AttachmentType.OPERATION_FAQ,
                                               AttachmentType.OPERATION_CERTIFICATE_VERIFICATION_FILE)
        .map(attachment -> new Documentation(convertToDocumentationType(attachment.getType()), attachment.getName(),
                                             Lang.valueOf(attachment.getLanguage()), attachment.getId()))
        .collect(Collectors.toSet());
  }


  private Stream<InformationAttachment> getInformationAttachmentsFileStream(ProtocolInstance protocolInstance,
                                                                            AttachmentType... attachmentTypes) {
    return protocolInstance.getConfiguration()
                           .getInformationAttachments()
                           .stream()
                           .filter(file -> Arrays.asList(attachmentTypes).contains(file.getType()));
  }

  private DocumentationType convertToDocumentationType(AttachmentType type) {
    switch (type) {
      case OPERATION_FAQ:
        return DocumentationType.FAQ;
      case OPERATION_CERTIFICATE_VERIFICATION_FILE:
        return DocumentationType.CERTIFICATES;
      case OPERATION_TERMS_OF_USAGE:
        return DocumentationType.TERMS;
      default:
        throw new IllegalStateException("Technically impossible since previously filtered");
    }
  }

  private ProtocolInstance getProtocolInstanceByProtocolId(String protocolId) {
    return protocolInstanceRepository
        .getByProtocolId(protocolId)
        .orElseThrow(
            () -> new EntityNotFoundException("Could not find protocol instance with protocol id: " + protocolId)
        );
  }


  /**
   * find attachment by attachment id for a given protocol id.
   *
   * @param protocolId   protocol identifier
   * @param attachmentId attachment identifier
   *
   * @return the attachment
   */
  public Attachment getAttachmentByProtocolIdAndAttachmentId(String protocolId, Long attachmentId) {
    ProtocolInstance protocol = getProtocolInstanceByProtocolId(protocolId);
    List<Attachment> attachments = new LinkedList<>();

    protocol.getConfiguration()
            .getOperationReferenceFiles()
            .stream()
            .filter(f -> f.getType() != AttachmentType.OPERATION_REGISTER)
            .collect(Collectors.toCollection(() -> attachments));

    attachments.addAll(protocol.getConfiguration().getInformationAttachments());
    attachments.addAll(protocol.getConfiguration().getHighlightedQuestions());
    attachments.addAll(protocol.getConfiguration().getBallotDocumentations());

    protocol.getOperation()
            .getVotingPeriodConfiguration()
            .map(VotingPeriodConfiguration::getElectoralAuthorityKey)
            .ifPresent(attachments::add);

    return attachments.stream()
                      .filter(attachment -> attachmentId.equals(attachment.getId()))
                      .findFirst()
                      .orElseThrow(() -> new EntityNotFoundException(
                          String.format("Could not find attachment for protocol instance :%s and attachment id %d",
                                        protocolId, attachmentId)));
  }

}
