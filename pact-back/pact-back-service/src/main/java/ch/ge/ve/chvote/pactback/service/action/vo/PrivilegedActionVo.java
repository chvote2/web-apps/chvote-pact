/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action.vo;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import java.time.LocalDateTime;

/**
 * A {@link PrivilegedAction} value object.
 */
public class PrivilegedActionVo {

  private final Long          id;
  private final String        type;
  private final String        operationName;
  private final LocalDateTime operationDate;
  private final LocalDateTime creationDate;
  private final LocalDateTime statusDate;
  private final String        ownerName;
  private final String        status;
  private final String        rejectionReason;

  /**
   * Creates a new privileged action value object based on the given {@link PrivilegedAction} entity.
   *
   * @param action the {@link PrivilegedAction} entity.
   */
  public PrivilegedActionVo(PrivilegedAction action) {
    this.id = action.getId();
    this.type = action.getType();
    this.operationName = action.getOperationName();
    this.operationDate = action.getOperationDate();
    this.creationDate = action.getCreationDate();
    this.statusDate = action.getStatusDate();
    this.ownerName = action.getRequester().getUsername();
    this.status = action.getStatus().name();
    this.rejectionReason = action.getRejectionReason();
  }

  /**
   * Creates a dummy privileged action value object that is not represented in the database. An instance created with
   * this constructor will always return "NOT_CREATED" when calling {@link #getType()}.
   *
   * @param type          The type of this dummy action.
   * @param operationName The operation name of this dummy action.
   * @param operationDate The operation date fo this dummy action.
   * @param ownerName     The owner name of this dummy action.
   * @param statusDate    The status date of this dummy action.
   */
  public PrivilegedActionVo(String type, String operationName, LocalDateTime operationDate, String ownerName,
                            LocalDateTime statusDate) {
    this.id = null;
    this.operationName = operationName;
    this.operationDate = operationDate;
    this.type = type;
    this.creationDate = null;
    this.statusDate = statusDate;
    this.ownerName = ownerName;
    this.status = "NOT_CREATED";
    this.rejectionReason = null;
  }

  public Long getId() {
    return id;
  }

  public String getType() {
    return type;
  }

  public String getOperationName() {
    return operationName;
  }

  public LocalDateTime getOperationDate() {
    return operationDate;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public LocalDateTime getStatusDate() {
    return statusDate;
  }

  public String getOwnerName() {
    return ownerName;
  }

  public String getStatus() {
    return status;
  }

  public String getRejectionReason() {
    return rejectionReason;
  }

}
