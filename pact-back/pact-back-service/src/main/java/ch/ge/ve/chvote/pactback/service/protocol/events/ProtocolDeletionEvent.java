/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.events;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import java.util.Objects;

/**
 * Event used in Spring event listener interaction.
 * This event is raised to notify that a protocol (DB instance and Protocol instance) has been deleted.
 * <p>
 *
 * @see org.springframework.context.ApplicationEventPublisher Spring's ApplicationEventPublisher
 * @see ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService ProtocolInstanceService
 */
// Note : originally inspired by : http://blog.codeleak.pl/2017/10/asynchrouns-and-transactional-event.html
public final class ProtocolDeletionEvent {

  private ProtocolInstance protocolInstance;

  public ProtocolDeletionEvent(ProtocolInstance protocolInstance) {
    this.protocolInstance = protocolInstance;
  }

  public ProtocolInstance getProtocolInstance() {
    return protocolInstance;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProtocolDeletionEvent that = (ProtocolDeletionEvent) o;
    return Objects.equals(protocolInstance, that.protocolInstance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(protocolInstance);
  }
}
