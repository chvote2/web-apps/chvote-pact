/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.chvote.pactback.contract.operation.status.DeployedConfigurationStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.InTestConfigurationStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.InTestConfigurationStatusVo.State;
import ch.ge.ve.chvote.pactback.contract.operation.status.OperationConfigurationStatusVo;
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties;
import ch.ge.ve.chvote.pactback.service.operation.materials.ProtocolStageProgressVoMapper;
import ch.ge.ve.chvote.pactback.service.protocol.processor.OutputFilesManager;
import ch.ge.ve.chvote.pactback.service.protocol.processor.PrinterArchiveGenerator;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * A utility class for mapping {@link Operation} to {@link OperationConfigurationStatusVo}.
 */
@Service
public class OperationConfigurationStatusVoMapper {
  private static final Logger logger = LoggerFactory.getLogger(OperationConfigurationStatusVoMapper.class);
  
  private static final String UTF_8_NAME  = Charset.forName("UTF-8").name();
  private static final String REAL_TARGET = StringUtils.EMPTY;
  private static final String TEST_TARGET = "test/";

  private final OutputFilesManager      outputFilesManager;
  private final PrinterArchiveGenerator printerArchiveGenerator;
  private final String                  appContextUrl;
  private final String                  vrIdentificationUrl;

  /**
   * Creates a new operation configuration status mapper.
   *
   * @param outputFilesManager      the {@link OutputFilesManager}
   * @param printerArchiveGenerator the {@link PrinterArchiveGenerator}
   * @param configurationProperties the {@link ChVoteConfigurationProperties}.
   */
  @Autowired
  public OperationConfigurationStatusVoMapper(OutputFilesManager outputFilesManager,
                                              PrinterArchiveGenerator printerArchiveGenerator,
                                              ChVoteConfigurationProperties configurationProperties) {
    this.outputFilesManager = outputFilesManager;
    this.printerArchiveGenerator = printerArchiveGenerator;
    this.appContextUrl = configurationProperties.getPactContextUrl();
    this.vrIdentificationUrl = configurationProperties.getVrIdentificationUrl();
  }

  private static State getStateValidated(OperationConfiguration configuration) {
    return configuration.getAction()
                        .map(OperationConfigurationStatusVoMapper::mapActionToState)
                        .orElse(State.VALIDATED);
  }

  private static State mapActionToState(PrivilegedAction action) {
    State state;
    switch (action.getStatus()) {
      case PENDING:
        state = State.DEPLOYMENT_REQUESTED;
        break;
      case REJECTED:
        state = State.DEPLOYMENT_REFUSED;
        break;
      case APPROVED:
        state = State.DEPLOYED;
        break;
      default:
        throw new IllegalStateException("The state of the configuration deployment is unknown");
    }
    return state;
  }

  private static State getStateInValidation(OperationConfiguration configuration) {
    State state;
    Optional<ProtocolInstance> protocolInstance = configuration.getProtocolInstance();
    if (!protocolInstance.isPresent()) {
      state = State.TEST_SITE_IN_DEPLOYMENT;
    } else {
      switch (protocolInstance.get().getStatus()) {
        case INITIALIZING:
        case PUBLIC_PARAMETERS_INITIALIZED:
        case GENERATING_KEYS:
        case KEYS_GENERATED:
        case GENERATING_CREDENTIALS:
          state = State.TEST_SITE_IN_DEPLOYMENT;
          break;
        case CREDENTIALS_GENERATION_FAILURE:
          state = State.IN_ERROR;
          break;
        default:
          state = State.IN_VALIDATION;
      }
    }
    return state;
  }

  private static String deploymentPageUrl(OperationConfiguration configuration, final String appContextUrl) {
    return String.format("%s/configuration-deployments/%s", appContextUrl, configuration.getId());
  }

  /**
   * Creates a new {@link OperationConfigurationStatusVo} for the given {@link Operation}.
   * <p>
   * <p>May not be invoked outside a parent transaction.</p>
   *
   * @param operation the {@link Operation} to map.
   *
   * @return the mapped {@link OperationConfigurationStatusVo}.
   */
  @Transactional(propagation = Propagation.REQUIRED)
  public OperationConfigurationStatusVo map(Operation operation) {
    return new OperationConfigurationStatusVo(
        operation.getConfigurationInTest()
                 .map(configuration -> mapInTestInstance(configuration, appContextUrl))
                 .orElse(null),


        mapDeployedInstance(operation)
    );
  }

  private InTestConfigurationStatusVo mapInTestInstance(OperationConfiguration configuration, String appContextUrl) {
    final PrinterConfiguration printerConfiguration = configuration.getTestPrinter();

    return new InTestConfigurationStatusVo(
        userForLastChange(configuration),
        dateForLastChange(configuration),
        mapStateInTest(configuration),
        configuration.getAction().map(PrivilegedAction::getRejectionReason).orElse(null),
        deploymentPageUrl(configuration, appContextUrl),
        configuration.getProtocolInstance()
                     .map(pi -> getVotingCardLocation(pi, printerConfiguration)).orElse(null),
        configuration.getProtocolInstance().map(ProtocolStageProgressVoMapper::mapProgress).orElse(null),
        computeVrIdentificationUrl(configuration, TEST_TARGET)
    );

  }

  private String computeVrIdentificationUrl(OperationConfiguration configuration, String target) {

    if (StringUtils.isBlank(vrIdentificationUrl) || !configuration.getProtocolInstance().isPresent()) {
      return StringUtils.EMPTY;
    }

    try {
      return MessageFormat.format(vrIdentificationUrl,
                                  target,
                                  configuration.getCanton().toLowerCase(),
                                  URLEncoder.encode(configuration.fetchProtocolInstance().getProtocolId(), UTF_8_NAME),
                                  Lang.FR.name().toLowerCase());
    } catch (UnsupportedEncodingException e) {
      logger.error("Error encoding protocolInstanceId path, ", e);
      return StringUtils.EMPTY;
    }
  }


  private String getVotingCardLocation(ProtocolInstance instance, PrinterConfiguration printerConfiguration) {
    if (instance.getPrinterArchiveCreationDate() == null) {
      return null;
    }
    final Path outputPath = outputFilesManager.getOutputPath(instance);
    Path archivePath = printerArchiveGenerator.getArchivePath(
        instance.getConfiguration().getOperationLabel(),
        outputPath,
        printerConfiguration.getBusinessIdentifier(),
        instance.getPrinterArchiveCreationDate()
    );
    return outputFilesManager.relativize(archivePath).toString();
  }

  private DeployedConfigurationStatusVo mapDeployedInstance(Operation operation) {
    return operation.getDeployedConfiguration()
                    .map(configuration ->
                             new DeployedConfigurationStatusVo(
                                 userForLastChange(configuration),
                                 dateForLastChange(configuration),
                                 computeVrIdentificationUrl(
                                     configuration,
                                     operation.getVotingMaterialsConfiguration()
                                              .map(VotingMaterialsConfiguration::getTarget)
                                              .filter(t -> t != DeploymentTarget.SIMULATION)
                                              .map(t -> REAL_TARGET)
                                              .orElse(TEST_TARGET)
                                 )
                             )
                    ).orElse(null);
  }

  private LocalDateTime dateForLastChange(OperationConfiguration configuration) {
    return configuration.getAction()
                        .map(PrivilegedAction::getStatusDate)
                        .orElse(configuration.getLastChangeDate());
  }

  private String userForLastChange(OperationConfiguration configuration) {
    return configuration.getAction()
                        .map(action ->
                                 Optional.ofNullable(action.getValidator())
                                         .orElse(action.getRequester()).getUsername()
                        )
                        .orElse(configuration.getLastChangeUser());
  }

  private State mapStateInTest(OperationConfiguration configuration) {
    State state;
    switch (configuration.getValidationStatus()) {
      case NOT_YET_VALIDATED:
        state = getStateInValidation(configuration);
        break;
      case INVALIDATED:
        state = State.INVALIDATED;
        break;
      case VALIDATED:
        state = getStateValidated(configuration);
        break;
      default:
        throw new IllegalStateException("The state of the configuration is unknown");
    }

    return state;
  }

}
