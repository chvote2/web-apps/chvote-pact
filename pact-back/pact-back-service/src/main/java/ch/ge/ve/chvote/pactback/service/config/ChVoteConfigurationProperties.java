/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "chvote")
public class ChVoteConfigurationProperties {

  private String timezone;
  private String pactContextUrl;
  private int    batchSize;
  private String vrIdentificationUrl;

  private final ProtocolCore protocolCore = new ProtocolCore();

  public String getTimezone() {
    return timezone;
  }

  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }

  public String getPactContextUrl() {
    return pactContextUrl;
  }

  public void setPactContextUrl(String pactContextUrl) {
    this.pactContextUrl = pactContextUrl;
  }

  public String getVrIdentificationUrl() {
    return vrIdentificationUrl;
  }

  public void setVrIdentificationUrl(String vrIdentificationUrl) {
    this.vrIdentificationUrl = vrIdentificationUrl;
  }

  public int getBatchSize() {
    return batchSize;
  }

  public void setBatchSize(int batchSize) {
    this.batchSize = batchSize;
  }

  public ProtocolCore getProtocolCore() {
    return protocolCore;
  }

  public static class ProtocolCore {
    private int controlComponentsCount;
    private int votingMaterialsTimeoutMinutes;
    private int votingPeriodTimeoutMinutes;

    public int getControlComponentsCount() {
      return controlComponentsCount;
    }

    public void setControlComponentsCount(int controlComponentsCount) {
      this.controlComponentsCount = controlComponentsCount;
    }

    public int getVotingMaterialsTimeoutMinutes() {
      return votingMaterialsTimeoutMinutes;
    }

    public void setVotingMaterialsTimeoutMinutes(int votingMaterialsTimeoutMinutes) {
      this.votingMaterialsTimeoutMinutes = votingMaterialsTimeoutMinutes;
    }

    public int getVotingPeriodTimeoutMinutes() {
      return votingPeriodTimeoutMinutes;
    }

    public void setVotingPeriodTimeoutMinutes(int votingPeriodTimeoutMinutes) {
      this.votingPeriodTimeoutMinutes = votingPeriodTimeoutMinutes;
    }

  }
}