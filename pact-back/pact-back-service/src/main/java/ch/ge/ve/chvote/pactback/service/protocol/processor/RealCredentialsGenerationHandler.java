/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.contract.printerarchive.PrinterOperationConfigurationVo;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.service.exception.CannotDeployOperationConfigurationException;
import ch.ge.ve.chvote.pactback.service.operation.voter.VoterBatchService;
import ch.ge.ve.model.convert.api.ElectionConverter;
import ch.ge.ve.model.convert.api.VotationConverter;
import ch.ge.ve.model.convert.api.VoterConverter;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A protocol processor handler for the generation of real credentials.
 */
@Component
public class RealCredentialsGenerationHandler extends CredentialsGenerationHandler {

  private final VoterBatchService           voterBatchService;
  private final RegistersLoader             registersLoader;
  private final PrinterConfigurationsLoader printerConfigurationsLoader;

  /**
   * Creates a new credentials handler for a production protocol instance.
   *
   * @param voterBatchService             the {@link VoterBatchService}.
   * @param votationConverter             the {@link VotationConverter}.
   * @param electionConverter             the {@link ElectionConverter}.
   * @param voterConverter                the {@link VoterConverter}.
   * @param registersLoader               a service to lazily retrieve the registers.
   * @param printerConfigurationsLoader   a service to lazily retrieve the printer configurations.
   * @param operationReferenceFilesLoader the {@link OperationReferenceFilesLoader}.
   * @param outputFilesManager            the {@link OutputFilesManager}.
   * @param printerArchiveGenerator       the {@link PrinterArchiveGenerator}
   */
  @Autowired
  public RealCredentialsGenerationHandler(VoterBatchService voterBatchService,
                                          VotationConverter votationConverter,
                                          ElectionConverter electionConverter,
                                          VoterConverter voterConverter,
                                          RegistersLoader registersLoader,
                                          PrinterConfigurationsLoader printerConfigurationsLoader,
                                          OperationReferenceFilesLoader operationReferenceFilesLoader,
                                          OutputFilesManager outputFilesManager,
                                          PrinterArchiveGenerator printerArchiveGenerator) {
    super(votationConverter, electionConverter, operationReferenceFilesLoader, outputFilesManager, voterConverter,
          printerArchiveGenerator);
    this.voterBatchService = voterBatchService;
    this.registersLoader = registersLoader;
    this.printerConfigurationsLoader = printerConfigurationsLoader;
  }

  @Override
  protected VotersCreationHelper createHelper(ProtocolState state, VoterConverter voterConverter) {
    final VotingMaterialsConfiguration votingMaterialsConfiguration = loadVotingMaterialsConfiguration(state);

    return new VotersCreationHelper(
        voterBatchService,
        voterConverter,
        state.getInstance(),
        printerConfigurationsLoader.generateMunicipalityToPrintersMap(votingMaterialsConfiguration.getId()),
        printerConfigurationsLoader.generateCantonToPrintersMap(
            votingMaterialsConfiguration.getId(), state.getConfiguration().getCanton()),
        () -> registersLoader.loadRegistersAsInputStreams(votingMaterialsConfiguration.getId()));
  }

  @Override
  protected List<PrintingAuthorityWithPublicKey> getPrintingAuthorities(ProtocolState state) {
    final VotingMaterialsConfiguration votingMaterialsConfiguration = loadVotingMaterialsConfiguration(state);

    return printerConfigurationsLoader.createPrintingAuthority(votingMaterialsConfiguration.getId());
  }

  @Override
  protected PrinterOperationConfigurationVo toPrinterOperationConfigurationVo(ProtocolState state) {
    VotingMaterialsConfiguration votingMaterialsConfiguration = loadVotingMaterialsConfiguration(state);

    return new PrinterOperationConfigurationVo(
        state.getConfiguration().getOperationName(),
        votingMaterialsConfiguration.getVotingCardLabel()
    );
  }

  private VotingMaterialsConfiguration loadVotingMaterialsConfiguration(ProtocolState state) {
    return state.withInstanceInTransaction(instance -> {
      Operation operation = instance.getOperation();
      return operation.getVotingMaterialsConfiguration()
                      .orElseThrow(() -> new CannotDeployOperationConfigurationException(
                          "No voting materials configuration defined yet"));
    });
  }
}
