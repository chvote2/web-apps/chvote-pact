/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration.attachment;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.Attachment;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile;
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties;
import ch.ge.ve.chvote.pactback.service.exception.AttachmentParsingException;
import ch.ge.ve.chvote.pactback.service.operation.TimeService;
import com.google.common.hash.Hashing;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * A service to interact and generate {@link Attachment}s.
 */
public class AttachmentService {

  protected static final String DELIVERY_ROOT             = "(delivery|voterDelivery)";
  private static final   String MESSAGE_ID_XPATH          = DELIVERY_ROOT + "/deliveryHeader/messageId";
  private static final   String MESSAGE_DATE_XPATH        = DELIVERY_ROOT + "/deliveryHeader/messageDate";
  private static final   String APP_MANUFACTURER_XPATH    =
      DELIVERY_ROOT + "/deliveryHeader/sendingApplication/manufacturer";
  private static final   String APP_PRODUCT_XPATH         =
      DELIVERY_ROOT + "/deliveryHeader/sendingApplication/product";
  private static final   String APP_PRODUCT_VERSION_XPATH =
      DELIVERY_ROOT + "/deliveryHeader/sendingApplication/productVersion";

  private final TimeService timeService;

  private final DocumentBuilderFactory documentBuilderFactory;

  private final XPathFactory xpathFactory;
  private final ZoneId       zoneId;

  /**
   * Create a new attachment service.
   *
   * @param timeService                   the date-time service.
   * @param chVoteConfigurationProperties the global configuration properties of the application.
   */
  public AttachmentService(TimeService timeService, ChVoteConfigurationProperties chVoteConfigurationProperties) {
    this.timeService = timeService;
    this.documentBuilderFactory = DocumentBuilderFactory.newInstance();
    this.xpathFactory = XPathFactory.newInstance();
    this.zoneId = ZoneId.of(chVoteConfigurationProperties.getTimezone());
  }

  private String calculateHash(byte[] b) {
    return Hashing.sha256().newHasher().putBytes(b).hash().toString();
  }

  protected <T extends Attachment> T populate(T attachment, AttachmentType type, String name,
                                              LocalDateTime importDate, byte[] file, Long externalIdentifier) {
    attachment.setFileHash(calculateHash(file));
    attachment.setFile(file);
    attachment.setName(name);
    attachment.setImportDate(importDate);
    attachment.setType(type);
    attachment.setExternalIdentifier(externalIdentifier);
    return attachment;
  }


  protected Document parse(String entryName, byte[] file) throws IOException {
    try {
      return documentBuilderFactory.newDocumentBuilder().parse(new ByteArrayInputStream(file));
    } catch (SAXException | ParserConfigurationException ex) {
      throw new AttachmentParsingException(
          String.format("An error occurred while parsing attachment at %s", entryName), ex);
    }
  }

  protected OperationReferenceFile populateInFileAttributes(OperationReferenceFile attachment,
                                                            String entryName,
                                                            Document document) {
    try {
      XPath xpath = xpathFactory.newXPath();

      attachment.setMessageId(xpath.compile(MESSAGE_ID_XPATH).evaluate(document));
      attachment.setSourceApplicationManufacturer(xpath.compile(APP_MANUFACTURER_XPATH).evaluate(document));
      attachment.setSourceApplicationProduct(xpath.compile(APP_PRODUCT_XPATH).evaluate(document));
      attachment.setSourceApplicationProductVersion(xpath.compile(APP_PRODUCT_VERSION_XPATH).evaluate(document));
      String messageDateIsoString = xpath.compile(MESSAGE_DATE_XPATH).evaluate(document);
      LocalDateTime generationDate =
          LocalDateTime.ofInstant(DatatypeConverter.parseDateTime(messageDateIsoString).toInstant(), zoneId);
      attachment.setGenerationDate(generationDate);

    } catch (XPathExpressionException ex) {
      throw new AttachmentParsingException(
          String.format("An error occurred while parsing attachment at %s", entryName), ex);
    }

    attachment.setSignatureExpiryDate(LocalDateTime.now().plusYears(1));
    attachment.setSignatureAuthor("test@test.ch");
    attachment.setSignatureStatus(OperationReferenceFile.SignatureStatus.INVALID_SIGNATURE);

    return attachment;
  }

  protected XPathFactory getXpathFactory() {
    return xpathFactory;
  }

  protected TimeService getTimeService() {
    return timeService;
  }

}
