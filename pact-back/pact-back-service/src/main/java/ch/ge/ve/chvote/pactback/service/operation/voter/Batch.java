/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.voter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Instances of this class hold elements in a buffer until its size limit is reached or a flush is explicitly requested.
 * Should be thread safe.
 *
 * @param <T> the type of elements in this batch.
 */
public final class Batch<T> {

  private final int               batchSize;
  private final Consumer<List<T>> onFlush;
  private final List<T>           buffer;
  private final Object            lock;

  Batch(int batchSize, Consumer<List<T>> onFlush) {
    this.onFlush = onFlush;
    this.batchSize = batchSize;
    this.buffer = new ArrayList<>(batchSize);
    this.lock = new Object();
  }

  /**
   * Adds the given element to the batch. If, after adding the given element, the batch size is reached, the batch
   * is flushed.
   *
   * @param t the element to add.
   *
   * @throws NullPointerException if {@code t} is {@code null}.
   */
  public void add(T t) {
    synchronized (lock) {
      buffer.add(Objects.requireNonNull(t));
      if (buffer.size() >= batchSize) {
        flush();
      }
    }
  }

  /**
   * Flushes the batch's internal buffer content and empties it.
   */
  public void flush() {
    synchronized (lock) {
      onFlush.accept(buffer);
      buffer.clear();
    }
  }
}
