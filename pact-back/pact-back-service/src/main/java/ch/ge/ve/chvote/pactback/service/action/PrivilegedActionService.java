/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action;


import ch.ge.ve.chvote.pactback.repository.action.PrivilegedActionRepository;
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.user.UserRepository;
import ch.ge.ve.chvote.pactback.service.action.vo.PrivilegedActionVo;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A specialised {@link BaseActionService} for interacting with {@link PrivilegedActionVo}s regardless of the
 * implementation.
 */
@Service
public class PrivilegedActionService extends BaseActionService<PrivilegedActionVo> {

  private final PrivilegedActionRepository                                   repository;
  private final PendingActionRetriever<PrivilegedAction, PrivilegedActionVo> pendingActionRetriever;

  /**
   * Create a new {@link ConfigurationDeploymentService} with the given repository.
   *
   * @param repository the {@link PrivilegedAction} repository.
   */
  @Autowired
  public PrivilegedActionService(UserRepository userRepository,
                                 PrivilegedActionRepository repository) {
    super(userRepository);
    this.repository = repository;
    this.pendingActionRetriever = new PendingActionRetriever<>(repository, PrivilegedActionVo::new);
  }

  /**
   * Retrieve the number of actions that the given user can approve.
   *
   * @param username the user's name.
   *
   * @return the number of actions for the given condition.
   */
  @Transactional(readOnly = true)
  public int actionsPendingApprovalCount(String username) {
    return repository.countByRequesterIdNotAndStatus(getUserByName(username).getId(), PrivilegedAction.Status.PENDING);
  }

  /**
   * Retrieve all {@link PrivilegedActionVo} which status is {@link PrivilegedAction.Status#PENDING}.
   *
   * @return a list of {@link PrivilegedActionVo}.
   */
  @Transactional(readOnly = true)
  public List<PrivilegedActionVo> getPendingPrivilegedActions() {
    return repository.findByStatus(PrivilegedAction.Status.PENDING)
                     .map(PrivilegedActionVo::new)
                     .collect(Collectors.toList());
  }

  @Override
  public PrivilegedActionVo getPendingPrivilegedAction(long id) {
    return pendingActionRetriever.getPendingPrivilegedActionVo(id);
  }
}
