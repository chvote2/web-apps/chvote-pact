/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.voter.vo;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Objects;

/**
 * A voter value object.
 */
public final class VoterVo {

  private final Long                  id;
  private final int                   protocolVoterIndex;
  private final String                registerPersonId;
  private final DateOfBirthVo         dateOfBirth;
  private final VoterCountingCircleVo countingCircle;
  private final String                printingAuthorityName;
  private final List<String>          allowedDoiIds;

  public VoterVo(int protocolVoterIndex, String registerPersonId, DateOfBirthVo dateOfBirth,
                 VoterCountingCircleVo countingCircle, String printingAuthorityName, List<String> allowedDoiIds) {
    this(null, protocolVoterIndex, registerPersonId, dateOfBirth,
         countingCircle, printingAuthorityName, allowedDoiIds);
  }

  public VoterVo(Long id, int protocolVoterIndex, String registerPersonId, DateOfBirthVo dateOfBirth,
                 VoterCountingCircleVo countingCircle, String printingAuthorityName, List<String> allowedDoiIds) {
    this.id = id;
    this.protocolVoterIndex = protocolVoterIndex;
    this.registerPersonId = Objects.requireNonNull(registerPersonId);
    this.dateOfBirth = Objects.requireNonNull(dateOfBirth);
    this.countingCircle = Objects.requireNonNull(countingCircle);
    this.printingAuthorityName = Objects.requireNonNull(printingAuthorityName);
    this.allowedDoiIds = ImmutableList.copyOf(allowedDoiIds);
  }

  public Long getId() {
    return id;
  }

  public int getProtocolVoterIndex() {
    return protocolVoterIndex;
  }

  public String getRegisterPersonId() {
    return registerPersonId;
  }

  public DateOfBirthVo getDateOfBirth() {
    return dateOfBirth;
  }

  public VoterCountingCircleVo getCountingCircle() {
    return countingCircle;
  }

  public String getPrintingAuthorityName() {
    return printingAuthorityName;
  }

  public List<String> getAllowedDoiIds() {
    return allowedDoiIds;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    // Strictly speaking this class represents an entity, not a value...
    // Two objects are thus considered equal if and only if they have the same id.
    return Objects.equals(id, ((VoterVo) o).id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
