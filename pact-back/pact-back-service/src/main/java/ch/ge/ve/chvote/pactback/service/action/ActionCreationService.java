/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.user.UserRepository;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import ch.ge.ve.chvote.pactback.service.action.vo.PrivilegedActionVo;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyExistsException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionNotFoundException;
import ch.ge.ve.chvote.pactback.service.exception.SelfActionResolutionException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * A base service with common functionality for retrieving and modifying {@link PrivilegedAction} entities that have an
 * associated business object by exposing them as {@link PrivilegedActionVo}s.
 *
 * @param <T> A subtype of {@link PrivilegedActionVo}.
 */
public abstract class ActionCreationService<T extends PrivilegedActionVo> extends BaseActionService<T> {

  protected ActionCreationService(UserRepository userRepository) {
    super(userRepository);
  }

  /**
   * Populate common fields of a new {@link PrivilegedAction} for the given parameters.
   *
   * @param action        the action to populate.
   * @param requesterName the user that has requested the creation of the action.
   * @param operationName the name of the operation associated to this action.
   * @param operationDate the date of the operation associated to this action.
   * @param <E>           the type of the {@link PrivilegedAction} to populate.
   *
   * @return the populated {@link PrivilegedAction}.
   */
  protected <E extends PrivilegedAction> E populateNewAction(E action, String requesterName, String operationName,
                                                             LocalDateTime operationDate) {
    User requester = getUserByName(requesterName);

    action.setRequester(requester);
    action.setOperationName(operationName);
    action.setOperationDate(operationDate);
    action.setStatus(PrivilegedAction.Status.PENDING);
    action.setStatusDate(LocalDateTime.now());
    action.setCreationDate(LocalDateTime.now());

    return action;
  }

  protected <E extends PrivilegedAction> E updatePrivilegedAction(E action, User validator,
                                                                  PrivilegedAction.Status status) {
    return this.updatePrivilegedAction(action, validator, status, null);
  }

  protected <E extends PrivilegedAction> E updatePrivilegedAction(E action, User validator,
                                                                  PrivilegedAction.Status status,
                                                                  String rejectReason) {

    if (action.getRequester().equals(validator)) {
      throw new SelfActionResolutionException("Users may not approve their own privileged actions");
    }

    action.setStatus(status);
    action.setRejectionReason(rejectReason);
    action.setValidator(validator);
    action.setStatusDate(LocalDateTime.now());

    return action;
  }

  /**
   * Approve the {@link PrivilegedAction} for the given id and validator's name.
   *
   * @param actionId      the action id to approve.
   * @param validatorName the validator's name. Must be different than the requester.
   *
   * @throws PrivilegedActionNotFoundException         if the action cannot be found.
   * @throws PrivilegedActionAlreadyProcessedException if the action exists but its status is not {@link
   *                                                   PrivilegedAction.Status#PENDING}.
   */
  public abstract void approveAction(long actionId, String validatorName);

  /**
   * Reject the {@link PrivilegedAction} for the given id and validator's name.
   *
   * @param actionId      the action id to approve.
   * @param validatorName the validator's name. Must be different than the requester.
   * @param reason        the reason for rejecting this {@link PrivilegedAction}.
   *
   * @throws PrivilegedActionNotFoundException         if the action cannot be found.
   * @throws PrivilegedActionAlreadyProcessedException if the action exists but its status is not {@link
   *                                                   PrivilegedAction.Status#PENDING}.
   */
  public abstract void rejectAction(long actionId, String validatorName, String reason);

  /**
   * Get a specialised {@link PrivilegedActionVo} of type {@link T} by the id of its associated business object.
   *
   * @param businessId the business object id associated to the action.
   *
   * @return the matching {@link PrivilegedActionVo} of type {@link T}.
   *
   * @throws EntityNotFoundException                   if the given business id does not have an associated entity.
   * @throws PrivilegedActionAlreadyProcessedException if the action exists but cannot be displayed (i.e. its status is
   *                                                   not {@link PrivilegedAction.Status#PENDING}).
   */
  public abstract T getByBusinessId(Long businessId);

  /**
   * Create and persist a new {@link PrivilegedAction} for the given business id of the entity associated to the
   * specialised action where the requester is the user identified by the given requester's name.
   *
   * @param businessId    the business id.
   * @param requesterName the requester 's name.
   *
   * @throws EntityNotFoundException                if the given business id does not have an associated entity.
   * @throws PrivilegedActionAlreadyExistsException if the action already exists.
   */
  public abstract void createAction(Long businessId, String requesterName);

  /**
   * Retrieve a list of {@link PrivilegedActionVo} of type {@link T} that need approval or have not yet been requested
   * for approval.
   *
   * @return a list of {@link PrivilegedActionVo} of type {@link T}.
   */
  public abstract List<T> getNewOrPendingActions();
}
