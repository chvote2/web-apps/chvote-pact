/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.receiver;

import java.nio.charset.StandardCharsets;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * services relatives to vote receiver.
 *
 * @deprecated Should use pact-notification-events library.
 */
@Deprecated
@Service
public class VoteReceiverService {
  private static final String         VOTE_RECEIVER_EXCHANGE = "vote-receiver-exchange";
  private final        RabbitTemplate rabbitTemplate;

  public VoteReceiverService(RabbitTemplate rabbitTemplate) {
    this.rabbitTemplate = rabbitTemplate;
  }

  @Async
  public void deployInstance(String protocolInstanceId) {
    sendToVoteReceiver("vote.receiver.new-operation", protocolInstanceId);
  }

  @Async
  public void destroyInstance(String protocolInstanceId) {
    sendToVoteReceiver("vote.receiver.destroy-operation", protocolInstanceId);
  }


  private void sendToVoteReceiver(String routingKey, String body) {
    MessageProperties messageProperties = new MessageProperties();
    messageProperties.setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN);
    Message message =
        MessageBuilder.withBody(body.getBytes(StandardCharsets.UTF_8)).andProperties(messageProperties).build();
    rabbitTemplate.send(VOTE_RECEIVER_EXCHANGE, routingKey, message);
  }

}
