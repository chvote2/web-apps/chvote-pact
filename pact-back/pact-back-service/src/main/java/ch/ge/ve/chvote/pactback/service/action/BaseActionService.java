/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.user.UserRepository;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import ch.ge.ve.chvote.pactback.service.action.vo.PrivilegedActionVo;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionNotFoundException;
import ch.ge.ve.chvote.pactback.service.exception.UserNotFoundException;
import com.google.common.base.Preconditions;
import org.springframework.transaction.annotation.Transactional;

/**
 * A base service with common functionality for retrieving and modifying {@link PrivilegedAction} entities by exposing
 * them as {@link PrivilegedActionVo}s.
 *
 * @param <T> A subtype of {@link PrivilegedActionVo}.
 */
@Transactional(readOnly = true)
public abstract class BaseActionService<T extends PrivilegedActionVo> {

  private final UserRepository userRepository;

  protected BaseActionService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  /**
   * Retrieve a {@link User} entity by its username.
   *
   * @param username the name of the {@link User} to retrieve.
   *
   * @return the {@link User} entity for the given username.
   *
   * @throws UserNotFoundException if no user was found.
   */
  protected User getUserByName(String username) {
    Preconditions.checkNotNull(username);
    return userRepository.findByUsername(username).orElseThrow(
        () -> new UserNotFoundException(String.format("No user found for name %s", username)));
  }

  /**
   * Retrieve a {@link PrivilegedActionVo} of type {@link T} by the given id which status is {@link
   * PrivilegedAction.Status#PENDING}.
   *
   * @param id the id of the action.
   *
   * @return the specialised {@link PrivilegedActionVo}.
   *
   * @throws PrivilegedActionNotFoundException         if the action does not exists.
   * @throws PrivilegedActionAlreadyProcessedException if the action exists but its status is not {@link
   *                                                   PrivilegedAction.Status#PENDING}.
   */
  public abstract T getPendingPrivilegedAction(long id);

}
