/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration.printer;

import ch.ge.ve.chvote.pactback.contract.operation.MunicipalityVo;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.Municipality;

/**
 * A utility class for mapping {@link MunicipalityVo} to {@link Municipality}.
 */
public class MunicipalityVoMapper {

  /**
   * Map the incoming municipality VO to a real municipality.
   *
   * @param municipalityVo the incoming VO.
   *
   * @return the mapped municipality.
   */
  public static Municipality mapToRealMunicipality(
      MunicipalityVo municipalityVo) {
    return mapToMunicipality(municipalityVo, false);
  }

  /**
   * Map the incoming municipality VO to a virtual municipality.
   *
   * @param municipalityVo the incoming VO.
   *
   * @return the mapped municipality.
   */
  public static Municipality mapToVirtualMunicipality(
      MunicipalityVo municipalityVo) {
    return mapToMunicipality(municipalityVo, true);
  }

  private static Municipality mapToMunicipality(
      MunicipalityVo municipalityVo, boolean virtual) {
    Municipality municipality = new Municipality();
    municipality.setOfsId(municipalityVo.getOfsId());
    municipality.setName(municipalityVo.getName());
    municipality.setVirtual(virtual);
    return municipality;
  }

  /**
   * Hide utility class constructor.
   */
  private MunicipalityVoMapper() {
    throw new AssertionError("Not instantiable");
  }
}
