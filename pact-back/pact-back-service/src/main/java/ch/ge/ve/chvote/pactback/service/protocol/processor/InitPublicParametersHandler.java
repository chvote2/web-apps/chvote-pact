/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.protocol.model.PublicParameters;
import org.springframework.stereotype.Component;

/**
 * A {@link ProtocolProcessorHandler} that sends the public parameters to the protocol.
 */
@Component
public class InitPublicParametersHandler extends ProtocolProcessorHandler {

  @Override
  protected void process(ProtocolState state) {
    PublicParameters publicParameters = state.getClient().sendPublicParameters(state.getPublicParametersFactory());
    state.updateProtocolPublicParameters(publicParameters);

    state.getClient().ensurePublicParametersForwardedToCCs(
        state.createProgressTracker(ProtocolStage.SEND_PUBLIC_PARAMETERS));

    state.updateProtocolStatus(ProtocolInstanceStatus.PUBLIC_PARAMETERS_INITIALIZED);
  }

  @Override
  public boolean hasBeenProcessed(ProtocolState state) {
    return state.isProtocolInstanceStatusAtOrAfter(ProtocolInstanceStatus.PUBLIC_PARAMETERS_INITIALIZED);
  }
}
