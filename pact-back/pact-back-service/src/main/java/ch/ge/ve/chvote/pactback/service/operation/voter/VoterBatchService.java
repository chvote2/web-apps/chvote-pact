/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.voter;

import ch.ge.ve.chvote.pactback.service.operation.voter.vo.VoterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Voter service that operates on batch of voters that may span on multiple transactions.
 *
 * Implementation note: This class exists mainly to leverage the transactional behaviour of {@link VoterService}.
 * Indeed, the transactional behaviour of {@code VoterService} is given by a Spring-created proxy, so we need to have a
 * reference to the proxy.
 */
@Service
public class VoterBatchService {

  private final int          batchSize;
  private final VoterService voterService;

  @Autowired
  public VoterBatchService(VoterService voterService,
                           @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}") int batchSize) {
    this.voterService = voterService;
    this.batchSize = batchSize;
  }

  /**
   * Creates and returns a new batch to insert voters bound to the given protocol instance.
   *
   * @param protocolInstanceId the protocol instance id the voters are bound to.
   *
   * @return the batch.
   */
  public Batch<VoterVo> startBatchInsert(long protocolInstanceId) {
    return new Batch<>(batchSize, voters -> voterService.saveAll(protocolInstanceId, voters));
  }
}
