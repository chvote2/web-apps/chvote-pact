/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.voter;

import ch.ge.ve.chvote.pactback.contract.tallyarchive.VotersPerCountingCircleVo;
import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.DateOfBirth;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterCountingCircle;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterEntity;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.DateOfBirthVo;
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.VoterCountingCircleVo;
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.VoterVo;
import ch.ge.ve.protocol.model.CountingCircle;
import java.time.Month;
import java.time.Year;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class VoterService {

  private final VoterRepository            voterRepository;
  private final ProtocolInstanceRepository protocolInstanceRepository;

  @Autowired
  public VoterService(VoterRepository voterRepository, ProtocolInstanceRepository protocolInstanceRepository) {
    this.voterRepository = voterRepository;
    this.protocolInstanceRepository = protocolInstanceRepository;
  }

  public void saveAll(long protocolInstanceId, List<VoterVo> voters) {
    ProtocolInstance protocolInstance = getProtocolInstance(protocolInstanceId);
    List<VoterEntity> entities = voters.stream()
                                       .map(voter -> toEntity(voter, protocolInstance))
                                       .collect(Collectors.toList());
    voterRepository.saveAll(entities);
  }


  @Transactional(readOnly = true)
  public Stream<VoterVo> findByProtocolId(String protocolId) {
    return voterRepository.findByProtocolInstance_ProtocolId(protocolId).map(VoterService::toVo);
  }

  @Transactional(readOnly = true)
  public List<VotersPerCountingCircleVo> countVotersPerDoiIdsPerCountingCircles(long protocolInstanceId) {
    VotersCounterPerDoiPerCountingCircle counter = new VotersCounterPerDoiPerCountingCircle();
    Page<VoterEntity> currentPage = voterRepository.findByProtocolInstance_Id(protocolInstanceId,
                                                                              PageRequest.of(0, 500));
    counter.process(currentPage.getContent());
    while (currentPage.hasNext()) {
      currentPage = voterRepository.findByProtocolInstance_Id(protocolInstanceId, currentPage.nextPageable());
      counter.process(currentPage.getContent());
    }
    return counter.count.entrySet().stream()
                        .map(e -> new VotersPerCountingCircleVo(toModel(e.getKey()), e.getValue()))
                        .sorted(Comparator.comparing(count -> count.getCountingCircle().getBusinessId()))
                        .collect(Collectors.toList());
  }

  private ProtocolInstance getProtocolInstance(long id) {
    return protocolInstanceRepository.findById(id)
                                     .orElseThrow(
                                         () -> new EntityNotFoundException("Protocol instance [" + id + "] not found"));
  }

  private static CountingCircle toModel(VoterCountingCircleVo voterCountingCircleVo) {
    return new CountingCircle(voterCountingCircleVo.getProtocolCountingCircleIndex(),
                              voterCountingCircleVo.getBusinessId(),
                              voterCountingCircleVo.getBusinessId());
  }

  private static VoterEntity toEntity(VoterVo vo, ProtocolInstance protocolInstance) {
    VoterEntity entity = new VoterEntity();
    entity.setId(vo.getId());
    entity.setLocalPersonId(vo.getRegisterPersonId());
    entity.setProtocolVoterIndex(vo.getProtocolVoterIndex());
    entity.setDateOfBirth(toEntity(vo.getDateOfBirth()));
    entity.setPrintingAuthorityName(vo.getPrintingAuthorityName());
    entity.setVoterCountingCircle(toEntity(vo.getCountingCircle()));
    entity.setAllowedDoiIds(vo.getAllowedDoiIds());
    entity.setProtocolInstance(protocolInstance);
    return entity;
  }

  private static VoterVo toVo(VoterEntity entity) {
    return new VoterVo(entity.getId(), entity.getProtocolVoterIndex(), entity.getLocalPersonId(),
                       toVo(entity.getDateOfBirth()), toVo(entity.getVoterCountingCircle()),
                       entity.getPrintingAuthorityName(), entity.getAllowedDoiIds());
  }

  private static VoterCountingCircle toEntity(VoterCountingCircleVo countingCircle) {
    return new VoterCountingCircle(countingCircle.getProtocolCountingCircleIndex(),
                                   countingCircle.getBusinessId(), countingCircle.getName());
  }

  private static VoterCountingCircleVo toVo(VoterCountingCircle entity) {
    return new VoterCountingCircleVo(entity.getProtocolCountingCircleIndex(), entity.getBusinessId(), entity.getName());
  }

  private static DateOfBirth toEntity(DateOfBirthVo vo) {
    return new DateOfBirth(vo.getYear().getValue(),
                           vo.getMonthOfYear().map(Month::getValue).orElse(null),
                           vo.getDayOfMonth().orElse(null));
  }

  private static DateOfBirthVo toVo(DateOfBirth entity) {
    return new DateOfBirthVo(Year.of(entity.getYear()),
                             entity.getMonthOfYear() == null ? null : Month.of(entity.getMonthOfYear()),
                             entity.getDayOfMonth());
  }

  private static final class VotersCounterPerDoiPerCountingCircle {

    private final Map<VoterCountingCircleVo, Map<String, Long>> count = new HashMap<>();

    void process(List<VoterEntity> voters) {
      for (VoterEntity voter : voters) {
        Map<String, Long> countPerDoi =
            count.computeIfAbsent(toVo(voter.getVoterCountingCircle()), k -> new HashMap<>());
        for (String doi : voter.getAllowedDoiIds()) {
          countPerDoi.put(doi, countPerDoi.computeIfAbsent(doi, k -> 0L) + 1);
        }
      }
    }
  }
}
