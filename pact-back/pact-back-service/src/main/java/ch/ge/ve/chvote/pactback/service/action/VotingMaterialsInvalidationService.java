/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.action.materials.VotingMaterialsInvalidationActionRepository;
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsInvalidation;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats;
import ch.ge.ve.chvote.pactback.repository.user.UserRepository;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import ch.ge.ve.chvote.pactback.service.action.vo.VotingMaterialsInvalidationVo;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyExistsException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException;
import ch.ge.ve.chvote.pactback.service.operation.OperationService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A specialised {@link BaseActionService} for {@link VotingMaterialsInvalidationVo}s.
 */
@Service
public class VotingMaterialsInvalidationService extends ActionCreationService<VotingMaterialsInvalidationVo> {

  private final VotingMaterialsInvalidationActionRepository actionRepository;
  private final VotingMaterialsConfigurationRepository      materialsRepository;
  private final OperationService                            operationService;
  private final PendingActionRetriever<VotingMaterialsInvalidation, VotingMaterialsInvalidationVo>
                                                            pendingActionRetriever;

  /**
   * Create a new {@link VotingMaterialsInvalidationService} with the given repositories.
   *
   * @param userRepository      the {@link User} repository.
   * @param materialsRepository the {@link VotingMaterialsConfiguration} repository.
   * @param actionRepository    the {@link VotingMaterialsInvalidation} repository.
   */
  public VotingMaterialsInvalidationService(UserRepository userRepository,
                                            VotingMaterialsConfigurationRepository materialsRepository,
                                            VotingMaterialsInvalidationActionRepository actionRepository,
                                            OperationService operationService) {
    super(userRepository);
    this.actionRepository = actionRepository;
    this.materialsRepository = materialsRepository;
    this.operationService = operationService;
    this.pendingActionRetriever =
        new PendingActionRetriever<>(actionRepository, this::mapActionToMaterialInvalidationVo);
  }

  @Override
  @Transactional(readOnly = true)
  public VotingMaterialsInvalidationVo getPendingPrivilegedAction(long id) {
    return pendingActionRetriever.getPendingPrivilegedActionVo(id);
  }

  @Override
  @Transactional(readOnly = true)
  public VotingMaterialsInvalidationVo getByBusinessId(Long businessId) {
    VotingMaterialsConfiguration configuration = materialsRepository.getOne(businessId);
    configuration.getInvalidationAction().ifPresent(action -> {
      if (action.getStatus() == PrivilegedAction.Status.APPROVED) {
        throw new PrivilegedActionAlreadyProcessedException(action.getId());
      }
    });
    return mapToMaterialInvalidationVo(configuration);
  }

  @Override
  @Transactional
  public void createAction(Long businessId, String requesterName) {

    VotingMaterialsConfiguration votingMaterialsConfiguration =
        materialsRepository.findById(businessId)
                           .orElseThrow(() -> new EntityNotFoundException(
                               "No VotingMaterialsConfiguration found for id: " + businessId));

    votingMaterialsConfiguration.getInvalidationAction().ifPresent(
        invalidationAction -> {
          if (invalidationAction.getStatus() == PrivilegedAction.Status.REJECTED) {
            actionRepository.delete(invalidationAction);
          } else {
            throw new PrivilegedActionAlreadyExistsException(
                "An action was already created for the VotingMaterialsConfiguration object with ID " + businessId);
          }
        });

    VotingMaterialsInvalidation action = new VotingMaterialsInvalidation();
    action.setVotingMaterialsConfiguration(votingMaterialsConfiguration);

    Operation operation = operationService.getOperationByVotingMaterialsConfigurationId(
        votingMaterialsConfiguration.getId());
    OperationConfiguration deployedConfiguration = operation.fetchDeployedConfiguration();

    actionRepository.save(populateNewAction(action, requesterName, deployedConfiguration.getOperationName(),
                                            deployedConfiguration.getOperationDate()));
  }

  private VotingMaterialsInvalidationVo mapActionToMaterialInvalidationVo(VotingMaterialsInvalidation action) {
    List<VotersCreationStats> stats = operationService
        .getOperationByVotingMaterialsConfigurationId(action.getVotingMaterialsConfiguration().getId())
        .fetchDeployedConfiguration()
        .fetchProtocolInstance()
        .getStatistics();
    return new VotingMaterialsInvalidationVo(action, stats);
  }

  private VotingMaterialsInvalidationVo mapToMaterialInvalidationVo(VotingMaterialsConfiguration configuration) {
    Operation operation = operationService.getOperationByVotingMaterialsConfigurationId(configuration.getId());
    OperationConfiguration deployedConfiguration = operation.fetchDeployedConfiguration();

    List<VotersCreationStats> stats = deployedConfiguration
        .fetchProtocolInstance()
        .getStatistics();

    return configuration
        .getInvalidationAction()
        .map(action -> action.getStatus() == PrivilegedAction.Status.REJECTED ? null : action)
        .map(action -> new VotingMaterialsInvalidationVo(action, stats))
        .orElse(new VotingMaterialsInvalidationVo(
            operation, deployedConfiguration.getOperationName(), deployedConfiguration.getOperationDate(), stats));
  }

  // TODO This method should throw OperationNotSupportedException and a new method should be created.
  /**
   * <p>
   * Attention! This method only returns action which status is {@link PrivilegedAction.Status#PENDING}, actions that
   * have not yet been requested for approval are omitted by this implementation.
   * </p>
   * {@inheritDoc}
   */
  @Override
  @Transactional(readOnly = true)
  public List<VotingMaterialsInvalidationVo> getNewOrPendingActions() {
    return materialsRepository
        .findAllByInvalidationActionIsNullOrInvalidationActionStatus(PrivilegedAction.Status.PENDING)
        .map(this::mapToMaterialInvalidationVo)
        .collect(Collectors.toList());
  }

  @Override
  @Transactional
  public void approveAction(long actionId, String validatorName) {
    User validator = getUserByName(validatorName);

    VotingMaterialsInvalidation action = pendingActionRetriever.getPendingPrivilegedAction(actionId);
    actionRepository.save(this.updatePrivilegedAction(action, validator, PrivilegedAction.Status.APPROVED));

    Operation operation = operationService.getOperationByVotingMaterialsConfigurationId(
        action.getVotingMaterialsConfiguration().getId());
    operationService.deleteDeployedConfigurationInstance(operation);
  }

  @Override
  @Transactional
  public void rejectAction(long actionId, String validatorName, String reason) {
    User validator = getUserByName(validatorName);

    VotingMaterialsInvalidation action = pendingActionRetriever.getPendingPrivilegedAction(actionId);
    actionRepository.save(this.updatePrivilegedAction(action, validator, PrivilegedAction.Status.REJECTED, reason));
  }

}
