/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation

import java.time.LocalDateTime
import spock.lang.Specification

/**
 * Quick tests for a simple implementation
 */
class PactSchedulerInMemoryImplTest extends Specification {

  private PactSchedulerInMemoryImpl service = new PactSchedulerInMemoryImpl()

  private String sampleId = "test-sample"

  private LocalDateTime sampleScheduledTime = LocalDateTime.now().plusDays(2)

  void setup() {
    service.@repository.put(sampleId, sampleScheduledTime)
  }

  def "Register should register a new operation"() {
    given:
    def operationId = "test-operation"
    def scheduledTime = LocalDateTime.now().plusWeeks(1)

    when:
    service.register(operationId, scheduledTime)

    then:
    service.readRepository().size() == 2
    service.readRepository().get(operationId) == scheduledTime
  }

  def "Register should fail to register an already registered operation"() {
    when:
    service.register(sampleId, sampleScheduledTime.plusDays(1))

    then:
    thrown Exception
  }

  def "Unregister should remove an operation"() {
    when:
    service.unregister(sampleId)

    then:
    service.readRepository().isEmpty()
  }
}
