/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.voter

import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.DateOfBirth
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterCountingCircle
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterEntity
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.DateOfBirthVo
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.VoterCountingCircleVo
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.VoterVo
import java.time.Year
import java.util.stream.Collectors
import java.util.stream.Stream
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import spock.lang.Specification

class VoterServiceTest extends Specification {

  def voterRepository = Mock(VoterRepository)
  def protocolInstanceRepository = Mock(ProtocolInstanceRepository)
  def service = new VoterService(voterRepository, protocolInstanceRepository)
  def protocolInstance = new ProtocolInstance(id: 1L) // we only need the id here...

  def "findByProtocolId should call the repository to get the requested voters"() {
    given:
    voterRepository.findByProtocolInstance_ProtocolId(protocolInstance.protocolId) >> Stream.of(
            createVoterEntity(10L, 1, "a", new DateOfBirth(1984, null, null),
                    new VoterCountingCircle(1, "GE", "Geneve"), "printer1", ["CH", "GE"])
    )

    when:
    def voters = service.findByProtocolId(protocolInstance.protocolId).collect(Collectors.toList())

    then:
    voters.size() == 1
    voters[0].id == 10L
    voters[0].protocolVoterIndex == 1
    voters[0].registerPersonId == "a"
    voters[0].dateOfBirth.year == Year.of(1984)
    voters[0].countingCircle.businessId == "GE"
    voters[0].printingAuthorityName == "printer1"
    voters[0].allowedDoiIds == ["CH", "GE"]
  }

  def "saveAll should call the repository to persist the voters"() {
    given:
    def voters = [
            createVoterEntity(10L, 1, "a", new DateOfBirth(1984, null, null),
                    new VoterCountingCircle(1, "GE", "Geneve"), "printer1", ["CH", "GE"]),
            createVoterEntity(20L, 2, "b", new DateOfBirth(1970, null, null),
                    new VoterCountingCircle(1, "GE", "Geneve"), "printer2", ["GE"])
    ]

    when:
    service.saveAll(
            protocolInstance.id,
            [
                    createVoterVo(10L, 1, "a", new DateOfBirthVo(Year.of(1984), null, null),
                            new VoterCountingCircleVo(1, "GE", "Geneve"), "printer1", ["CH", "GE"]),
                    createVoterVo(20L, 2, "b", new DateOfBirthVo(Year.of(1970), null, null),
                            new VoterCountingCircleVo(1, "GE", "Geneve"), "printer2", ["CH", "GE"])
            ]
    )

    then:
    1 * protocolInstanceRepository.findById(protocolInstance.id) >> Optional.of(protocolInstance)
    1 * voterRepository.saveAll(voters) >> voters
  }

  def "countVotersPerDoiIdsPerCountingCircles should count the voters per DOI per counting circle"() {
    given:
    def voters = [
            createVoterEntity(10L, 1, "a", new DateOfBirth(1984, null, null),
                    new VoterCountingCircle(1, "1", "CC1"), "printer1", ["CH", "GE"]),
            createVoterEntity(20L, 2, "b", new DateOfBirth(1970, null, null),
                    new VoterCountingCircle(1, "1", "CC1"), "printer2", ["CH"]),
            createVoterEntity(30L, 3, "c", new DateOfBirth(1978, null, null),
                    new VoterCountingCircle(2, "2", "CC2"), "printer3", ["CH", "VD"])
    ]

    when:
    def count = service.countVotersPerDoiIdsPerCountingCircles(protocolInstance.id)

    then:
    2 * voterRepository.findByProtocolInstance_Id(protocolInstance.id, _ as Pageable) >>> [
            fakePage([voters[0]], true),
            fakePage([voters[1], voters[2]], false)
    ]
    count.size() == 2
    count[0].countingCircle.businessId == "1"
    count[0].registeredVotersPerDoi["CH"] == 2
    count[0].registeredVotersPerDoi["GE"] == 1
    count[1].countingCircle.businessId == "2"
    count[1].registeredVotersPerDoi["CH"] == 1
    count[1].registeredVotersPerDoi["VD"] == 1
  }

  private static VoterEntity createVoterEntity(Long id, int protocolVoterIndex, String registerPersonId,
                                               DateOfBirth dateOfBirth, VoterCountingCircle countingCircle,
                                               String printingAuthorityName, List<String> allowedDoiIds) {
    VoterEntity voter = new VoterEntity()
    voter.setId(id)
    voter.setProtocolVoterIndex(protocolVoterIndex)
    voter.setLocalPersonId(registerPersonId)
    voter.setDateOfBirth(dateOfBirth)
    voter.setVoterCountingCircle(countingCircle)
    voter.setPrintingAuthorityName(printingAuthorityName)
    voter.setAllowedDoiIds(allowedDoiIds)
    return voter
  }

  private static VoterVo createVoterVo(Long id, int protocolVoterIndex, String registerPersonId,
                                       DateOfBirthVo dateOfBirth, VoterCountingCircleVo countingCircle,
                                       String printingAuthorityName, List<String> allowedDoiIds) {
    return new VoterVo(id, protocolVoterIndex, registerPersonId, dateOfBirth,
            countingCircle, printingAuthorityName, allowedDoiIds)
  }

  def fakePage(List content, boolean hasNext) {
    return Mock(Page) {
      it.getContent() >> content
      it.hasNext() >> hasNext
      it.nextPageable() >> Mock(Pageable)
    }
  }

}
