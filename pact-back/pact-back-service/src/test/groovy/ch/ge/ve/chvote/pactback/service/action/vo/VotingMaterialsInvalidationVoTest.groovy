/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action.vo

import ch.ge.ve.chvote.pactback.contract.operation.VirtualCountingCircle
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsInvalidation
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats
import ch.ge.ve.chvote.pactback.repository.user.entity.User
import java.time.LocalDateTime
import spock.lang.Specification

class VotingMaterialsInvalidationVoTest extends Specification {


  def "should create and prepare stats for pending action"() {
    given:
    def action = new VotingMaterialsInvalidation()
    action.id = 1
    action.operationDate = LocalDateTime.of(2010, 1, 1, 0, 0)
    action.creationDate = LocalDateTime.of(2010, 1, 2, 0, 0)
    action.statusDate = LocalDateTime.of(2010, 1, 3, 0, 0)
    User user = new User()
    user.username = "requester"
    action.requester = user
    action.status = PrivilegedAction.Status.PENDING
    VotingMaterialsConfiguration configuration = new VotingMaterialsConfiguration()
    configuration.id = 2
    action.votingMaterialsConfiguration = configuration

    def votersCreationStats = new ArrayList<VotersCreationStats>()


    votersCreationStats.add(voterCreationStats("prn1", 1, 10))
    votersCreationStats.add(voterCreationStats("prn1", 2, 5))
    votersCreationStats.add(voterCreationStats("prn1", 3, 4))
    votersCreationStats.add(voterCreationStats("prn2", 1, 7))
    votersCreationStats.add(voterCreationStats("prn2", VirtualCountingCircle.CONTROLLER_TESTING_CARDS, 11))
    votersCreationStats.add(voterCreationStats("prn2", VirtualCountingCircle.NOT_PRINTABLE_TESTING_CARDS, 12))
    votersCreationStats.add(voterCreationStats("prn2", VirtualCountingCircle.PRINTABLE_TESTING_CARDS, 13))
    votersCreationStats.add(voterCreationStats("prn2", VirtualCountingCircle.PRINTER_TESTING_CARDS, 14))

    when:
    VotingMaterialsInvalidationVo vo = new VotingMaterialsInvalidationVo(action, votersCreationStats);

    then:

    assert vo.id == 1
    assert vo.status == 'PENDING'
    assert vo.votersCreationStats.size() == 2
    assert vo.votersCreationStats.get(0).printer == 'prn1'
    assert vo.votersCreationStats.get(0).statByCountingCircle.size() == 3
    assert vo.votersCreationStats.get(0).statByCountingCircle.get(0).countingCircleName == 'CC1'
    assert vo.votersCreationStats.get(0).statByCountingCircle.get(0).numberOfVoters == 10
    assert vo.votersCreationStats.get(0).statByCountingCircle.get(2).countingCircleName == 'CC3'
    assert vo.votersCreationStats.get(1).printer == 'prn2'
  }

  def "should create and prepare stats for new action"() {
    given:

    def operation = new Operation();
    VotingMaterialsConfiguration configuration = new VotingMaterialsConfiguration();
    configuration.author = "author";
    configuration.submissionDate = LocalDateTime.of(2010, 1, 5, 0, 0); ;
    configuration.id = 2;
    operation.setVotingMaterialsConfiguration(configuration);

    def votersCreationStats = new ArrayList<VotersCreationStats>();

    votersCreationStats.add(voterCreationStats("prn1", 1, 10))
    votersCreationStats.add(voterCreationStats("prn1", 2, 5))
    votersCreationStats.add(voterCreationStats("prn1", 3, 4))
    votersCreationStats.add(voterCreationStats("prn2", 1, 7))
    votersCreationStats.add(voterCreationStats("prn2", VirtualCountingCircle.CONTROLLER_TESTING_CARDS, 11))
    votersCreationStats.add(voterCreationStats("prn2", VirtualCountingCircle.NOT_PRINTABLE_TESTING_CARDS, 12))
    votersCreationStats.add(voterCreationStats("prn2", VirtualCountingCircle.PRINTABLE_TESTING_CARDS, 13))
    votersCreationStats.add(voterCreationStats("prn2", VirtualCountingCircle.PRINTER_TESTING_CARDS, 14))

    when:
    VotingMaterialsInvalidationVo vo = new VotingMaterialsInvalidationVo(operation, "ope",
            LocalDateTime.of(2010, 1, 6, 0, 0), votersCreationStats);

    then:
    assert vo.id == null
    assert vo.status == 'NOT_CREATED'
    assert vo.votersCreationStats.size() == 2
    assert vo.votersCreationStats.get(0).printer == 'prn1'
    assert vo.votersCreationStats.get(0).statByCountingCircle.size() == 3
    assert vo.votersCreationStats.get(0).statByCountingCircle.get(0).countingCircleName == 'CC1'
    assert vo.votersCreationStats.get(0).statByCountingCircle.get(0).numberOfVoters == 10
    assert vo.votersCreationStats.get(0).statByCountingCircle.get(2).countingCircleName == 'CC3'
    assert vo.votersCreationStats.get(1).printer == 'prn2'
    assert vo.votersCreationStats.get(1).statByCountingCircle.size() == 1
    assert vo.votersCreationStats.get(1).statByCountingCircle.get(0).countingCircleName == 'CC1'
    assert vo.votersCreationStats.get(1).statByCountingCircle.get(0).numberOfVoters == 7

  }

  VotersCreationStats voterCreationStats(String printer, Integer id, Integer numberOfVoters) {
    def stats = new VotersCreationStats()
    stats.setCountingCircleId(id)
    stats.setCountingCircleName("CC" + id)
    stats.numberOfVoters = numberOfVoters
    stats.setPrinterAuthorityName(printer)
    return stats
  }


  VotersCreationStats voterCreationStats(String printer, VirtualCountingCircle countingCircle, Integer numberOfVoters) {
    def stats = new VotersCreationStats()
    stats.setCountingCircleBusinessId(countingCircle.id)
    stats.setCountingCircleName(countingCircle.countingCircleName)
    stats.numberOfVoters = numberOfVoters
    stats.setPrinterAuthorityName(printer)
    return stats
  }

}
