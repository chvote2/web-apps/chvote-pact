/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.materials

import ch.ge.ve.chvote.pactback.contract.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.contract.operation.MunicipalityToPrinterLinkVo
import ch.ge.ve.chvote.pactback.contract.operation.MunicipalityVo
import ch.ge.ve.chvote.pactback.contract.operation.PrinterConfigurationVo
import ch.ge.ve.chvote.pactback.contract.operation.RegisterFileEntryVo
import ch.ge.ve.chvote.pactback.contract.operation.VotingMaterialsConfigurationSubmissionVo
import ch.ge.ve.chvote.pactback.contract.operation.status.VotersCreationStatisticsVo
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo
import ch.ge.ve.chvote.pactback.fixtures.PrintersPublicKeys
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsInvalidation
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException
import ch.ge.ve.chvote.pactback.service.exception.InvalidVotingMaterialsConfigurationException
import ch.ge.ve.chvote.pactback.service.operation.TimeService
import groovy.transform.Memoized
import java.time.LocalDateTime
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import spock.lang.Specification

class VotingMaterialsServiceTest extends Specification {

  private static final int NB_PRINTERS = 4
  private static final int OFS_ID_START = 6601
  private static final int OFS_ID_END = 6646
  private static final int VIRTUAL_MUNICIPALITY_OFS_ID = 6699
  private static final LocalDateTime REGISTER_IMPORT_DATE = LocalDateTime.parse("2017-11-12T12:01:52.123")
  private static final String VIRTUAL_MUNICIPALITY_NAME = "Virtual municipality"
  private static final String SWISS_ABROAD_PRINTER_ID = "Swiss abroad printer ID"
  private static final String SWISS_ABROAD_PRINTER_NAME = "Swiss abroad printer"

  @Memoized(maxCacheSize = 4)
  static BigInteger loadPublicKey(int num) {
    return PrintersPublicKeys."publicKeyPa$num"()
  }

  private OperationRepository operationRepository
  private VotingMaterialsService votingMaterialService
  private LocalDateTime dateNow
  private VotingMaterialsStatusVoMapper statusVoMapper

  void setup() {
    operationRepository = Mock(OperationRepository)

    statusVoMapper = Mock(VotingMaterialsStatusVoMapper)

    dateNow = LocalDateTime.now()
    def timeService = Mock(TimeService)
    timeService.now() >> dateNow

    def chVoteConfigurationProperties = Mock(ChVoteConfigurationProperties)
    chVoteConfigurationProperties.timezone >> "Europe/Zurich"
    def attachmentService = new VotingMaterialsAttachmentService(timeService, chVoteConfigurationProperties)

    votingMaterialService = new VotingMaterialsService(
            attachmentService,
            operationRepository,
            statusVoMapper,
            timeService)
  }

  def "createVotingMaterialsConfiguration should return a VotingMaterialsConfiguration with the common metadata"() {
    given:
    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()
    submissionVo.target = DeploymentTarget.REAL
    submissionVo.setUser("BO user")
    submissionVo.setVotingCardLabel("label")
    submissionVo.setSwissAbroadWithoutMunicipalityPrinter(createSwissAbroadPrinter())

    when:
    def votingMaterialsConfiguration = votingMaterialService.createVotingMaterialsConfiguration(
            submissionVo, new LinkedList<MultipartFile>())

    then:
    votingMaterialsConfiguration.votingCardLabel == "label"
    votingMaterialsConfiguration.author == "BO user"
    votingMaterialsConfiguration.submissionDate == dateNow
  }

  def "createVotingMaterialsConfiguration should return a VotingMaterialsConfiguration with a register files catalog"() {
    given:
    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()
    submissionVo.target = DeploymentTarget.SIMULATION
    submissionVo.simulationName = "simulation-0"
    submissionVo.setUser("BO user")
    submissionVo.setVotingCardLabel("label")
    submissionVo.setSwissAbroadWithoutMunicipalityPrinter(createSwissAbroadPrinter())
    submissionVo.setRegisterFilesCatalog(createRegisterFilesCatalog())

    when:
    def votingMaterialsConfiguration = votingMaterialService.createVotingMaterialsConfiguration(
            submissionVo, createAttachments())

    then:
    votingMaterialsConfiguration.votingCardLabel == "label"
    votingMaterialsConfiguration.target == ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget.SIMULATION
    votingMaterialsConfiguration.simulationName == "simulation-0"
    votingMaterialsConfiguration.author == "BO user"
    votingMaterialsConfiguration.submissionDate == dateNow
    votingMaterialsConfiguration.registers.size() == 2
    votingMaterialsConfiguration.registers[0].importDate == REGISTER_IMPORT_DATE
    votingMaterialsConfiguration.registers[0].nbOfVoters == 1
  }

  def "should fail to populate the VotingMaterialsConfiguration entity, due to a missing ZIP files"() {
    given:
    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()
    submissionVo.target = DeploymentTarget.REAL
    submissionVo.setSwissAbroadWithoutMunicipalityPrinter(createSwissAbroadPrinter())
    submissionVo.setRegisterFilesCatalog(createRegisterFilesCatalog())

    when:
    votingMaterialService.createVotingMaterialsConfiguration(submissionVo, new ArrayList<>())

    then:
    thrown(InvalidVotingMaterialsConfigurationException)
  }

  def "createVotingMaterialsConfiguration should return a VotingMaterialsConfiguration with a printers configuration with a different printer for Swiss abroad"() {
    given:
    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()
    submissionVo.target = DeploymentTarget.REAL

    def printers = createPrinters(NB_PRINTERS)
    submissionVo.setPrinters(printers)

    def municipalities = createMunicipalities()
    submissionVo.setMunicipalities(municipalities)

    def virtualMunicipalities = createVirtualMunicipalities()
    submissionVo.setVirtualMunicipalities(virtualMunicipalities)

    def municipalityToPrinterLinks = createLinks(NB_PRINTERS)
    submissionVo.setMunicipalitiesToPrintersLinks(municipalityToPrinterLinks)

    def swissAbroadPrinter = createSwissAbroadPrinter()
    submissionVo.setSwissAbroadWithoutMunicipalityPrinter(swissAbroadPrinter)

    when:
    def votingMaterialsConfiguration = votingMaterialService.createVotingMaterialsConfiguration(
            submissionVo, new LinkedList<MultipartFile>())

    then:
    votingMaterialsConfiguration.target == ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget.REAL
    votingMaterialsConfiguration.printers.size() == NB_PRINTERS + 1

    votingMaterialsConfiguration.printers[0].businessIdentifier == "printer-0"
    votingMaterialsConfiguration.printers[0].printerName == "Printer 0"
    votingMaterialsConfiguration.printers[0].publicKey == loadPublicKey(0).toByteArray()
    votingMaterialsConfiguration.printers[0].municipalities.size() == 12
    !votingMaterialsConfiguration.printers[0].swissAbroadWithoutMunicipality

    votingMaterialsConfiguration.printers[0].municipalities[0].ofsId == 6604
    votingMaterialsConfiguration.printers[0].municipalities[0].name == "Municipality 6604"
    !votingMaterialsConfiguration.printers[0].municipalities[0].isVirtual()

    votingMaterialsConfiguration.printers[0].municipalities[11].ofsId == VIRTUAL_MUNICIPALITY_OFS_ID
    votingMaterialsConfiguration.printers[0].municipalities[11].name == VIRTUAL_MUNICIPALITY_NAME
    votingMaterialsConfiguration.printers[0].municipalities[11].isVirtual()

    votingMaterialsConfiguration.printers[1].businessIdentifier == "printer-1"
    votingMaterialsConfiguration.printers[1].printerName == "Printer 1"
    votingMaterialsConfiguration.printers[1].publicKey == loadPublicKey(1).toByteArray()
    votingMaterialsConfiguration.printers[1].municipalities.size() == 12
    !votingMaterialsConfiguration.printers[1].swissAbroadWithoutMunicipality

    votingMaterialsConfiguration.printers[NB_PRINTERS].businessIdentifier == SWISS_ABROAD_PRINTER_ID
    votingMaterialsConfiguration.printers[NB_PRINTERS].printerName == SWISS_ABROAD_PRINTER_NAME
    votingMaterialsConfiguration.printers[NB_PRINTERS].publicKey == loadPublicKey(0).toByteArray()
    votingMaterialsConfiguration.printers[NB_PRINTERS].municipalities.size() == 0
    votingMaterialsConfiguration.printers[NB_PRINTERS].swissAbroadWithoutMunicipality
  }

  def "createVotingMaterialsConfiguration should return a VotingMaterialsConfiguration with a printers configuration with the same printer for Swiss abroad"() {
    given:
    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()
    submissionVo.target = DeploymentTarget.SIMULATION
    submissionVo.simulationName = "simulation-0"

    def printers = createPrinters(NB_PRINTERS)
    submissionVo.setPrinters(printers)

    def municipalities = createMunicipalities()
    submissionVo.setMunicipalities(municipalities)

    def virtualMunicipalities = createVirtualMunicipalities()
    submissionVo.setVirtualMunicipalities(virtualMunicipalities)

    def municipalityToPrinterLinks = createLinks(NB_PRINTERS)
    submissionVo.setMunicipalitiesToPrintersLinks(municipalityToPrinterLinks)

    def swissAbroadPrinter = printers[0]
    submissionVo.setSwissAbroadWithoutMunicipalityPrinter(swissAbroadPrinter)

    when:
    def votingMaterialsConfiguration = votingMaterialService.createVotingMaterialsConfiguration(
            submissionVo, new LinkedList<MultipartFile>())

    then:
    votingMaterialsConfiguration.printers.size() == NB_PRINTERS + 1

    votingMaterialsConfiguration.printers[0].businessIdentifier == "printer-0"
    votingMaterialsConfiguration.printers[0].printerName == "Printer 0"
    votingMaterialsConfiguration.printers[0].publicKey == loadPublicKey(0).toByteArray()
    votingMaterialsConfiguration.printers[0].municipalities.size() == 12
    !votingMaterialsConfiguration.printers[0].swissAbroadWithoutMunicipality

    votingMaterialsConfiguration.printers[0].municipalities[0].ofsId == 6604
    votingMaterialsConfiguration.printers[0].municipalities[0].name == "Municipality 6604"
    !votingMaterialsConfiguration.printers[0].municipalities[0].isVirtual()

    votingMaterialsConfiguration.printers[0].municipalities[11].ofsId == VIRTUAL_MUNICIPALITY_OFS_ID
    votingMaterialsConfiguration.printers[0].municipalities[11].name == VIRTUAL_MUNICIPALITY_NAME
    votingMaterialsConfiguration.printers[0].municipalities[11].isVirtual()

    votingMaterialsConfiguration.printers[1].businessIdentifier == "printer-1"
    votingMaterialsConfiguration.printers[1].printerName == "Printer 1"
    votingMaterialsConfiguration.printers[1].publicKey == loadPublicKey(1).toByteArray()
    votingMaterialsConfiguration.printers[1].municipalities.size() == 12
    !votingMaterialsConfiguration.printers[1].swissAbroadWithoutMunicipality

    votingMaterialsConfiguration.printers[NB_PRINTERS].businessIdentifier == "printer-0"
    votingMaterialsConfiguration.printers[NB_PRINTERS].printerName == "Printer 0"
    votingMaterialsConfiguration.printers[NB_PRINTERS].publicKey == loadPublicKey(0).toByteArray()
    votingMaterialsConfiguration.printers[NB_PRINTERS].municipalities.size() == 0
    votingMaterialsConfiguration.printers[NB_PRINTERS].swissAbroadWithoutMunicipality
  }

  def "createVotingMaterialsConfiguration should fail with a different configurations for same printer business id but different public keys"() {
    given:
    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()
    submissionVo.target = DeploymentTarget.REAL

    def printer = new PrinterConfigurationVo()
    printer.id = "printer-0"
    printer.name = "printer 0"
    printer.publicKey = loadPublicKey(0)
    submissionVo.setPrinters([printer].toSet())

    def municipality = new MunicipalityVo()
    municipality.setOfsId(6601)
    municipality.setName("Municipality 6601")
    submissionVo.setMunicipalities([municipality].toSet())

    def virtualMunicipality = new MunicipalityVo()
    virtualMunicipality.setOfsId(6699)
    virtualMunicipality.setName("Municipality 6699")
    submissionVo.setVirtualMunicipalities([virtualMunicipality].toSet())

    def link6601 = new MunicipalityToPrinterLinkVo()
    link6601.setMunicipalityOfsId(6601)
    link6601.setPrinterId("printer-0")

    def link6699 = new MunicipalityToPrinterLinkVo()
    link6699.setMunicipalityOfsId(6699)
    link6699.setPrinterId("printer-0")

    submissionVo.setMunicipalitiesToPrintersLinks([link6601, link6699].toSet())

    def swissAbroadPrinter = new PrinterConfigurationVo()
    swissAbroadPrinter.id = "printer-0"
    swissAbroadPrinter.name = "printer 0"
    swissAbroadPrinter.publicKey = loadPublicKey(1)
    submissionVo.setSwissAbroadWithoutMunicipalityPrinter(swissAbroadPrinter)

    when:
    votingMaterialService.createVotingMaterialsConfiguration(submissionVo, new LinkedList<MultipartFile>())

    then:
    InvalidPrinterConfigurationException ex = thrown()
    ex.message == "Printer [printer-0] configurations reference [2] distinct public keys"
  }

  def "createVotingMaterialsConfiguration should fail if a municipality is associated to more than 1 printer"() {
    given:
    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()
    submissionVo.target = DeploymentTarget.SIMULATION
    submissionVo.simulationName = "simulation-0"

    def printer0 = new PrinterConfigurationVo()
    printer0.id = "printer-0"
    printer0.name = "printer 0"
    printer0.publicKey = loadPublicKey(0)

    def printer1 = new PrinterConfigurationVo()
    printer1.id = "printer-1"
    printer1.name = "printer 1"
    printer1.publicKey = loadPublicKey(1)

    submissionVo.setPrinters([printer0, printer1].toSet())

    def municipality = new MunicipalityVo()
    municipality.setOfsId(6601)
    municipality.setName("Municipality 6601")
    submissionVo.setMunicipalities([municipality].toSet())

    def virtualMunicipality = new MunicipalityVo()
    virtualMunicipality.setOfsId(6699)
    virtualMunicipality.setName("Municipality 6699")
    submissionVo.setVirtualMunicipalities([virtualMunicipality].toSet())

    def link6601_0 = new MunicipalityToPrinterLinkVo()
    link6601_0.setMunicipalityOfsId(6601)
    link6601_0.setPrinterId("printer-0")

    def link6601_1 = new MunicipalityToPrinterLinkVo()
    link6601_1.setMunicipalityOfsId(6601)
    link6601_1.setPrinterId("printer-1")


    def link6699 = new MunicipalityToPrinterLinkVo()
    link6699.setMunicipalityOfsId(6699)
    link6699.setPrinterId("printer-0")

    submissionVo.setMunicipalitiesToPrintersLinks([link6601_0, link6601_1, link6699].toSet())

    def swissAbroadPrinter = new PrinterConfigurationVo()
    swissAbroadPrinter.id = "printer-0"
    swissAbroadPrinter.name = "printer 0"
    swissAbroadPrinter.publicKey = loadPublicKey(0)
    submissionVo.setSwissAbroadWithoutMunicipalityPrinter(swissAbroadPrinter)

    when:
    votingMaterialService.createVotingMaterialsConfiguration(submissionVo, new LinkedList<MultipartFile>())

    then:
    InvalidVotingMaterialsConfigurationException ex = thrown()
    ex.message == "The list of links municipalities/printers associates a municipality to more than one printer."
  }

  private static Set<RegisterFileEntryVo> createRegisterFilesCatalog() {
    def catalog = new ArrayList()
    for (int i = 1; i < 3; i++) {
      def file = new RegisterFileEntryVo()
      file.importDateTime = REGISTER_IMPORT_DATE
      file.zipFileName = "votersRegister" + i + ".zip"
      catalog.add(file)
    }
    catalog
  }

  private List<MultipartFile> createAttachments() {
    def is1 = this.class.getClassLoader().getResourceAsStream("attachments/votersRegister1.zip")
    def is2 = this.class.getClassLoader().getResourceAsStream("attachments/votersRegister2.zip")
    return Arrays.asList(
            new MockMultipartFile("reg1", "votersRegister1.zip", null, is1),
            new MockMultipartFile("reg2", "votersRegister2.zip", null, is2)
    )
  }

  private static Set<PrinterConfigurationVo> createPrinters(int nbPrinters) {
    def printers = new HashSet<PrinterConfigurationVo>()
    for (int i = 0; i < nbPrinters; i++) {
      def printer = new PrinterConfigurationVo()
      printer.setId("printer-${i}")
      printer.setName("Printer ${i}")
      printer.publicKey = loadPublicKey(i)
      printers.add(printer)
    }
    return printers
  }

  private static Set<MunicipalityVo> createMunicipalities() {
    def municipalities = new HashSet<MunicipalityVo>()
    for (int ofsId = OFS_ID_START; ofsId <= OFS_ID_END; ofsId++) {
      def municipality = new MunicipalityVo()
      municipality.setOfsId(ofsId)
      municipality.setName("Municipality ${ofsId}")
      municipalities.add(municipality)
    }
    return municipalities
  }

  private static Set<MunicipalityVo> createVirtualMunicipalities() {
    def virtualMunicipalities = new HashSet<MunicipalityVo>()
    def virtualMunicipality = new MunicipalityVo()
    virtualMunicipality.setOfsId(VIRTUAL_MUNICIPALITY_OFS_ID)
    virtualMunicipality.setName(VIRTUAL_MUNICIPALITY_NAME)
    virtualMunicipalities.add(virtualMunicipality)
    return virtualMunicipalities
  }

  private
  static Set<MunicipalityToPrinterLinkVo> createLinks(Integer nbPrinters) {

    def municipalityToPrinterLinks = new LinkedHashSet<MunicipalityToPrinterLinkVo>()
    for (int ofsId = OFS_ID_START; ofsId <= OFS_ID_END; ofsId++) {
      def link = new MunicipalityToPrinterLinkVo()
      link.setMunicipalityOfsId(ofsId)
      link.setPrinterId("printer-" + ofsId % nbPrinters)
      municipalityToPrinterLinks.add(link)
    }

    def virtualMunicipalityLink = new MunicipalityToPrinterLinkVo()
    virtualMunicipalityLink.setMunicipalityOfsId(VIRTUAL_MUNICIPALITY_OFS_ID)
    virtualMunicipalityLink.setPrinterId("printer-0")
    municipalityToPrinterLinks.add(virtualMunicipalityLink)

    return municipalityToPrinterLinks
  }

  private static PrinterConfigurationVo createSwissAbroadPrinter() {
    def printer = new PrinterConfigurationVo()
    printer.id = SWISS_ABROAD_PRINTER_ID
    printer.name = SWISS_ABROAD_PRINTER_NAME
    printer.publicKey = loadPublicKey(0)
    return printer
  }

  def "getVotingMaterialConfigurationStatusByClientId should raise an exception if the operation does not exist"() {
    given:
    def clientId = "OPERATION-1"
    operationRepository.findByClientId(clientId) >> Optional.empty()

    when:
    votingMaterialService.getVotingMaterialsStatusByClientId(clientId)

    then: "an exception should be thrown"
    EntityNotFoundException ex = thrown()
    ex.message == "No operation found for clientId [OPERATION-1]"
  }

  def "getVotingMaterialConfigurationStatusByClientId should raise an exception if the voting materials configuration does not exist"() {
    given:
    def clientId = "OPERATION-2"
    def operation = new Operation()
    operation.clientId = clientId
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    votingMaterialService.getVotingMaterialsStatusByClientId(clientId)

    then: "an exception should be thrown"
    EntityNotFoundException ex = thrown()
    ex.message == "No voting materials configuration found for clientId [OPERATION-2]"
  }

  def "getDeployedMaterialsCreationStatistics should return associated statistics"() {
    given:
    def clientId = "SomeOperation"
    def operation = new Operation()
    operation.deployedConfiguration = new OperationConfiguration()
    def protocolInstance = new ProtocolInstance()
    protocolInstance.statistics = [
            new VotersCreationStats(id: 1L, printerAuthorityName: "a printer", countingCircleId: 1, numberOfVoters: 55L, countingCircleBusinessId: "1001"),
            new VotersCreationStats(id: 4L, printerAuthorityName: "a printer", countingCircleId: 3, numberOfVoters: 33L, countingCircleBusinessId: "1003"),
            new VotersCreationStats(id: 8L, printerAuthorityName: "a printer", countingCircleId: 15, numberOfVoters: 2L, countingCircleBusinessId: "1015"),
    ]
    operation.fetchDeployedConfiguration().protocolInstance = protocolInstance

    operationRepository.findByClientId(clientId) >> { Optional.of(operation) }

    when:
    def stats = votingMaterialService.getDeployedMaterialsCreationStatistics(clientId)

    then:
    stats == [
            new VotersCreationStatisticsVo("a printer", "1001", "Test 1", 55L),
            new VotersCreationStatisticsVo("a printer", "1003", "Test 3", 33L),
            new VotersCreationStatisticsVo("a printer", "1015", "Test 15", 2L)
    ]
  }

  def "getDeployedMaterialsCreationStatistics should indicate EntityNotFound when operation doesn't exist"() {
    given:
    def clientId = "SomeOperation"
    operationRepository.findByClientId(clientId) >> { Optional.empty() }

    when:
    votingMaterialService.getDeployedMaterialsCreationStatistics(clientId)

    then:
    thrown EntityNotFoundException
  }

  def "getDeployedMaterialsCreationStatistics should indicate EntityNotFound when no deployed config"() {
    given:
    def clientId = "SomeOperation"
    def operation = new Operation()
    operationRepository.findByClientId(clientId) >> { Optional.of(operation) }

    when:
    votingMaterialService.getDeployedMaterialsCreationStatistics(clientId)

    then:
    thrown EntityNotFoundException
  }

  def "getDeployedMaterialsCreationStatistics should indicate EntityNotFound when no protocolInstance"() {
    given:
    def clientId = "SomeOperation"
    def operation = new Operation()
    operation.deployedConfiguration = new OperationConfiguration()

    operationRepository.findByClientId(clientId) >> { Optional.of(operation) }

    when:
    votingMaterialService.getDeployedMaterialsCreationStatistics(clientId)

    then:
    thrown EntityNotFoundException
  }

  def "getDeployedMaterialsCreationStatistics should indicate EntityNotFound when stats list is empty"() {
    given:
    def clientId = "SomeOperation"
    def operation = new Operation()
    operation.deployedConfiguration = new OperationConfiguration()
    operation.fetchDeployedConfiguration().protocolInstance = new ProtocolInstance()

    operationRepository.findByClientId(clientId) >> { Optional.of(operation) }

    when:
    votingMaterialService.getDeployedMaterialsCreationStatistics(clientId)

    then:
    thrown EntityNotFoundException
  }

  def "validateConfigurationByClientId can validate if the status of the voting material is created"() {
    given:
    def clientId = "SomeOperation"
    def operation = new Operation()
    operationRepository.findByClientId(clientId) >> Optional.of(operation)
    operation.votingMaterialsConfiguration = new VotingMaterialsConfiguration()

    statusVoMapper.map(operation) >> new VotingMaterialsStatusVo(
            "user", LocalDateTime.now(), VotingMaterialsStatusVo.State.CREATED, null, null, null, null, null)

    when:
    votingMaterialService.validateConfigurationByClientId(clientId, "user")

    then:
    1 * operationRepository.save(operation)
    operation.votingMaterialsConfiguration.get().validator == "user"
    operation.votingMaterialsConfiguration.get().validationDate != null
    operation.votingMaterialsConfiguration.get().validationStatus == ValidationStatus.VALIDATED
  }

  def "validateConfigurationByClientId can validate if there is an existing invalidation rejected and remove it"() {
    given:
    def clientId = "SomeOperation"
    def operation = new Operation()
    operationRepository.findByClientId(clientId) >> Optional.of(operation)
    operation.votingMaterialsConfiguration = new VotingMaterialsConfiguration()
    operation.votingMaterialsConfiguration.get().setInvalidationAction(Mock(VotingMaterialsInvalidation))
    statusVoMapper.map(operation) >> new VotingMaterialsStatusVo(
            "user", LocalDateTime.now(), VotingMaterialsStatusVo.State.INVALIDATION_REJECTED, null, null, null, null, null)

    when:
    votingMaterialService.validateConfigurationByClientId(clientId, "user")

    then:
    1 * operationRepository.save(operation)
    !operation.votingMaterialsConfiguration.get().invalidationAction.isPresent()
    operation.votingMaterialsConfiguration.get().validator == "user"
    operation.votingMaterialsConfiguration.get().validationDate != null
    operation.votingMaterialsConfiguration.get().validationStatus == ValidationStatus.VALIDATED
  }

  def "validateConfigurationByClientId cannot validate if there is an existing invalidation validated"() {
    given:
    def clientId = "SomeOperation"
    def operation = new Operation()
    operationRepository.findByClientId(clientId) >> Optional.of(operation)
    operation.votingMaterialsConfiguration = new VotingMaterialsConfiguration()

    statusVoMapper.map(operation) >> new VotingMaterialsStatusVo(
            "user", LocalDateTime.now(), VotingMaterialsStatusVo.State.INVALIDATED, null, null, null, null, null)

    when:
    votingMaterialService.validateConfigurationByClientId(clientId, "user")

    then:
    def exception = thrown(IllegalStateException)
    exception.message == 'Current state of voting materials [INVALIDATED] does not allow validation'
  }

  def "validateConfigurationByClientId cannot validate if the status of the voting material is not created"() {
    given:
    def clientId = "SomeOperation"
    def operation = new Operation()
    operationRepository.findByClientId(clientId) >> Optional.of(operation)
    operation.votingMaterialsConfiguration = new VotingMaterialsConfiguration()

    statusVoMapper.map(operation) >> new VotingMaterialsStatusVo(
            "user", LocalDateTime.now(), VotingMaterialsStatusVo.State.VALIDATED, null, null, null, null, null)

    when:
    votingMaterialService.validateConfigurationByClientId(clientId, "user")

    then:
    def exception = thrown(IllegalStateException)
    exception.message == "Current state of voting materials [VALIDATED] does not allow validation"
    0 * operationRepository.save(operation)
  }

  def "validateConfigurationByClientId should fail if there is no configuration found"() {
    given:
    def clientId = "SomeOperation"
    operationRepository.findByClientId(clientId) >> Optional.empty()

    when:
    votingMaterialService.validateConfigurationByClientId(clientId, "user")

    then:
    def exception = thrown(EntityNotFoundException)
    exception.message == "No operation where clientId = SomeOperation"
  }

}
