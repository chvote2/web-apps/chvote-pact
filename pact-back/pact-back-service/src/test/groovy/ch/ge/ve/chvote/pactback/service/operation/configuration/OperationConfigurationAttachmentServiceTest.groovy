/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration

import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo
import ch.ge.ve.chvote.pactback.contract.operation.BallotDocumentationVo
import ch.ge.ve.chvote.pactback.contract.operation.HighlightedQuestionsVo
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.exception.AttachmentParsingException
import ch.ge.ve.chvote.pactback.service.operation.TimeService
import com.google.common.io.ByteStreams
import java.time.LocalDateTime
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import org.springframework.mock.web.MockMultipartFile
import spock.lang.Specification

class OperationConfigurationAttachmentServiceTest extends Specification {

  private OperationConfigurationAttachmentService service

  void setup() {
    def timeService = new TimeService()
    def chVoteConfigurationProperties = Mock(ChVoteConfigurationProperties)
    chVoteConfigurationProperties.timezone >> "Europe/Zurich"
    service = new OperationConfigurationAttachmentService(timeService, chVoteConfigurationProperties)
  }

  def "should parse attachment file content and populate the given OperationConfiguration entity"() {
    def operation = new OperationConfiguration()
    def doiFile = new MockMultipartFile("doi", "doi.zip", null,
            zip("doi.xml",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/doi.xml")))
    def doiEntry = new AttachmentFileEntryVo()
    doiEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOMAIN_OF_INFLUENCE)
    doiEntry.setImportDateTime(LocalDateTime.now())
    doiEntry.setZipFileName("doi.zip")

    def faqDEFile = new MockMultipartFile("op_doc_faq.de", "op_doc_faq.de.zip", null,
            zip("op_doc_faq.de.pdf",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/op_doc_faq.de.pdf")))
    def faqDEEntry = new AttachmentFileEntryVo()
    faqDEEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOCUMENT_FAQ)
    faqDEEntry.setLanguage("DE")
    faqDEEntry.setImportDateTime(LocalDateTime.now())
    faqDEEntry.setZipFileName("op_doc_faq.de.zip")

    def faqFRFile = new MockMultipartFile("op_doc_faq.fr", "op_doc_faq.fr.zip", null,
            zip("op_doc_faq.fr.pdf",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/op_doc_faq.fr.pdf")))
    def faqFREntry = new AttachmentFileEntryVo()
    faqFREntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOCUMENT_FAQ)
    faqFREntry.setLanguage("FR")
    faqFREntry.setImportDateTime(LocalDateTime.now())
    faqFREntry.setZipFileName("op_doc_faq.fr.zip")

    def orepFile = new MockMultipartFile("orep", "orep.zip", null,
            zip("orep.xml",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/orep.xml")))
    def orepEntry = new AttachmentFileEntryVo()
    orepEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REPOSITORY_ELECTION)
    orepEntry.setImportDateTime(LocalDateTime.now())
    orepEntry.setZipFileName("orep.zip")

    def registerFile = new MockMultipartFile("register", "register.zip", null,
            zip("register.xml",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/register.xml")))
    def registerEntry = new AttachmentFileEntryVo()
    registerEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REGISTER)
    registerEntry.setImportDateTime(LocalDateTime.now())
    registerEntry.setZipFileName("register.zip")



    def bdocFile = new MockMultipartFile("ballot-documentation", "ballot-documentation.zip", null,
            zip("bdoc.pdf", this.class.getClassLoader().getResourceAsStream("attachments/operation/test.pdf")))
    def bdocEntry = new AttachmentFileEntryVo()
    bdocEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.BALLOT_DOCUMENTATION)
    bdocEntry.setImportDateTime(LocalDateTime.now())
    bdocEntry.setLanguage("fr")
    bdocEntry.setZipFileName("ballot-documentation.zip")
    def bdoc = new BallotDocumentationVo()
    bdoc.ballot = "ballot 1"
    bdoc.documentation = bdocEntry
    bdoc.label = "label"


    def hqFile = new MockMultipartFile("highlighted-question", "highlighted-question.zip", null,
            zip("hq.pdf", this.class.getClassLoader().getResourceAsStream("attachments/operation/test.pdf")))
    def hqEntry = new AttachmentFileEntryVo()
    hqEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.HIGHLIGHTED_QUESTION)
    hqEntry.setImportDateTime(LocalDateTime.now())
    hqEntry.setLanguage("fr")
    hqEntry.setZipFileName("highlighted-question.zip")
    def hq = new HighlightedQuestionsVo()
    hq.answerFile = hqEntry
    hq.question = "question"

    def files = Arrays.asList(doiFile, faqDEFile, faqFRFile, orepFile, registerFile, bdocFile, hqFile)
    def catalog = Arrays.asList(doiEntry, faqDEEntry, faqFREntry, orepEntry, registerEntry, bdocEntry, hqEntry)

    def ballotDocumentations = [bdoc] as Set
    def highlightedQuestions = [hq] as Set

    when:
    operation = service.populateAttachments(operation, ballotDocumentations, highlightedQuestions, catalog, files)


    then:
    operation.getOperationReferenceFiles().size() == 3

    then:
    def doi = operation.getOperationReferenceFiles().find { f -> f.type == AttachmentType.DOMAIN_OF_INFLUENCE }
    doi.getMessageId() == "70dc0e5d947d44bdbbef2092ee473f99"
    doi.getSourceApplicationManufacturer() == "OCSIN - SIDP"
    doi.getSourceApplicationProduct() == "CH-Vote - Back Office"
    doi.getSourceApplicationProductVersion() == "1.0.0"

    then:
    def orep = operation.getOperationReferenceFiles().find { f -> f.type == AttachmentType.OPERATION_REPOSITORY_ELECTION }
    orep.getMessageId() == "6c6f4b14b6d349ef97703f2af84ffbc6"
    orep.getSourceApplicationManufacturer() == "OCSIN - SIDP"
    orep.getSourceApplicationProduct() == "VOTA3"
    orep.getSourceApplicationProductVersion() == "1.3.1.1-SN"

    then:
    def reg = operation.getOperationReferenceFiles().find { f -> f.type == AttachmentType.OPERATION_REGISTER }
    reg.getMessageId() == "messageID2"
    reg.getSourceApplicationManufacturer() == "manufacturer"
    reg.getSourceApplicationProduct() == "product"
    reg.getSourceApplicationProductVersion() == "1"

    then:
    operation.getInformationAttachments().size() == 2
    operation.getInformationAttachments().count { f -> f.type == AttachmentType.OPERATION_FAQ } == 2
    operation.getInformationAttachments().count { f -> f.language == "DE" } == 1
    operation.getInformationAttachments().count { f -> f.language == "FR" } == 1

    then:
    operation.getBallotDocumentations().label == ["label"]
    operation.getBallotDocumentations().language == ["fr"]

    then:
    operation.getHighlightedQuestions().question == ["question"]
    operation.getHighlightedQuestions().language == ["fr"]
  }

  def "should abort parsing on invalid attachment"() {
    given:
    def operation = new OperationConfiguration()
    def doiFile = new MockMultipartFile("doi", "doi.zip", null,
            zip("op_doc_faq.fr.pdf",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/op_doc_faq.fr.pdf")))
    def doiEntry = new AttachmentFileEntryVo()
    doiEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOMAIN_OF_INFLUENCE)
    doiEntry.setImportDateTime(LocalDateTime.now())
    doiEntry.setZipFileName("doi.zip")

    def files = Arrays.asList(doiFile)
    def catalog = Arrays.asList(doiEntry)


    when:
    service.populateAttachments(operation, [] as Set, [] as Set, catalog, files)

    then:
    AttachmentParsingException ex = thrown()
    ex.message == "An error occurred while parsing attachment at op_doc_faq.fr.pdf"
  }

  static zip(String filename, InputStream input) {
    def bInput = ByteStreams.toByteArray(input)
    def baos = new ByteArrayOutputStream()
    def zos = new ZipOutputStream(baos)
    def entry = new ZipEntry(filename)

    entry.setSize(bInput.length)
    zos.putNextEntry(entry)
    zos.write(bInput)
    zos.closeEntry()
    zos.close()

    return new ByteArrayInputStream(baos.toByteArray())
  }

}
