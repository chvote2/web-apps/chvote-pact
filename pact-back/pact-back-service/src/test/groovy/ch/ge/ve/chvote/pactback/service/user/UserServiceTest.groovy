/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.user

import ch.ge.ve.chvote.pactback.repository.user.UserRepository
import ch.ge.ve.chvote.pactback.repository.user.entity.User
import spock.lang.Specification

class UserServiceTest extends Specification {

  def repository = Mock(UserRepository)
  private UserService service

  void setup() {
    service = new UserService(repository)
  }

  def "findByUsername"() {
    given:
    def userMock = Mock(User)
    userMock.id >> 1L
    repository.findByUsername('toto') >> Optional.of(userMock)

    when:
    def user = service.findByUsername('toto')

    then:
    user.id == 1L
  }
}
