/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.tally

import static ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget.REAL
import static ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget.SIMULATION
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.CREDENTIALS_GENERATED
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.DECRYPTING
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.GENERATING_RESULTS
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.RESULTS_AVAILABLE
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.SHUFFLING

import ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolTallyArchiveService
import java.time.LocalDateTime
import spock.lang.Specification

class TallyArchiveServiceTest extends Specification {

  def operationRepository = Mock(OperationRepository)
  def tallyStatusVoMapper = Mock(TallyArchiveStatusVoMapper)
  def protocolTallyService = Mock(ProtocolTallyArchiveService)

  private TallyArchiveService service

  private String clientId = "business-operation-for-test"
  private LocalDateTime now = LocalDateTime.now()

  def setup() {
    service = new TallyArchiveService(operationRepository, tallyStatusVoMapper, protocolTallyService)
  }

  // Helper method to create an operation in several configurations
  def prepareOperation(Map config) {
    ProtocolInstanceStatus protocolInstanceStatus = config.protocolInstanceStatus
    LocalDateTime siteOpeningDate = config.siteOpeningDate ?: now.minusDays(7)
    LocalDateTime siteClosingDate = config.siteClosingDate ?: now.minusHours(1)
    int gracePeriod = config.gracePeriod ?: 30
    DeploymentTarget targetMode = config.targetMode

    def operation = new Operation(clientId: clientId)
    def protocolInstance = new ProtocolInstance(operation: operation, status: protocolInstanceStatus)
    operation.deployedConfiguration = new OperationConfiguration(protocolInstance: protocolInstance)
    operation.votingPeriodConfiguration = new VotingPeriodConfiguration(
            siteOpeningDate: siteOpeningDate, siteClosingDate: siteClosingDate, gracePeriod: gracePeriod,
            target: targetMode)

    return operation
  }

  def "triggerGenerationManually should start the tallying of an operation in SIMULATION with an ongoing voting period"() {
    given:
    def operation = prepareOperation(protocolInstanceStatus: READY_TO_RECEIVE_VOTES,
            targetMode: SIMULATION, siteClosingDate: now.plusDays(3))
    def protocolInstance = operation.fetchDeployedConfiguration().fetchProtocolInstance()
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    service.triggerGenerationManually(clientId)

    then:
    1 * protocolTallyService.startTallying(protocolInstance)
  }

  def "triggerGenerationManually should fail when the operation is in REAL mode"() {
    given:
    def operation = prepareOperation(protocolInstanceStatus: READY_TO_RECEIVE_VOTES, targetMode: REAL)
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    service.triggerGenerationManually(clientId)

    then:
    def ex = thrown(CannotPublishTallyArchiveException)
    ex.message == 'Operation [business-operation-for-test] is not in SIMULATION and cannot be manually closed.'
    and:
    0 * protocolTallyService.startTallying(_)
  }

  def "triggerGenerationManually should fail if there is no deployed configuration for the given client id"(Operation operation) {
    given:
    operationRepository.findByClientId(clientId) >> Optional.ofNullable(operation)

    when:
    service.triggerGenerationManually(clientId)

    then:
    def ex = thrown EntityNotFoundException
    ex.message == 'Operation [business-operation-for-test] has no deployed protocol instance'
    0 * protocolTallyService.startTallying(_)

    where:
    operation << [
            null,
            new Operation(configurationInTest: new OperationConfiguration(protocolInstance: new ProtocolInstance(status: READY_TO_RECEIVE_VOTES))),
            new Operation(deployedConfiguration: new OperationConfiguration())
    ]
  }

  def "triggerGenerationManually should fail if the protocol instance is not yet ready to receive votes"() {
    given:
    def operation = prepareOperation(protocolInstanceStatus: CREDENTIALS_GENERATED, targetMode: SIMULATION)
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    service.triggerGenerationManually(clientId)

    then:
    def ex = thrown CannotPublishTallyArchiveException
    ex.message == 'Operation [business-operation-for-test] is not ready for publishing the tally.'
    and:
    0 * protocolTallyService.startTallying(_)
  }

  def "triggerGenerationManually should fail if the site has not opened yet"() {
    given:
    def operation = prepareOperation(protocolInstanceStatus: READY_TO_RECEIVE_VOTES, targetMode: SIMULATION,
            siteOpeningDate: now.plusMinutes(30), siteClosingDate: now.plusDays(7))
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    service.triggerGenerationManually(clientId)

    then:
    def ex = thrown CannotPublishTallyArchiveException
    ex.message == 'Operation [business-operation-for-test] is not opened yet'
    and:
    0 * protocolTallyService.startTallying(_)
  }

  def "triggerGenerationManually should fail with a specific exception if the generation is already in progress"(ProtocolInstanceStatus protocolInstanceStatus) {
    given:
    def operation = prepareOperation(protocolInstanceStatus: protocolInstanceStatus, targetMode: SIMULATION)
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    service.triggerGenerationManually(clientId)

    then:
    def ex = thrown GenerationInProgressException
    ex.message == 'Generation already in progress for operation business-operation-for-test'
    and:
    0 * protocolTallyService.startTallying(_)

    where:
    protocolInstanceStatus << [SHUFFLING, DECRYPTING, GENERATING_RESULTS]
  }


  def "triggerScheduledGeneration should start the tallying of an operation with an ended voting period"(DeploymentTarget targetMode) {
    given:
    def operation = prepareOperation(protocolInstanceStatus: READY_TO_RECEIVE_VOTES, targetMode: targetMode,
            siteClosingDate: now.minusMinutes(30), gracePeriod: 10)
    def protocolInstance = operation.fetchDeployedConfiguration().fetchProtocolInstance()
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    service.triggerScheduledGeneration(clientId)

    then:
    1 * protocolTallyService.startTallying(protocolInstance)

    where:
    targetMode << [ SIMULATION, REAL ]
  }

  def "triggerScheduledGeneration should fail if the site has not opened yet"() {
    given:
    def operation = prepareOperation(protocolInstanceStatus: READY_TO_RECEIVE_VOTES, targetMode: REAL,
            siteOpeningDate: now.plusMinutes(10), siteClosingDate: now.plusDays(7))
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    service.triggerScheduledGeneration(clientId)

    then:
    def ex = thrown CannotPublishTallyArchiveException
    ex.message == 'Operation [business-operation-for-test] is not opened yet'
    and:
    0 * protocolTallyService.startTallying(_)
  }

  def "triggerScheduledGeneration should fail if the voting period has not yet been closed"() {
    given:
    def operation = prepareOperation(protocolInstanceStatus: READY_TO_RECEIVE_VOTES, targetMode: REAL,
            siteClosingDate: now.minusMinutes(55), gracePeriod: 60)
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    service.triggerScheduledGeneration(clientId)

    then:
    def ex = thrown CannotPublishTallyArchiveException
    ex.message == 'Voting period of operation [business-operation-for-test] has not ended yet - cannot be closed.'
    and:
    0 * protocolTallyService.startTallying(_)
  }

  def "triggerScheduledGeneration should fail if there is no deployed configuration for the given client id"(Operation operation) {
    given:
    operationRepository.findByClientId(clientId) >> Optional.ofNullable(operation)

    when:
    service.triggerScheduledGeneration(clientId)

    then:
    def ex = thrown EntityNotFoundException
    ex.message == 'Operation [business-operation-for-test] has no deployed protocol instance'
    0 * protocolTallyService.startTallying(_)

    where:
    operation << [
            null,
            new Operation(configurationInTest: new OperationConfiguration(protocolInstance: new ProtocolInstance(status: READY_TO_RECEIVE_VOTES))),
            new Operation(deployedConfiguration: new OperationConfiguration())
    ]
  }

  def "triggerScheduledGeneration should fail if there is no voting period configuration for the given client id"() {
    given:
    def operation = new Operation(clientId: clientId)
    def configuration = new OperationConfiguration(protocolInstance: new ProtocolInstance(operation: operation, status: READY_TO_RECEIVE_VOTES))
    operation.deployedConfiguration = configuration
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    service.triggerScheduledGeneration(clientId)

    then:
    def ex = thrown EntityNotFoundException
    ex.message == 'Operation [business-operation-for-test] has not yet a voting period configuration.'
    0 * protocolTallyService.startTallying(_)
  }


  def "getTallyArchiveStatus should retrieve the status by clientId"() {
    given:
    def operation = new Operation(clientId: clientId)
    def protocolInstance = new ProtocolInstance(operation: operation, status: RESULTS_AVAILABLE)
    def deployedConfiguration = new OperationConfiguration(protocolInstance: protocolInstance)
    operation.deployedConfiguration = deployedConfiguration

    and:
    operationRepository.findByClientId(clientId) >> Optional.of(operation)
    tallyStatusVoMapper.map(protocolInstance) >> new TallyArchiveStatusVo(TallyArchiveStatusVo.State.CREATED, "/archive.zip")

    when:
    def result = service.getTallyArchiveStatus(clientId)

    then:
    result.state == TallyArchiveStatusVo.State.CREATED
    result.tallyArchiveLocation == "/archive.zip"
  }

  def "getTallyArchiveStatus should retrieve the status even if the protocol instance is not ready for tallying"() {
    given:
    def operation = new Operation(clientId: clientId)
    def protocolInstance = new ProtocolInstance(operation: operation, status: CREDENTIALS_GENERATED)
    def deployedConfiguration = new OperationConfiguration(protocolInstance: protocolInstance)
    operation.deployedConfiguration = deployedConfiguration

    and:
    operationRepository.findByClientId(clientId) >> Optional.of(operation)
    tallyStatusVoMapper.map(protocolInstance) >> new TallyArchiveStatusVo(TallyArchiveStatusVo.State.NOT_REQUESTED, null)

    when:
    def result = service.getTallyArchiveStatus(clientId)

    then:
    result.state == TallyArchiveStatusVo.State.NOT_REQUESTED
    result.tallyArchiveLocation == null
  }

  def "getTallyArchiveStatus should fail the deployed configuration has not yet initialize a protocol instance"() {
    given:
    def operation = new Operation(deployedConfiguration: new OperationConfiguration())
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    when:
    service.getTallyArchiveStatus(clientId)

    then:
    thrown EntityNotFoundException
  }
}
