/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.exception.InvalidApplicationConfigurationException
import com.google.common.io.Files
import java.nio.file.Paths
import spock.lang.Specification

class OutputFilesManagerTest extends Specification {

  private OutputFilesManager outputFilesManager

  def "getOutputPath should return the correct directory"() {
    given:
    File f = Files.createTempDir();
    outputFilesManager = new OutputFilesManager(f.toPath())
    def protocol = new ProtocolInstance()
    protocol.protocolId = "some-protocol-id"

    when:
    def path = outputFilesManager.getOutputPath(protocol)

    then:
    path.toString() == f.toPath().toString() + File.separator + "protocol" + File.separator + "some-protocol-id"
  }

  def "getOutputPath should fail when the output directory does not exist"() {
    given:
    outputFilesManager = new OutputFilesManager(Paths.get("does not exist"))
    def protocol = new ProtocolInstance()

    when:
    def path = outputFilesManager.getOutputPath(protocol)

    then:
    thrown(InvalidApplicationConfigurationException)
  }

  def "deleteAllFiles should fail if the outputFileManager is ill-configured"() {
    given:
    outputFilesManager = new OutputFilesManager(Paths.get(""))

    when:
    outputFilesManager.deleteAllFiles(null)

    then:
    InvalidApplicationConfigurationException ex = thrown()
    ex.message == "No base directory for the output files of the protocol instances has been configured"
  }

   def "deleteAllFiles should physically delete the directory of the protocol instance"() {
    given:
    def baseDir = Files.createTempDir()
    outputFilesManager = new OutputFilesManager(Paths.get(baseDir.getAbsolutePath()))

    def allProtocolsDir = Paths.get(baseDir.getAbsolutePath(), "protocol")
    allProtocolsDir.toFile().mkdir()

    def someProtocolDir = Paths.get(allProtocolsDir.toFile().getAbsolutePath(), "someProtocolId")
    someProtocolDir.toFile().mkdir()

    def protocol = new ProtocolInstance()
    protocol.protocolId = "someProtocolId"

    when:
    outputFilesManager.deleteAllFiles(protocol)

    then:
    allProtocolsDir.toFile().exists()
    !someProtocolDir.toFile().exists()
  }

  def "deleteAllFiles should not fail if the directory of the protocol instance does not exist"() {
    given:
    def baseDir = Files.createTempDir()
    outputFilesManager = new OutputFilesManager(Paths.get(baseDir.getAbsolutePath()))

    def protocol = new ProtocolInstance()
    protocol.id = 1
    protocol.protocolId = "someProtocolId"

    outputFilesManager.deleteAllFiles(protocol)
  }

}
