/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol

import ch.ge.ve.chvote.pactback.service.protocol.vo.DeadLetterEventVo
import java.nio.charset.StandardCharsets
import java.time.LocalDateTime
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageProperties
import spock.lang.Specification

class DeadLetterListenerTest extends Specification {

  def service = Mock(DeadLetterEventService)
  def listener = new DeadLetterListener(service)

  def "processDeadLetterEvent should fetch all available information"() {
    given:
    def headers = [
        "x-original-exchange"   : 'my_channel',
        "x-original-routingKey" : 'NYSE',
        "protocol-id"           : 'protocol #1',
        "__TypeId__"            : 'ch.ge.ve.SomeEvent',
        "x-exception-message"   : 'for test purposes',
        "x-exception-stacktrace": "A not so long stack trace...",
        "creation-date"         : '2018-04-29T15:39:56'
    ]
    def properties = Mock(MessageProperties)
    properties.headers >> headers
    properties.contentType >> "application/json"
    properties.contentEncoding >> StandardCharsets.UTF_8.toString()
    def message = Mock(Message)
    message.messageProperties >> properties
    message.body >> "Hello world!".getBytes(StandardCharsets.UTF_8)

    when:
    listener.processDeadLetterEvent(message)

    then:
    1 * service.save(_) >> { DeadLetterEventVo event ->
      assert event.protocolId == "protocol #1"
      assert event.originalChannel == "my_channel"
      assert event.originalRoutingKey == "NYSE"
      assert event.type == "ch.ge.ve.SomeEvent"
      assert event.exceptionMessage == "for test purposes"
      assert event.exceptionStacktrace == "A not so long stack trace..."
      assert event.creationDate == LocalDateTime.of(2018, 4, 29, 15, 39, 56)
      assert event.pickupDate != null
      assert event.content == "Hello world!"
      assert event.contentType == "application/json"
    }
  }
}
