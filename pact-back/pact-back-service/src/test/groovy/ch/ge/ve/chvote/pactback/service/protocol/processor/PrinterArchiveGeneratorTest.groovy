/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor

import ch.ge.ve.chvote.pactback.contract.printerarchive.ElectionConfigurationVo
import ch.ge.ve.chvote.pactback.contract.printerarchive.PrinterOperationConfigurationVo
import ch.ge.ve.model.convert.api.VoterConverter
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey
import ch.ge.ve.protocol.model.PrintingAuthority
import ch.ge.ve.protocol.model.PrintingAuthorityForVerification
import com.fasterxml.jackson.databind.ObjectMapper
import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDateTime
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class PrinterArchiveGeneratorTest extends Specification {

  private List<PrintingAuthority> testPrinters = ["pA1", "pA2"].collect {
    new PrintingAuthorityForVerification(it)
  }.asImmutable()

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  VoterConverter voterConverter
  ObjectMapper objectMapper
  PrinterArchiveGenerator printerArchiveGenerator
  PrinterArchiveGenerator.Packing packing

  Path outputDir
  
  void setup() {
    outputDir = temporaryFolder.newFolder("printer-archive-generator-test").toPath()

    voterConverter = Mock()
    objectMapper = Mock()

    printerArchiveGenerator = new PrinterArchiveGenerator(voterConverter, objectMapper)
    // Base for our tests : the "printer dirs" must exists
    packing = printerArchiveGenerator.prepareForPrinters(outputDir, testPrinters)
  }

  def getPrintersDirectory() {
    return outputDir.resolve(PrinterArchiveGenerator.@PRINTERS_ROOT_FOLDER_NAME)
  }

  def getCommonDirectory() {
    return outputDir.resolve(PrinterArchiveGenerator.@COMMON_FOLDER_NAME)
  }

  static writeToGivenStream(Object args, String value) {
    def stream = args[0] as OutputStream
    def writer = new BufferedWriter(new OutputStreamWriter(stream))
    writer.write value
    [writer, stream]*.close()
  }

  def "'prepareGeneration' should generate one directory per printer and a common directory"() {
    expect:
    outputDir.toFile().list().sort() == ["commonFiles", "printerFiles"]
    getPrintersDirectory().toFile().list().sort() == ["printer-archive-pA1", "printer-archive-pA2"]
  }

  def "setPublicParameters should create a public parameters file into the common folder"() {
    given: "a public parameters value"
    def publicParameters = '{"publicParameters": "some value"}"'

    when: "setting the public parameters"
    packing.setPublicParameters(publicParameters)

    then: "the parameters createOperationReferenceFile should be written once in the common directory"
    def path = getCommonDirectory().resolve("public-parameters.json")
    path.text == '{"publicParameters": "some value"}"'

    and: 'the printer folders should have remained untouched'
    getPrintersDirectory().resolve("printer-archive-pA1").toFile().list().length == 0
    getPrintersDirectory().resolve("printer-archive-pA2").toFile().list().length == 0
  }

  def "setElectionSet should create an election set file into the common folder"() {
    given: "an election set value"
    def electionSet = new ElectionSetWithPublicKey([], [], [], 1L, 1)
    objectMapper.writeValue(_ as OutputStream, electionSet) >> { args ->
      writeToGivenStream(args, '{"electionSet": "some value"}')
    }

    when: "setting the election set"
    packing.setElectionSet(electionSet)

    then: "the election set createOperationReferenceFile should be written once in the common directory"
    def path = getCommonDirectory().resolve("election-set.json")
    path.text == '{"electionSet": "some value"}'

    and: 'the printer folders should have remained untouched'
    getPrintersDirectory().resolve("printer-archive-pA1").toFile().list().length == 0
    getPrintersDirectory().resolve("printer-archive-pA2").toFile().list().length == 0
  }

  def "setElectionConfigurations should write election configurations file into the common folder"() {
    given: "election configurations"
    def electionConfigurations = [new ElectionConfigurationVo("1", false), new ElectionConfigurationVo("2", true)]
    objectMapper.writeValue(_ as OutputStream, electionConfigurations) >> { args ->
      writeToGivenStream(args, '["1": false, "2": true]')
    }

    when: "setting the election configurations"
    packing.setElectionConfigurations(electionConfigurations)

    then: "the election configurations createOperationReferenceFile should be written once in the common directory"
    def path = getCommonDirectory().resolve("election-configurations.json")
    path.text == '["1": false, "2": true]'

    and: 'the printer folders should have remained untouched'
    getPrintersDirectory().resolve("printer-archive-pA1").toFile().list().length == 0
    getPrintersDirectory().resolve("printer-archive-pA2").toFile().list().length == 0
  }

  def "setOperationConfiguration should write the operation configuration file into the common folder"() {
    given: "an operation configuration for a printer"
    def operationConfiguration = new PrinterOperationConfigurationVo("2018VP", "Test card")
    objectMapper.writeValue(_ as OutputStream, operationConfiguration) >> { args ->
      writeToGivenStream(args, '{"operationName": "2018VP", "votingCardLabel": "Test card"}')
    }

    when: "setting the operation configuration"
    packing.setOperationConfiguration(operationConfiguration)

    then: "the operation configuration createOperationReferenceFile should be written once in the common directory"
    def path = getCommonDirectory().resolve("operation-configuration.json")
    path.text == '{"operationName": "2018VP", "votingCardLabel": "Test card"}'

    and: 'the printer folders should have remained untouched'
    getPrintersDirectory().resolve("printer-archive-pA1").toFile().list().length == 0
    getPrintersDirectory().resolve("printer-archive-pA2").toFile().list().length == 0
  }

  def "writeRemappedVoterFiles should use the voter converter to remap the voter files"() {
    given: "a printing authority list"
    def byMunicipality = [] as Map<Integer, String>
    def byCanton = [] as Map<String, String>
    def nbrOfVotersByPrinter = [] as Map<String, Long>

    and: 'the converter writes directly each key (printer) to the value (outputStream)'
    1 * voterConverter.remapByPrinter(_, byMunicipality, byCanton, nbrOfVotersByPrinter, _) >> { args ->
      def outputStreamMap = args[0] as Map<String, OutputStream>
      outputStreamMap.each { it.getValue().write(it.getKey().getBytes()) }
    }

    when:
    packing.writeRemappedVoterFiles(byMunicipality, byCanton, nbrOfVotersByPrinter, new InputStream[0])

    then:
    def pA1Xml = getPrintersDirectory().resolve("printer-archive-pA1").resolve("eCH-0045.xml")
    pA1Xml.text == "pA1"

    def pA2Xml = getPrintersDirectory().resolve("printer-archive-pA2").resolve("eCH-0045.xml")
    pA2Xml.text == "pA2"
  }

  def "setOnePrinterFile should correctly move the expected file to the expected path"() {
    given: "some input printer createOperationReferenceFile"
    def epfFilename = "pA1_CC2_1-250.epf"
    def fromPath = temporaryFolder.newFolder("another-test-dir").toPath().resolve(epfFilename)
    fromPath.write("epf createOperationReferenceFile content")

    when:
    packing.setOnePrinterFile(fromPath)

    then: "the original createOperationReferenceFile no longer exists"
    fromPath.toFile().exists() == Boolean.FALSE

    and: "it has been copied at the expected location"
    def targetPath = getPrintersDirectory().resolve("printer-archive-pA1").resolve(epfFilename)
    targetPath.text == "epf createOperationReferenceFile content"
  }

  def "setOnePrinterFile should decode the printer name according to the file-namer encoding"() {
    given: "an input printer createOperationReferenceFile with escaped hyphen in the printer name"
    def printerName = "boh-vprt"
    def epfFilename = "boh--vprt_CC2_1-250.epf"
    def fromPath = temporaryFolder.newFile(epfFilename).toPath()

    and: "the printer directory exists"
    packing = printerArchiveGenerator.prepareForPrinters(outputDir, [new PrintingAuthorityForVerification(printerName)])
    def printerDirectory = getPrintersDirectory().resolve("printer-archive-$printerName")

    when:
    packing.setOnePrinterFile(fromPath)

    then: "the correct printer directory should have been found"
    Files.exists printerDirectory.resolve(epfFilename)
  }

  def "writeOperationRepositories should write the operation repositories to the common folder"() {
    given: "some sample operation repositories input streams"
    def repositoriesByName = [
            "firstRepository.xml" : new ByteArrayInputStream("first repository".getBytes()),
            "secondRepository.xml": new ByteArrayInputStream("second repository".getBytes())
    ]

    when:
    packing.writeOperationRepositories(repositoriesByName)

    then: "the operation repositories files should be written once in the common directory"
    def list = getCommonDirectory().toFile().list() as List<String>
    list.sort() == ["firstRepository.xml", "secondRepository.xml"]

    getCommonDirectory().resolve("firstRepository.xml").text == "first repository"
    getCommonDirectory().resolve("secondRepository.xml").text == "second repository"

    and: 'the printer folders should have remained untouched'
    getPrintersDirectory().resolve("printer-archive-pA1").toFile().list().length == 0
    getPrintersDirectory().resolve("printer-archive-pA2").toFile().list().length == 0
  }

  def "[Full Process] createPrinterArchives should generate one archive per printer"() {
    given: 'mocked component'
    objectMapper.writeValue(_ as OutputStream, _) >> { args -> writeToGivenStream(args, args[1] as String)}
    voterConverter.remapByPrinter(*_) >> { args ->
      def streams = args[0] as Map<String, OutputStream>
      streams.each { it.value.withCloseable { s -> s.write(it.key.getBytes()) } }
    }

    and: 'a controlled definition of the PrinterArchiveMakers'
    packing.setPublicParameters("public parameters")
    packing.setElectionSet(new ElectionSetWithPublicKey([], [], [], 0, 0))
    packing.setElectionConfigurations([])
    packing.setOperationConfiguration(new PrinterOperationConfigurationVo("test", "cards"))
    packing.writeOperationRepositories(["operation.xml": new ByteArrayInputStream("<empty/>".getBytes())])
    packing.writeRemappedVoterFiles([:], [:], [:], new InputStream[0])

    packing.setOnePrinterFile(temporaryFolder.newFile("pA1_CC0_0-25.epf").toPath())
    packing.setOnePrinterFile(temporaryFolder.newFile("pA2_CC0_0-25.epf").toPath())

    def creationDate = LocalDateTime.of(2018, 9, 23, 14, 1)

    when:
    packing.createPrinterArchives("201809VP", creationDate)

    then: 'one archive should have been created per printer'
    outputDir.resolve("printerFiles").toFile().list().findAll { it.endsWith(".paf") }.sort() == [
            "printer-archive_201809VP_pA1_2018-09-23-14h01m00s.paf", "printer-archive_201809VP_pA2_2018-09-23-14h01m00s.paf"
    ]
    // No need to verify the content : we trust PrinterArchiveMaker for that
  }
}
