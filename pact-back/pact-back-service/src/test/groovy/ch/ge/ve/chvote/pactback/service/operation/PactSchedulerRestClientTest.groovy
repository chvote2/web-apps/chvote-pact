/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess

import java.time.LocalDate
import java.time.LocalDateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.web.client.RestClientException
import spock.lang.Specification

/**
 * Simple tests of the behaviour against a mocked PACT-Scheduler server
 */
@ContextConfiguration(classes = LocalMockedTestContext)
@RestClientTest
class PactSchedulerRestClientTest extends Specification {

  @Configuration
  static class LocalMockedTestContext {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Bean PactSchedulerRestClient pactSchedulerRestClient(RestTemplateBuilder builder) {
      return new PactSchedulerRestClient("", builder)
    }
  }

  @Autowired
  private PactSchedulerRestClient client

  @Autowired
  private MockRestServiceServer server

  def "Register should call the REST endpoint"(HttpStatus returnedStatus) {
    given:
    def operationBusinessId = "my-test-operation"
    def scheduledTime = LocalDate.now().atTime(18, 30).plusDays(5)

    expect:
    server.expect(requestTo("/tasks/operations/$operationBusinessId?date=$scheduledTime"))
            .andExpect(method(HttpMethod.PUT))
            .andRespond(withStatus(returnedStatus))

    client.register(operationBusinessId, scheduledTime)
    server.verify()

    where:
    returnedStatus << [HttpStatus.CREATED, HttpStatus.OK]
  }

  def "Register - on CONFLICT the exception should be propagated"() {
    given:
    def operationBusinessId = "my-test-operation"
    def scheduledTime = LocalDate.now().atTime(18, 30).plusDays(5)

    and:
    server.expect(requestTo("/tasks/operations/$operationBusinessId?date=$scheduledTime"))
            .andExpect(method(HttpMethod.PUT))
            .andRespond(withStatus(HttpStatus.CONFLICT))

    when:
    client.register(operationBusinessId, scheduledTime)

    then:
    def ex = thrown RestClientException
    ex.message == '409 Conflict'
  }

  def "Register - parameters should be checked against null"(String clientId, LocalDateTime scheduledTime) {
    when:
    client.register(clientId, scheduledTime)

    then:
    thrown NullPointerException

    where:
    clientId  | scheduledTime
    "some-id" | null
    null      | LocalDateTime.now().plusDays(5)
  }

  def "Unregister should call the REST endpoint"() {
    given:
    def operationBusinessId = "my-test-operation"

    expect:
    server.expect(requestTo("/tasks/operations/$operationBusinessId"))
            .andExpect(method(HttpMethod.DELETE))
            .andRespond(withSuccess())

    client.unregister(operationBusinessId)
    server.verify()
  }

  def "Unregister a non-existing operation should not fail"() {
    given:
    def operationBusinessId = "my-test-operation"

    and:
    server.expect(requestTo("/tasks/operations/$operationBusinessId"))
            .andExpect(method(HttpMethod.DELETE))
            .andRespond(withStatus(HttpStatus.NOT_FOUND))

    when:
    client.unregister(operationBusinessId)

    then:
    noExceptionThrown()
    server.verify()
  }

  def "Unregister - other server error should be propagated"() {
    given:
    def operationBusinessId = "my-test-operation"

    and:
    server.expect(requestTo("/tasks/operations/$operationBusinessId"))
            .andExpect(method(HttpMethod.DELETE))
            .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR))

    when:
    client.unregister(operationBusinessId)

    then:
    def ex = thrown RestClientException
    ex.message == '500 Internal Server Error'
  }

  def "Unregister - parameter should be checked against null"() {
    when:
    client.unregister(null)

    then:
    thrown NullPointerException
  }
}
