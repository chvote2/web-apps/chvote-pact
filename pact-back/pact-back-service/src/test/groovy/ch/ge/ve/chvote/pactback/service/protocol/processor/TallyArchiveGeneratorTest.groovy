/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor

import ch.ge.ve.protocol.support.ElectionVerificationDataWriterToFiles
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.zip.ZipFile
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class TallyArchiveGeneratorTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  ObjectMapper objectMapper

  TallyArchiveGenerator tallyArchiveGenerator

  def setup() {
    objectMapper = Mock()
    tallyArchiveGenerator = new TallyArchiveGenerator(objectMapper)
  }

  def "createTallyArchive - should create a valid tally archive in the given directory"() {
    given:
    def electionDataWriter = Mock(ElectionVerificationDataWriterToFiles)
    def outputDirectory = temporaryFolder.newFolder().toPath()
    def repositoriesByName = [
            "firstRepository.xml" : new ByteArrayInputStream("first repository".getBytes()),
            "secondRepository.xml": new ByteArrayInputStream("second repository".getBytes())
    ]
    def votersPerCountingCircles = []

    when:
    def result = tallyArchiveGenerator.createTallyArchive(
            outputDirectory,
            electionDataWriter,
            "VP201908",
            repositoriesByName,
            votersPerCountingCircles
    )

    then:
    1 * electionDataWriter.getPublicParametersFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getElectionSetFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getPrimesFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getGeneratorsFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getPublicKeyPartsFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getPublicCredentialsFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getBallotsFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getConfirmationsFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getShufflesFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getShuffleProofsFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getPartialDecryptionsFilePath() >> temporaryFolder.newFile().toPath()
    1 * electionDataWriter.getDecryptionProofsFilePath() >> temporaryFolder.newFile().toPath()

    result != null
    result.fileName.toString().startsWith("tally-archive_VP201908")

    new ZipFile(result.toFile())
            .entries()
            .collect { entry -> entry.getName() }
            .containsAll(["firstRepository.xml",
                          "secondRepository.xml",
                          "ballots.json",
                          "confirmations.json",
                          "counting-circles.json",
                          "election-set-for-verification.json",
                          "generators.json",
                          "partial-decryption-proofs.json",
                          "partial-decryptions.json",
                          "primes.json",
                          "public-credentials.json",
                          "public-key-parts.json",
                          "public-parameters.json",
                          "shuffle-proofs.json",
                          "shuffles.json"])

  }
}
