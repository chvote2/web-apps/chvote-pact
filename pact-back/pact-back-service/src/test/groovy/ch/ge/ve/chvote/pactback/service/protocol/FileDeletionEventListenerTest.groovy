/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol

import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository
import ch.ge.ve.chvote.pactback.repository.protocol.DeadLetterEventRepository
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.protocol.client.ProtocolClientFactory
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolDeletionEvent
import ch.ge.ve.chvote.pactback.service.protocol.processor.OutputFilesManager
import ch.ge.ve.protocol.client.ProtocolClient
import com.fasterxml.jackson.databind.ObjectMapper
import javax.sql.DataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.jdbc.datasource.DriverManagerDataSource
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.transaction.TestTransaction
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification
import spock.mock.DetachedMockFactory

/**
 * Integration test featuring transaction management to test the transactional behaviour between:
 * <ul>
 *   <li>the destruction of the protocol (DB instance + kernel instance) = event producer</li>
 *   <li>and the destruction of the protocol's files = event consumer</li>
 * </ul>
 */
@ContextConfiguration(classes = TestConfig.class)
class FileDeletionEventListenerTest extends Specification {

  /**
   * Message producer.
   */
  @Autowired
  ProtocolInstanceService protocolInstanceService

  /**
   * Message consumer.
   */
  @Autowired
  ProtocolCleanupService protocolCleanupService

  @Autowired
  ProtocolClientFactory protocolClientFactory

  @Transactional
  def "The protocol's files should not be deleted when the deletion of the protocol failed"() {
    given:
    def protocolInstance = new ProtocolInstance(id: 1L)
    protocolInstance.protocolId = "protocol-id"

    assert TestTransaction.isActive()

    when:
    protocolInstanceService.deleteProtocolInstance(protocolInstance)
    TestTransaction.flagForRollback()
    TestTransaction.end()

    then:
    0 * protocolCleanupService.deleteProtocolFiles(*_)
  }

  @Transactional
  def "The protocol's files should be deleted when the deletion of the protocol succeeded"() {
    given:
    def protocolInstance = new ProtocolInstance(id: 1)
    protocolInstance.protocolId = "protocol-id"

    def client = Mock(ProtocolClient)
    protocolClientFactory.createClient(protocolInstance.protocolId) >> client

    def event = new ProtocolDeletionEvent(protocolInstance)

    assert TestTransaction.isActive()

    when:
    protocolInstanceService.deleteProtocolInstance(protocolInstance)
    TestTransaction.flagForCommit()
    TestTransaction.end()

    then:
    // we don't use the following unreliable interaction: just replace 'protocol-id' with 'protocol-id-WRONG' below
    // and the test will still (and wrongly) succeed, although it will log an exception
//    1 * protocolCleanupService.deleteProtocolFiles(*_) >> { arguments ->
//      assert (arguments[0] as ProtocolDeletionEvent).protocol.protocolId == 'protocol-id'
//    }
    // so we use:
    1 * protocolCleanupService.deleteProtocolFiles(event)
  }

  @TestConfiguration
  @EnableTransactionManagement
  static class TestConfig {

    private final mockFactory = new DetachedMockFactory()

    @Autowired
    ApplicationEventPublisher eventPublisher

    @Bean
    ProtocolInstanceService protocolInstanceService() {
      return new ProtocolInstanceService(protocolInstanceRepository(), deadLetterEventRepository(),
              protocolCleanupService(), voterRepository(), eventPublisher, objectMapper(), "TestKey")
    }

    @Bean
    ProtocolInstanceRepository protocolInstanceRepository() {
      return mockFactory.Mock(ProtocolInstanceRepository)
    }

    @Bean
    VoterRepository voterRepository() {
      return mockFactory.Mock(VoterRepository)
    }

    @Bean
    DeadLetterEventRepository deadLetterEventRepository() {
      return mockFactory.Mock(DeadLetterEventRepository)
    }

    @Bean
    ProtocolCleanupService protocolCleanupService() {
      return mockFactory.Mock(ProtocolCleanupService)
    }

    @Bean
    ProtocolClientFactory protocolClientFactory() {
      return mockFactory.Mock(ProtocolClientFactory)
    }

    @Bean
    OutputFilesManager outputFilesManager() {
      return mockFactory.Mock(OutputFilesManager)
    }

    @Bean
    ObjectMapper objectMapper() {
      return mockFactory.Mock(ObjectMapper)
    }

    @Bean
    PlatformTransactionManager platformTransactionManager() {
      return new DataSourceTransactionManager(datasource())
    }

    @Bean
    DataSource datasource() {
      DriverManagerDataSource dataSource = new DriverManagerDataSource()
      dataSource.setDriverClassName("org.h2.jdbcx.JdbcDataSource")
      dataSource.setUrl("jdbc:h2:mem:test;Mode=Oracle")
      dataSource.setUsername("h2_test")
      dataSource.setPassword("h2_test")
      return dataSource
    }
  }

}
