/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation

import ch.ge.ve.chvote.pact.b2b.client.model.Canton
import ch.ge.ve.chvote.pact.b2b.client.model.Lang
import ch.ge.ve.chvote.pact.b2b.client.model.OperationType
import ch.ge.ve.chvote.pactback.fixtures.repository.Operations
import ch.ge.ve.chvote.pactback.fixtures.repository.VoterEntities
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.BallotDocumentation
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.HighlightedQuestion
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.InformationAttachment
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.VotersRegisterFile
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.ElectionSiteConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException
import ch.ge.ve.protocol.model.DomainOfInfluence
import java.time.LocalDateTime
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class ProtocolEndpointSupportServiceTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  def voterRepository = Mock(VoterRepository)
  def protocolInstanceRepository = Mock(ProtocolInstanceRepository)
  def service = new ProtocolEndpointSupportService(voterRepository, protocolInstanceRepository)

  def "getVotersByProtocolId should retrieve all the voters associated to the given protocol id"() {
    given:
    voterRepository.findByProtocolInstance_ProtocolId("protocolId") >> [
            VoterEntities.voterEntity()
    ].stream()

    when:
    def result = service.getVotersByProtocolId("protocolId")

    then:
    result.size() == 1
    result[0].localPersonId == "fd34gb"
    result[0].voterIndex == 1
    result[0].countingCircle.id == 1
    result[0].countingCircle.businessId == "6621"
    result[0].countingCircle.name == "Genève"
    result[0].identificationCodeHash == "TODO"
    result[0].voterBirthYear == 1998
    result[0].voterBirthMonth == 7
    result[0].voterBirthDay == 15
    result[0].domainOfInfluenceIds == [new DomainOfInfluence("GE")]
  }


  def "getOperationConfigurationByProtocolId should return the configuration associated to the given protocol id"() {

    given:

    def operation = Operations.failedOnResultsGeneration()
    protocolInstanceRepository.getByProtocolId("protocolId") >>
            operation
                    .getDeployedConfiguration()
                    .map({ o -> o.fetchProtocolInstance() })
    operation.getDeployedConfiguration().get().operationReferenceFiles = [
            createOperationReferenceFile(1, "repositoryVotation.xml", AttachmentType.OPERATION_REPOSITORY_VOTATION),
            createOperationReferenceFile(2, "repositoryElection.xml", AttachmentType.OPERATION_REPOSITORY_ELECTION)
    ]


    when:
    def result = service.getOperationConfigurationByProtocolId("protocolId")

    then:
    result.canton == Canton.GE
    result.operationDate != null
    result.operationDate == LocalDateTime.of(2018, 8, 8, 12, 0)
    result.longLabel == "Fixture that failed on results generation"
    result.shortLabel == "failedOnResultsGeneration"
    result.defaultLanguage == Lang.FR
    result.simulationName == "Simulation of failedOnResultsGeneration fixture"
    result.type == OperationType.SIMULATION
    result.votingCardLabel == "Fixture cards"
    result.votingPeriod.openingDate == LocalDateTime.of(2018, 7, 25, 0, 0)
    result.votingPeriod.closingDate == LocalDateTime.of(2018, 8, 8, 18, 0)
    result.votingPeriod.gracePeriod == 30
    result.electionRepositoryIds == [2l]
    result.votationRepositoryIds == [1l]
  }


  def "getOperationConfigurationByProtocolId should return no voting period if operation is in TEST"() {
    given:
    def operation = Operations.createdTestConfiguration()

    protocolInstanceRepository.getByProtocolId("protocolId") >>
            operation.getConfigurationInTest().map({ o -> o.fetchProtocolInstance() })
    operation.getConfigurationInTest().get().operationReferenceFiles = []

    when:
    def result = service.getOperationConfigurationByProtocolId("protocolId")

    then:
    result.type == OperationType.TEST
    result.votingPeriod == null
  }


  OperationReferenceFile createOperationReferenceFile(int id, String filename, AttachmentType attachmentType) {
    OperationReferenceFile file = new OperationReferenceFile()
    file.id = id
    file.type = attachmentType
    file.file = getClass().getResourceAsStream("/protocol-endpoint-support-test/" + filename).bytes
    file.name = filename
    return file
  }


  VotersRegisterFile createOperationRegister(String filename) {
    VotersRegisterFile file = new VotersRegisterFile()
    file.type = AttachmentType.OPERATION_REGISTER
    file.file = getClass().getResourceAsStream("/protocol-endpoint-support-test/" + filename).bytes
    file.name = filename
    return file
  }

  InformationAttachment createInformationAttachmentFile(String filename, AttachmentType attachmentType) {
    InformationAttachment file = new InformationAttachment()
    file.type = attachmentType
    file.file = getClass().getResourceAsStream("/protocol-endpoint-support-test/" + filename).bytes
    file.name = filename
    return file
  }


  def "GetVotingSiteConfiguration should return valid configuration for instance id"() {
    given:
    aSimpleTestOperation()

    when:
    def result = service.getVotingSiteConfigurationByProtocolId("protocolId")

    then:
    result.highlightedQuestions.label == ["question"]
    result.highlightedQuestions.lang == [Lang.FR]
    result.highlightedQuestions.attachmentId == [4]
    result.votationBallotConfigList.ballotDocumentations.label == [["label"]]
    result.votationBallotConfigList.ballotDocumentations.attachmentId == [[2]]
    result.electionGroupBallotConfigList.ballotDocumentations.label == [["label-2"]]
    result.electionGroupBallotConfigList.ballotDocumentations.attachmentId == [[3]]
    result.translationsId == 1
  }


  def "getAttachmentByProtocolIdAndAttachmentId should return the attachment identified by id"() {
    given:
    aSimpleTestOperation()

    when:
    def result = service.getAttachmentByProtocolIdAndAttachmentId("protocolId", 2) as BallotDocumentation

    then:
    result.label == "label"
  }


  def "getAttachmentByProtocolIdAndAttachmentId should return repository files"() {
    given:
    aSimpleTestOperation()

    when:
    def result = service.getAttachmentByProtocolIdAndAttachmentId("protocolId", id)

    then:
    result.name == name

    where:
    _ | id | name
    _ | 88 | "repositoryVotation.xml"
    _ | 89 | "repositoryElection.xml"
  }

  def "getAttachmentByProtocolIdAndAttachmentId should not return the register file"() {
    given:
    aSimpleTestOperation()

    when:
    service.getAttachmentByProtocolIdAndAttachmentId("protocolId", id)

    then:
    def ex = thrown(EntityNotFoundException);
    ex.message == "Could not find attachment for protocol instance :protocolId and attachment id " + id

    where:
    _ | id
    _ | 90
    _ | 91
  }

  def "getAttachmentByProtocolIdAndAttachmentId should thrown an exception if no attachment was found"() {
    given:
    aSimpleTestOperation()

    when:
    service.getAttachmentByProtocolIdAndAttachmentId("protocolId", -1)

    then:
    def ex = thrown(EntityNotFoundException)
    ex.message == "Could not find attachment for protocol instance :protocolId and attachment id -1"
  }


  def aSimpleTestOperation() {
    ProtocolInstance protocolInstance = new ProtocolInstance()
    protocolInstance.protocolId = "1000"
    OperationConfiguration configuration = new OperationConfiguration()
    configuration.canton = "GE"
    configuration.electionSiteConfigurations = [electionSiteConfiguration()]
    protocolInstance.setConfiguration(configuration)
    protocolInstance.environment = ProtocolEnvironment.TEST

    protocolInstance.operation = new Operation()

    protocolInstanceRepository.getByProtocolId("protocolId") >> Optional.of(protocolInstance)

    configuration.operationReferenceFiles = [
            createOperationReferenceFile(88, "repositoryVotation.xml", AttachmentType.OPERATION_REPOSITORY_VOTATION),
            createOperationReferenceFile(89, "repositoryElection.xml", AttachmentType.OPERATION_REPOSITORY_ELECTION),
            createOperationReferenceFile(90, "register.xml", AttachmentType.OPERATION_REGISTER),
    ]

    def votingMaterialsConfiguration = new VotingMaterialsConfiguration()
    protocolInstance.operation.votingMaterialsConfiguration = votingMaterialsConfiguration;
    votingMaterialsConfiguration.registers = [createOperationRegister("register.xml")]
    votingMaterialsConfiguration.registers[0].id = 91




    def translation = new InformationAttachment()
    translation.id = 1
    translation.file = "translation".bytes
    translation.type = AttachmentType.OPERATION_TRANSLATION_FILE
    configuration.informationAttachments.add(translation)

    def votationBallotDocumentation = new BallotDocumentation()
    votationBallotDocumentation.id = 2
    votationBallotDocumentation.label = "label"
    votationBallotDocumentation.ballot = "202012SGA-COM-6624"
    votationBallotDocumentation.language = "FR"

    def electionBallotDocumentation = new BallotDocumentation()
    electionBallotDocumentation.id = 3
    electionBallotDocumentation.label = "label-2"
    electionBallotDocumentation.ballot = "election-ballot"
    electionBallotDocumentation.language = "FR"

    configuration.ballotDocumentations = [votationBallotDocumentation, electionBallotDocumentation]

    def question = new HighlightedQuestion()
    question.id = 4
    question.question = "question"
    question.language = "FR"
    configuration.highlightedQuestions = [question]
  }

  ElectionSiteConfiguration electionSiteConfiguration() {
    def electionSiteConfiguration = new ElectionSiteConfiguration()
    electionSiteConfiguration.ballot = "election-ballot"
    electionSiteConfiguration.displayedColumnsOnVerificationTable = ["col1", "col2"]
    electionSiteConfiguration.columnsOrderOnVerificationTable = ["col1"]
    return electionSiteConfiguration
  }


}
