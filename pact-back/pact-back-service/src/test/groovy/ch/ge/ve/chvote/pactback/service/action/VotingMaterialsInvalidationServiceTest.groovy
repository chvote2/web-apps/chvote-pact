/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import ch.ge.ve.chvote.pactback.repository.action.materials.VotingMaterialsInvalidationActionRepository
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsInvalidation
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.repository.user.UserRepository
import ch.ge.ve.chvote.pactback.repository.user.entity.User
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyExistsException
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionNotFoundException
import ch.ge.ve.chvote.pactback.service.exception.SelfActionResolutionException
import ch.ge.ve.chvote.pactback.service.operation.OperationService
import java.time.LocalDateTime
import java.util.stream.Stream
import spock.lang.Specification

class VotingMaterialsInvalidationServiceTest extends Specification {
  private VotingMaterialsInvalidationService service
  private UserRepository userRepository
  private VotingMaterialsInvalidationActionRepository actionRepository
  private VotingMaterialsConfigurationRepository materialsRepository
  private OperationService operationService

  void setup() {
    userRepository = Mock(UserRepository)
    actionRepository = Mock(VotingMaterialsInvalidationActionRepository)
    materialsRepository = Mock(VotingMaterialsConfigurationRepository)
    operationService = Mock(OperationService)
    service = new VotingMaterialsInvalidationService(userRepository, materialsRepository, actionRepository, operationService)
  }


  def "GetPendingPrivilegedAction should return pending actions"() {
    given:
    def action = buildVotingMaterialsConfiguration(1L, true).invalidationAction
    def operation = buildOperation(action.get().votingMaterialsConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation

    when:
    def pendingAction = service.getPendingPrivilegedAction(1L)

    then:
    1 * actionRepository.findById(1L) >> action
    pendingAction.status == "PENDING"
  }


  def "GetByBusinessId should retrieve existing pending action based on business id"() {
    given:
    def action = buildVotingMaterialsConfiguration(1L, true).invalidationAction
    def operation = buildOperation(action.get().votingMaterialsConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation
    materialsRepository.getOne(1l) >> action.get().votingMaterialsConfiguration

    when:
    def pendingAction = service.getByBusinessId(1L)

    then:
    pendingAction.status == "PENDING"
  }

  def "GetByBusinessId should failed on invalidation that has been approved"() {
    given:
    def action = buildVotingMaterialsConfiguration(1L, true).invalidationAction.get()
    action.setStatus(PrivilegedAction.Status.APPROVED)
    def operation = buildOperation(action.votingMaterialsConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation
    materialsRepository.getOne(1l) >> action.votingMaterialsConfiguration

    when:
    service.getByBusinessId(1L)

    then:
    def exception = thrown(PrivilegedActionAlreadyProcessedException);
    exception.message == "Action with id 1 has already been processed and cannot be displayed"
  }

  def "GetByBusinessId should return a new instance on invalidation that has been refused"() {
    given:
    def action = buildVotingMaterialsConfiguration(1L, true).invalidationAction.get()
    action.setStatus(PrivilegedAction.Status.REJECTED)
    def operation = buildOperation(action.votingMaterialsConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation
    materialsRepository.getOne(1l) >> action.votingMaterialsConfiguration

    when:
    def newAction = service.getByBusinessId(1L)

    then:
    newAction.status == "NOT_CREATED"
  }

  def "GetByBusinessId should create a new action based on business id"() {
    given:
    def conf = buildVotingMaterialsConfiguration(1L, false)
    def operation = buildOperation(conf)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation
    materialsRepository.getOne(1l) >> conf

    when:
    def pendingAction = service.getByBusinessId(1L)

    then:
    pendingAction.status == "NOT_CREATED"
  }


  def "CreateAction should allow creation of a new action"() {
    given:
    def user = Mock(User)
    userRepository.findByUsername("requester") >> Optional.of(user)

    def configuration = buildVotingMaterialsConfiguration(100L, false)
    materialsRepository.findById(100L) >> Optional.of(configuration)

    def operation = buildOperation(configuration)
    operationService.getOperationByVotingMaterialsConfigurationId(100L) >> operation

    when:
    service.createAction(100L, "requester")

    then:

    1 * actionRepository.save(*_) >> { arguments ->
      def action = arguments[0] as VotingMaterialsInvalidation
      assert action.status == PrivilegedAction.Status.PENDING
      assert action.votingMaterialsConfiguration == configuration
      assert action.requester == user
    }
  }


  def "CreateAction should allow creation of a new action after a previous refusal"() {
    given:
    def user = Mock(User)
    userRepository.findByUsername("requester") >> Optional.of(user)

    def configuration = buildVotingMaterialsConfiguration(100L, true)
    configuration.getInvalidationAction().get().setStatus(PrivilegedAction.Status.REJECTED);
    materialsRepository.findById(100L) >> Optional.of(configuration)

    def operation = buildOperation(configuration)
    operationService.getOperationByVotingMaterialsConfigurationId(100L) >> operation

    when:
    service.createAction(100L, "requester")

    then:
    1 * actionRepository.save(*_) >> { arguments ->
      def action = arguments[0] as VotingMaterialsInvalidation
      assert action.status == PrivilegedAction.Status.PENDING
      assert action.votingMaterialsConfiguration == configuration
      assert action.requester == user
    }
  }


  def "CreateAction should prevent creation of a new action if the request already exist"() {
    given:
    def user = Mock(User)
    userRepository.findByUsername("requester") >> Optional.of(user)

    def configuration = buildVotingMaterialsConfiguration(100L, true)
    configuration.getInvalidationAction().get().setStatus(status);
    materialsRepository.findById(100L) >> Optional.of(configuration)

    def operation = buildOperation(configuration)
    operationService.getOperationByVotingMaterialsConfigurationId(100L) >> operation

    when:
    service.createAction(100L, "requester")

    then:
    def exception = thrown(PrivilegedActionAlreadyExistsException);
    exception.message == "An action was already created for the VotingMaterialsConfiguration object with ID 100"

    where:
    _ | status
    _ | PrivilegedAction.Status.APPROVED
    _ | PrivilegedAction.Status.PENDING
  }


  def "GetNewOrPendingActions"() {
    given:
    def votingMaterialConfiguration = buildVotingMaterialsConfiguration(1L, true)
    def operation = buildOperation(votingMaterialConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation
    materialsRepository.findAllByInvalidationActionIsNullOrInvalidationActionStatus(PrivilegedAction.Status.PENDING) >>
            Stream.of(votingMaterialConfiguration)

    when:
    def value = service.getNewOrPendingActions()

    then:
    value.id == [1l]
  }

  def "ApproveAction should approve a pending action"() {
    given:
    def action = buildVotingMaterialsConfiguration(1L, true).invalidationAction.get()
    def operation = buildOperation(action.votingMaterialsConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation
    def user = Mock(User)
    userRepository.findByUsername("validator") >> Optional.of(user)
    actionRepository.findById(1L) >> Optional.of(action)

    when:
    service.approveAction(1L, "validator")

    then:
    1 * operationService.deleteDeployedConfigurationInstance(operation)
    1 * actionRepository.save(_) >> { arguments ->
      def savedAction = arguments[0] as VotingMaterialsInvalidation
      assert savedAction.status == PrivilegedAction.Status.APPROVED
      assert savedAction.validator == user
    }
  }

  def "ApproveAction should failed on absence of pending action"() {
    given:
    def user = Mock(User)
    userRepository.findByUsername("validator") >> Optional.of(user)

    actionRepository.findById(1L) >> Optional.empty()

    when:
    service.approveAction(1L, "validator")

    then:
    def exception = thrown(PrivilegedActionNotFoundException)
    exception.message == "No privileged action found for id 1"
  }

  def "ApproveAction should fail if the action status is not PENDING"() {
    given:
    def action = buildVotingMaterialsConfiguration(1L, true).invalidationAction.get()
    action.status = PrivilegedAction.Status.APPROVED
    def operation = buildOperation(action.votingMaterialsConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation
    def user = Mock(User)
    userRepository.findByUsername("validator") >> Optional.of(user)
    actionRepository.findById(1L) >> Optional.of(action)

    when:
    service.approveAction(1L, "validator")

    then:
    0 * operationService.deleteDeployedConfigurationInstance(_)
    0 * actionRepository.save(*_)
    thrown(PrivilegedActionAlreadyProcessedException)
  }


  def "ApproveAction should not approve on same user"() {
    given:
    def action = buildVotingMaterialsConfiguration(1L, true).invalidationAction.get()
    def operation = buildOperation(action.votingMaterialsConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation
    userRepository.findByUsername(action.requester.username) >> Optional.of(action.requester)
    actionRepository.findById(1L) >> Optional.of(action)
    when:
    service.approveAction(1L, action.requester.username)

    then:
    def exception = thrown(SelfActionResolutionException)
    exception.message == "Users may not approve their own privileged actions"
  }


  def "RejectAction should approve a pending action"() {
    given:
    def action = buildVotingMaterialsConfiguration(1L, true).invalidationAction.get()
    def operation = buildOperation(action.votingMaterialsConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation
    def user = Mock(User)
    userRepository.findByUsername("validator") >> Optional.of(user)
    actionRepository.findById(1L) >> Optional.of(action)
    when:
    service.rejectAction(1L, "validator", "reason")

    then:
    0 * operationService.deleteDeployedConfigurationInstance(operation)
    1 * actionRepository.save(_) >> { arguments ->
      def savedAction = arguments[0] as VotingMaterialsInvalidation;
      assert savedAction.status == PrivilegedAction.Status.REJECTED
      assert savedAction.validator == user
    }
  }

  def "RejectAction should failed on absence of pending action"() {
    given:
    def user = Mock(User)
    userRepository.findByUsername("validator") >> Optional.of(user)

    actionRepository.findById(1L) >> Optional.empty()

    when:
    service.rejectAction(1L, "validator", "reason")

    then:
    def exception = thrown(PrivilegedActionNotFoundException)
    exception.message == "No privileged action found for id 1"
  }

  def "RejectAction should not approve on same user"() {
    given:
    def action = buildVotingMaterialsConfiguration(1L, true).invalidationAction.get()
    def operation = buildOperation(action.votingMaterialsConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation
    userRepository.findByUsername(action.requester.username) >> Optional.of(action.requester)
    actionRepository.findById(1L) >> Optional.of(action)
    when:
    service.rejectAction(1L, action.requester.username, "reason")

    then:
    def exception = thrown(SelfActionResolutionException);
    exception.message == "Users may not approve their own privileged actions"
  }


  def buildVotingMaterialsConfiguration(Long id, boolean alreadyHasAction) {
    def votingConf = new VotingMaterialsConfiguration()

    votingConf.id = id
    votingConf.target = DeploymentTarget.SIMULATION
    votingConf.simulationName = "simulation-" + id
    votingConf.registers = new ArrayList<>()
    votingConf.printers = new ArrayList<>()

    if (alreadyHasAction) {
      def action = new VotingMaterialsInvalidation()
      def user = new User()

      action.id = 1L
      action.status = PrivilegedAction.Status.PENDING
      action.votingMaterialsConfiguration = votingConf
      user.username = "Anne Dupont"
      user.id = 1L
      action.requester = user

      votingConf.invalidationAction = action
    }

    return votingConf
  }

  def buildOperation(VotingMaterialsConfiguration configuration) {
    def operation = new Operation()

    operation.votingMaterialsConfiguration = configuration

    def deployedConf = new OperationConfiguration()
    deployedConf.operationName = "some op name"
    deployedConf.operationReferenceFiles = new ArrayList<>()
    deployedConf.setProtocolInstance(new ProtocolInstance())
    operation.deployedConfiguration = deployedConf

    return operation
  }

  def createPrivilegedActionMock(Long id, String username) {
    def user = new User()
    user.id = 1L
    user.username = username

    def configuration = new VotingMaterialsConfiguration()

    def action = Mock(VotingMaterialsInvalidation)
    action.id >> id
    action.requester >> user
    action.votingMaterialsConfiguration >> configuration
    action.creationDate >> LocalDateTime.now()
    action.status >> PrivilegedAction.Status.PENDING
    action.setStatus(_) >> { arguments -> action.status >> arguments[0] }
    action.setRejectionReason(_) >> { arguments -> action.rejectionReason >> arguments[0] }

    return action
  }
}
