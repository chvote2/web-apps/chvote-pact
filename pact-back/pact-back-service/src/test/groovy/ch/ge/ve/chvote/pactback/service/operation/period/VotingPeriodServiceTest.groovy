/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.period

import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo
import ch.ge.ve.chvote.pactback.contract.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.contract.operation.VotingPeriodConfigurationSubmissionVo
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException
import ch.ge.ve.chvote.pactback.service.operation.TimeService
import java.time.LocalDateTime
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import spock.lang.Specification

class VotingPeriodServiceTest extends Specification {
  private LocalDateTime dateNow
  def mapper = Mock(VotingPeriodStatusVoMapper)
  def operationRepository = Mock(OperationRepository)
  VotingPeriodService votingPeriodService

  void setup() {
    dateNow = LocalDateTime.now()
    def timeService = Mock(TimeService)
    timeService.now() >> dateNow

    def chVoteConfigurationProperties = Mock(ChVoteConfigurationProperties)
    chVoteConfigurationProperties.timezone >> "Europe/Zurich"

    def attachmentService = new VotingPeriodAttachmentService(timeService, chVoteConfigurationProperties)
    votingPeriodService = new VotingPeriodService(attachmentService, operationRepository, mapper)
  }

  def "convertToVotingPeriodConfiguration should convert a submission into a VotingPeriodConfiguration"() {
    given:
    def submission = new VotingPeriodConfigurationSubmissionVo()
    submission.setSiteOpeningDate(LocalDateTime.now())
    submission.setSiteClosingDate(LocalDateTime.now())
    submission.setGracePeriod(5)
    submission.setTarget(DeploymentTarget.SIMULATION)

    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    MultipartFile file = new MockMultipartFile("publicKey", "publicKey.txt", null, is)

    AttachmentFileEntryVo attachmentEntryVo = new AttachmentFileEntryVo()
    attachmentEntryVo.setImportDateTime(dateNow)
    submission.setElectoralAuthorityKey(attachmentEntryVo)

    when:
    def config = votingPeriodService.convertToVotingPeriodConfiguration(submission, file)

    then:
    config.siteOpeningDate == submission.siteOpeningDate
    config.siteClosingDate == submission.siteClosingDate
    config.gracePeriod == 5
    config.electoralAuthorityKey.name == 'publicKey'
    config.electoralAuthorityKey.type == AttachmentType.ELECTION_OFFICER_KEY
    config.electoralAuthorityKey.importDate == dateNow
  }

  def "getVotingMaterialsStatusByClientId should returns status if there is a voting period configuration set"() {
    given:
    def operation = new Operation()
    def votingPeriodConfiguration = new VotingPeriodConfiguration()
    operationRepository.findByClientId("clientId") >> Optional.of(operation)
    operation.votingPeriodConfiguration = votingPeriodConfiguration

    mapper.map(operation) >> new VotingPeriodStatusVo("user", LocalDateTime.now(), VotingPeriodStatusVo.State.INITIALIZED, "rejection", "url")
    when:
    def config = votingPeriodService.getVotingPeriodStatusByClientId("clientId")

    then:
    config.state == VotingPeriodStatusVo.State.INITIALIZED
  }

  def "getVotingMaterialsStatusByClientId should fail if there is no voting period configuration set"() {
    given:
    def operation = new Operation()
    operationRepository.findByClientId("clientId") >> Optional.of(operation)

    when:
    votingPeriodService.getVotingPeriodStatusByClientId("clientId")

    then:
    def exception = thrown(EntityNotFoundException)
    exception.message == "No voting period configuration found for clientId [clientId]"
  }


  def "getVotingMaterialsStatusByClientId should fail if there is no configuration set"() {
    given:
    operationRepository.findByClientId("clientId") >> Optional.empty()

    when:
    votingPeriodService.getVotingPeriodStatusByClientId("clientId")

    then:
    def exception = thrown(EntityNotFoundException)
    exception.message == "No operation found for clientId [clientId]"
  }
}
