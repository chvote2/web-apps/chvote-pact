/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action

import ch.ge.ve.chvote.pactback.repository.action.PrivilegedActionRepository
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import ch.ge.ve.chvote.pactback.repository.user.UserRepository
import ch.ge.ve.chvote.pactback.repository.user.entity.User
import java.time.LocalDateTime
import java.util.stream.Stream
import spock.lang.Specification

class PrivilegedActionServiceTest extends Specification {

  private UserRepository userRepository
  private PrivilegedActionRepository repository
  private PrivilegedActionService service

  void setup() {
    userRepository = Mock(UserRepository)
    repository = Mock(PrivilegedActionRepository)
    service = new PrivilegedActionService(userRepository, repository)
  }

  def "actionsPendingApprovalCount should return the number of PENDING actions that the given user can approve"() {
    given:
    def user = new User()
    user.id = 1L
    user.username = "Bart"

    when:
    def count = service.actionsPendingApprovalCount("Bart")

    then:
    1 * userRepository.findByUsername("Bart") >> Optional.of(user)
    1 * repository.countByRequesterIdNotAndStatus(1L, PrivilegedAction.Status.PENDING) >> 42
    count == 42
  }

  def "pendingPrivilegedActions should return the total number of PENDING actions"() {
    given:
    def action1 = createPrivilegedActionMock(1L, 'Bart')
    def action2 = createPrivilegedActionMock(2L, 'Kenny')

    when:
    def pendingPrivilegedActions = service.pendingPrivilegedActions

    then:
    1 * repository.findByStatus(PrivilegedAction.Status.PENDING) >> Stream.of(action1, action2)
    pendingPrivilegedActions.size() == 2
    pendingPrivilegedActions[0].id == 1L
    pendingPrivilegedActions[0].ownerName == 'Bart'
    pendingPrivilegedActions[1].id == 2L
    pendingPrivilegedActions[1].ownerName == 'Kenny'
  }

  def "getPendingAction should return a pending action"() {
    given:
    def action = createPrivilegedActionMock(1L, 'Bart')

    when:
    def pendingAction = service.getPendingPrivilegedAction(1L)

    then:
    1 * repository.findById(1L) >> Optional.of(action)
    pendingAction.status == "PENDING"
  }

  def createPrivilegedActionMock(Long id, String username) {
    def action = Mock(PrivilegedAction)
    def user = new User()
    user.id = 1L
    user.username = username

    action.id >> id
    action.requester >> user
    action.creationDate >> LocalDateTime.now()
    action.status >> PrivilegedAction.Status.PENDING
    action.setStatus(_) >> { arguments -> action.status >> arguments[0] }
    action.setRejectionReason(_) >> { arguments -> action.rejectionReason >> arguments[0] }
    return action
  }

}
