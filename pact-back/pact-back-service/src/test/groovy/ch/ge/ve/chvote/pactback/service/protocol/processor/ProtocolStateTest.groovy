/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor

import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.GENERATING_KEYS
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.INITIALIZING
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.KEYS_GENERATED
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.PUBLIC_PARAMETERS_INITIALIZED

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService
import ch.ge.ve.protocol.client.ProtocolClient
import ch.ge.ve.protocol.support.PublicParametersFactory
import spock.lang.Specification

class ProtocolStateTest extends Specification {

  def protocolInstanceService = Mock(ProtocolInstanceService)

  def publicParametersFactory = Mock(PublicParametersFactory)

  def protocolClient = Mock(ProtocolClient)

  def createStateWithInstanceStatus(ProtocolInstanceStatus status) {
    def instance = new ProtocolInstance()
    instance.status = status

    return new ProtocolState(instance, protocolInstanceService, publicParametersFactory, protocolClient)
  }

  def "IsProtocolInstanceStatusAtOrAfter should return compare statuses"(ProtocolInstanceStatus instance, ProtocolInstanceStatus query, result) {
    expect:
    createStateWithInstanceStatus(instance).isProtocolInstanceStatusAtOrAfter(query) == result

    where:
    instance        | query                         || result
    null            | INITIALIZING                  || Boolean.FALSE
    INITIALIZING    | PUBLIC_PARAMETERS_INITIALIZED || Boolean.FALSE
    GENERATING_KEYS | GENERATING_KEYS               || true
    KEYS_GENERATED  | GENERATING_KEYS               || true
  }
}
