/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.materials

import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialCreationStage
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgress
import java.time.LocalDateTime
import spock.lang.Specification

class ProtocolStageProgressVoMapperTest extends Specification {
  private static final LocalDateTime DATE_CREATED = LocalDateTime.now().minusDays(3)
  private static final LocalDateTime DATE_STARTED = LocalDateTime.now().minusDays(2)
  private static final LocalDateTime DATE_FINISHED = LocalDateTime.now().minusDays(2)
  private static final LocalDateTime NOW = LocalDateTime.now()

  def "should be mapped to ProtocolStageProgressVo"() {
    given:
    def protocolInstance = createProtocolInstance()

    when:
    def result = ProtocolStageProgressVoMapper.mapProgress(protocolInstance)

    then:
    def initProgress = result.find {
      progress -> VotingMaterialCreationStage.INITIALIZE_PARAMETERS == progress.getStage()
    }
    def credentialsProgress = result.find {
      progress -> VotingMaterialCreationStage.REQUEST_PRIVATE_CREDENTIALS == progress.getStage()
    }
    initProgress != null
    credentialsProgress != null
    result.size() == 2
    initProgress.lastModificationDate == NOW
    initProgress.startDate == DATE_STARTED
    initProgress.ratio == 1/5
    initProgress.ccIndex == -1

    credentialsProgress.lastModificationDate == NOW
    credentialsProgress.startDate == NOW
    credentialsProgress.ratio == 0
    credentialsProgress.ccIndex == 0
  }

  def createProtocolInstance() {
    def protocolInstance = new ProtocolInstance()

    protocolInstance.status = ProtocolInstanceStatus.GENERATING_CREDENTIALS
    protocolInstance.statusDate = DATE_CREATED

    def sendPublicParametersProgress = new ProtocolStageProgress()
    sendPublicParametersProgress.stage = ProtocolStage.SEND_PUBLIC_PARAMETERS
    sendPublicParametersProgress.ccIndex = -1
    sendPublicParametersProgress.done = 1
    sendPublicParametersProgress.total = 1
    sendPublicParametersProgress.startDate = DATE_STARTED
    sendPublicParametersProgress.lastModificationDate = DATE_FINISHED
    protocolInstance.progress.add(sendPublicParametersProgress)

    def sendElectionSetProgress = new ProtocolStageProgress()
    sendElectionSetProgress.stage = ProtocolStage.SEND_ELECTION_SET
    sendElectionSetProgress.ccIndex = -1
    sendElectionSetProgress.done = 1
    sendElectionSetProgress.total = 2
    sendElectionSetProgress.startDate = DATE_STARTED
    sendElectionSetProgress.lastModificationDate = NOW
    protocolInstance.progress.add(sendElectionSetProgress)

    def credentialsProgress = new ProtocolStageProgress()
    credentialsProgress.stage = ProtocolStage.REQUEST_PRIVATE_CREDENTIALS
    credentialsProgress.ccIndex = 0
    credentialsProgress.done = 0
    credentialsProgress.total = 10
    credentialsProgress.startDate = NOW
    credentialsProgress.lastModificationDate = NOW
    protocolInstance.progress.add(credentialsProgress)

    return protocolInstance
  }

}
