/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation

import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo
import ch.ge.ve.chvote.pactback.contract.operation.MunicipalityVo
import ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo
import ch.ge.ve.chvote.pactback.contract.operation.PrinterConfigurationVo
import ch.ge.ve.chvote.pactback.contract.operation.TestSitePrinter
import ch.ge.ve.chvote.pactback.contract.operation.VotingMaterialsConfigurationSubmissionVo
import ch.ge.ve.chvote.pactback.contract.operation.VotingPeriodConfigurationSubmissionVo
import ch.ge.ve.chvote.pactback.contract.operation.status.InTestConfigurationStatusVo
import ch.ge.ve.chvote.pactback.fixtures.PrintersPublicKeys
import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeployment
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreation
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsInvalidation
import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.entity.VotingPeriodInitialization
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.PublicKeyFileRepository
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.period.VotingPeriodConfigurationRepository
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.repository.user.entity.User
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.exception.CannotCreateOrUpdateVotingPeriodConfigurationException
import ch.ge.ve.chvote.pactback.service.exception.CannotUpdateVotingMaterialConfigurationException
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException
import ch.ge.ve.chvote.pactback.service.operation.configuration.OperationConfigurationService
import ch.ge.ve.chvote.pactback.service.operation.materials.VotingMaterialsService
import ch.ge.ve.chvote.pactback.service.operation.period.VotingPeriodService
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInitializationService
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolVotingPeriodInitializationService
import ch.ge.ve.chvote.pactback.service.protocol.processor.OutputFilesManager
import ch.ge.ve.chvote.pactback.service.protocol.processor.PrinterArchiveGenerator
import java.nio.file.Paths
import java.time.LocalDateTime
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import spock.lang.Specification

class OperationServiceTest extends Specification {

  private OperationService service
  private OperationRepository operationRepository
  private ProtocolInstanceService protocolInstanceService
  private ProtocolInitializationService protocolInitializationService
  private ProtocolVotingPeriodInitializationService protocolVotingPeriodInitializationService
  private OperationConfigurationService configurationService
  private VotingMaterialsService votingMaterialsService
  private VotingMaterialsConfigurationRepository votingMaterialsConfigurationRepository
  private ChVoteConfigurationProperties configurationProps
  private OutputFilesManager outputFilesManager
  private PrinterArchiveGenerator printerArchiveGenerator
  private VotingPeriodService votingPeriodService
  private VotingPeriodConfigurationRepository votingPeriodConfigurationRepository
  private PublicKeyFileRepository publicKeyFileRepository

  void setup() {
    operationRepository = Mock()
    protocolInstanceService = Mock()
    protocolInitializationService = Mock()
    protocolVotingPeriodInitializationService = Mock()
    configurationService = Mock()
    votingMaterialsService = Mock()
    votingMaterialsConfigurationRepository = Mock()
    configurationProps = Mock()
    configurationProps.pactContextUrl >> "/pact"
    configurationProps.vrIdentificationUrl >> "http://host/chvote-receiver/{0}vote/{1}/{2}/{3}/identification"
    outputFilesManager = Mock()
    outputFilesManager.getOutputPath(*_) >> Paths.get("parent")
    outputFilesManager.relativize(*_) >> { args ->
      args[0]
    }
    printerArchiveGenerator = Mock()
    printerArchiveGenerator.getArchivePath(_, _, _, _) >> Paths.get("parent", "printer")
    votingPeriodService = Mock()
    votingPeriodConfigurationRepository = Mock()
    publicKeyFileRepository = Mock()

    service = new OperationService(operationRepository, protocolInstanceService, protocolInitializationService, protocolVotingPeriodInitializationService,
            new OperationConfigurationStatusVoMapper(outputFilesManager, printerArchiveGenerator, configurationProps), configurationService,
            votingMaterialsService, votingPeriodService, votingMaterialsConfigurationRepository, votingPeriodConfigurationRepository,
            publicKeyFileRepository)
  }

  def "getOperationStatusByClientId should return VALIDATED status"() {
    given:
    def configuration = new OperationConfiguration(id: 9901)
    configuration.operationName = "201801VP"
    configuration.lastChangeUser = "user1"
    configuration.canton = "GE"
    configuration.validationStatus = ValidationStatus.VALIDATED

    def printerConfiguration = new PrinterConfiguration()
    printerConfiguration.printerName = "printer"
    configuration.testPrinter = printerConfiguration

    def instance = new ProtocolInstance()
    configuration.protocolInstance = instance
    instance.configuration = configuration
    instance.environment = ProtocolEnvironment.TEST
    instance.protocolId = "r2332432gdfgf"

    def operation = new Operation(id: 1)
    operation.clientId = "1"
    operation.configurationInTest = configuration

    operationRepository.findByClientId("1") >> Optional.of(operation)

    when:
    def status = service.getOperationStatusByClientId("1")

    then:
    status.inTest.comment == null
    status.inTest.state == InTestConfigurationStatusVo.State.VALIDATED
    status.inTest.lastChangeUser == "user1"
    status.inTest.configurationPageUrl == "/pact/configuration-deployments/9901"
    status.deployed == null
  }

  def "getOperationStatusByClientId should return an operation configuration with a REJECTED action"() {
    given:
    def configuration = new OperationConfiguration(id: 9902)
    configuration.operationName = "201801VP"
    configuration.lastChangeUser = "user1"
    configuration.validationStatus = ValidationStatus.VALIDATED
    configuration.canton = "GE"

    def printerConfiguration = new PrinterConfiguration()
    printerConfiguration.printerName = "printer"
    configuration.testPrinter = printerConfiguration

    def instance = new ProtocolInstance()
    configuration.protocolInstance = instance
    instance.configuration = configuration
    instance.environment = ProtocolEnvironment.TEST
    instance.protocolId = "r2332432gdfgf"

    def action = new ConfigurationDeployment()
    configuration.action = action
    action.status = PrivilegedAction.Status.REJECTED
    action.requester = new User()
    action.requester.username = "user1"
    action.validator = new User()
    action.validator.username = "user2"
    action.rejectionReason = "rejection"

    def operation = new Operation(id: 1)
    operation.clientId = "1"
    operation.configurationInTest = configuration

    operationRepository.findByClientId("1") >> Optional.of(operation)

    when:
    def status = service.getOperationStatusByClientId("1")

    then:
    status.inTest.comment == "rejection"
    status.inTest.state == InTestConfigurationStatusVo.State.DEPLOYMENT_REFUSED
    status.inTest.lastChangeUser == "user2"
    status.inTest.configurationPageUrl == "/pact/configuration-deployments/9902"
    status.deployed == null
  }

  def "getOperationStatusByClientId should return an operation configuration with a APPROVED action"() {
    given:
    def configuration = new OperationConfiguration(id: 9903)
    configuration.operationName = "201801VP"
    configuration.lastChangeUser = "user1"
    configuration.validationStatus = ValidationStatus.VALIDATED
    configuration.canton = "GE"

    def printerConfiguration = new PrinterConfiguration()
    printerConfiguration.printerName = "printer"
    configuration.testPrinter = printerConfiguration

    def instance = new ProtocolInstance()
    configuration.protocolInstance = instance
    instance.configuration = configuration
    instance.environment = ProtocolEnvironment.TEST
    instance.protocolId = "ProToCoLId=1"

    def action = new ConfigurationDeployment()
    configuration.action = action
    action.status = PrivilegedAction.Status.APPROVED
    action.requester = new User()
    action.requester.username = "user1"
    action.validator = new User()
    action.validator.username = "user2"

    def votingMaterialsConfiguration = new VotingMaterialsConfiguration()
    votingMaterialsConfiguration.target = DeploymentTarget.REAL

    def operation = new Operation(id: 1)
    operation.clientId = "1"
    operation.configurationInTest = configuration
    operation.deployedConfiguration = configuration
    operation.votingMaterialsConfiguration = votingMaterialsConfiguration

    operationRepository.findByClientId("1") >> Optional.of(operation)

    when:
    def status = service.getOperationStatusByClientId("1")

    then:
    status.inTest.state == InTestConfigurationStatusVo.State.DEPLOYED
    status.inTest.lastChangeUser == "user2"
    status.inTest.configurationPageUrl == "/pact/configuration-deployments/9903"
    status.deployed.lastChangeUser == "user2"
    status.inTest.voteReceiverUrl == "http://host/chvote-receiver/test/vote/ge/ProToCoLId%3D1/fr/identification"
    status.deployed.voteReceiverUrl == "http://host/chvote-receiver/vote/ge/ProToCoLId%3D1/fr/identification"
  }

  def "getOperationStatusByClientId should contain the deployed configuration"() {
    given:
    def configuration = new OperationConfiguration()
    configuration.operationName = "201801VP"
    configuration.lastChangeUser = "user1"
    configuration.canton = "GE"

    def instance = new ProtocolInstance()
    configuration.protocolInstance = instance
    instance.configuration = configuration
    instance.environment = ProtocolEnvironment.PRODUCTION
    instance.protocolId = "ProToCoLId=1"

    def action = new ConfigurationDeployment()
    configuration.action = action
    action.status = PrivilegedAction.Status.APPROVED
    action.requester = new User()
    action.requester.username = "user2"
    action.validator = new User()
    action.validator.username = "user3"

    def votingMaterialsConfiguration = new VotingMaterialsConfiguration()
    votingMaterialsConfiguration.target = DeploymentTarget.SIMULATION

    def operation = new Operation(id: 1)
    operation.clientId = "1"
    operation.deployedConfiguration = configuration
    operation.votingMaterialsConfiguration = votingMaterialsConfiguration

    operationRepository.findByClientId("1") >> Optional.of(operation)

    when:
    def status = service.getOperationStatusByClientId("1")

    then:
    status.inTest == null
    status.deployed.lastChangeUser == "user3"
    status.deployed.voteReceiverUrl == "http://host/chvote-receiver/test/vote/ge/ProToCoLId%3D1/fr/identification"
  }

  def "getOperationByVotingMaterialsConfigurationId should return an operation"() {
    given:
    def configurationId = 10L
    def configuration = new VotingMaterialsConfiguration()
    configuration.id = configurationId
    def operation = new Operation()
    operation.votingMaterialsConfiguration = configuration
    operationRepository.findByVotingMaterialsConfiguration_Id(configurationId) >> Optional.of(operation)

    when:
    def op = service.getOperationByVotingMaterialsConfigurationId(configurationId)

    then:
    op == operation
  }

  def "getOperationByVotingMaterialsConfigurationId should raise an exception when the voting material configuration is not found"() {
    given:
    def configurationId = 10L
    operationRepository.findByVotingMaterialsConfiguration_Id(configurationId) >> Optional.empty()

    when:
    service.getOperationByVotingMaterialsConfigurationId(configurationId)

    then:
    def ex = thrown(EntityNotFoundException)
    ex.message == "No Operation has a VotingMaterialConfiguration with ID [10]"
  }

  def "createOrUpdateOperation should persist a new Operation entity"() {
    given:
    operationRepository.findByClientId("1") >> Optional.empty()
    def vo = createOperationConfigurationSubmissionVo()
    vo.clientId = "1"

    def files = Collections.emptyList()

    def newInstance = new ProtocolInstance(id: 1)
    def newConf = new OperationConfiguration()
    newConf.validationStatus = ValidationStatus.NOT_YET_VALIDATED

    when:
    def createdOperation = service.createOrUpdateOperation(vo, files)

    then:
    createdOperation.clientId == '1'
    createdOperation.id == 100L
    createdOperation.configurationInTest == Optional.of(newConf)
    1 * configurationService.createOperationConfiguration(vo, files) >> newConf
    0 * protocolInstanceService.deleteProtocolInstance(*_)
    1 * protocolInstanceService.createProtocolInstance(_, _, ProtocolEnvironment.TEST) >> newInstance
    1 * protocolInitializationService.startProtocol(newInstance)
    1 * operationRepository.save(_) >> { arguments ->
      def op = (arguments[0] as Operation)
      op.id = 100L
      return op
    }
  }

  def "createOrUpdateOperation should persist a new configuration in an existing Operation entity"() {
    given:
    def operation = new Operation(id: 3)
    operation.clientId = "1"

    def previousConfiguration = new OperationConfiguration(id: 1)
    previousConfiguration.validationStatus = ValidationStatus.NOT_YET_VALIDATED
    operation.configurationInTest = previousConfiguration

    def instance = new ProtocolInstance(id: 1)
    previousConfiguration.protocolInstance = instance
    instance.environment = ProtocolEnvironment.TEST
    instance.operation = operation
    instance.configuration = previousConfiguration

    operationRepository.findByClientId("1") >> Optional.of(operation)

    def vo = createOperationConfigurationSubmissionVo()
    vo.clientId = "1"
    def files = Collections.emptyList()

    def newInstance = new ProtocolInstance(id: 2)
    def newConf = new OperationConfiguration()
    newConf.validationStatus = ValidationStatus.NOT_YET_VALIDATED

    when:
    def createdOperation = service.createOrUpdateOperation(vo, files)

    then:
    createdOperation.clientId == '1'
    createdOperation.id == 100L
    createdOperation.configurationInTest == Optional.of(newConf)

    1 * configurationService.createOperationConfiguration(vo, files) >> newConf
    1 * protocolInstanceService.deleteProtocolInstance(instance)
    1 * protocolInstanceService.createProtocolInstance(_, _, ProtocolEnvironment.TEST) >> newInstance
    1 * protocolInitializationService.startProtocol(newInstance)
    1 * operationRepository.save(_) >> { arguments ->
      def op = (arguments[0] as Operation)
      op.id = 100L
      return op
    }
  }

  def "invalidateConfigurationByClientId should invalidate the configuration"() {
    given: "an operation"
    def operation = new Operation()
    operationRepository.findByClientId("TEST-OP") >> Optional.of(operation)

    when: "the invalidation method is called"
    service.invalidateConfigurationByClientId("TEST-OP", "Invalidator")

    then: "the invalidation is propagated"
    1 * configurationService.invalidateLatestOperationConfiguration(operation, "Invalidator")
  }

  def "invalidateConfigurationByClientId should fail if no matching operation is found"() {
    given: "no operation"
    operationRepository.findByClientId("Not an op") >> Optional.empty()

    when: "the invalidation method is called"
    service.invalidateConfigurationByClientId("Not an op", "Invalidator")

    then: "an exception is thrown"
    thrown(EntityNotFoundException)
  }

  def "validateConfigurationByClientId should validate the configuration"() {
    given: "an operation"
    def operation = new Operation()
    operationRepository.findByClientId("TEST-OP") >> Optional.of(operation)

    when: "the validation method is called"
    service.validateConfigurationByClientId("TEST-OP", "Validator")

    then: "the validation is propagated"
    1 * configurationService.validateLatestOperationConfiguration(operation, "Validator")
  }

  def "validateConfigurationByClientId should fail if no matching operation is found"() {
    given: "no operation"
    operationRepository.findByClientId("Not an op") >> Optional.empty()

    when: "the validation method is called"
    service.validateConfigurationByClientId("Not an op", "Validator")

    then: "an exception is thrown"
    thrown(EntityNotFoundException)
  }

  def "deployConfigurationInTest should set a configuration as deployed"() {
    given:
    def configuration = new OperationConfiguration(id: 9903)
    configuration.operationName = "201801VP"
    configuration.lastChangeUser = "user1"
    configuration.validationStatus = ValidationStatus.VALIDATED
    def instance = new ProtocolInstance()
    configuration.protocolInstance = instance
    instance.configuration = configuration
    instance.environment = ProtocolEnvironment.TEST

    def operation = new Operation(id: 1)
    operation.clientId = "1"
    operation.configurationInTest = configuration
    operation.deployedConfiguration = null

    operationRepository.findByConfigurationInTest_Id(1L) >> Optional.of(operation)

    when:
    service.deployConfigurationInTest(1L)

    then:
    1 * protocolInstanceService.deleteProtocolInstance(instance)
    1 * operationRepository.save(operation) >> { arguments ->
      def result = (arguments[0] as Operation)
      assert result.configurationInTest == Optional.empty()
      assert result.deployedConfiguration == Optional.of(configuration)
    }
  }

  def "deployConfigurationInTest should fail if the operation has neither a configuration in test nor a deployed configuration"() {
    given:
    def operationConfigurationId = 10L
    operationRepository.findByConfigurationInTest_Id(operationConfigurationId) >> Optional.empty()
    operationRepository.findByDeployedConfiguration_Id(operationConfigurationId) >> Optional.empty()

    when:
    service.deployConfigurationInTest(operationConfigurationId)

    then:
    EntityNotFoundException ex = thrown()
    ex.message == "No Operation has a either a test OperationConfiguration or a real OperationConfiguration with ID [10]"
  }

  def "addVotingMaterialsConfiguration should persist a new voting materials configuration entity"() {
    given:
    def clientId = "OPER1"
    def operation = new Operation()
    def deployedConfiguration = Mock(OperationConfiguration)
    operation.deployedConfiguration = deployedConfiguration
    deployedConfiguration.protocolInstance >> Optional.empty()
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()

    def newVotingMaterials = new VotingMaterialsConfiguration()

    votingMaterialsService.createVotingMaterialsConfiguration(_) >> { args -> newVotingMaterials }

    when:
    service.addVotingMaterialsConfiguration(clientId, submissionVo, new LinkedList<MultipartFile>())

    then:
    1 * votingMaterialsService.createVotingMaterialsConfiguration(
            submissionVo, new LinkedList<MultipartFile>()) >> newVotingMaterials
    1 * votingMaterialsConfigurationRepository.save(newVotingMaterials)
  }

  def "addVotingMaterialsConfiguration that has non rejected voting material configuration should refuse to persist a new one"() {
    given:
    def clientId = "OPER2"
    def operation = new Operation()
    operation.clientId = clientId
    def deployedConfiguration = Mock(OperationConfiguration)
    operation.deployedConfiguration = deployedConfiguration
    def protocolInstance = Mock(ProtocolInstance)
    protocolInstance.status >> ProtocolInstanceStatus.INITIALIZING
    deployedConfiguration.protocolInstance >> Optional.of(protocolInstance)
    def action = Mock(VotingMaterialsCreation)
    action.status >> status
    def votingMaterialsConfiguration = Mock(VotingMaterialsConfiguration)
    votingMaterialsConfiguration.creationAction >> Optional.of(action)
    votingMaterialsConfiguration.invalidationAction >> Optional.empty()
    operation.votingMaterialsConfiguration = votingMaterialsConfiguration
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()

    def newVotingMaterials = new VotingMaterialsConfiguration()

    votingMaterialsService.createVotingMaterialsConfiguration(_) >> { args -> newVotingMaterials }

    when:
    service.addVotingMaterialsConfiguration(clientId, submissionVo, new LinkedList<MultipartFile>())

    then:
    CannotUpdateVotingMaterialConfigurationException ex = thrown()
    ex.message == "Cannot update voting material configuration for Operation [OPER2] because it has not been rejected or invalidated"
    0 * votingMaterialsConfigurationRepository.save(_)
    0 * votingMaterialsConfigurationRepository.delete(_)

    where:
    status                           | _
    PrivilegedAction.Status.APPROVED | _
    PrivilegedAction.Status.PENDING  | _
  }


  def "addVotingMaterialsConfiguration that has a pending or refused invalidation should refuse to persist a new one"() {
    given:
    def clientId = "OPER2"
    def operation = new Operation()
    operation.clientId = clientId
    def deployedConfiguration = Mock(OperationConfiguration)
    operation.deployedConfiguration = deployedConfiguration
    def protocolInstance = Mock(ProtocolInstance)
    protocolInstance.status >> ProtocolInstanceStatus.INITIALIZING
    deployedConfiguration.protocolInstance >> Optional.of(protocolInstance)
    def action = Mock(VotingMaterialsCreation)
    action.status >> PrivilegedAction.Status.APPROVED
    def votingMaterialsConfiguration = Mock(VotingMaterialsConfiguration)
    votingMaterialsConfiguration.creationAction >> Optional.of(action)

    def invalidationAction = new VotingMaterialsInvalidation()
    invalidationAction.setStatus(status)
    votingMaterialsConfiguration.invalidationAction >> Optional.of(invalidationAction)

    operation.votingMaterialsConfiguration = votingMaterialsConfiguration
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()

    def newVotingMaterials = new VotingMaterialsConfiguration()

    votingMaterialsService.createVotingMaterialsConfiguration(_) >> { args -> newVotingMaterials }

    when:
    service.addVotingMaterialsConfiguration(clientId, submissionVo, new LinkedList<MultipartFile>())

    then:
    CannotUpdateVotingMaterialConfigurationException ex = thrown()
    ex.message == "Cannot update voting material configuration for Operation [OPER2] because it has not been rejected or invalidated"
    0 * votingMaterialsConfigurationRepository.save(_)
    0 * votingMaterialsConfigurationRepository.delete(_)

    where:
    status                           | _
    PrivilegedAction.Status.REJECTED | _
    PrivilegedAction.Status.PENDING  | _
  }

  def "invalidateVotingMaterial should call appropriate service"() {
    given:
    Operation operation = new Operation()
    operation.setDeployedConfiguration(new OperationConfiguration())
    def oldProtocolInstance = new ProtocolInstance()
    operation.deployedConfiguration.get().setProtocolInstance(oldProtocolInstance)

    when:
    service.deleteDeployedConfigurationInstance(operation)

    then:
    1 * protocolInstanceService.deleteProtocolInstance(oldProtocolInstance)
    !operation.fetchDeployedConfiguration().protocolInstance.isPresent()
  }

  def "addVotingMaterialsConfiguration that has an approved invalidation should persist a new one"() {
    given:
    def clientId = "OPER2"
    def operation = new Operation()
    operation.clientId = clientId
    def deployedConfiguration = Mock(OperationConfiguration)
    operation.deployedConfiguration = deployedConfiguration
    def protocolInstance = Mock(ProtocolInstance)
    protocolInstance.status >> ProtocolInstanceStatus.INITIALIZING
    deployedConfiguration.protocolInstance >> Optional.of(protocolInstance)
    def action = Mock(VotingMaterialsCreation)
    action.status >> PrivilegedAction.Status.APPROVED
    def votingMaterialsConfiguration = Mock(VotingMaterialsConfiguration)
    votingMaterialsConfiguration.creationAction >> Optional.of(action)

    def invalidationAction = new VotingMaterialsInvalidation()
    invalidationAction.setStatus(PrivilegedAction.Status.APPROVED)
    votingMaterialsConfiguration.invalidationAction >> Optional.of(invalidationAction)

    operation.votingMaterialsConfiguration = votingMaterialsConfiguration
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()
    def oldVotingMaterial = operation.votingMaterialsConfiguration.get()
    def newVotingMaterials = new VotingMaterialsConfiguration()
    newVotingMaterials.id = 5465465L
    votingMaterialsService.createVotingMaterialsConfiguration(_) >> { args -> newVotingMaterials }
    def old = new VotingMaterialsConfiguration()
    when:
    service.addVotingMaterialsConfiguration(clientId, submissionVo, new LinkedList<MultipartFile>())

    then:
    1 * votingMaterialsConfigurationRepository.save(_)
    1 * votingMaterialsConfigurationRepository.delete(_)
    votingMaterialsConfigurationRepository.getOne(5465465L) >> newVotingMaterials
    newVotingMaterials.id == 5465465L

    votingMaterialsConfigurationRepository.getOne(oldVotingMaterial.id) >> old
    old.id == null
  }


  def "addVotingMaterialsConfiguration that has non rejected voting material configuration which its generation has failed should persist a new one"() {
    given:
    def clientId = "OPER2"
    def operation = new Operation()
    operation.clientId = clientId
    def deployedConfiguration = Mock(OperationConfiguration)
    operation.deployedConfiguration = deployedConfiguration
    def protocolInstance = Mock(ProtocolInstance)
    protocolInstance.status >> ProtocolInstanceStatus.CREDENTIALS_GENERATION_FAILURE
    deployedConfiguration.protocolInstance >> Optional.of(protocolInstance)
    def action = Mock(VotingMaterialsCreation)
    action.status >> status
    def votingMaterialsConfiguration = Mock(VotingMaterialsConfiguration)
    votingMaterialsConfiguration.action >> Optional.of(action)
    operation.votingMaterialsConfiguration = votingMaterialsConfiguration
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()

    def newVotingMaterials = new VotingMaterialsConfiguration()

    votingMaterialsService.createVotingMaterialsConfiguration(_) >> { args -> newVotingMaterials }

    when:
    service.addVotingMaterialsConfiguration(clientId, submissionVo, new LinkedList<MultipartFile>())

    then:
    1 * votingMaterialsService.createVotingMaterialsConfiguration(
            submissionVo, new LinkedList<MultipartFile>()) >> newVotingMaterials
    1 * votingMaterialsConfigurationRepository.save(newVotingMaterials)

    where:
    status                           | _
    PrivilegedAction.Status.APPROVED | _
    PrivilegedAction.Status.PENDING  | _
  }

  def "addVotingMaterialsConfiguration that has a rejected voting material configuration should persist a new one"() {
    given:
    def clientId = "OPER2"
    def operation = new Operation()
    operation.clientId = clientId
    def deployedConfiguration = Mock(OperationConfiguration)
    deployedConfiguration.protocolInstance >> Optional.empty()
    operation.deployedConfiguration = deployedConfiguration
    def action = Mock(VotingMaterialsCreation)
    action.status >> PrivilegedAction.Status.REJECTED
    def votingMaterialsConfiguration = Mock(VotingMaterialsConfiguration)
    votingMaterialsConfiguration.creationAction >> Optional.of(action)
    votingMaterialsConfiguration.invalidationAction >> Optional.empty()
    operation.votingMaterialsConfiguration = votingMaterialsConfiguration
    operationRepository.findByClientId(clientId) >> Optional.of(operation)

    def submissionVo = new VotingMaterialsConfigurationSubmissionVo()

    def newVotingMaterials = new VotingMaterialsConfiguration()

    votingMaterialsService.createVotingMaterialsConfiguration(_) >> { args -> newVotingMaterials }

    when:
    service.addVotingMaterialsConfiguration(clientId, submissionVo, new LinkedList<MultipartFile>())

    then:
    1 * votingMaterialsService.createVotingMaterialsConfiguration(
            submissionVo, new LinkedList<MultipartFile>()) >> newVotingMaterials
    1 * votingMaterialsConfigurationRepository.save(newVotingMaterials)
  }


  def "addVotingPeriodConfiguration should persist a new voting period configuration entity for real if date hasn't changed"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()
            .withDeployedConfiguration()
            .withVotingMaterialConfiguration(DeploymentTarget.REAL)
            .withProtocolInstance(ProtocolInstanceStatus.CREDENTIALS_GENERATED)

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(false)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    1 * votingPeriodService.convertToVotingPeriodConfiguration(submission, publicKey) >> configurer.newVotingPeriod
    //1 * votingPeriodConfigurationRepository.save(configurer.newVotingPeriod)
  }


  def "addVotingPeriodConfiguration should prevent creation if the voting material hasen't been validated"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()
            .withDeployedConfiguration()
            .withVotingMaterialConfiguration(DeploymentTarget.REAL, ValidationStatus.NOT_YET_VALIDATED)
            .withProtocolInstance(ProtocolInstanceStatus.CREDENTIALS_GENERATED)

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(false)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    def exception = thrown(CannotCreateOrUpdateVotingPeriodConfigurationException)
    exception.message == "Cannot update voting period configuration for Operation [OPER1] because voting material has not benn validated"
  }


  def "addVotingPeriodConfiguration should persist a new voting period configuration entity for Simulation even if the date has changed"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()
            .withDeployedConfiguration()
            .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
            .withProtocolInstance(ProtocolInstanceStatus.CREDENTIALS_GENERATED)

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(true)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    1 * votingPeriodService.convertToVotingPeriodConfiguration(submission, publicKey) >> configurer.newVotingPeriod
  }

  def "addVotingPeriodConfiguration should prevent creation of a new voting period configuration entity for real if date has changed"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()
            .withDeployedConfiguration()
            .withVotingMaterialConfiguration(DeploymentTarget.REAL)
            .withProtocolInstance(ProtocolInstanceStatus.CREDENTIALS_GENERATED)

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(true)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    def exception = thrown(CannotCreateOrUpdateVotingPeriodConfigurationException)
    exception.message == "Cannot create or update voting period configuration for Operation [OPER1] because site opening and/or closing date has change since operation creation"
  }


  def "addVotingPeriodConfiguration should prevent creation of voting period if configuration is not already deployed"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(true)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    def exception = thrown(CannotCreateOrUpdateVotingPeriodConfigurationException)
    exception.message == "Cannot create or update voting period configuration for Operation [OPER1] because configuration hasn't been deployed"
  }


  def "addVotingPeriodConfiguration should prevent creation of voting period if the voting material hasn't been already imported"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()
            .withDeployedConfiguration()

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(false)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    def exception = thrown(CannotCreateOrUpdateVotingPeriodConfigurationException)
    "Cannot create or update voting period configuration for Operation [OPER1] because voting material hasn't been sent" == exception.message
  }

  def "addVotingPeriodConfiguration should prevent creation if there is no protocol instance"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()
            .withDeployedConfiguration()
            .withVotingMaterialConfiguration(DeploymentTarget.REAL)

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(false)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    def exception = thrown(CannotCreateOrUpdateVotingPeriodConfigurationException)
    "Cannot create or update voting period configuration for Operation [OPER1] because protocol is in incompatible state [null]" == exception.message
  }

  def "addVotingPeriodConfiguration should prevent creation if there is the protocol instance is in not compatible state"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()
            .withDeployedConfiguration()
            .withVotingMaterialConfiguration(DeploymentTarget.REAL)
            .withProtocolInstance(state)

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(false)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    def exception = thrown(CannotCreateOrUpdateVotingPeriodConfigurationException)
    "Cannot create or update voting period configuration for Operation [OPER1] because protocol is in incompatible state [$state]" == exception.message

    where:
    _ | state
    _ | ProtocolInstanceStatus.INITIALIZING
    _ | ProtocolInstanceStatus.PUBLIC_PARAMETERS_INITIALIZED
    _ | ProtocolInstanceStatus.GENERATING_KEYS
    _ | ProtocolInstanceStatus.KEYS_GENERATED
    _ | ProtocolInstanceStatus.GENERATING_CREDENTIALS
    _ | ProtocolInstanceStatus.CREDENTIALS_GENERATION_FAILURE
    _ | ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZING
    _ | ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZED
    _ | ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES
    _ | ProtocolInstanceStatus.SHUFFLING
    _ | ProtocolInstanceStatus.DECRYPTING
    _ | ProtocolInstanceStatus.RESULTS_AVAILABLE
  }


  def "addVotingPeriodConfiguration should prevent creation if the protocol instance is in CREDENTIALS_GENERATED but there is already a voting period initialization request but not canceled"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()
            .withDeployedConfiguration()
            .withVotingMaterialConfiguration(DeploymentTarget.REAL)
            .withProtocolInstance(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
            .withExistingVotingPeriodConfiguration(state)

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(false)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    def exception = thrown(CannotCreateOrUpdateVotingPeriodConfigurationException)
    "Cannot update voting period configuration for Operation [OPER1] because request has not been rejected before" == exception.message

    where:
    _ | state
    _ | PrivilegedAction.Status.PENDING
    _ | PrivilegedAction.Status.APPROVED
  }

  def "addVotingPeriodConfiguration should allow creation if the protocol instance is in CREDENTIALS_GENERATED if request has been canceled"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()
            .withDeployedConfiguration()
            .withVotingMaterialConfiguration(DeploymentTarget.REAL)
            .withProtocolInstance(ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE)
            .withExistingVotingPeriodConfiguration(PrivilegedAction.Status.REJECTED)

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(false)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    1 * votingPeriodService.convertToVotingPeriodConfiguration(submission, publicKey) >> configurer.newVotingPeriod
  }

  def "addVotingPeriodConfiguration should allow creation if the protocol instance is in ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE even if request hasn't been canceled"() {
    given:
    def configurer = new AddVotingPeriodConfigurationTestConfigurer()
            .withDeployedConfiguration()
            .withVotingMaterialConfiguration(DeploymentTarget.REAL)
            .withProtocolInstance(ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE)
            .withExistingVotingPeriodConfiguration(state)

    operationRepository.findByClientId(configurer.clientId) >> Optional.of(configurer.operation)
    votingPeriodService.convertToVotingPeriodConfiguration(_) >> { args -> configurer.newVotingPeriod }

    def submission = configurer.createSubmission(false)
    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    service.addVotingPeriodConfiguration(configurer.clientId, submission, publicKey)

    then:
    1 * votingPeriodService.convertToVotingPeriodConfiguration(submission, publicKey) >> configurer.newVotingPeriod

    where:
    _ | state
    _ | PrivilegedAction.Status.PENDING
    _ | PrivilegedAction.Status.APPROVED
    _ | PrivilegedAction.Status.REJECTED
  }


  class AddVotingPeriodConfigurationTestConfigurer {
    private def openingDate = LocalDateTime.of(2010, 1, 1, 9, 0)
    private def closingDate = LocalDateTime.of(2010, 1, 1, 10, 0)
    private def clientId = "OPER1"
    private def operation = new Operation()
    private OperationConfiguration deployedConfiguration = new OperationConfiguration()
    private VotingPeriodConfiguration newVotingPeriod = new VotingPeriodConfiguration()
    private VotingMaterialsConfiguration votingMaterialConfiguration = new VotingMaterialsConfiguration()
    private ProtocolInstance protocolInstance = new ProtocolInstance()

    AddVotingPeriodConfigurationTestConfigurer() {
      operation.setClientId(clientId)
    }

    def withDeployedConfiguration() {
      deployedConfiguration.siteOpeningDate = openingDate
      deployedConfiguration.siteClosingDate = closingDate
      operation.deployedConfiguration = deployedConfiguration
      return this
    }

    def withVotingMaterialConfiguration(DeploymentTarget target = DeploymentTarget.REAL,
                                        ValidationStatus validationStatus = ValidationStatus.VALIDATED) {
      votingMaterialConfiguration.target = target
      votingMaterialConfiguration.validationStatus = validationStatus
      operation.votingMaterialsConfiguration = votingMaterialConfiguration

      return this
    }

    def withProtocolInstance(ProtocolInstanceStatus status = ProtocolInstanceStatus.CREDENTIALS_GENERATED) {
      protocolInstance.status = status
      deployedConfiguration.setProtocolInstance(protocolInstance)
      return this
    }

    def withExistingVotingPeriodConfiguration(PrivilegedAction.Status status = null) {
      def votingPeriodConfiguration = new VotingPeriodConfiguration()
      if (status != null) {
        def votingPeriodInitialization = new VotingPeriodInitialization()
        votingPeriodInitialization.status = status
        votingPeriodConfiguration.setAction(votingPeriodInitialization)
      }
      operation.votingPeriodConfiguration = votingPeriodConfiguration
      return this
    }

    VotingPeriodConfigurationSubmissionVo createSubmission(boolean dateChange = false) {
      def submissionVo = new VotingPeriodConfigurationSubmissionVo()
      submissionVo.siteOpeningDate = openingDate
      submissionVo.siteClosingDate = dateChange ? closingDate.plusHours(1) : closingDate
      return submissionVo
    }

  }


  def "publishDeployedConfiguration should create a protocol instance in PRODUCTION"() {
    given:
    def configuration = new OperationConfiguration(id: 9903)
    configuration.operationName = "201801VP"
    configuration.lastChangeUser = "user1"
    configuration.validationStatus = ValidationStatus.VALIDATED

    def operation = new Operation(id: 1)
    operation.clientId = "1"
    operation.configurationInTest = null
    operation.deployedConfiguration = configuration

    def instance = new ProtocolInstance(id: 2)
    instance.configuration = configuration
    instance.environment = ProtocolEnvironment.PRODUCTION

    when:
    service.publishDeployedConfiguration(operation)

    then:
    1 * protocolInstanceService.createProtocolInstance(operation, _, ProtocolEnvironment.PRODUCTION) >> { arguments ->
      return instance
    }
    1 * protocolInitializationService.startProtocol(instance)
  }

  def createOperationConfigurationSubmissionVo() {
    def doiEntry = new AttachmentFileEntryVo()
    doiEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOMAIN_OF_INFLUENCE)
    doiEntry.setImportDateTime(LocalDateTime.now())
    doiEntry.setZipFileName("doi.zip")

    def faqDEEntry = new AttachmentFileEntryVo()
    faqDEEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOCUMENT_FAQ)
    faqDEEntry.setLanguage("DE")
    faqDEEntry.setImportDateTime(LocalDateTime.now())
    faqDEEntry.setZipFileName("op_doc_faq.de.zip")

    def faqFREntry = new AttachmentFileEntryVo()
    faqFREntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOCUMENT_FAQ)
    faqFREntry.setLanguage("FR")
    faqFREntry.setImportDateTime(LocalDateTime.now())
    faqFREntry.setZipFileName("op_doc_faq.fr.zip")

    def orepEntry = new AttachmentFileEntryVo()
    orepEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REPOSITORY_VOTATION)
    orepEntry.setImportDateTime(LocalDateTime.now())
    orepEntry.setZipFileName("orep.zip")

    def orepEntry2 = new AttachmentFileEntryVo()
    orepEntry2.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REPOSITORY_ELECTION)
    orepEntry2.setImportDateTime(LocalDateTime.now())
    orepEntry2.setZipFileName("orep2.zip")

    def registerEntry = new AttachmentFileEntryVo()
    registerEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REGISTER)
    registerEntry.setImportDateTime(LocalDateTime.now())
    registerEntry.setZipFileName("register.zip")

    def vo = new OperationConfigurationSubmissionVo()
    vo.user = "Fred"
    vo.gracePeriod = 500
    vo.canton = "GE "
    vo.groupVotation = true
    vo.operationName = "201706VP"
    vo.operationDate = LocalDateTime.now()
    vo.clientId = "1000"
    vo.milestones = [:]
    vo.testSitePrinter = new TestSitePrinter()
    vo.testSitePrinter.municipality = new MunicipalityVo()
    vo.testSitePrinter.municipality.ofsId = 6601
    vo.testSitePrinter.municipality.name = "Municipality 6601"
    vo.testSitePrinter.printer = new PrinterConfigurationVo()
    vo.testSitePrinter.printer.id = "printer-0"
    vo.testSitePrinter.printer.name = "Printer 0"
    vo.testSitePrinter.printer.publicKey = PrintersPublicKeys.publicKeyPa0()
    vo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_OPEN] = LocalDateTime.now()
    vo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_CLOSE] = LocalDateTime.now()
    vo.attachments = Arrays.asList(doiEntry, faqDEEntry, faqFREntry, orepEntry, registerEntry, orepEntry2)

    return vo
  }

}
