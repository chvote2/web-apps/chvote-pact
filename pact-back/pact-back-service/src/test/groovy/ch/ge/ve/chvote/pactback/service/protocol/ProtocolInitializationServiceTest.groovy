/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol

import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo
import ch.ge.ve.chvote.pactback.repository.operation.configuration.OperationConfigurationRepository
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.operation.TimeService
import ch.ge.ve.chvote.pactback.service.operation.configuration.OperationConfigurationAttachmentService
import ch.ge.ve.chvote.pactback.service.operation.voter.VoterBatchService
import ch.ge.ve.chvote.pactback.service.operation.voter.VoterService
import ch.ge.ve.chvote.pactback.service.protocol.client.ProtocolClientFactory
import ch.ge.ve.chvote.pactback.service.protocol.processor.InitPublicParametersHandler
import ch.ge.ve.chvote.pactback.service.protocol.processor.KeyGenerationHandler
import ch.ge.ve.chvote.pactback.service.protocol.processor.OperationReferenceFilesLoader
import ch.ge.ve.chvote.pactback.service.protocol.processor.OutputFilesManager
import ch.ge.ve.chvote.pactback.service.protocol.processor.PrinterArchiveGenerator
import ch.ge.ve.chvote.pactback.service.protocol.processor.PrinterConfigurationsLoader
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolStateFactory
import ch.ge.ve.chvote.pactback.service.protocol.processor.RealCredentialsGenerationHandler
import ch.ge.ve.chvote.pactback.service.protocol.processor.RegistersLoader
import ch.ge.ve.chvote.pactback.service.protocol.processor.TestCredentialsGenerationHandler
import ch.ge.ve.chvote.pactback.service.protocol.processor.TestElectoralOfficerPublicKeyPublicationHandler
import ch.ge.ve.model.convert.api.ElectionConverter
import ch.ge.ve.model.convert.api.VotationConverter
import ch.ge.ve.model.convert.api.VoterConverter
import ch.ge.ve.model.convert.impl.DefaultElectionConverter
import ch.ge.ve.model.convert.impl.DefaultVotationConverter
import ch.ge.ve.model.convert.impl.DefaultVoterConverter
import ch.ge.ve.model.convert.impl.VoterIdGenerator
import ch.ge.ve.protocol.client.ProtocolClient
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey
import java.time.LocalDateTime
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import org.springframework.mock.web.MockMultipartFile
import spock.lang.Specification

class ProtocolInitializationServiceTest extends Specification {
  private ProtocolInitializationService service

  private ProtocolInstanceService instanceService
  private ProtocolClientFactory protocolClientFactory
  private VoterConverter voterConverter
  private VotationConverter votationConverter
  private ElectionConverter electionConverter
  private RegistersLoader registersLoader
  private PrinterConfigurationsLoader printerConfigurationsLoader
  private OperationConfigurationRepository operationConfigurationRepository
  private OperationReferenceFilesLoader operationReferenceFilesLoader
  private OutputFilesManager outputFilesManager
  private PrinterArchiveGenerator printerArchiveGenerator
  private VoterService voterService
  private VoterBatchService voterBatchService
  private TestElectoralOfficerPublicKeyPublicationHandler sendForTestElectoralOfficerPublicKeyHandler

  void setup() {
    protocolClientFactory = Mock(ProtocolClientFactory)
    instanceService = Mock(ProtocolInstanceService)
    registersLoader = Mock(RegistersLoader)
    printerConfigurationsLoader = Mock(PrinterConfigurationsLoader)
    operationConfigurationRepository = Mock(OperationConfigurationRepository)
    operationReferenceFilesLoader = new OperationReferenceFilesLoader(operationConfigurationRepository)
    outputFilesManager = Mock(OutputFilesManager)
    printerArchiveGenerator = Mock()
    voterConverter = new DefaultVoterConverter(new VoterIdGenerator())
    votationConverter = new DefaultVotationConverter()
    electionConverter = new DefaultElectionConverter()
    voterService = Mock()
    voterBatchService = new VoterBatchService(voterService, 10)
    sendForTestElectoralOfficerPublicKeyHandler = Mock()

    service = new ProtocolInitializationService(
            new InitPublicParametersHandler(),
            new KeyGenerationHandler(),
            new RealCredentialsGenerationHandler(
                    voterBatchService, votationConverter, electionConverter, voterConverter, registersLoader,
                    printerConfigurationsLoader, operationReferenceFilesLoader, outputFilesManager, printerArchiveGenerator),
            new TestCredentialsGenerationHandler(
                    voterBatchService, votationConverter, electionConverter, voterConverter, printerConfigurationsLoader,
                    operationReferenceFilesLoader, outputFilesManager, printerArchiveGenerator),
            new ProtocolStateFactory(instanceService, protocolClientFactory, 1),
            sendForTestElectoralOfficerPublicKeyHandler
    )
  }

  def "startProtocol should initialize a protocol instance and its parameters"() {
    given:
    def timeService = new TimeService()
    def client = Mock(ProtocolClient)
    def chVoteConfigurationProperties = Mock(ChVoteConfigurationProperties)
    chVoteConfigurationProperties.timezone >> "Europe/Zurich"

    def instance = new ProtocolInstance()
    instance.id = 1L
    instance.protocolId = "42"
    instance.environment = ProtocolEnvironment.TEST
    instance.status = ProtocolInstanceStatus.INITIALIZING
    instance.configuration = new OperationConfiguration()
    instance.configuration.id = 2
    instance.configuration.operationReferenceFiles = new ArrayList<>()
    instance.configuration.operationReferenceFiles.add(new OperationReferenceFile())
    instance.operation = new Operation()
    instance.operation.configurationInTest = instance.configuration

    def doiFile = new MockMultipartFile("doi", "doi.zip", null, zip("doi.xml"))
    def doiEntry = new AttachmentFileEntryVo(
            attachmentType: AttachmentFileEntryVo.AttachmentType.DOMAIN_OF_INFLUENCE,
            importDateTime: LocalDateTime.now(),
            zipFileName: "doi.zip"
    )
    def orepFile = new MockMultipartFile("orep", "orep.zip", null, zip("orep.xml"))
    def orepEntry = new AttachmentFileEntryVo(
            attachmentType: AttachmentFileEntryVo.AttachmentType.REPOSITORY_VOTATION,
            importDateTime: LocalDateTime.now(),
            zipFileName: "orep.zip"
    )
    def registerFile = new MockMultipartFile("register", "register.zip", null,
            zip("register.xml"))
    def registerEntry = new AttachmentFileEntryVo(
            attachmentType: AttachmentFileEntryVo.AttachmentType.REGISTER,
            importDateTime: LocalDateTime.now(),
            zipFileName: "register.zip"
    )
    def files = Arrays.asList(doiFile, orepFile, registerFile)
    def catalog = Arrays.asList(doiEntry, orepEntry, registerEntry)

    new OperationConfigurationAttachmentService(timeService, chVoteConfigurationProperties)
            .populateAttachments(instance.configuration, [] as Set, [] as Set, catalog, files)

    operationConfigurationRepository.getOne(_) >> instance.configuration
    printerConfigurationsLoader.generateTestMunicipalityToPrintersMap(2) >>
            ((6601..6646) + 6699).collectEntries { [it, "printingAuth1"] }

    printerConfigurationsLoader.createTestPrintingAuthority(2) >> {
      return Collections.singletonList(new PrintingAuthorityWithPublicKey("printingAuth1", BigInteger.ONE))
    }

    instanceService.withProtocolInstance(instance.id, _) >> { id, action -> action.apply(instance) }

    printerArchiveGenerator.prepareForPrinters(*_) >> Mock(PrinterArchiveGenerator.Packing)

    when:
    service.startProtocol(instance)

    then:
    1 * protocolClientFactory.createClient(instance.protocolId) >> client
    1 * client.sendPublicParameters(*_)
    1 * client.ensurePublicParametersForwardedToCCs(*_)
    1 * instanceService.setPublicParameters(instance.id, _) >> instance

    then:
    1 * instanceService.updateProtocolStatus(instance.id, ProtocolInstanceStatus.PUBLIC_PARAMETERS_INITIALIZED) >> instance

    then:
    1 * client.requestKeyGeneration()
    1 * instanceService.updateProtocolStatus(instance.id, ProtocolInstanceStatus.GENERATING_KEYS) >> instance
    1 * client.ensurePublicKeyPartsPublishedToBB(*_)
    1 * client.ensurePublicKeyPartsForwardedToCCs(*_)

    then:
    1 * instanceService.updateProtocolStatus(instance.id, ProtocolInstanceStatus.KEYS_GENERATED) >> instance

    then:
    1 * instanceService.updateProtocolStatus(instance.id, ProtocolInstanceStatus.GENERATING_CREDENTIALS) >> instance
    (1.._) * voterService.saveAll(instance.id, _)
    1 * client.sendElectionSet(*_)
    1 * client.ensureElectionSetForwardedToCCs(*_)
    1 * client.ensurePrimesSentToBB(*_)

    then:
    1 * client.sendVoters(*_)
    1 * instanceService.saveCreationStatistics(instance.id, _) >> instance
    1 * client.ensureVotersForwardedToCCs(*_)
    1 * client.ensurePublicCredentialsBuiltForAllCCs(*_)
    1 * client.requestPrivateCredentials(*_)
    1 * instanceService.updatePrinterArchiveCreationDate(instance.id, _) >> instance
    1 * instanceService.updateProtocolStatus(instance.id, ProtocolInstanceStatus.CREDENTIALS_GENERATED) >> instance
  }

  static zip(String filename) {
    def source = Thread.currentThread().contextClassLoader.getResourceAsStream("attachments/operation/$filename")

    def out = new ByteArrayOutputStream()
    new ZipOutputStream(out).withCloseable { zipped ->
      zipped.putNextEntry(new ZipEntry(filename))
      zipped.write(source.bytes)
    }

    return out.toByteArray()
  }
}
