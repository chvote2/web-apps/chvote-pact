/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol

import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.exception.ProtocolInitializationException
import ch.ge.ve.chvote.pactback.service.protocol.processor.InitPublicParametersHandler
import ch.ge.ve.chvote.pactback.service.protocol.processor.KeyGenerationHandler
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolHandlerChain
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolState
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolStateFactory
import ch.ge.ve.chvote.pactback.service.protocol.processor.RealCredentialsGenerationHandler
import ch.ge.ve.chvote.pactback.service.protocol.processor.TestCredentialsGenerationHandler
import ch.ge.ve.chvote.pactback.service.protocol.processor.TestElectoralOfficerPublicKeyPublicationHandler
import spock.lang.Specification

class ProtocolInitializationServiceUnitTest extends Specification {

  def initPublicParametersHandler = Mock(InitPublicParametersHandler)
  def keyGenerationHandler = Mock(KeyGenerationHandler)
  def realCredentialsGenerationHandler = Mock(RealCredentialsGenerationHandler)
  def testCredentialsGenerationHandler = Mock(TestCredentialsGenerationHandler)
  def protocolStateFactory = Mock(ProtocolStateFactory)
  def sendForTestElectoralOfficerPublicKeyHandler = Mock(TestElectoralOfficerPublicKeyPublicationHandler)

  ProtocolInitializationService initializationService

  void setup() {
    initializationService = new ProtocolInitializationService(initPublicParametersHandler, keyGenerationHandler,
            realCredentialsGenerationHandler, testCredentialsGenerationHandler, protocolStateFactory, sendForTestElectoralOfficerPublicKeyHandler)
  }

  def "StartProtocol should run the test chain for TEST env"() {
    given:
    def instance = new ProtocolInstance(environment: ProtocolEnvironment.TEST)
    def state = Mock(ProtocolState)

    protocolStateFactory.initProtocolState(instance) >> state

    when:
    initializationService.startProtocol(instance)

    then:
    1 * initPublicParametersHandler.process(state, _) >> { ProtocolState s, ProtocolHandlerChain chain -> chain.proceed(s) }
    1 * keyGenerationHandler.process(state, _) >> { ProtocolState s, ProtocolHandlerChain chain -> chain.proceed(s) }
    1 * testCredentialsGenerationHandler.process(state, _) >> { ProtocolState s, ProtocolHandlerChain chain -> chain.proceed(s) }

    0 * realCredentialsGenerationHandler.process(state, _)
  }

  def "StartProtocol should run the real chain for PRODUCTION env"() {
    given:
    def instance = new ProtocolInstance(environment: ProtocolEnvironment.PRODUCTION)
    def state = Mock(ProtocolState)

    protocolStateFactory.initProtocolState(instance) >> state

    when:
    initializationService.startProtocol(instance)

    then:
    1 * initPublicParametersHandler.process(state, _) >> { ProtocolState s, ProtocolHandlerChain chain -> chain.proceed(s) }
    1 * keyGenerationHandler.process(state, _) >> { ProtocolState s, ProtocolHandlerChain chain -> chain.proceed(s) }
    1 * realCredentialsGenerationHandler.process(state, _) >> { ProtocolState s, ProtocolHandlerChain chain -> chain.proceed(s) }

    0 * testCredentialsGenerationHandler.process(state, _)
  }

  def "StartProtocol - instance should be set to CREDENTIALS_GENERATION_FAILURE when an exception occurs in chain"() {
    given:
    def instance = new ProtocolInstance()
    def state = Mock(ProtocolState)

    state.getOperation() >> new Operation(clientId: "Some client Id") // Needed for exception message

    protocolStateFactory.initProtocolState(instance) >> state
    initPublicParametersHandler.process(state, _) >> { ProtocolState s, ProtocolHandlerChain chain -> chain.proceed(s) }
    keyGenerationHandler.process(state, _) >> { throw new RuntimeException("Exception thrown for test purposes") }

    when:
    initializationService.startProtocol(instance)

    then:
    thrown ProtocolInitializationException
    1 * state.updateProtocolStatus(ProtocolInstanceStatus.CREDENTIALS_GENERATION_FAILURE)
  }

  def "ContinueProtocol - already processed handlers should not be reprocessed"() {
    given:
    def instance = new ProtocolInstance(environment: ProtocolEnvironment.PRODUCTION)
    def state = Mock(ProtocolState)

    protocolStateFactory.initProtocolState(instance) >> state

    initPublicParametersHandler.hasBeenProcessed(state) >> true
    keyGenerationHandler.hasBeenProcessed(state) >> true
    realCredentialsGenerationHandler.hasBeenProcessed(state) >> false

    when:
    initializationService.continueProtocol(instance)

    then:
    0 * initPublicParametersHandler.process(state, _)
    0 * keyGenerationHandler.process(state, _)
    1 * realCredentialsGenerationHandler.process(state, _) >> { ProtocolState s, ProtocolHandlerChain chain -> chain.proceed(s) }

    0 * testCredentialsGenerationHandler.process(state, _)
  }
}
