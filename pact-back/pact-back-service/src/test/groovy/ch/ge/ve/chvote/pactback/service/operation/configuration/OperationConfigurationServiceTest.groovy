/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration

import ch.ge.ve.chvote.pactback.contract.operation.ElectionSiteConfigurationVo
import ch.ge.ve.chvote.pactback.contract.operation.MunicipalityVo
import ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo
import ch.ge.ve.chvote.pactback.contract.operation.PrinterConfigurationVo
import ch.ge.ve.chvote.pactback.contract.operation.TestSitePrinter
import ch.ge.ve.chvote.pactback.fixtures.PrintersPublicKeys
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus
import ch.ge.ve.chvote.pactback.repository.operation.configuration.OperationConfigurationRepository
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService
import java.time.LocalDateTime
import java.time.ZoneOffset
import spock.lang.Specification

class OperationConfigurationServiceTest extends Specification {

  private static final String timezone = "Europe/Zurich"

  private OperationConfigurationRepository configurationRepository
  private OperationConfigurationAttachmentService attachmentService
  private ProtocolInstanceService protocolInstanceService
  private OperationRepository operationRepository
  private OperationConfigurationService service
  private ChVoteConfigurationProperties configurationProperties

  void setup() {
    configurationRepository = Mock(OperationConfigurationRepository)
    attachmentService = Mock(OperationConfigurationAttachmentService)
    protocolInstanceService = Mock(ProtocolInstanceService)
    operationRepository = Mock(OperationRepository)
    configurationProperties = Mock(ChVoteConfigurationProperties)
    configurationProperties.timezone >> "Europe/Zurich"
    service = new OperationConfigurationService(configurationRepository, attachmentService, protocolInstanceService)
  }

  def "createOperationConfiguration should return a new OperationConfiguration entity"() {
    given:
    def operation = new Operation()
    operation.id = 3
    def vo = new OperationConfigurationSubmissionVo()
    vo.user = "Fred"
    vo.gracePeriod = 500
    vo.canton = "GE"
    vo.groupVotation = true
    vo.operationName = "201706VP"
    vo.clientId = "1"
    vo.milestones = [:]
    vo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_OPEN] = LocalDateTime.ofEpochSecond(1, 0, ZoneOffset.UTC)
    vo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_CLOSE] = LocalDateTime.ofEpochSecond(2, 0, ZoneOffset.UTC)
    vo.testSitePrinter = new TestSitePrinter()
    vo.testSitePrinter.municipality = new MunicipalityVo()
    vo.testSitePrinter.municipality.ofsId = 6601
    vo.testSitePrinter.municipality.name = "Municipality 6601"
    vo.testSitePrinter.printer = new PrinterConfigurationVo()
    vo.testSitePrinter.printer.id = "printer-0"
    vo.testSitePrinter.printer.name = "Printer 0"
    vo.testSitePrinter.printer.publicKey = PrintersPublicKeys.publicKeyPa0()
    vo.electionSiteConfigurations = [
        createElectionsSiteConfigurationVo("ballot1"), createElectionsSiteConfigurationVo("ballot2")
    ]
    def files = Collections.emptyList()
    def before = LocalDateTime.now()
    operationRepository.findByClientId("1") >> Optional.of(operation)

    when:
    def configuration = service.createOperationConfiguration(vo, files)

    then:
    1 * attachmentService.populateAttachments(*_) >> { args -> args[0] }
    configuration.lastChangeUser == "Fred"
    configuration.gracePeriod == 500
    configuration.operationName == "201706VP"
    configuration.siteOpeningDate == LocalDateTime.ofEpochSecond(1, 0, ZoneOffset.UTC)
    configuration.siteClosingDate == LocalDateTime.ofEpochSecond(2, 0, ZoneOffset.UTC)
    configuration.lastChangeDate >= before
    configuration.testPrinter.businessIdentifier == "printer-0"
    configuration.testPrinter.printerName == "Printer 0"
    configuration.testPrinter.publicKey == PrintersPublicKeys.publicKeyPa0().toByteArray()
    configuration.testPrinter.municipalities.size() == 1
    configuration.testPrinter.municipalities.get(0).ofsId == 6601
    configuration.testPrinter.municipalities.get(0).name == "Municipality 6601"
    configuration.electionSiteConfigurations.size() == 2
    configuration.electionSiteConfigurations[0].ballot == "ballot1"
    configuration.electionSiteConfigurations[1].ballot == "ballot2"
  }

  private static ElectionSiteConfigurationVo createElectionsSiteConfigurationVo(String ballot) {
    ElectionSiteConfigurationVo vo = new ElectionSiteConfigurationVo();
    vo.ballot = ballot
    vo.columnsOrderOnVerificationTable = [] as String[]
    vo.displayedColumnsOnVerificationTable = [] as String[]
    return vo;
  }

  def "invalidateLatestOperationConfiguration should correctly invalidate a configuration"() {
    given: "an operation"
    def operation = new Operation()

    and: "a configuration"
    def configuration = new OperationConfiguration()
    configuration.validationStatus = ValidationStatus.NOT_YET_VALIDATED
    operation.configurationInTest = configuration

    and: "a protocol instance"
    def protocolInstance = new ProtocolInstance()
    configuration.protocolInstance = protocolInstance

    when: "the invalidation method is called"
    service.invalidateLatestOperationConfiguration(operation, "Invalidator")

    then: "the configuration should be updated with the proper status"
    1 * configurationRepository.save(_) >> { arguments ->
      def updatedConfiguration = (arguments[0] as OperationConfiguration)

      assert updatedConfiguration.validationStatus == ValidationStatus.INVALIDATED
      assert updatedConfiguration.lastChangeUser == "Invalidator"
      assert !updatedConfiguration.protocolInstance.isPresent()
    }

    and: "the protocol instance should be stopped"
    1 * protocolInstanceService.deleteProtocolInstance(protocolInstance)
  }

  def "invalidateLatestOperationConfiguration should not fail if there is no protocol instance"() {
    given: "an operation"
    def operation = new Operation()

    and: "a configuration"
    def configuration = new OperationConfiguration()
    configuration.validationStatus = ValidationStatus.NOT_YET_VALIDATED
    operation.configurationInTest = configuration

    when: "the invalidation method is called"
    service.invalidateLatestOperationConfiguration(operation, "Invalidator")

    then: "the configuration should be update with the proper status"
    1 * configurationRepository.save(_) >> { arguments ->
      def updatedConfiguration = (arguments[0] as OperationConfiguration)

      assert updatedConfiguration.validationStatus == ValidationStatus.INVALIDATED
      assert updatedConfiguration.lastChangeUser == "Invalidator"
      assert !updatedConfiguration.protocolInstance.isPresent()
    }

    and: "the protocol instance should be stopped"
    0 * protocolInstanceService.deleteProtocolInstance(_)
  }

  def "invalidateLatestOperationConfiguration should fail if the attached protocol is running in production"() {
    given: "an operation"
    def operation = new Operation()

    and: "a configuration"
    def configuration = new OperationConfiguration()
    configuration.validationStatus = ValidationStatus.NOT_YET_VALIDATED
    operation.configurationInTest = configuration

    and: "a protocol instance running in production"
    def protocolInstance = new ProtocolInstance()
    protocolInstance.environment = ProtocolEnvironment.PRODUCTION
    configuration.protocolInstance = protocolInstance

    when: "the invalidation method is called"
    service.invalidateLatestOperationConfiguration(operation, "Invalidator")

    then: "an exception should be thrown"
    thrown(IllegalStateException)
  }

  def "validateLatestOperationConfiguration should correctly validate a configuration"() {
    given: "an operation"
    def operation = new Operation()

    and: "a configuration"
    def configuration = new OperationConfiguration()
    configuration.validationStatus = ValidationStatus.NOT_YET_VALIDATED
    operation.configurationInTest = configuration

    and: "a protocol instance"
    def protocolInstance = new ProtocolInstance()
    configuration.protocolInstance = protocolInstance

    when: "the validation method is called"
    service.validateLatestOperationConfiguration(operation, "Validator")

    then: "the configuration should be updated with the proper status"
    1 * configurationRepository.save(_) >> { arguments ->
      def updatedConfiguration = (arguments[0] as OperationConfiguration)

      assert updatedConfiguration.validationStatus == ValidationStatus.VALIDATED
      assert updatedConfiguration.lastChangeUser == "Validator"
      assert updatedConfiguration.protocolInstance == Optional.of(protocolInstance)
    }
  }

  def "validateLatestOperationConfiguration should fail if there is no protocol instance"() {
    given: "an operation"
    def operation = new Operation()

    and: "a configuration"
    def configuration = new OperationConfiguration()
    configuration.validationStatus = ValidationStatus.NOT_YET_VALIDATED
    operation.configurationInTest = configuration

    when: "the validation method is called"
    service.validateLatestOperationConfiguration(operation, "Validator")

    then: "an exception should be thrown"
    thrown(IllegalStateException)
  }

  def "validateLatestOperationConfiguration should fail if the attached protocol is running in production"() {
    given: "an operation"
    def operation = new Operation()

    and: "a configuration"
    def configuration = new OperationConfiguration()
    configuration.validationStatus = ValidationStatus.NOT_YET_VALIDATED
    operation.configurationInTest = configuration

    and: "a protocol instance running in production"
    def protocolInstance = new ProtocolInstance()
    protocolInstance.environment = ProtocolEnvironment.PRODUCTION
    configuration.protocolInstance = protocolInstance

    when: "the validation method is called"
    service.validateLatestOperationConfiguration(operation, "Validator")

    then: "an exception should be thrown"
    thrown(IllegalStateException)
  }

}
