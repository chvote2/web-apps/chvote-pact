/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol

import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.exception.TallyArchiveGenerationException
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolHandlerChain
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolState
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolStateFactory
import ch.ge.ve.chvote.pactback.service.protocol.processor.TallyArchiveGenerationHandler
import ch.ge.ve.protocol.client.ProtocolClient
import spock.lang.Specification

class ProtocolTallyArchiveServiceTest extends Specification {

  def protocolStateFactory = Mock(ProtocolStateFactory)
  def tallyGenerationHandler = Mock(TallyArchiveGenerationHandler)

  ProtocolTallyArchiveService service

  def setup() {
    service = new ProtocolTallyArchiveService(protocolStateFactory, tallyGenerationHandler)
  }

  def "startTallying should not initiate the tallying process if there are not enough confirmed votes"() {
    given:
    def instance = new ProtocolInstance()
    def protocolClient = Mock(ProtocolClient)
    protocolClient.getVotesCastCount() >> 0L
    def state = Mock(ProtocolState)
    state.getClient() >> protocolClient

    protocolStateFactory.initProtocolState(instance) >> state

    when:
    service.startTallying(instance)

    then:
    0 * tallyGenerationHandler.process(state, _)
    1 * state.updateProtocolStatus(ProtocolInstanceStatus.NOT_ENOUGH_VOTES_TO_INITIATE_TALLY)
  }

  def "startTallying should run the tally generation chain if there are enough confirmed votes"() {
    given:
    def instance = new ProtocolInstance()
    def protocolClient = Mock(ProtocolClient)
    protocolClient.getVotesCastCount() >> 10L
    def state = Mock(ProtocolState)
    state.getClient() >> protocolClient

    protocolStateFactory.initProtocolState(instance) >> state

    when:
    service.startTallying(instance)

    then:
    1 * tallyGenerationHandler.process(state, _) >> { ProtocolState s, ProtocolHandlerChain chain -> chain.proceed(s) }
  }

  def "startTallying - instance should be set to TALLY_GENERATION_FAILURE when an exception occurs in chain"() {
    given:
    def instance = new ProtocolInstance()
    def protocolClient = Mock(ProtocolClient)
    protocolClient.getVotesCastCount() >> 10L
    def state = Mock(ProtocolState)
    state.getClient() >> protocolClient

    state.getOperation() >> new Operation(clientId: "Some client Id") // Needed for exception message

    protocolStateFactory.initProtocolState(instance) >> state
    tallyGenerationHandler.process(state, _) >> { throw new RuntimeException("Exception thrown for test purposes") }

    when:
    service.startTallying(instance)

    then:
    thrown TallyArchiveGenerationException
    1 * tallyGenerationHandler.indicateFailure(state)
  }
}
