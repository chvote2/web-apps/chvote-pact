/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action

import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.APPROVED
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.PENDING
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.REJECTED

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.VotingPeriodInitializationActionRepository
import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.entity.VotingPeriodInitialization
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.PublicKeyFile
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.period.VotingPeriodConfigurationRepository
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration
import ch.ge.ve.chvote.pactback.repository.user.UserRepository
import ch.ge.ve.chvote.pactback.repository.user.entity.User
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyExistsException
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionNotFoundException
import ch.ge.ve.chvote.pactback.service.exception.SelfActionResolutionException
import ch.ge.ve.chvote.pactback.service.operation.OperationService
import java.util.stream.Stream
import spock.lang.Specification

class VotingPeriodInitializationServiceTest extends Specification {
  def userRepository = Mock(UserRepository)
  def periodConfigurationRepository = Mock(VotingPeriodConfigurationRepository)
  def actionRepository = Mock(VotingPeriodInitializationActionRepository)
  def operationService = Mock(OperationService)

  VotingPeriodInitializationService service = new VotingPeriodInitializationService(
          userRepository, periodConfigurationRepository, actionRepository, operationService)


  def "CreateAction should fail if there is no prior voting period configuration"() {
    given:
    actionRepository.existsByVotingPeriodConfiguration_Id(1) >> false
    periodConfigurationRepository.findById(1) >> Optional.empty()

    when:
    service.createAction(1, "User1")

    then:
    def exception = thrown(EntityNotFoundException)
    exception.message == "No VotingPeriodConfiguration found for id: 1"
  }


  def "CreateAction should fail if there is already a request"() {
    given:
    actionRepository.existsByVotingPeriodConfiguration_Id(1) >> true

    when:
    service.createAction(1, "User1")

    then:
    def exception = thrown(PrivilegedActionAlreadyExistsException)
    exception.message == "An action was already created for the VotingPeriodConfiguration object with ID 1"
  }


  def "CreateAction should store action in the database"() {
    given:
    def user = Mock(User)
    VotingPeriodConfiguration configuration = buildConfiguration(1L)
    def operation = buildOperation(configuration)
    operationService.getOperationByVotingPeriodConfigurationId(1L) >> operation
    userRepository.findByUsername("User1") >> Optional.of(user)
    periodConfigurationRepository.findById(1) >> Optional.of(configuration)
    actionRepository.existsByVotingPeriodConfiguration_Id(1) >> false

    when:
    service.createAction(1, "User1")

    then:
    1 * actionRepository.save(*_) >> { arguments ->
      assert (arguments[0] as VotingPeriodInitialization).status == PENDING
      assert (arguments[0] as VotingPeriodInitialization).votingPeriodConfiguration == configuration
      assert (arguments[0] as VotingPeriodInitialization).requester == user
    }
  }

  VotingPeriodConfiguration buildConfiguration(long id) {
    def config = new VotingPeriodConfiguration(id: id)
    config.target = DeploymentTarget.SIMULATION
    config.electoralAuthorityKey = new PublicKeyFile()
    config.electoralAuthorityKey.type = AttachmentType.ELECTION_OFFICER_KEY
    return config
  }

  Operation buildOperation(VotingPeriodConfiguration votingPeriodConfiguration) {
    Operation operation = new Operation()
    operation.setDeployedConfiguration(new OperationConfiguration())
    operation.setVotingPeriodConfiguration(votingPeriodConfiguration)
    return operation
  }

  def "ApproveAction should  fail if there is no action with status PENDING"() {
    given:
    def user = Mock(User)
    userRepository.findByUsername("validator") >> Optional.of(user)
    actionRepository.findById(1) >> Optional.empty()

    when:
    service.approveAction(1, "validator")

    then:
    def exception = thrown(PrivilegedActionNotFoundException)
    exception.message == "No privileged action found for id 1"
  }

  def "ApproveAction should fail if the validator is the requestor"() {
    given:
    def requester = user(1, "requester")
    userRepository.findByUsername(requester.username) >> Optional.of(requester)
    actionRepository.findById(1) >> Optional.of(action(requester))

    when:
    service.approveAction(1, requester.username)

    then:
    def exception = thrown(SelfActionResolutionException)
    exception.message == "Users may not approve their own privileged actions"

  }


  def "ApproveAction should succeed if the validator is not the requestor"() {
    given:
    def requester = user(1, "requester")
    def validator = user(2, "validator")
    userRepository.findByUsername(requester.username) >> Optional.of(requester)
    userRepository.findByUsername(validator.username) >> Optional.of(validator)
    actionRepository.findById(1) >> Optional.of(action(requester))

    when:
    service.approveAction(1, validator.username)

    then:
    1 * actionRepository.save(*_) >> { arguments ->
      assert (arguments[0] as PrivilegedAction).status == APPROVED
    }
  }

  def "ApproveAction should fail if the action status is not PENDING"() {
    given:
    def requester = user(1, "requester")
    def validator = user(2, "validator")
    userRepository.findByUsername(requester.username) >> Optional.of(requester)
    userRepository.findByUsername(validator.username) >> Optional.of(validator)
    actionRepository.findById(1) >> Optional.of(action(requester, APPROVED))

    when:
    service.approveAction(1L, "validator")

    then:
    0 * operationService.publishVotingPeriodConfiguration(_)
    0 * actionRepository.save(*_)
    thrown(PrivilegedActionAlreadyProcessedException)
  }

  User user(long id, String name) {
    User user = new User()
    user.id = id
    user.username = name
    user
  }

  VotingPeriodInitialization action(User requester, PrivilegedAction.Status status = PENDING) {
    def action = new VotingPeriodInitialization()
    action.id = 1
    action.setRequester(requester)
    action.setStatus(status)
    action.setVotingPeriodConfiguration(new VotingPeriodConfiguration())

    return action
  }

  def "RejectAction should  fail if there is no action with status PENDING"() {
    given:
    def user = Mock(User)
    userRepository.findByUsername("validator") >> Optional.of(user)
    actionRepository.findById(1) >> Optional.empty()

    when:
    service.rejectAction(1, "validator", "not validated")

    then:
    def exception = thrown(PrivilegedActionNotFoundException)
    exception.message == "No privileged action found for id 1"
  }

  def "RejectAction should fail if the validator is the requestor"() {
    given:
    def requester = user(1, "requester")
    userRepository.findByUsername(requester.username) >> Optional.of(requester)
    actionRepository.findById(1) >> Optional.of(action(requester))

    when:
    service.rejectAction(1, requester.username, "not validated")

    then:
    def exception = thrown(SelfActionResolutionException)
    exception.message == "Users may not approve their own privileged actions"

  }


  def "RejectAction should succeed if the validator is not the requestor"() {
    given:
    def requester = user(1, "requester")
    def validator = user(2, "validator")
    userRepository.findByUsername(requester.username) >> Optional.of(requester)
    userRepository.findByUsername(validator.username) >> Optional.of(validator)
    actionRepository.findById(1) >> Optional.of(action(requester))

    when:
    service.rejectAction(1, validator.username, "not validated")

    then:
    1 * actionRepository.save(*_) >> { arguments ->
      assert (arguments[0] as PrivilegedAction).status == REJECTED
      assert (arguments[0] as PrivilegedAction).rejectionReason == "not validated"
    }
  }


  def "getByBusinessId should return an action if it is not created already"() {
    given:
    def configuration = buildConfiguration(1L)
    def operation = buildOperation(configuration)
    operationService.getOperationByVotingPeriodConfigurationId(1L) >> operation
    periodConfigurationRepository.getOne(1L) >> configuration

    when:
    def pendingAction = service.getByBusinessId(1L)

    then:
    pendingAction.status == "NOT_CREATED"
  }

  def "getByBusinessId should return an action if an action is pending"() {
    given:
    def requester = user(1, "requester")
    def configuration = buildConfiguration(1L)
    configuration.setAction(action(requester))
    def operation = buildOperation(configuration)
    operationService.getOperationByVotingPeriodConfigurationId(1L) >> operation
    periodConfigurationRepository.getOne(1L) >> configuration

    when:
    def pendingAction = service.getByBusinessId(1L)

    then:
    pendingAction.status == "PENDING"
  }

  def "getByBusinessId should fail if an action is rejected or approved"() {
    given:
    def requester = user(1, "requester")
    def configuration = buildConfiguration(1L)
    configuration.setAction(action(requester, APPROVED))
    def operation = buildOperation(configuration)
    operationService.getOperationByVotingPeriodConfigurationId(1L) >> operation
    periodConfigurationRepository.getOne(1L) >> configuration

    when:
    service.getByBusinessId(1L)

    then:
    def exception = thrown(PrivilegedActionAlreadyProcessedException)
    exception.message == "Action with id 1 has already been processed and cannot be displayed"

    where:
    _ | status
    _ | APPROVED
    _ | REJECTED
  }

  def "GetNewOrPendingActions should return all actions that can be created or that are pending"() {
    def requester = user(1, "requester")
    def configuration1 = buildConfiguration(1L)
    def configuration2 = buildConfiguration(2L)
    configuration2.setAction(action(requester))

    operationService.getOperationByVotingPeriodConfigurationId(1L) >> buildOperation(configuration1)
    operationService.getOperationByVotingPeriodConfigurationId(2L) >> buildOperation(configuration2)
    periodConfigurationRepository.findAllByActionIsNullOrActionStatus(PENDING) >> Stream.of(configuration1, configuration2)

    when:
    def actions = service.getNewOrPendingActions()

    then:
    actions.size() == 2
    actions.status == ["NOT_CREATED", "PENDING"]
  }

  def "getPendingAction should return a pending action"() {
    given:
    def requester = user(1, "requester")
    def configuration = buildConfiguration(1l)
    def action = action(requester)
    action.setVotingPeriodConfiguration(configuration)
    def operation = buildOperation(configuration)
    operationService.getOperationByVotingPeriodConfigurationId(1L) >> operation
    actionRepository.findById(1L) >> Optional.of(action)

    when:
    def pendingAction = service.getPendingPrivilegedAction(1L)

    then:
    pendingAction.status == "PENDING"
  }

}
