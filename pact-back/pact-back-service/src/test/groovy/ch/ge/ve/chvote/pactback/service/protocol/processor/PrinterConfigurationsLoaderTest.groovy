/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor

import static ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.MunicipalityBuilder.aMunicipality
import static ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.MunicipalityBuilder.aVirtualMunicipality
import static ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfigurationBuilder.aPrinterConfiguration

import ch.ge.ve.chvote.pactback.fixtures.PrintersPublicKeys
import ch.ge.ve.chvote.pactback.repository.operation.configuration.OperationConfigurationRepository
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfigurationBuilder
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration
import spock.lang.Specification

class PrinterConfigurationsLoaderTest extends Specification {
  private VotingMaterialsConfigurationRepository votingMaterialsConfigurationRepository
  private OperationConfigurationRepository operationConfigurationRepository
  private PrinterConfigurationsLoader loader

  private basePrinter1 = aPrinterConfiguration().withBusinessIdentifier("printer-1")
          .withPrinterName("Printer 1")
          .withPublicKey(PrintersPublicKeys.publicKeyPa0())
          .build()
  private basePrinter2 = aPrinterConfiguration().withBusinessIdentifier("printer-2")
          .withPrinterName("Printer 2")
          .withPublicKey(PrintersPublicKeys.publicKeyPa1())
          .build()

  void setup() {
    votingMaterialsConfigurationRepository = Mock(VotingMaterialsConfigurationRepository)
    operationConfigurationRepository = Mock(OperationConfigurationRepository)
    loader = new PrinterConfigurationsLoader(votingMaterialsConfigurationRepository, operationConfigurationRepository)
  }

  def "should create a map of municipality to printer business id"() {
    given:
    VotingMaterialsConfiguration materialsConf = new VotingMaterialsConfiguration()
    materialsConf.printers = [
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withMunicipalities([ aMunicipality().quickBuild(6601, "Municipality 1") ])
                    .build(),
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withMunicipalities([ aVirtualMunicipality().quickBuild(6699, "Municipality 2") ])
                    .build(),
            PrinterConfigurationBuilder.copyOf(basePrinter2)
                    .withMunicipalities([
                            aMunicipality().quickBuild(6602, "Municipality 3"),
                            aMunicipality().quickBuild(6603, "Municipality 4")
                    ]).build()
    ]

    materialsConf.setSwissAbroadPrinter(
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withSwissAbroadWithoutMunicipality(true)
                    .build())

    votingMaterialsConfigurationRepository.getOne(_) >> materialsConf

    when:
    def map = loader.generateMunicipalityToPrintersMap(1L)

    then:
    map.size() == 4
    map.get(6601) == "printer-1"
    map.get(6699) == "printer-1"
    map.get(6602) == "printer-2"
    map.get(6602) == "printer-2"
  }

  def "should create a map of canton to default printer business id"() {
    given:
    VotingMaterialsConfiguration materialsConf = new VotingMaterialsConfiguration()
    materialsConf.printers = [
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withMunicipalities([ aMunicipality().quickBuild(6601, "Municipality 1") ])
                    .build(),
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withMunicipalities([ aVirtualMunicipality().quickBuild(6699, "Municipality 2") ])
                    .build(),
            PrinterConfigurationBuilder.copyOf(basePrinter2)
                    .withMunicipalities([
                    aMunicipality().quickBuild(6602, "Municipality 3"),
                    aMunicipality().quickBuild(6603, "Municipality 4")
            ]).build()
    ]

    materialsConf.setSwissAbroadPrinter(
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withSwissAbroadWithoutMunicipality(true)
                    .build())

    votingMaterialsConfigurationRepository.getOne(_) >> materialsConf

    when:
    def map = loader.generateCantonToPrintersMap(1L, "GE")

    then:
    map.size() == 1
    map.get("GE") == "printer-1"
  }

  def "should create an empty map of canton to default printer business id if no swiss abroad printer configuration"() {
    given:
    VotingMaterialsConfiguration materialsConf = new VotingMaterialsConfiguration()
    materialsConf.printers = [
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withMunicipalities([ aMunicipality().quickBuild(6601, "Municipality 1") ])
                    .build(),
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withMunicipalities([ aVirtualMunicipality().quickBuild(6699, "Municipality 2") ])
                    .build(),
            PrinterConfigurationBuilder.copyOf(basePrinter2)
                    .withMunicipalities([
                    aMunicipality().quickBuild(6602, "Municipality 3"),
                    aMunicipality().quickBuild(6603, "Municipality 4")
            ]).build()
    ]

    votingMaterialsConfigurationRepository.getOne(_) >> materialsConf

    when:
    def map = loader.generateCantonToPrintersMap(1L, "GE")

    then:
    map.size() == 0
  }

  def "should create a unique printer authority by printer business id"() {
    given:
    VotingMaterialsConfiguration materialsConf = new VotingMaterialsConfiguration()
    materialsConf.printers = [
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withMunicipalities([ aMunicipality().quickBuild(6601, "Municipality 1") ])
                    .build(),
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withMunicipalities([ aVirtualMunicipality().quickBuild(6699, "Municipality 2") ])
                    .build()
    ]

    materialsConf.setSwissAbroadPrinter(
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withSwissAbroadWithoutMunicipality(true)
                    .build())

    votingMaterialsConfigurationRepository.getOne(_) >> materialsConf

    when:
    def printingAuthoritiesWithCert = loader.createPrintingAuthority(1L)

    then:
    printingAuthoritiesWithCert.size() == 1
    printingAuthoritiesWithCert[0].name == "printer-1"
  }

  def "should create a unique printer authority if only swiss abroad printer"() {
    given:
    VotingMaterialsConfiguration materialsConf = new VotingMaterialsConfiguration()
    materialsConf.printers = []

    materialsConf.setSwissAbroadPrinter(
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withSwissAbroadWithoutMunicipality(true)
                    .build())

    votingMaterialsConfigurationRepository.getOne(_) >> materialsConf

    when:
    def printingAuthoritiesWithCert = loader.createPrintingAuthority(1L)

    then:
    printingAuthoritiesWithCert.size() == 1
    printingAuthoritiesWithCert[0].name == "printer-1"
  }

  def "should create a unique printer authority if only municipalities printers"() {
    given:
    VotingMaterialsConfiguration materialsConf = new VotingMaterialsConfiguration()
    materialsConf.printers = [
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withMunicipalities([ aMunicipality().quickBuild(6601, "Municipality 1") ])
                    .build(),
            PrinterConfigurationBuilder.copyOf(basePrinter1)
                    .withMunicipalities([ aVirtualMunicipality().quickBuild(6699, "Municipality 2") ])
                    .build()
    ]

    votingMaterialsConfigurationRepository.getOne(_) >> materialsConf

    when:
    def printingAuthoritiesWithCert = loader.createPrintingAuthority(1L)

    then:
    printingAuthoritiesWithCert.size() == 1
    printingAuthoritiesWithCert[0].name == "printer-1"
  }

}
