# CHVote: PACT Backend 

The privileged action confirmation tool (PACT) server side application.

# Modules

- [pact-back-repository](pact-back-repository): The persistence layer of the PACT application.
- [pact-back-service](pact-back-service):The service layer of the PACT backend application.
- [pact-back-mock-server](pact-back-mock-server): A server that allows the injection of mocked data directly into the PACT repository for testing purposes.
- [pact-back-rest](pact-back-rest): The collection of REST interfaces of the PACT backend application, it's also the application entry point.
- [pact-back-b2b-client](pact-back-b2b-client): A B2B endpoint client library.
- [pact-back-contract](pact-back-contract): A library to share the contract objects of the B2B REST API.
- [pact-back-fixtures](pact-back-fixtures): A collection of fixtures used for testing.
- [pact-back-scheduler](pact-back-scheduler): A server that allows the scheduling of tasks to be executed at a later time (most notably to close an operation at a given date).
- [pact-back-notification-events](pact-back-notification-events): A library exposing common functionality to register a listener to the PACT notification event queue.

# Development guidelines

Refer to the common [CHVote java coding style guidelines](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/development-directives/coding-style-Java.md).

# Building

## Pre-requisites

* JDK 11
* Maven

## Build steps

To build the project simply do:

```sh
mvn clean verify
```

A jacoco coverage report will be generated and stored under the `./target/site/jacoco/` subfolder of each module.

# Running

The application entry point is the `pact-back-rest` module, all the other modules are contained here as dependencies.
To start the server that exposes the REST API simply do:

```sh
cd pact-back-rest
mvn spring-boot:run -P dev
```

The available REST API endpoints are:

- Frontend [http://localhost:8281/api/](http://localhost:8281/api/): The REST API of the PACT web application.
- B2B [http://localhost:8281/b2b/](http://localhost:8281/b2b/): The interface between the PACT and the others CHVote backend applications.
- Management [http://localhost:8281/management/](http://localhost:8281/management/): The Spring Boot Actuator API (for monitoring).

e.g.:

```sh 
curl -H "X-Requested-With: XMLHttpRequest" --user user1:password  http://localhost:8281/api/version
```


**WARNING:** There is a recent bug in the `spring-boot-maven-plugin` that prevents the previous method from starting the 
REST API with the `dev` profile activated, for the time being you can workaround this problem by activating the profile 
with the following environment variable:

```sh
export SPRING_PROFILES_ACTIVE=dev
```

## Running the PACT scheduler

To launch the `pact-back-scheduler` do:

```sh
cd pact-back-scheduler
mvn spring-boot:run -P dev -Dserver.port=8181
```

To link the PACT application to the scheduler you'll need to indicate the scheduler location through the 
`CHVOTE_PACT_SCHEDULER_BASE_URI` environment variable and restart the application, e.g.:

```sh
export CHVOTE_PACT_SCHEDULER_BASE_URI=http://localhost:8181
cd pact-back-rest
mvn spring-boot:run -P dev
```

**NOTE**: You need to set the server port manually in the scheduler when using it along the main application
to avoid any port allocation conflicts.

## Running the mock server

The mock server entry point is the `chvote-mock-server/chvote-mock-server-rest` submodule.
To run the mock server simply do:

```sh
cd chvote-mock-server/chvote-mock-server-rest
mvn spring-boot:run
```

**NOTE** In order for the mock server to correctly start up, the PACT application must already be running with the `dev` spring profile active.

An interactive Swagger API documentation page will be available at [http://localhost:47646/mock/swagger-ui.html](http://localhost:47646/mock/swagger-ui.html).
The mock server REST API can also be queried at [http://localhost:47646/mock/](http://localhost:47646/mock/), e.g:

```sh
# Reset the database
curl -X DELETE http://localhost:47646/mock/database

# Request the list of pending actions, should return an empty list
curl -H "X-Requested-With: XMLHttpRequest" --user user1:password  http://localhost:8281/api/privileged-actions/pending-actions

# Intialize the database with a set of predefined operations
curl -X POST http://localhost:47646/mock/database

# Request the list of operations, should return a list with 5 mocked operations
curl -H "X-Requested-With: XMLHttpRequest" --user user1:password http://localhost:8281/api/privileged-actions/pending-actions
```

# Tips

## HTTP headers

Please note that there are **two mandatory HTTP headers** to set:
- `Authorization: Basic <base64-encoded username:password>` - basic authentication.
- `X-Requested-With: XMLHttpRequest` - to protect the API against Cross-Site Request Forgery, see [OWASP CSRF Prevention Cheat Sheat](https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)_Prevention_Cheat_Sheet#Protecting_REST_Services:_Use_of_Custom_Request_Headers).

## H2 database console

When activating spring profile `dev`, you can connect to the H2 database console at http://localhost:8381/api/h2-console/ using the following parameters:

- *JDBC URL*: jdbc:h2:mem:test;MODE=ORACLE;DB_CLOSE_DELAY=-1
- *User Name*: h2_test
- *Password*: h2_test

**WARNING**: This feature does not work for the time being because of the CORS header configuration.

## CHVote platform integration

Run a RabbitMQ queue that serves as the communication channel between the PACT and the CHVote platform:

```bash
docker-compose -f  pact-back-rest/docker-compose.integration-tests.yml up
```