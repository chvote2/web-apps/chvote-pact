/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.scheduler.tasks;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The registered scheduled tasks.
 * <p>
 *   The tasks handled by the "PACT-Scheduler" application are associated to an operation, in order to ask
 *   the PACT to generate the tally archive after the voting period has ended.
 *   The time when the request has been made is also indicated, as a marker of a "triggered task".
 * </p>
 */
@Entity
@Table(name = "PACT_T_SCHEDULED_TASKS")
@ApiModel(description = "A scheduled task for an operation")
public class ScheduledTask {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "SCHED_N_ID")
  @JsonIgnore
  private Long id;

  @Column(name = "SCHED_V_OPERATION_ID", nullable = false, unique = true)
  @ApiModelProperty(value = "identifies the operation in the caller's referential", required = true)
  private String operationId;

  @Column(name = "SCHED_D_END_TIME", nullable = false)
  @ApiModelProperty(value = "datetime when the voting period is considered finished", required = true)
  private LocalDateTime votingPeriodEndTime;

  @Column(name = "SCHED_D_TRIGGERED")
  @ApiModelProperty(value = "datetime when the scheduled task has been triggered")
  private LocalDateTime triggeredTime;

  @Column(name = "SCHED_N_STATUS")
  @ApiModelProperty(value = "HTTP status received when the task has been triggered")
  private Integer httpStatusReceived;

  public ScheduledTask() {
    // JPA constructor
  }

  public ScheduledTask(long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public String getOperationId() {
    return operationId;
  }

  public void setOperationId(String operationId) {
    this.operationId = operationId;
  }

  public LocalDateTime getVotingPeriodEndTime() {
    return votingPeriodEndTime;
  }

  public void setVotingPeriodEndTime(LocalDateTime votingPeriodEndTime) {
    this.votingPeriodEndTime = votingPeriodEndTime;
  }

  public LocalDateTime getTriggeredTime() {
    return triggeredTime;
  }

  public void setTriggeredTime(LocalDateTime triggeredTime) {
    this.triggeredTime = triggeredTime;
  }

  public Integer getHttpStatusReceived() {
    return httpStatusReceived;
  }

  public void setHttpStatusReceived(Integer httpStatusReceived) {
    this.httpStatusReceived = httpStatusReceived;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ScheduledTask that = (ScheduledTask) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "ScheduledTask{" +
           "id=" + id +
           ", operationId='" + operationId + '\'' +
           ", votingPeriodEndTime=" + votingPeriodEndTime +
           ", triggeredTime=" + triggeredTime +
           ", httpStatusReceived=" + httpStatusReceived +
           '}';
  }
}
