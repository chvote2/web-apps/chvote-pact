/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.scheduler.tally;

import ch.ge.ve.chvote.pactback.scheduler.tasks.ScheduledTask;
import ch.ge.ve.chvote.pactback.scheduler.tasks.ScheduledTasksRepository;
import java.time.LocalDateTime;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

/**
 * Scheduling service that will constantly poll the repository and execute the scheduled tasks.
 * <p>
 *   Uses Spring's @{@link Scheduled} mechanism to run the trigger method at a fixed rate.
 * </p>
 * <p>
 *   This component won't be loaded in the context if the Spring profile {@code "no-scheduler"} is active.
 * </p>
 */
@Service
@Profile("!no-scheduler")
public class ScheduledGenerationCommand {

  private static final Logger logger = LoggerFactory.getLogger(ScheduledGenerationCommand.class);

  private final ScheduledTasksRepository         scheduledTasksRepository;
  private final TransactionTemplate              transactionTemplate;
  private final TallyArchiveGenerationRestClient generationClient;

  public ScheduledGenerationCommand(ScheduledTasksRepository scheduledTasksRepository,
                                    TransactionTemplate transactionTemplate,
                                    TallyArchiveGenerationRestClient generationClient) {
    this.scheduledTasksRepository = scheduledTasksRepository;
    this.transactionTemplate = transactionTemplate;
    this.generationClient = generationClient;
  }

  /**
   * Requests the PACT to start the archive generation for all operations whose results are to be tallied.
   * <p>
   *   Searches the repository for all operations that have ended (excluding the already triggered ones).
   *   For each of those, makes a request to the PACT's endpoint and persists the result : response code
   *   and triggered datetime.
   * </p>
   * <p>
   *   Errors are caught so that we independently take care of each operation. If the endpoint returns
   *   a client error code (4xx) it is persisted and won't be retried <i>(client's call wouldn't change)</i>.
   *   If the endpoints returns a server error code (5xx), or any other exception occurs, nothing will be
   *   persisted and the task will be triggered again on the next scheduled run.
   * </p>
   */
  @Scheduled(initialDelayString = "${chvote.scheduler.init-delay}", fixedRateString = "${chvote.scheduler.rate}")
  public void triggerTallyArchivesGeneration() {
    logger.info("=====  Search for operations to be triggered");
    final List<ScheduledTask> tasks = scheduledTasksRepository.findAvailableTasksAt(LocalDateTime.now());
    logger.info("  > Found {} scheduled tasks", tasks.size());

    for (ScheduledTask task : tasks) {
      final String operationId = task.getOperationId();
      try {
        final HttpStatus httpStatus = generationClient.demandArchiveGeneration(operationId);
        logger.info("Call for operation [{}] returned a {} status", operationId, httpStatus);
        persistTriggeredTask(task, httpStatus);
      } catch (HttpClientErrorException e) {
        logger.warn("Call for operation [{}] failed from client side with status {}", operationId, e.getStatusCode());
        logger.debug("  > Client-side exception detail : ", e);
        persistTriggeredTask(task, e.getStatusCode());
      } catch (HttpServerErrorException e) {
        logger.error("Call for operation [{}] - Server failed with status {}", operationId, e.getStatusCode(), e);
      } catch (Exception e) {
        logger.error("Call for operation [{}] failed with exception", operationId, e);
      }
    }
  }

  // Saves the new state in a dedicated transaction
  private void persistTriggeredTask(ScheduledTask task, HttpStatus httpStatus) {
    task.setTriggeredTime(LocalDateTime.now());
    task.setHttpStatusReceived(httpStatus.value());

    transactionTemplate.execute(ts -> scheduledTasksRepository.save(task));
  }
}
