/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.scheduler.tasks;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A service to manage the scheduled tasks.
 */
@Transactional
@Service
public class ScheduledTasksService {

  private final ScheduledTasksRepository repository;

  @Autowired
  public ScheduledTasksService(ScheduledTasksRepository repository) {
    this.repository = repository;
  }

  /**
   * @return a list of all configured tasks
   */
  public List<ScheduledTask> findAll() {
    return repository.findAll();
  }

  /**
   * Creates a task for an operation.
   *
   * @param operationId identifier of the operation
   * @param votingPeriodEndTime datetime when the voting period ends - the task will be triggered from this instant on
   *
   * @return the scheduled task created for this operation
   *
   * @throws OperationAlreadyConfiguredException if a task already exists for the same "operationId"
   */
  public ScheduledTask createFor(String operationId, LocalDateTime votingPeriodEndTime) {
    final ScheduledTask taskInRepository = repository.findByOperationId(operationId);
    if (taskInRepository != null) {
      throw new OperationAlreadyConfiguredException(operationId, taskInRepository.getVotingPeriodEndTime(), votingPeriodEndTime);
    }

    ScheduledTask task = new ScheduledTask();
    task.setOperationId(operationId);
    task.setVotingPeriodEndTime(votingPeriodEndTime);
    return repository.save(task);
  }

  /**
   * Removes the scheduled task for an operation.
   *
   * @param operationId identifier of the operation
   * @return the id of the removed task
   *
   * @throws EntityNotFoundException if no task exists for this "operationId"
   */
  public long removeFor(String operationId) {
    final ScheduledTask task = repository.findByOperationId(operationId);
    if (task == null) {
      throw new EntityNotFoundException("No task found for operation " + operationId);
    }
    repository.delete(task);
    return task.getId();
  }

}
