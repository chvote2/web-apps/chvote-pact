/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.scheduler.tasks

import ch.ge.ve.chvote.pactback.scheduler.SpringBootTestWithoutScheduler
import java.time.LocalDateTime
import javax.transaction.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.jdbc.Sql
import spock.lang.Specification

@SpringBootTestWithoutScheduler
@Transactional
class ScheduledTasksRepositoryTest extends Specification {

  @Autowired
  private ScheduledTasksRepository repository

  @Sql(scripts = "classpath:sql/scheduled-tasks-repository-test.sql")
  def "FindTasksAvailableAt"() {
    when:
    def tasks = repository.findAvailableTasksAt(LocalDateTime.of(2018, 10, 10, 15, 55))

    then:
    tasks.collect { it.operationId } == ["operation-1", "operation-4"]
  }

}
