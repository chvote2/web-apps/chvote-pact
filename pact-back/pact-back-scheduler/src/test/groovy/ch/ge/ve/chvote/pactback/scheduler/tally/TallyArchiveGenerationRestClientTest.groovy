/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.scheduler.tally

import static org.springframework.test.web.client.match.MockRestRequestMatchers.header
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.web.client.HttpStatusCodeException
import spock.lang.Specification

@RestClientTest(TallyArchiveGenerationRestClient)
class TallyArchiveGenerationRestClientTest extends Specification {

  @Autowired
  private TallyArchiveGenerationRestClient client

  @Autowired
  private MockRestServiceServer server

  def "DemandArchiveGeneration should PUT with the clientId and return the status"(HttpStatus httpStatus) {
    given:
    def clientId = "sample-op"
    server.expect(requestTo("/operation/$clientId/tally-archive/demand")).andExpect(method(HttpMethod.PUT))
            .andRespond(withStatus(httpStatus))

    expect:
    client.demandArchiveGeneration(clientId) == httpStatus

    where:
    httpStatus << [HttpStatus.CREATED, HttpStatus.OK, HttpStatus.ACCEPTED]
  }

  def "DemandArchiveGeneration should throw an exception containing the status when the request has been rejected"(HttpStatus httpStatus) {
    given:
    def clientId = "sample-op"
    server.expect(requestTo("/operation/$clientId/tally-archive/demand")).andExpect(method(HttpMethod.PUT))
            .andRespond(withStatus(httpStatus))

    when:
    client.demandArchiveGeneration(clientId)

    then:
    def ex = thrown HttpStatusCodeException
    ex.statusCode == httpStatus

    where:
    httpStatus << [HttpStatus.NOT_FOUND, HttpStatus.FORBIDDEN, HttpStatus.INTERNAL_SERVER_ERROR]
  }

  def "DemandArchiveGeneration requests should contain the custom 'X-Requested-With' header"() {
    given:
    def clientId = "sample-op"
    server.expect(requestTo("/operation/$clientId/tally-archive/demand")).andExpect(method(HttpMethod.PUT))
            .andExpect(header("X-Requested-With", "XMLHttpRequest"))
            .andRespond(withSuccess())

    expect:
    client.demandArchiveGeneration(clientId) == HttpStatus.OK
  }
}
