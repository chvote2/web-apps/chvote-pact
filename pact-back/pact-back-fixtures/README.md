Pact fixtures : features
===================
The goal of this module is as a catalyst for writing tests. It proposes objects ready to be used in tests, as well as
some tools to easily customize them for specific test needs.

Pre-defined objects
-------------------
Factories offer pre-conceived objects in a well-known, business-valid state.
Optionally one can request a builder rather than the real object, in order to change a few things.
```java
List<Municipality> towns = Arrays.asList(Municipalities.geneve(), Municipalities.carouge());
Operation standardOperation = Operations.aStandardOperation();
Operation myOperation = Operations.standardOperationBuilder().withClientId("My personal ID").build();
```


Copies
------
Each builder offers a "copyOf" method so that it is easy to create variations by capturing an existing state :
```java
VotingMaterialsConfiguration knownConfig = Operations.aStandardOperation().getVotingMaterialsConfiguration();
VotingMaterialsConfiguration variation = VotingMaterialsConfigurationBuilder.copyOf(knownConfig)  // Deep copy of the knownConfig
												.withPrinters(preDefinedOtherPrinters).build();
```


Builders may offer simpler / coherent / multi-values setter methods (customized) :
----------
This eases the declaration in tests, thus both the usability and readability. But it requires a little bit of customization of the (generated) builders...
```java
  /**
   * Sets the file's content. Methods exist to set both content and hash consistently.
   *
   * @see #withRealFile(Path)
   * @see #withCalculatedFileHash()
   */
  public OperationReferenceFileBuilder withFile(byte[] content) { /* ... */ }

  /**
   * Sets the file hash - independent of the file's content. Methods exist to set both content and hash consistently.
   *
   * @see #withRealFile(Path)
   * @see #withCalculatedFileHash()
   */
  public OperationReferenceFileBuilder withFileHash(String fileHash) { /* ... */ }

  /**
   * Sets the hash for the file's content - content MUST have been set before calling this method !
   */
  public OperationReferenceFileBuilder withCalculatedFileHash() { /* ... */ }

  /**
   * Sets both file's content and hash by reading a real file resource.
   */
  public OperationReferenceFileBuilder withRealFile(Path path) { /* ... */ }
  
  
  // Collections :
  
  /** standard setter - accept any type of Collection */
  public OperationConfigurationBuilder withHighlightedQuestions(Collection<QuestionsVo> highlightedQuestions) {
    this.highlightedQuestions = new LinkedHashSet<>(highlightedQuestions);
    return this;
  }
  
  /** caller can modify the currently defined collection (add, remove, whatever..) */
  public OperationConfigurationBuilder withHighlightedQuestions(Consumer<Set<QuestionsVo>> consumer) { /* ... */ }
```


Builders may offer readable coherent factory methods (customized) :
----------
Same idea than about the setter methods but for creating builder objects : it can be very readable, but such additional
construction end-points should be hand-crafted.
```java
public final class OperationReferenceFileBuilder {
  private AttachmentType                         type;
  /* .. other fields .. */

  // Simpler and more explicit signature than using the "enum type flag"
  public static OperationReferenceFileBuilder aVotationRepository(String name) {
    return new OperationReferenceFileBuilder().withType(AttachmentType.OPERATION_REPOSITORY_VOTATION).withName(name);
  }

  public static OperationReferenceFileBuilder anElectionRepository(String name) { /* ... */ }

  public static OperationReferenceFileBuilder aDomainOfInfluence(String name) { /* ... */ }

  // etc.
}
```


Generated classes
--------------
One foreseen drawback of this project is related to maintaining the builder classes / fixtures factories when the target "business" classes change (structure).
This can be partially adressed by leveraging the cost of adapting our fixtures model and our tests :
 - IntelliJ has a plugin that can scaffhold a Builder for any mutable class.
 - we can provide the "fixtures" module with scripts (ie Groovy) to generate certain variations (like the "copyOf" method)
 - the fixtures factories offer sort of a facade API - tests using them should be moderately impacted by those changes,
   and "repairing" one factory method (after a structural change) could repair all tests using it