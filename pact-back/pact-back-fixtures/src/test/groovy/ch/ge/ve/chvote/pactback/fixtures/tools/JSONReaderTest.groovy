/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.fixtures.tools

import ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo
import ch.ge.ve.chvote.pactback.fixtures.PrintersPublicKeys
import ch.ge.ve.chvote.pactback.fixtures.ResourceLoadingException
import com.fasterxml.jackson.databind.JsonMappingException
import java.time.LocalDateTime
import spock.lang.Specification

class JSONReaderTest extends Specification {

  def "ReadFromClasspath should deserialize dates and public keys in CHVote format"() {
    when:
    def object = JSONReader.readFromClasspath("json/simple_operation.json", OperationConfigurationSubmissionVo)

    then: "The object should have been deserialized without exception"
    object.user == "test"
    object.clientId == "Some client ID"

    and: "the dates and public key should be correctly deserialized as well"
    object.operationDate == LocalDateTime.parse("2057-06-12T00:00:00")
    object.testSitePrinter.printer.publicKey == PrintersPublicKeys.publicKeyPa0()
  }

  def "ReadFromClasspath should wrap any Exception into ResourceLoadingException"() {
    when:
    JSONReader.readFromClasspath("src/main/resources/non_existing.json", OperationConfigurationSubmissionVo)

    then:
    def exc = thrown ResourceLoadingException
    exc.cause instanceof JsonMappingException
  }
}
