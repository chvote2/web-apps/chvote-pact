/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgressBuilder
//
// -------------------------------------------
// Create builder static method factory for each
// value of an enum attribute
// -------------------------------------------

def builder = ProtocolStageProgressBuilder
def values = ProtocolStage.values()

// ==========  FUNCTIONS  ==========
static toCamelCase(String snakeString, String prefix) {
  return prefix + snakeString.toLowerCase().split('_').collect { it.capitalize() }.join()
}

static getAttributeName(Class owner, Class attributeType) {
  def current = owner
  while (current != null && current != Object) {
    def candidate = current.declaredFields.find { it.type == attributeType }
    if (candidate != null) {
      return candidate.name
    }

    current = current.superclass
  }
}

static asString(Enum val) {
  return val.class.simpleName + "." + val.name()
}
// =================================


// -------------- Script begins here --------------
values.each {
  def methodName = toCamelCase(it.name(), "a")
  def attributeName = getAttributeName(builder, it.class)

  println "public static ${builder.simpleName} ${methodName}() {"
  println "  return new ${builder.simpleName}().with${attributeName.capitalize()}(${asString(it)});"
  println "}\n"
}