/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.fixtures.repository;

import static ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.MunicipalityBuilder.aVirtualMunicipality;
import static ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfigurationBuilder.aPrinterConfiguration;

import ch.ge.ve.chvote.pactback.fixtures.PrintersPublicKeys;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import java.util.Collections;

/**
 * A static factory to obtain (entities) {@link PrinterConfiguration} objects.
 */
public class Printers {

  private Printers() {
    throw new AssertionError("Not instantiable");
  }

  public static PrinterConfiguration bohVirtualPrinter() {
    return aPrinterConfiguration()
        .withPrinterName("BOH Virtual Printer")
        .withBusinessIdentifier("boh-vprt")
        .withPublicKey(PrintersPublicKeys.publicKeyPa0())
        .withMunicipalities(Collections.singleton(
            aVirtualMunicipality().quickBuild(9999, "Virtual municipality for printer boh-vprt")
        ))
        .build();
  }

}
