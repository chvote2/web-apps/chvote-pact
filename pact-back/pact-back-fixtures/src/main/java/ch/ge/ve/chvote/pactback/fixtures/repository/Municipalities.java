/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.fixtures.repository;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.Municipality;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.MunicipalityBuilder;

/**
 * A static factory to obtain (entities) {@link Municipality} objects.
 */
public class Municipalities {

  private Municipalities() {
    throw new AssertionError("Not instantiable");
  }

  public static Municipality carouge() {
    return MunicipalityBuilder.aMunicipality().withOfsId(6608).withName("Carouge (GE)").build();
  }

  public static Municipality geneve() {
    return MunicipalityBuilder.aMunicipality().withOfsId(6621).withName("Genève").build();
  }

  public static Municipality lancy() {
    return MunicipalityBuilder.aMunicipality().withOfsId(6628).withName("Lancy").build();
  }

  public static Municipality onex() {
    return MunicipalityBuilder.aMunicipality().withOfsId(6631).withName("Onex").build();
  }

}
