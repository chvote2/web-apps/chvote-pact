/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A builder for {@link PrinterConfiguration} objects.
 * <p>
 * Get a new builder with one of its static factory method.
 * </p>
 */
public final class PrinterConfigurationBuilder {
  private String             businessIdentifier;
  private String             printerName;
  private BigInteger         publicKey;
  private List<Municipality> municipalities = new ArrayList<>();
  private boolean            swissAbroadWithoutMunicipality;

  private PrinterConfigurationBuilder() {
  }

  public static PrinterConfigurationBuilder aPrinterConfiguration() {
    return new PrinterConfigurationBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   * All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   *
   * @return a new builder instance
   */
  public static PrinterConfigurationBuilder copyOf(PrinterConfiguration prototype) {
    return new PrinterConfigurationBuilder()
        .withBusinessIdentifier(prototype.getBusinessIdentifier())
        .withPrinterName(prototype.getPrinterName())
        .withPublicKey(new BigInteger(prototype.getPublicKey()))
        .withMunicipalities(prototype.getMunicipalities().stream()
                                     .map(MunicipalityBuilder::copyOf)
                                     .map(MunicipalityBuilder::build)
                                     .collect(Collectors.toList()))
        .withSwissAbroadWithoutMunicipality(prototype.isSwissAbroadWithoutMunicipality());
  }

  public PrinterConfigurationBuilder withBusinessIdentifier(String businessIdentifier) {
    this.businessIdentifier = businessIdentifier;
    return this;
  }

  public PrinterConfigurationBuilder withPrinterName(String printerName) {
    this.printerName = printerName;
    return this;
  }

  public PrinterConfigurationBuilder withPublicKey(BigInteger publicKey) {
    this.publicKey = publicKey;
    return this;
  }


  public PrinterConfigurationBuilder withMunicipalities(Collection<Municipality> municipalities) {
    this.municipalities = new ArrayList<>(municipalities);
    return this;
  }

  public PrinterConfigurationBuilder withSwissAbroadWithoutMunicipality(boolean swissAbroadWithoutMunicipality) {
    this.swissAbroadWithoutMunicipality = swissAbroadWithoutMunicipality;
    return this;
  }

  public PrinterConfiguration build() {
    PrinterConfiguration printerConfiguration = new PrinterConfiguration();
    printerConfiguration.setBusinessIdentifier(businessIdentifier);
    printerConfiguration.setPrinterName(printerName);
    printerConfiguration.setPublicKey(publicKey.toByteArray());
    printerConfiguration.setMunicipalities(municipalities);
    printerConfiguration.setSwissAbroadWithoutMunicipality(swissAbroadWithoutMunicipality);
    return printerConfiguration;
  }
}
