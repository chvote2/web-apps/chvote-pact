/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import static java.util.stream.Collectors.toList;

import ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo.Milestone;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A builder for {@link OperationConfigurationSubmissionVo} objects.
 * <p>
 * Get a new builder with one of its static factory method.
 * </p>
 */
public final class OperationConfigurationSubmissionVoBuilder {
  private String                           user;
  private String                           clientId;
  private String                           operationName;
  private LocalDateTime                    operationDate;
  private Integer                          gracePeriod;
  private String                           canton;
  private boolean                          groupVotation;
  private TestSitePrinter                  testSitePrinter;
  private List<AttachmentFileEntryVo>      attachments;
  private Set<ElectionSiteConfigurationVo> electionSiteConfigurations;
  private Set<HighlightedQuestionsVo>      highlightedQuestions;
  private Set<BallotDocumentationVo>       ballotDocumentations;
  private Map<Milestone, LocalDateTime>    milestones;

  private OperationConfigurationSubmissionVoBuilder() {
  }

  public static OperationConfigurationSubmissionVoBuilder anOperationConfigurationSubmissionVo() {
    return new OperationConfigurationSubmissionVoBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   * All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   *
   * @return a new builder instance
   */
  public static OperationConfigurationSubmissionVoBuilder copyOf(OperationConfigurationSubmissionVo prototype) {
    return new OperationConfigurationSubmissionVoBuilder()
        .withUser(prototype.getUser())
        .withClientId(prototype.getClientId())
        .withOperationName(prototype.getOperationName())
        .withOperationDate(prototype.getOperationDate())
        .withGracePeriod(prototype.getGracePeriod())
        .withCanton(prototype.getCanton())
        .withGroupVotation(prototype.isGroupVotation())
        .withTestSitePrinter(nullsafeCopy(prototype.getTestSitePrinter()))
        .withAttachments(prototype.getAttachments().stream()
                                  .map(AttachmentFileEntryVoBuilder::copyOf)
                                  .map(AttachmentFileEntryVoBuilder::build)
                                  .collect(toList()))
        .withElectionSiteConfigurations(prototype.getElectionSiteConfigurations().stream()
                                                 .map(ElectionSiteConfigurationVoBuilder::copyOf)
                                                 .map(ElectionSiteConfigurationVoBuilder::build)
                                                 .collect(toList()))
        .withHighlightedQuestions(prototype.getHighlightedQuestions().stream()
                                           .map(HighlightedQuestionsVoBuilder::copyOf)
                                           .map(HighlightedQuestionsVoBuilder::build)
                                           .collect(toList()))
        .withBallotDocumentations(prototype.getBallotDocumentations().stream()
                                           .map(BallotDocumentationVoBuilder::copyOf)
                                           .map(BallotDocumentationVoBuilder::build)
                                           .collect(toList()))
        .withMilestones(prototype.getMilestones());
  }

  private static TestSitePrinter nullsafeCopy(TestSitePrinter value) {
    return value == null ? null : TestSitePrinterBuilder.copyOf(value).build();
  }

  public OperationConfigurationSubmissionVoBuilder withUser(String user) {
    this.user = user;
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withClientId(String clientId) {
    this.clientId = clientId;
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withOperationName(String operationName) {
    this.operationName = operationName;
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withOperationDate(LocalDateTime operationDate) {
    this.operationDate = operationDate;
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withGracePeriod(Integer gracePeriod) {
    this.gracePeriod = gracePeriod;
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withCanton(String canton) {
    this.canton = canton;
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withGroupVotation(boolean groupVotation) {
    this.groupVotation = groupVotation;
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withTestSitePrinter(TestSitePrinter testSitePrinter) {
    this.testSitePrinter = testSitePrinter;
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withAttachments(Collection<AttachmentFileEntryVo> attachments) {
    this.attachments = new ArrayList<>(attachments);
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withAttachments(AttachmentFileEntryVo... attachments) {
    return withAttachments(Arrays.asList(attachments));
  }

  public OperationConfigurationSubmissionVoBuilder withElectionSiteConfigurations(
      Collection<ElectionSiteConfigurationVo> electionSiteConfigurations) {
    this.electionSiteConfigurations = new LinkedHashSet<>(electionSiteConfigurations);
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withElectionSiteConfigurations(
      ElectionSiteConfigurationVo... electionSiteConfigurations) {
    return withElectionSiteConfigurations(Arrays.asList(electionSiteConfigurations));
  }

  public OperationConfigurationSubmissionVoBuilder withHighlightedQuestions(
      Collection<HighlightedQuestionsVo> highlightedQuestions) {
    this.highlightedQuestions = new LinkedHashSet<>(highlightedQuestions);
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withHighlightedQuestions(HighlightedQuestionsVo... values) {
    return withHighlightedQuestions(Arrays.asList(values));
  }

  public OperationConfigurationSubmissionVoBuilder withBallotDocumentations(Collection<BallotDocumentationVo> values) {
    this.ballotDocumentations = new LinkedHashSet<>(values);
    return this;
  }

  public OperationConfigurationSubmissionVoBuilder withBallotDocumentations(BallotDocumentationVo... values) {
    return withBallotDocumentations(Arrays.asList(values));
  }

  public OperationConfigurationSubmissionVoBuilder withMilestones(Map<Milestone, LocalDateTime> milestones) {
    this.milestones = new EnumMap<>(milestones);
    return this;
  }

  public OperationConfigurationSubmissionVo build() {
    OperationConfigurationSubmissionVo operationConfigurationSubmissionVo = new OperationConfigurationSubmissionVo();
    operationConfigurationSubmissionVo.setUser(user);
    operationConfigurationSubmissionVo.setClientId(clientId);
    operationConfigurationSubmissionVo.setOperationName(operationName);
    operationConfigurationSubmissionVo.setOperationDate(operationDate);
    operationConfigurationSubmissionVo.setGracePeriod(gracePeriod);
    operationConfigurationSubmissionVo.setCanton(canton);
    operationConfigurationSubmissionVo.setGroupVotation(groupVotation);
    operationConfigurationSubmissionVo.setTestSitePrinter(testSitePrinter);
    operationConfigurationSubmissionVo.setAttachments(attachments);
    operationConfigurationSubmissionVo.setElectionSiteConfigurations(electionSiteConfigurations);
    operationConfigurationSubmissionVo.setHighlightedQuestions(highlightedQuestions);
    operationConfigurationSubmissionVo.setBallotDocumentations(ballotDocumentations);
    operationConfigurationSubmissionVo.setMilestones(milestones);
    return operationConfigurationSubmissionVo;
  }
}
