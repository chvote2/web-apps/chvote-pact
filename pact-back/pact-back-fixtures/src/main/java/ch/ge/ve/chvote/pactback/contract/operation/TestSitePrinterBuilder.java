/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import java.math.BigInteger;

/**
 * A builder for {@link TestSitePrinter} objects.
 * <p>
 * Get a new builder with one of its static factory method.
 * </p>
 */
public final class TestSitePrinterBuilder {

  private MunicipalityVo         municipality;
  private PrinterConfigurationVo printer;

  private TestSitePrinterBuilder() {
  }

  public static TestSitePrinterBuilder aTestSitePrinter() {
    return new TestSitePrinterBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   * All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   *
   * @return a new builder instance
   */
  public static TestSitePrinterBuilder copyOf(TestSitePrinter prototype) {
    return new TestSitePrinterBuilder()
        .withMunicipality(copy(prototype.getMunicipality()))
        .withPrinter(copy(prototype.getPrinter()));
  }

  public TestSitePrinterBuilder withMunicipality(MunicipalityVo municipality) {
    this.municipality = municipality;
    return this;
  }

  public TestSitePrinterBuilder withMunicipality(int municipalityId, String name) {
    MunicipalityVo vo = new MunicipalityVo();
    vo.setOfsId(municipalityId);
    vo.setName(name);
    return withMunicipality(vo);
  }

  public TestSitePrinterBuilder withPrinter(PrinterConfigurationVo printer) {
    this.printer = printer;
    return this;
  }

  public TestSitePrinterBuilder withPrinter(String id, String name, BigInteger publicKey) {
    PrinterConfigurationVo vo = new PrinterConfigurationVo();
    vo.setId(id);
    vo.setName(name);
    vo.setPublicKey(publicKey);
    return withPrinter(vo);
  }

  public TestSitePrinter build() {
    TestSitePrinter testSitePrinter = new TestSitePrinter();
    testSitePrinter.setMunicipality(municipality);
    testSitePrinter.setPrinter(printer);
    return testSitePrinter;
  }

  private static MunicipalityVo copy(MunicipalityVo source) {
    if (source == null) {
      return null;
    }
    MunicipalityVo vo = new MunicipalityVo();
    vo.setOfsId(source.getOfsId());
    vo.setName(source.getName());
    return vo;
  }

  private static PrinterConfigurationVo copy(PrinterConfigurationVo source) {
    if (source == null) {
      return null;
    }
    PrinterConfigurationVo vo = new PrinterConfigurationVo();
    vo.setId(source.getId());
    vo.setName(source.getName());
    vo.setPublicKey(source.getPublicKey());
    return vo;
  }
}
