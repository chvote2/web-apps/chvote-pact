/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.fixtures.repository;

import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterCountingCircle;

/**
 * A static factory to obtain (entities) {@link VoterCountingCircle} objects.
 */
public class VoterCountingCircles {

  private VoterCountingCircles() {
    throw new AssertionError("Not instantiable");
  }

  public static VoterCountingCircle geneve() {
    return new VoterCountingCircle(1, "6621", "Genève");
  }

  public static VoterCountingCircle cheneBougeries() {
    return new VoterCountingCircle(1, "6612", "Chêne-Bougeries");
  }

  public static VoterCountingCircle carouge() {
    return new VoterCountingCircle(1, "6608", "Carouge");
  }

  public static VoterCountingCircle lancy() {
    return new VoterCountingCircle(1, "6628", "Lancy");
  }

  public static VoterCountingCircle gy() {
    return new VoterCountingCircle(1, "6624", "Gy");
  }

}
