/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.periods.entity;

import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.PublicKeyFile;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import java.time.LocalDateTime;

/**
 * A builder for {@link VotingPeriodConfiguration} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class VotingPeriodConfigurationBuilder {
  private String           author;
  private LocalDateTime    submissionDate;
  private PublicKeyFile    electoralAuthorityKey;
  private DeploymentTarget target;
  private String           simulationName;
  private LocalDateTime    siteOpeningDate;
  private LocalDateTime    siteClosingDate;
  private Integer          gracePeriod;

  private VotingPeriodConfigurationBuilder() {
  }

  public static VotingPeriodConfigurationBuilder aVotingPeriodConfiguration() {
    return new VotingPeriodConfigurationBuilder();
  }

  public VotingPeriodConfigurationBuilder withTarget(DeploymentTarget target) {
    this.target = target;
    return this;
  }

  public VotingPeriodConfigurationBuilder withSiteClosingDate(LocalDateTime siteClosingDate) {
    this.siteClosingDate = siteClosingDate;
    return this;
  }

  public VotingPeriodConfigurationBuilder withGracePeriod(Integer gracePeriod) {
    this.gracePeriod = gracePeriod;
    return this;
  }

  public VotingPeriodConfigurationBuilder withSiteOpeningDate(LocalDateTime siteOpeningDate) {
    this.siteOpeningDate = siteOpeningDate;
    return this;
  }

  public VotingPeriodConfigurationBuilder withElectoralAuthorityKey(PublicKeyFile electoralAuthorityKey) {
    this.electoralAuthorityKey = electoralAuthorityKey;
    return this;
  }

  public VotingPeriodConfigurationBuilder withAuthor(String author) {
    this.author = author;
    return this;
  }

  public VotingPeriodConfigurationBuilder withSubmissionDate(LocalDateTime submissionDate) {
    this.submissionDate = submissionDate;
    return this;
  }


  public VotingPeriodConfigurationBuilder withSimulationName(String simulationName) {
    this.simulationName = simulationName;
    return this;
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static VotingPeriodConfigurationBuilder copyOf(VotingPeriodConfiguration prototype) {
    return new VotingPeriodConfigurationBuilder()
        .withAuthor(prototype.getAuthor())
        .withSimulationName(prototype.getSimulationName())
        .withSubmissionDate(prototype.getSubmissionDate())
        .withTarget(prototype.getTarget())
        .withSiteClosingDate(prototype.getSiteClosingDate())
        .withGracePeriod(prototype.getGracePeriod())
        .withSiteOpeningDate(prototype.getSiteOpeningDate())
        .withElectoralAuthorityKey(prototype.getElectoralAuthorityKey());
  }

  public VotingPeriodConfiguration build() {
    VotingPeriodConfiguration votingPeriodConfiguration = new VotingPeriodConfiguration();
    votingPeriodConfiguration.setTarget(target);
    votingPeriodConfiguration.setElectoralAuthorityKey(electoralAuthorityKey);
    votingPeriodConfiguration.setSiteOpeningDate(siteOpeningDate);
    votingPeriodConfiguration.setGracePeriod(gracePeriod);
    votingPeriodConfiguration.setSiteClosingDate(siteClosingDate);
    votingPeriodConfiguration.setAuthor(author);
    votingPeriodConfiguration.setSubmissionDate(submissionDate);
    votingPeriodConfiguration.setSimulationName(simulationName);
    return votingPeriodConfiguration;
  }

}
