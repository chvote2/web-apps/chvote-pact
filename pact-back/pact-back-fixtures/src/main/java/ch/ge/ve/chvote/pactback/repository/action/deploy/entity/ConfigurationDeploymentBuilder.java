/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.action.deploy.entity;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfigurationBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.entity.OperationBuilder;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import java.time.LocalDateTime;

/**
 * A builder for {@link ConfigurationDeployment} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class ConfigurationDeploymentBuilder {
  private Long                    id;
  private OperationConfiguration  operationConfiguration;
  private String                  operationName;
  private LocalDateTime           operationDate;
  private String                  rejectionReason;
  private LocalDateTime           creationDate;
  private User                    requester;
  private User                    validator;
  private PrivilegedAction.Status status;
  private LocalDateTime           statusDate;

  private ConfigurationDeploymentBuilder() {
  }

  public static ConfigurationDeploymentBuilder aConfigurationDeployment() {
    return new ConfigurationDeploymentBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   * <p>
   *   <strong>This method won't copy bidirectional bindings</strong>. To avoid cyclic references copy problems,
   *   this method does not copy the field {@code operationConfiguration}. <br>
   *   If you want to copy a full graph, please consider copying from a "higher" entity.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   *
   * @see OperationConfigurationBuilder
   * @see OperationBuilder
   */
  public static ConfigurationDeploymentBuilder copyWithoutBidirectionnal(ConfigurationDeployment prototype) {
    return new ConfigurationDeploymentBuilder()
        .withId(prototype.getId())
        .withOperationName(prototype.getOperationName())
        .withOperationDate(prototype.getOperationDate())
        .withRejectionReason(prototype.getRejectionReason())
        .withCreationDate(prototype.getCreationDate())
        .withRequester(nullSafeCopy(prototype.getRequester()))
        .withValidator(nullSafeCopy(prototype.getValidator()))
        .withStatus(prototype.getStatus())
        .withStatusDate(prototype.getStatusDate());
  }

  public ConfigurationDeploymentBuilder withOperationConfiguration(OperationConfiguration operationConfiguration) {
    this.operationConfiguration = operationConfiguration;
    return this;
  }

  public ConfigurationDeploymentBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public ConfigurationDeploymentBuilder withOperationName(String operationName) {
    this.operationName = operationName;
    return this;
  }

  public ConfigurationDeploymentBuilder withOperationDate(LocalDateTime operationDate) {
    this.operationDate = operationDate;
    return this;
  }

  public ConfigurationDeploymentBuilder withRejectionReason(String rejectionReason) {
    this.rejectionReason = rejectionReason;
    return this;
  }

  public ConfigurationDeploymentBuilder withCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
    return this;
  }

  public ConfigurationDeploymentBuilder withRequester(User requester) {
    this.requester = requester;
    return this;
  }

  public ConfigurationDeploymentBuilder withValidator(User validator) {
    this.validator = validator;
    return this;
  }

  public ConfigurationDeploymentBuilder withStatus(PrivilegedAction.Status status) {
    this.status = status;
    return this;
  }

  public ConfigurationDeploymentBuilder withStatusDate(LocalDateTime statusDate) {
    this.statusDate = statusDate;
    return this;
  }

  /**
   * Shortcut to {@code withStatus(PENDING)}
   */
  public ConfigurationDeploymentBuilder pending() {
    return withStatus(PrivilegedAction.Status.PENDING);
  }

  /**
   * Shortcut to {@code withStatus(APPROVED).withValidator(validator).withStatusDate(validationDate)}
   */
  public ConfigurationDeploymentBuilder approved(User validator, LocalDateTime validationDate) {
    return withStatus(PrivilegedAction.Status.APPROVED).withValidator(validator).withStatusDate(validationDate);
  }

  /**
   * Shortcut to {@code withStatus(REJECTED).withValidator(validator).withStatusDate(rejectionDate)}
   */
  public ConfigurationDeploymentBuilder rejected(User validator, LocalDateTime rejectionDate) {
    return withStatus(PrivilegedAction.Status.REJECTED).withValidator(validator).withStatusDate(rejectionDate);
  }

  public ConfigurationDeployment build() {
    ConfigurationDeployment configurationDeployment = new ConfigurationDeployment();
    configurationDeployment.setOperationConfiguration(operationConfiguration);
    configurationDeployment.setId(id);
    configurationDeployment.setOperationName(operationName);
    configurationDeployment.setOperationDate(operationDate);
    configurationDeployment.setRejectionReason(rejectionReason);
    configurationDeployment.setCreationDate(creationDate);
    configurationDeployment.setRequester(requester);
    configurationDeployment.setValidator(validator);
    configurationDeployment.setStatus(status);
    configurationDeployment.setStatusDate(statusDate);
    return configurationDeployment;
  }

  private static User nullSafeCopy(User user) {
    if (user == null) {
      return null;
    }
    return new User(user.getId(), user.getUsername());
  }
}
