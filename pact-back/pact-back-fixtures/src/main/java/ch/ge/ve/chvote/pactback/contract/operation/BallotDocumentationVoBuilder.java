/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

/**
 * A builder for {@link BallotDocumentationVo} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class BallotDocumentationVoBuilder {
  private AttachmentFileEntryVo documentation;
  private String label;
  private String ballot;

  private BallotDocumentationVoBuilder() {

  }

  public static BallotDocumentationVoBuilder aBallotDocumentationVo() {
    return new BallotDocumentationVoBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static BallotDocumentationVoBuilder copyOf(BallotDocumentationVo prototype) {
    return new BallotDocumentationVoBuilder()
        .withDocumentation(nullsafeCopy(prototype.getDocumentation()))
        .withLabel(prototype.getLabel())
        .withBallot(prototype.getBallot());
  }

  private static AttachmentFileEntryVo nullsafeCopy(AttachmentFileEntryVo value) {
    return value==null ? null : AttachmentFileEntryVoBuilder.copyOf(value).build();
  }

  public BallotDocumentationVoBuilder withDocumentation(AttachmentFileEntryVo documentation) {
    this.documentation = documentation;
    return this;
  }

  public BallotDocumentationVoBuilder withLabel(String label) {
    this.label = label;
    return this;
  }

  public BallotDocumentationVoBuilder withBallot(String ballot) {
    this.ballot = ballot;
    return this;
  }

  public BallotDocumentationVo build() {
    BallotDocumentationVo ballotDocumentationVo = new BallotDocumentationVo();
    ballotDocumentationVo.setDocumentation(documentation);
    ballotDocumentationVo.setLabel(label);
    ballotDocumentationVo.setBallot(ballot);
    return ballotDocumentationVo;
  }
}
