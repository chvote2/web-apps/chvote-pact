/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

/**
 * A builder for {@link HighlightedQuestionsVo} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class HighlightedQuestionsVoBuilder {
  private AttachmentFileEntryVo answerFile;
  private String question;

  private HighlightedQuestionsVoBuilder() {
  }

  public static HighlightedQuestionsVoBuilder aHighlightedQuestionsVo() {
    return new HighlightedQuestionsVoBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static HighlightedQuestionsVoBuilder copyOf(HighlightedQuestionsVo prototype) {
    return new HighlightedQuestionsVoBuilder()
        .withAnswerFile(nullsafeCopy(prototype.getAnswerFile()))
        .withQuestion(prototype.getQuestion());
  }

  private static AttachmentFileEntryVo nullsafeCopy(AttachmentFileEntryVo value) {
    return value==null ? null : AttachmentFileEntryVoBuilder.copyOf(value).build();
  }

  public HighlightedQuestionsVoBuilder withAnswerFile(AttachmentFileEntryVo answerFile) {
    this.answerFile = answerFile;
    return this;
  }

  public HighlightedQuestionsVoBuilder withQuestion(String question) {
    this.question = question;
    return this;
  }

  public HighlightedQuestionsVo build() {
    HighlightedQuestionsVo highlightedQuestionsVo = new HighlightedQuestionsVo();
    highlightedQuestionsVo.setAnswerFile(answerFile);
    highlightedQuestionsVo.setQuestion(question);
    return highlightedQuestionsVo;
  }
}
