/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.materials.entity;

import static java.util.stream.Collectors.toList;

import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreation;
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreationBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.VotersRegisterFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.VotersRegisterFileBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfigurationBuilder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A builder for {@link VotingMaterialsConfiguration} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class VotingMaterialsConfigurationBuilder {
  private String                     author;
  private LocalDateTime              submissionDate;
  private String                     votingCardLabel;
  private DeploymentTarget           target;
  private String                     simulationName;
  private List<VotersRegisterFile>   registers = new ArrayList<>();
  private List<PrinterConfiguration> printers  = new ArrayList<>();
  private PrinterConfiguration       swissAbroadPrinter;
  private VotingMaterialsCreation    action;

  private VotingMaterialsConfigurationBuilder() {
  }

  public static VotingMaterialsConfigurationBuilder aVotingMaterialsConfiguration() {
    return new VotingMaterialsConfigurationBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   * <p>
   *   <strong>This method won't copy bidirectional bindings</strong>. To avoid cyclic references copy problems,
   *   this method does not copy the field {@code action}. <br>
   *   This field would be copied if you use {@link #copyOf(VotingMaterialsConfiguration)}, or you could copy and set
   *   the {@code action} field independently.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static VotingMaterialsConfigurationBuilder copyWithoutBidirectionnal(VotingMaterialsConfiguration prototype) {
    return new VotingMaterialsConfigurationBuilder()
        .withAuthor(prototype.getAuthor())
        .withSubmissionDate(prototype.getSubmissionDate())
        .withVotingCardLabel(prototype.getVotingCardLabel())
        .withTarget(prototype.getTarget())
        .withSimulationName(prototype.getSimulationName())
        .withRegisters(prototype.getRegisters().stream()
                                .map(VotersRegisterFileBuilder::copyOf)
                                .map(VotersRegisterFileBuilder::build)
                                .collect(toList()))
        .withPrinters(prototype.getPrinters().stream()
                               .map(PrinterConfigurationBuilder::copyOf)
                               .map(PrinterConfigurationBuilder::build)
                               .collect(toList()))
        .withSwissAbroadPrinter(nullsafeCopy(prototype.getSwissAbroadPrinter()));
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static VotingMaterialsConfigurationBuilder copyOf(VotingMaterialsConfiguration prototype) {
    return copyWithoutBidirectionnal(prototype)
        .withAction(prototype.getCreationAction()
                             .map(VotingMaterialsCreationBuilder::copyWithoutBidirectionnal)
                             .map(VotingMaterialsCreationBuilder::build)
                             .orElse(null));
  }

  private static PrinterConfiguration nullsafeCopy(PrinterConfiguration value) {
    return value==null ? null : PrinterConfigurationBuilder.copyOf(value).build();
  }

  public VotingMaterialsConfigurationBuilder withAuthor(String author) {
    this.author = author;
    return this;
  }

  public VotingMaterialsConfigurationBuilder withSubmissionDate(LocalDateTime submissionDate) {
    this.submissionDate = submissionDate;
    return this;
  }

  public VotingMaterialsConfigurationBuilder withVotingCardLabel(String votingCardLabel) {
    this.votingCardLabel = votingCardLabel;
    return this;
  }

  public VotingMaterialsConfigurationBuilder withTarget(DeploymentTarget target) {
    this.target = target;
    return this;
  }

  public VotingMaterialsConfigurationBuilder withSimulationName(String simulationName) {
    this.simulationName = simulationName;
    return this;
  }

  public VotingMaterialsConfigurationBuilder withRegisters(Collection<VotersRegisterFile> registers) {
    this.registers = new ArrayList<>(registers);
    return this;
  }

  public VotingMaterialsConfigurationBuilder withPrinters(Collection<PrinterConfiguration> printers) {
    this.printers = new ArrayList<>(printers);
    return this;
  }

  public VotingMaterialsConfigurationBuilder withSwissAbroadPrinter(PrinterConfiguration swissAbroadPrinter) {
    this.swissAbroadPrinter = swissAbroadPrinter;
    return this;
  }

  public VotingMaterialsConfigurationBuilder withAction(VotingMaterialsCreation action) {
    this.action = action;
    return this;
  }

  public VotingMaterialsConfiguration build() {
    VotingMaterialsConfiguration votingMaterialsConfiguration = new VotingMaterialsConfiguration();
    votingMaterialsConfiguration.setAuthor(author);
    votingMaterialsConfiguration.setSubmissionDate(submissionDate);
    votingMaterialsConfiguration.setVotingCardLabel(votingCardLabel);
    votingMaterialsConfiguration.setTarget(target);
    votingMaterialsConfiguration.setSimulationName(simulationName);
    votingMaterialsConfiguration.setRegisters(registers);
    votingMaterialsConfiguration.setPrinters(printers);
    votingMaterialsConfiguration.setSwissAbroadPrinter(swissAbroadPrinter);
    bind(votingMaterialsConfiguration, action);
    return votingMaterialsConfiguration;
  }

  // Ensures that the bidirectional binding between those objects is coherent
  private static void bind(VotingMaterialsConfiguration configuration, VotingMaterialsCreation action) {
    if (configuration.getCreationAction().orElse(null) != action) {
      configuration.setAction(action);
    }
    if (action != null && action.getVotingMaterialsConfiguration() != configuration) {
      action.setVotingMaterialsConfiguration(configuration);
    }
  }
}
