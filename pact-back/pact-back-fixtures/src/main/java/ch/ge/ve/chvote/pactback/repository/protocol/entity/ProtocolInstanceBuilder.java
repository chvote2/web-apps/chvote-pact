/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol.entity;

import static java.util.stream.Collectors.toList;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfigurationBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.entity.OperationBuilder;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgress;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgressBuilder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A builder for {@link ProtocolInstance} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class ProtocolInstanceBuilder {
  private String                      protocolId;
  private String                      nodeRepartitionKey;
  private ProtocolEnvironment         environment;
  private ProtocolInstanceStatus      status;
  private LocalDateTime               statusDate;
  private OperationConfiguration      configuration;
  private Operation                   operation;
  private List<ProtocolStageProgress> progress   = new ArrayList<>();
  private List<VotersCreationStats>   statistics = new ArrayList<>();
  private String                      publicParameters;

  private ProtocolInstanceBuilder() {
  }

  public static ProtocolInstanceBuilder aProtocolInstance() {
    return new ProtocolInstanceBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   * <p>
   *   <strong>This method won't copy bidirectional bindings</strong>. To avoid cyclic references copy problems,
   *   this method does not copy the fields {@code operation} and {@code configuration}. <br>
   *   If you want to copy a full graph, please consider copying from a "higher" entity. Or you can copy these two
   *   fields using their own builders.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   *
   * @see OperationBuilder
   * @see OperationConfigurationBuilder
   */
  public static ProtocolInstanceBuilder copyWithoutBidirectional(ProtocolInstance prototype) {
    return new ProtocolInstanceBuilder()
        .withProtocolId(prototype.getProtocolId())
        .withNodeRepartitionKey(prototype.getNodeRepartitionKey())
        .withEnvironment(prototype.getEnvironment())
        .withStatus(prototype.getStatus())
        .withStatusDate(prototype.getStatusDate())
        .withProgress(prototype.getProgress().stream()
                               .map(ProtocolStageProgressBuilder::copyOf)
                               .map(ProtocolStageProgressBuilder::build)
                               .collect(toList()))
        .withStatistics(prototype.getStatistics().stream()
                                 .map(VotersCreationStatsBuilder::copyOf)
                                 .map(VotersCreationStatsBuilder::build)
                                 .collect(toList()))
        .withPublicParameters(prototype.getPublicParameters());
  }

  public ProtocolInstanceBuilder withProtocolId(String protocolId) {
    this.protocolId = protocolId;
    return this;
  }

  public ProtocolInstanceBuilder withNodeRepartitionKey(String nodeRepartitionKey) {
    this.nodeRepartitionKey = nodeRepartitionKey;
    return this;
  }

  public ProtocolInstanceBuilder withEnvironment(ProtocolEnvironment environment) {
    this.environment = environment;
    return this;
  }

  public ProtocolInstanceBuilder withStatus(ProtocolInstanceStatus status) {
    this.status = status;
    return this;
  }

  public ProtocolInstanceBuilder withStatusDate(LocalDateTime statusDate) {
    this.statusDate = statusDate;
    return this;
  }

  public ProtocolInstanceBuilder withConfiguration(OperationConfiguration configuration) {
    this.configuration = configuration;
    return this;
  }

  public ProtocolInstanceBuilder withOperation(Operation operation) {
    this.operation = operation;
    return this;
  }

  public ProtocolInstanceBuilder withProgress(Collection<ProtocolStageProgress> progress) {
    this.progress = new ArrayList<>(progress);
    return this;
  }

  public ProtocolInstanceBuilder withStatistics(Collection<VotersCreationStats> statistics) {
    this.statistics = new ArrayList<>(statistics);
    return this;
  }

  public ProtocolInstanceBuilder withPublicParameters(String publicParameters) {
    this.publicParameters = publicParameters;
    return this;
  }

  public ProtocolInstance build() {
    ProtocolInstance protocolInstance = new ProtocolInstance();
    protocolInstance.setProtocolId(protocolId);
    protocolInstance.setNodeRepartitionKey(nodeRepartitionKey);
    protocolInstance.setEnvironment(environment);
    protocolInstance.setStatus(status);
    protocolInstance.setStatusDate(statusDate);
    protocolInstance.setConfiguration(configuration);
    protocolInstance.setOperation(operation);
    protocolInstance.setProgress(progress);
    protocolInstance.setStatistics(statistics);
    protocolInstance.setPublicParameters(publicParameters);
    return protocolInstance;
  }
}
