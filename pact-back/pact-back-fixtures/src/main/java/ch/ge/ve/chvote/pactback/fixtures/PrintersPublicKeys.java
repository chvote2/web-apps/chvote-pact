/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.fixtures;

import com.google.common.io.CharStreams;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * A static factory to get / load printers public keys.
 */
public class PrintersPublicKeys {

  /**
   * Reads a public key from a classpath resource.
   *
   * @param sourcePath path to the public key resource
   *
   * @return the public key value
   */
  public static BigInteger readPublicKeyFromClasspath(String sourcePath) {

    try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(sourcePath)) {
      Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
      return new BigInteger(Base64.getDecoder().decode(CharStreams.toString(reader)));
    } catch (Exception e) {
      throw new ResourceLoadingException("Failed to read public key from classpath \"" + sourcePath + "\"", e);
    }
  }

  public static BigInteger publicKeyPa0() {
    return readPublicKeyFromClasspath("fixtures/public-keys/public-key-pa0.pub");
  }

  public static BigInteger publicKeyPa1() {
    return readPublicKeyFromClasspath("fixtures/public-keys/public-key-pa1.pub");
  }

  public static BigInteger publicKeyPa2() {
    return readPublicKeyFromClasspath("fixtures/public-keys/public-key-pa2.pub");
  }

  public static BigInteger publicKeyPa3() {
    return readPublicKeyFromClasspath("fixtures/public-keys/public-key-pa3.pub");
  }

  /**
   * Hide utility class constructor.
   */
  private PrintersPublicKeys() {
    throw new AssertionError("Not instantiable");
  }
}
