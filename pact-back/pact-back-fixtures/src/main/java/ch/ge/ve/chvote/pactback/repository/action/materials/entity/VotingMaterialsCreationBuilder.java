/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.action.materials.entity;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfigurationBuilder;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import java.time.LocalDateTime;

/**
 * A builder for {@link VotingMaterialsCreation} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class VotingMaterialsCreationBuilder {
  private Long                         id;
  private VotingMaterialsConfiguration votingMaterialsConfiguration;
  private String                       operationName;
  private LocalDateTime                operationDate;
  private String                       rejectionReason;
  private LocalDateTime                creationDate;
  private User                         requester;
  private User                         validator;
  private PrivilegedAction.Status      status;
  private LocalDateTime                statusDate;

  private VotingMaterialsCreationBuilder() {
  }

  public static VotingMaterialsCreationBuilder aVotingMaterialsCreation() {
    return new VotingMaterialsCreationBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   * <p>
   *   <strong>This method won't copy bidirectional bindings</strong>. To avoid cyclic references copy problems,
   *   this method does not copy the field {@code votingMaterialsConfiguration}. <br>
   *   If you want to copy a full graph, please consider copying from a "higher" entity.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   *
   * @see VotingMaterialsConfigurationBuilder
   */
  public static VotingMaterialsCreationBuilder copyWithoutBidirectionnal(VotingMaterialsCreation prototype) {
    return new VotingMaterialsCreationBuilder()
        .withId(prototype.getId())
        .withOperationName(prototype.getOperationName())
        .withOperationDate(prototype.getOperationDate())
        .withRejectionReason(prototype.getRejectionReason())
        .withCreationDate(prototype.getCreationDate())
        .withRequester(nullSafeCopy(prototype.getRequester()))
        .withValidator(nullSafeCopy(prototype.getValidator()))
        .withStatus(prototype.getStatus())
        .withStatusDate(prototype.getStatusDate());
  }

  public VotingMaterialsCreationBuilder withVotingMaterialsConfiguration(VotingMaterialsConfiguration
                                                                             votingMaterialsConfiguration) {
    this.votingMaterialsConfiguration = votingMaterialsConfiguration;
    return this;
  }

  public VotingMaterialsCreationBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public VotingMaterialsCreationBuilder withOperationName(String operationName) {
    this.operationName = operationName;
    return this;
  }

  public VotingMaterialsCreationBuilder withOperationDate(LocalDateTime operationDate) {
    this.operationDate = operationDate;
    return this;
  }

  public VotingMaterialsCreationBuilder withRejectionReason(String rejectionReason) {
    this.rejectionReason = rejectionReason;
    return this;
  }

  public VotingMaterialsCreationBuilder withCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
    return this;
  }

  public VotingMaterialsCreationBuilder withRequester(User requester) {
    this.requester = requester;
    return this;
  }

  public VotingMaterialsCreationBuilder withValidator(User validator) {
    this.validator = validator;
    return this;
  }

  public VotingMaterialsCreationBuilder withStatus(PrivilegedAction.Status status) {
    this.status = status;
    return this;
  }

  public VotingMaterialsCreationBuilder withStatusDate(LocalDateTime statusDate) {
    this.statusDate = statusDate;
    return this;
  }

  public VotingMaterialsCreation build() {
    VotingMaterialsCreation votingMaterialsCreation = new VotingMaterialsCreation();
    votingMaterialsCreation.setVotingMaterialsConfiguration(votingMaterialsConfiguration);
    votingMaterialsCreation.setId(id);
    votingMaterialsCreation.setOperationName(operationName);
    votingMaterialsCreation.setOperationDate(operationDate);
    votingMaterialsCreation.setRejectionReason(rejectionReason);
    votingMaterialsCreation.setCreationDate(creationDate);
    votingMaterialsCreation.setRequester(requester);
    votingMaterialsCreation.setValidator(validator);
    votingMaterialsCreation.setStatus(status);
    votingMaterialsCreation.setStatusDate(statusDate);
    return votingMaterialsCreation;
  }

  private static User nullSafeCopy(User user) {
    if (user == null) {
      return null;
    }
    return new User(user.getId(), user.getUsername());
  }
}
