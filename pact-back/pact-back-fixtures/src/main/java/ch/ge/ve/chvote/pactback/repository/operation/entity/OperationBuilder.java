/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.entity;

import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeploymentBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfigurationBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfigurationBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.periods.entity.VotingPeriodConfigurationBuilder;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstanceBuilder;
import java.time.LocalDateTime;
import java.util.function.Consumer;

/**
 * A builder for {@link Operation} objects.
 * <p>
 * Get a new builder with one of its static factory method.
 * </p>
 */
public final class OperationBuilder {
  private String                       clientId;
  private LocalDateTime                creationDate;
  private OperationConfiguration       configurationInTest;
  private OperationConfiguration       deployedConfiguration;
  private VotingMaterialsConfiguration votingMaterialsConfiguration;
  private VotingPeriodConfiguration    votingPeriodConfiguration;

  private OperationBuilder() {
  }

  public static OperationBuilder anOperation() {
    return new OperationBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   * All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   *
   * @return a new builder instance
   */
  public static OperationBuilder copyOf(Operation prototype) {
    return new OperationBuilder()
        .withClientId(prototype.getClientId())
        .withCreationDate(prototype.getCreationDate())
        .withVotingPeriodConfiguration(prototype.getVotingPeriodConfiguration()
                                                .map(VotingPeriodConfigurationBuilder::copyOf)
                                                .map(VotingPeriodConfigurationBuilder::build)
                                                .orElse(null))
        .withConfigurationInTest(prototype.getConfigurationInTest()
                                          .map(OperationBuilder::deepCopy)
                                          .orElse(null))
        .withDeployedConfiguration(prototype.getDeployedConfiguration()
                                            .map(OperationBuilder::deepCopy)
                                            .orElse(null))
        .withVotingMaterialsConfiguration(prototype.getVotingMaterialsConfiguration()
                                                   .map(VotingMaterialsConfigurationBuilder::copyOf)
                                                   .map(VotingMaterialsConfigurationBuilder::build)
                                                   .orElse(null));
  }

  private static OperationConfiguration deepCopy(OperationConfiguration configuration) {
    return OperationConfigurationBuilder
        .copyWithoutBidirectionnal(configuration)
        // We do manually the mapping "withoutBidirectionnal", the OperationConfigurationBuilder will
        //    ensure bidirectionnal relations on "build()"
        .withProtocolInstance(configuration.getProtocolInstance()
                                           .map(ProtocolInstanceBuilder::copyWithoutBidirectional)
                                           .map(ProtocolInstanceBuilder::build)
                                           .orElse(null))
        .withAction(configuration.getAction()
                                 .map(ConfigurationDeploymentBuilder::copyWithoutBidirectionnal)
                                 .map(ConfigurationDeploymentBuilder::build)
                                 .orElse(null))
        .build();
  }

  public OperationBuilder withClientId(String clientId) {
    this.clientId = clientId;
    return this;
  }

  public OperationBuilder withCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
    return this;
  }

  public OperationBuilder withConfigurationInTest(OperationConfiguration configurationInTest) {
    this.configurationInTest = configurationInTest;
    return this;
  }

  public OperationBuilder withDeployedConfiguration(OperationConfiguration deployedConfiguration) {
    this.deployedConfiguration = deployedConfiguration;
    return this;
  }

  public OperationBuilder withVotingMaterialsConfiguration(VotingMaterialsConfiguration votingMaterialsConfiguration) {
    this.votingMaterialsConfiguration = votingMaterialsConfiguration;
    return this;
  }

  public OperationBuilder withVotingPeriodConfiguration(VotingPeriodConfiguration votingPeriodConfiguration) {
    this.votingPeriodConfiguration = votingPeriodConfiguration;
    return this;
  }


  public Operation build() {
    Operation operation = new Operation();
    operation.setClientId(clientId);
    operation.setCreationDate(creationDate);
    operation.setConfigurationInTest(configurationInTest);
    operation.setDeployedConfiguration(deployedConfiguration);
    operation.setVotingMaterialsConfiguration(votingMaterialsConfiguration);
    operation.setVotingPeriodConfiguration(votingPeriodConfiguration);
    ensureCyclicReferences(operation);
    return operation;
  }

  // Simple bi-directionnal relations are OK, but there are some cyclic references that should be verified
  private static void ensureCyclicReferences(Operation operation) {
    Consumer<ProtocolInstance> bindOperation = instance -> setReference(instance, operation);

    operation.getConfigurationInTest().flatMap(OperationConfiguration::getProtocolInstance).ifPresent(bindOperation);
    operation.getDeployedConfiguration().flatMap(OperationConfiguration::getProtocolInstance).ifPresent(bindOperation);
  }

  private static void setReference(ProtocolInstance protocolInstance, Operation operation) {
    if (protocolInstance.getOperation() != operation) {
      protocolInstance.setOperation(operation);
    }
  }
}
