/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity;

/**
 * A builder for {@link Municipality} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class MunicipalityBuilder {
  private Integer ofsId;
  private String name;
  private final boolean virtual;

  private MunicipalityBuilder(boolean virtual) {
    this.virtual = virtual;
  }

  public static MunicipalityBuilder aMunicipality() {
    return new MunicipalityBuilder(false);
  }

  public static MunicipalityBuilder aVirtualMunicipality() {
    return new MunicipalityBuilder(true);
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static MunicipalityBuilder copyOf(Municipality prototype) {
    return new MunicipalityBuilder(prototype.isVirtual())
        .withOfsId(prototype.getOfsId())
        .withName(prototype.getName());
  }

  public MunicipalityBuilder withOfsId(Integer ofsId) {
    this.ofsId = ofsId;
    return this;
  }

  public MunicipalityBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public Municipality build() {
    Municipality municipality = new Municipality();
    municipality.setOfsId(ofsId);
    municipality.setName(name);
    municipality.setVirtual(virtual);
    return municipality;
  }

  /** An alternative to set id and name and build the object at once */
  public Municipality quickBuild(int id, String name) {
    return this.withOfsId(id).withName(name).build();
  }
}
