/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.notification.listener

import spock.lang.Specification

class NotificationEventsConsumersRegistryTest extends Specification {

  def "it should be possible to add multiple ballot box initialized event consumers"() {
    given:
    def registry = new NotificationEventsConsumersRegistry()

    expect:
    registry.getBallotBoxInitializedEventConsumers().isEmpty()

    when:
    registry.onBallotBoxInitializedEvent({})

    then:
    registry.getBallotBoxInitializedEventConsumers().size() == 1

    when:
    registry.onBallotBoxInitializedEvent({})

    then:
    registry.getBallotBoxInitializedEventConsumers().size() == 2
  }

  def "it should be possible to add multiple voting materials generated event consumers"() {
    given:
    def registry = new NotificationEventsConsumersRegistry()

    expect:
    registry.getVotingMaterialsGeneratedEventConsumers().isEmpty()

    when:
    registry.onVotingMaterialsGeneratedEvent({})

    then:
    registry.getVotingMaterialsGeneratedEventConsumers().size() == 1

    when:
    registry.onVotingMaterialsGeneratedEvent({})

    then:
    registry.getVotingMaterialsGeneratedEventConsumers().size() == 2
  }

  def "it should not accept null consumers"() {
    given:
    def registry = new NotificationEventsConsumersRegistry()

    when:
    registry.onBallotBoxInitializedEvent(null)

    then:
    thrown(NullPointerException)

    when:
    registry.onVotingMaterialsGeneratedEvent(null)

    then:
    thrown(NullPointerException)
  }
}
