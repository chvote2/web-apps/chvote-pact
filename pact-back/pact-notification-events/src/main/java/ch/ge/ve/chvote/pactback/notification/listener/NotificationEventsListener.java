/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.notification.listener;

import ch.ge.ve.chvote.pactback.notification.config.Exchanges;
import ch.ge.ve.chvote.pactback.notification.event.BallotBoxInitializedEvent;
import ch.ge.ve.chvote.pactback.notification.event.VotingMaterialsGeneratedEvent;
import java.util.List;
import java.util.function.Consumer;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Listens to notification events and invokes registered consumers.
 *
 * @see NotificationEventsConsumersRegistry
 */
@RabbitListener(
    containerFactory = "notificationEventsRabbitListenerContainerFactory",
    bindings = @QueueBinding(
        value = @Queue(
            value = Exchanges.PACT_NOTIFICATION_EVENTS + "-${application.name}",
            arguments = @Argument(name = "x-dead-letter-exchange", value = Exchanges.PACT_NOTIFICATION_EVENTS_DLX)
        ),
        exchange = @Exchange(type = ExchangeTypes.FANOUT, value = Exchanges.PACT_NOTIFICATION_EVENTS)
    )
)
@Component
public class NotificationEventsListener {

  private final NotificationEventsConsumersRegistry registry;

  @Autowired
  public NotificationEventsListener(NotificationEventsConsumersRegistry registry) {
    this.registry = registry;
  }

  @RabbitHandler
  public void processBallotBoxInitializedEvent(BallotBoxInitializedEvent event) {
    List<Consumer<BallotBoxInitializedEvent>> consumers = registry.getBallotBoxInitializedEventConsumers();
    new NotificationEventsProcessor<>(consumers).accept(event);
  }

  @RabbitHandler
  public void processVotingMaterialsGeneratedEvent(VotingMaterialsGeneratedEvent event) {
    List<Consumer<VotingMaterialsGeneratedEvent>> consumers = registry.getVotingMaterialsGeneratedEventConsumers();
    new NotificationEventsProcessor<>(consumers).accept(event);
  }
}
