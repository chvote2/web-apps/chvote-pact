/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.notification.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("notificationEventsRabbitMqConfiguration")
public class RabbitMqConfiguration {

  @Bean
  public Exchange notificationEventsDeadLetterExchange() {
    return ExchangeBuilder.fanoutExchange(Exchanges.PACT_NOTIFICATION_EVENTS_DLX).durable(true).build();
  }

  @Bean
  public Exchange notificationEventsExchange() {
    return ExchangeBuilder.fanoutExchange(Exchanges.PACT_NOTIFICATION_EVENTS).durable(true).build();
  }

  @Bean
  public SimpleRabbitListenerContainerFactory notificationEventsRabbitListenerContainerFactory(
      ConnectionFactory connectionFactory, ObjectMapper objectMapper) {
    SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
    factory.setConnectionFactory(connectionFactory);
    factory.setMessageConverter(notificationEventsMessageConverter(objectMapper));
    factory.setDefaultRequeueRejected(false); // do not requeue failed events, send it to the DLX (if configured)
    return factory;
  }

  @Bean
  public MessageConverter notificationEventsMessageConverter(ObjectMapper objectMapper) {
    Jackson2JsonMessageConverter converter = new Jackson2JsonMessageConverter(objectMapper);
    converter.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.TYPE_ID);
    return converter;
  }

  @Bean
  public RabbitTemplate notificationEventsRabbitTemplate(ConnectionFactory connectionFactory,
                                                         ObjectMapper objectMapper) {
    RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
    rabbitTemplate.setMessageConverter(notificationEventsMessageConverter(objectMapper));
    return rabbitTemplate;
  }
}
