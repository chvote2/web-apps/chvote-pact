/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.notification.listener;

import ch.ge.ve.chvote.pactback.notification.event.NotificationEvent;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class NotificationEventsProcessor<T extends NotificationEvent> implements Consumer<T> {

  private static final Logger logger = LoggerFactory.getLogger(NotificationEventsProcessor.class);

  private final List<Consumer<T>> consumers;

  NotificationEventsProcessor(List<Consumer<T>> consumers) {
    this.consumers = ImmutableList.copyOf(consumers);
  }

  @Override
  public void accept(T t) {
    List<Exception> exceptions = new ArrayList<>();
    for (Consumer<T> consumer : consumers) {
      try {
        consumer.accept(t);
      } catch (Exception e) {
        logger.warn(String.format("Failed to process event [%s]", t.getClass().getSimpleName()), e);
        exceptions.add(e);
      }
    }
    if (!exceptions.isEmpty()) {
      throw new NotificationEventsListenerRuntimeException(exceptions);
    }
  }
}
