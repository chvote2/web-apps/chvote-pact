/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.notification.listener;

import ch.ge.ve.chvote.pactback.notification.event.BallotBoxInitializedEvent;
import ch.ge.ve.chvote.pactback.notification.event.VotingMaterialsGeneratedEvent;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;
import org.springframework.stereotype.Component;

/**
 * Notification events consumers registry. Clients willing to be notified of particular events must register a consumer
 * for these events. Multiple consumers may be registered for the same event. Upon reception of an event, consumers will
 * be invoked sequentially. Consumers are assumed to be thread-safe. Instances of this class are thread safe.
 */
@Component
public class NotificationEventsConsumersRegistry {

  private final List<Consumer<BallotBoxInitializedEvent>>     ballotBoxInitializedEventConsumers;
  private final List<Consumer<VotingMaterialsGeneratedEvent>> votingMaterialsGeneratedEventConsumers;

  /**
   * Creates a new empty registry.
   */
  public NotificationEventsConsumersRegistry() {
    this.ballotBoxInitializedEventConsumers = new CopyOnWriteArrayList<>();
    this.votingMaterialsGeneratedEventConsumers = new CopyOnWriteArrayList<>();
  }

  /**
   * Adds the given {@code BallotBoxInitializedEvent} consumer.
   *
   * @param consumer the consumer.
   *
   * @throws NullPointerException if {@code consumer} is {@code null}.
   */
  public void onBallotBoxInitializedEvent(Consumer<BallotBoxInitializedEvent> consumer) {
    ballotBoxInitializedEventConsumers.add(Objects.requireNonNull(consumer));
  }

  /**
   * Adds the given {@code VotingMaterialsGeneratedEvent} consumer.
   *
   * @param consumer the consumer.
   *
   * @throws NullPointerException if {@code consumer} is {@code null}.
   */
  public void onVotingMaterialsGeneratedEvent(Consumer<VotingMaterialsGeneratedEvent> consumer) {
    votingMaterialsGeneratedEventConsumers.add(Objects.requireNonNull(consumer));
  }

  /**
   * Returns an unmodifiable view of registered consumers for {@code BallotBoxInitializedEvent}.
   *
   * @return an unmodifiable view of registered consumers for {@code BallotBoxInitializedEvent}.
   */
  List<Consumer<BallotBoxInitializedEvent>> getBallotBoxInitializedEventConsumers() {
    return Collections.unmodifiableList(ballotBoxInitializedEventConsumers);
  }

  /**
   * Returns an unmodifiable view of registered consumers for {@code VotingMaterialsGeneratedEvent}.
   *
   * @return an unmodifiable view of registered consumers for {@code VotingMaterialsGeneratedEvent}.
   */
  List<Consumer<VotingMaterialsGeneratedEvent>> getVotingMaterialsGeneratedEventConsumers() {
    return Collections.unmodifiableList(votingMaterialsGeneratedEventConsumers);
  }
}
