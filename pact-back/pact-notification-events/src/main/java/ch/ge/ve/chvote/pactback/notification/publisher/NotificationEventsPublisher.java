/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.notification.publisher;

import ch.ge.ve.chvote.pactback.notification.config.Exchanges;
import ch.ge.ve.chvote.pactback.notification.event.NotificationEvent;
import java.util.Objects;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Notification events publisher.
 */
@Service
public class NotificationEventsPublisher {

  private static final String DEFAULT_ROUTING_KEY = "";

  private final RabbitTemplate rabbitTemplate;

  @Autowired
  public NotificationEventsPublisher(@Qualifier("notificationEventsRabbitTemplate") RabbitTemplate rabbitTemplate) {
    this.rabbitTemplate = rabbitTemplate;
  }

  /**
   * Publishes the given notification event.
   *
   * @param event the event to publish.
   *
   * @throws NullPointerException if {@code event} is {@code null}.
   */
  public void publish(NotificationEvent event) {
    Objects.requireNonNull(event);
    rabbitTemplate.convertAndSend(Exchanges.PACT_NOTIFICATION_EVENTS, DEFAULT_ROUTING_KEY, event);
  }
}
