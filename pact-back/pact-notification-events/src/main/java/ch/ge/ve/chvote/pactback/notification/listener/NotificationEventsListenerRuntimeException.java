/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.notification.listener;

import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 * Thrown when consumers fail to process a notification event.
 */
public final class NotificationEventsListenerRuntimeException extends RuntimeException {

  private final List<Exception> exceptions;

  /**
   * Creates a new {@code NotificationEventsListenerRuntimeException}.
   *
   * @param exceptions the exceptions thrown by the consumers.
   *
   * @throws NullPointerException if {@code exceptions} is {@code null} or if it contains a {@code null} reference.
   */
  NotificationEventsListenerRuntimeException(List<Exception> exceptions) {
    super(exceptions.size() + " consumer(s) failed to process event");
    this.exceptions = ImmutableList.copyOf(exceptions);
  }

  /**
   * Returns an immutable {@code List} of exceptions thrown by consumers.
   *
   * @return an immutable {@code List} of exceptions thrown by consumers.
   */
  public List<Exception> getExceptions() {
    return exceptions;
  }
}
