/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.mock.server;

import ch.ge.ve.chvote.pactback.repository.action.BasePrivilegedActionRepository;
import ch.ge.ve.chvote.pactback.repository.action.PrivilegedActionRepository;
import ch.ge.ve.chvote.pactback.repository.action.deploy.ConfigurationDeploymentRepository;
import ch.ge.ve.chvote.pactback.repository.action.materials.VotingMaterialsCreationActionRepository;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
public class DatabaseController {

  private static final Logger logger = LoggerFactory.getLogger(DatabaseController.class);

  private final ConfigurationDeploymentRepository       configurationDeploymentRepository;
  private final VotingMaterialsConfigurationRepository  votingMaterialsConfigurationRepository;
  private final VotingMaterialsCreationActionRepository votingMaterialsCreationActionRepository;
  private final BasePrivilegedActionRepository          basePrivilegedActionRepository;
  private final PrivilegedActionRepository              privilegedActionRepository;
  private final OperationRepository                     operationRepository;
  private final ProtocolInstanceRepository              protocolInstanceRepository;
  private final OperationsCreationService               operationsCreationService;
  private final VoterRepository                         voterRepository;

  @Autowired
  public DatabaseController(ConfigurationDeploymentRepository configurationDeploymentRepository,
                            VotingMaterialsConfigurationRepository votingMaterialsConfigurationRepository,
                            VotingMaterialsCreationActionRepository votingMaterialsCreationActionRepository,
                            BasePrivilegedActionRepository basePrivilegedActionRepository,
                            PrivilegedActionRepository privilegedActionRepository,
                            OperationRepository operationRepository,
                            ProtocolInstanceRepository protocolInstanceRepository,
                            OperationsCreationService operationsCreationService, VoterRepository voterRepository) {

    this.configurationDeploymentRepository = configurationDeploymentRepository;
    this.votingMaterialsConfigurationRepository = votingMaterialsConfigurationRepository;
    this.votingMaterialsCreationActionRepository = votingMaterialsCreationActionRepository;
    this.basePrivilegedActionRepository = basePrivilegedActionRepository;
    this.privilegedActionRepository = privilegedActionRepository;
    this.operationRepository = operationRepository;
    this.protocolInstanceRepository = protocolInstanceRepository;
    this.operationsCreationService = operationsCreationService;
    this.voterRepository = voterRepository;
  }

  /**
   * Reset database to create a new test
   */
  @RequestMapping(path = "database/reset", method = RequestMethod.GET)
  public void reset() {
    operationsCreationService.createUsersIfAbsent();
    logger.info("Start cleaning database ...");

    voterRepository.deleteAll();
    configurationDeploymentRepository.deleteAll();
    basePrivilegedActionRepository.deleteAll();
    privilegedActionRepository.deleteAll();
    operationRepository.deleteAll();
    protocolInstanceRepository.deleteAll();
    votingMaterialsConfigurationRepository.deleteAll();
    votingMaterialsCreationActionRepository.deleteAll();
    logger.info("The database has been cleaned !");
  }

  @RequestMapping(path = "operations/create", method = RequestMethod.GET)
  public void createOperation() {
    operationsCreationService.createAllOperations();
  }


  @RequestMapping(path = "operations/test-case/voting-material-with-pending-invalidation", method = RequestMethod.GET)
  public IdHolder votingMaterialWithPendingInvalidation() {
    return operationsCreationService.testCase(
        builder ->
            builder.withConfigurationDeployed()
                   .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                   .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                   .withPendingInvalidation()
    );
  }

  @RequestMapping(path = "operations/test-case/voting-material-created", method = RequestMethod.GET)
  public IdHolder votingMaterialCreated() {
    return operationsCreationService.testCase(
        builder ->
            builder.withConfigurationDeployed()
                   .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                   .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
    );
  }

  @RequestMapping(path = "operations/test-case/voting-period-created-in-simulation", method = RequestMethod.GET)
  public IdHolder votingPeriodConfigurationSent() {
    return operationsCreationService.testCase(
        builder ->
            builder.withConfigurationDeployed()
                   .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                   .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                   .withVotingPeriodCreated()
    );
  }

}
