/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.mock.server;

import static ch.ge.ve.chvote.pactback.contract.operation.VirtualCountingCircle.CONTROLLER_TESTING_CARDS;

import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
public class WorkflowController {
  private final OperationRepository operationRepository;

  @Autowired
  public WorkflowController(OperationRepository operationRepository) {
    this.operationRepository = operationRepository;
  }

  /**
   * Warning this impact only database. There is no ack or deletion on messages in Rabbit MQ.
   * Can lead to inconsistent state
   *
   * @param clientId The operation client ID
   */
  @RequestMapping(path = "workflow/create-test-site", method = RequestMethod.GET)
  public void setTestSiteToCreated(String clientId) {
    Operation operation =
        operationRepository.findByClientId(clientId).orElseThrow(
            () -> new EntityNotFoundException("No OperationConfiguration found for clientId [" + clientId + "] "));

    operation.fetchConfigurationInTest().fetchProtocolInstance()
             .setStatus(ProtocolInstanceStatus.CREDENTIALS_GENERATED);
    operationRepository.save(operation);
  }

  /**
   * Warning this impact only database. There is no ack or deletion on messages in Rabbit MQ.
   * Can lead to inconsistent state
   *
   * @param clientId The operation client ID
   */
  @RequestMapping(path = "workflow/create-voting-material", method = RequestMethod.GET)
  public void setVotingMaterialToCreated(String clientId) {
    Operation operation =
        operationRepository.findByClientId(clientId).orElseThrow(
            () -> new EntityNotFoundException("No OperationConfiguration found for clientId [" + clientId + "] "));

    ProtocolInstance protocolInstance = operation.fetchDeployedConfiguration().fetchProtocolInstance();
    protocolInstance.setStatus(ProtocolInstanceStatus.CREDENTIALS_GENERATED);
    protocolInstance.getStatistics().addAll(createStat(operation.fetchVotingMaterialsConfiguration().getPrinters()));

    operationRepository.save(operation);
  }

  private List<VotersCreationStats> createStat(List<PrinterConfiguration> printers) {
    return printers
        .stream()
        .map(PrinterConfiguration::getPrinterName)
        .distinct()
        .flatMap(printer -> {
          VotersCreationStats ccStats = getVotersCreationStatsForTestCards(1, "cc", "1", 10, printer);
          VotersCreationStats testingCard = getVotersCreationStatsForTestCards(
              2, CONTROLLER_TESTING_CARDS.countingCircleName, CONTROLLER_TESTING_CARDS.id,
              CONTROLLER_TESTING_CARDS.ordinal(), printer);

          return Stream.of(ccStats, testingCard);
        }).collect(Collectors.toList());
  }

  private VotersCreationStats getVotersCreationStatsForTestCards(int id, String name, String businessId, int ordinal,
                                                                 String printerName) {
    VotersCreationStats testingCard = new VotersCreationStats();
    testingCard.setCountingCircleName(name);
    testingCard.setCountingCircleBusinessId(businessId);
    testingCard.setCountingCircleId(id);
    testingCard.setNumberOfVoters(ordinal);
    testingCard.setPrinterAuthorityName(printerName);
    return testingCard;
  }


}
