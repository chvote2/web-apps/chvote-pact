/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.mock.server;

import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.APPROVED;
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.PENDING;
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.REJECTED;
import static ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus.INVALIDATED;
import static ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus.NOT_YET_VALIDATED;
import static ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus.VALIDATED;
import static ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType.DOMAIN_OF_INFLUENCE;
import static ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType.OPERATION_REPOSITORY_ELECTION;
import static ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType.OPERATION_REPOSITORY_VOTATION;
import static ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile.SignatureStatus.EXPIRED_CERTIFICATE;
import static ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile.SignatureStatus.INVALID_SIGNATURE;
import static ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile.SignatureStatus.OK;
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.CREDENTIALS_GENERATED;
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES;
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.RESULTS_AVAILABLE;
import static ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage.REQUEST_PRIVATE_CREDENTIALS;
import static ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage.SEND_PUBLIC_PARAMETERS;

import ch.ge.ve.chvote.pactback.fixtures.PrintersPublicKeys;
import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeployment;
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.action.materials.VotingMaterialsCreationActionRepository;
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreation;
import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.VotingPeriodInitializationActionRepository;
import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.entity.VotingPeriodInitialization;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.PublicKeyFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.VotersRegisterFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.Municipality;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgress;
import ch.ge.ve.chvote.pactback.repository.user.UserRepository;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import ch.ge.ve.interfaces.ech.eCH0007.v6.CantonAbbreviationType;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OperationsCreationService {

  private static final Logger logger = LoggerFactory.getLogger(OperationsCreationService.class);

  private static final String ATTACHMENT_HASH                    = "6153A6FA0E4880D9B8D0BE4720F78E895265D0A9";
  private static final String ATTACHMENT_MESSAGE_ID              = "aaa-bbb-401";
  private static final String SIGNATURE_AUTHORITY                = "hélène@etat-ge-ch";
  private static final String USER_1                             = "user1";
  private static final String USER_2                             = "user2";
  private static final String USER_3                             = "user3";
  private static final String SOURCE_APPLICATION_MANUFACTURER    = "SESAM - GE";
  private static final String SOURCE_APPLICATION_PRODUCT_VERSION = "SOURCE_APPLICATION_PRODUCT_VERSION";
  private static final String SOURCE_APPLICATION_PRODUCT         = "Materiel de vote";

  private static final String SITE_TEXT_HASH          =
      "8f3899aead04b608c0d444a50714bc997fcc23028d4e71ce8e9323c17a798ba1";
  private static final String PRINTER_1_NAME          = "Imprimeur 1";
  private static final String PRINTER_1_BUSINESS_ID   = "imprimeur-1";
  private static final String PRINTER_10_BUSINESS_ID  = "imprimeur-10";
  private static final String PRINTER_10_NAME         = "Imprimeur 10";
  private static final String LAST_CHANGE_USER        = "TEST";
  private static final String GENEVE                  = "Genève";
  private static final String OPERATION_NAME_201907VP = "201907VP";
  private static final String OPERATION_NAME_201908VP = "201908VP";

  private final OperationRepository                        operationRepository;
  private final ProtocolInstanceRepository                 protocolInstanceRepository;
  private final UserRepository                             userRepository;
  private final VotingMaterialsCreationActionRepository    votingMaterialsCreationActionRepository;
  private final VotingPeriodInitializationActionRepository votingPeriodInitializationActionRepository;
  private final VoterRepository                            voterRepository;
  private       BigInteger                                 publicKey;
  private       User                                       user1;
  private       User                                       user2;
  private       User                                       user3;


  @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
  @Autowired
  public OperationsCreationService(OperationRepository operationRepository,
                                   ProtocolInstanceRepository protocolInstanceRepository,
                                   UserRepository userRepository,
                                   VotingMaterialsCreationActionRepository votingMaterialsCreationActionRepository,
                                   VotingPeriodInitializationActionRepository
                                         votingPeriodInitializationActionRepository,
                                   VoterRepository voterRepository) {
    this.operationRepository = operationRepository;
    this.protocolInstanceRepository = protocolInstanceRepository;
    this.userRepository = userRepository;
    this.votingMaterialsCreationActionRepository = votingMaterialsCreationActionRepository;
    this.votingPeriodInitializationActionRepository = votingPeriodInitializationActionRepository;
    this.voterRepository = voterRepository;
    this.publicKey = PrintersPublicKeys.publicKeyPa0();
  }

  private static ProtocolStageProgress createProtocolStageProgress(ProtocolStage ps, Integer ccIndex, Long done, Long
      total) {
    ProtocolStageProgress protocolStageProgress = new ProtocolStageProgress();
    protocolStageProgress.setCcIndex(ccIndex);
    protocolStageProgress.setStage(ps);
    protocolStageProgress.setDone(done);
    protocolStageProgress.setTotal(total);
    protocolStageProgress.setStartDate(LocalDateTime.of(2018, 2, 19, 12, 15, 0));
    protocolStageProgress.setLastModificationDate(LocalDateTime.of(2018, 2, 19, 12, 15, 0));
    return protocolStageProgress;
  }

  @Transactional
  public void createUsersIfAbsent() {
    logger.info("Start creating users...");
    user1 = createUserIfNeeded(1, USER_1);
    user2 = createUserIfNeeded(2, USER_2);
    user3 = createUserIfNeeded(3, USER_3);
    logger.info("Users created !");
  }

  @Transactional
  public void createAllOperations() {
    // Create an operation without privileged action and 2 attachments
    withoutPrivilegedActionAnd2Attachments();

    //Create an operation without privileged action nor attachments
    withoutPrivilegedActionNorAttachments();

    //Create an operation with a PENDING privileged action and without attachments
    withPendingPrivilegedActionAndWithoutAttachments();

    //Create an operation with a PENDING privileged action and one attachment
    withPendingPrivilegedActionAndOneAttachment();

    //Create an operation with an APPROVED configuration deployment action and a PENDING voting materials creation
    // action
    withApprovedConfigurationDeploymentAndPendingVotingMaterialsCreation();

    //Create an operation with an invalidated configuration
    withAnInvalidatedConfiguration();

    //Create an operation with an in_validation configuration
    withIn_ValidationConfiguration();

    //Create a operation with a REJECTED privileged action
    withRejectedPrivilegedAction();

    //Create an operation with an APPROVED privileged action and voting materials without action
    withApprovedPrivilegedActionAndVotingMaterialsWithoutAction();

    //Create an operation with a deployed configuration with no action and with no voting materials configuration
    withDeployedConfigurationWithNoActionAndWithNoVotingMaterialsConfiguration();

    withApprovedPrivilegedActionAndVotingMaterialsAndInitialiseVotingPeriodWithoutAction();

    withOtherApprovedPrivilegedActionAndVotingMaterialsAndInitialiseVotingPeriodWithoutAction();

  }


  @Transactional
  public IdHolder testCase(Consumer<TestCaseBuilderWorkflow.WithNoConfiguration> onBuilder) {
    return new IdHolder(testCaseWithOperation(onBuilder));
  }

  @Transactional
  public Operation testCaseWithOperation(Consumer<TestCaseBuilderWorkflow.WithNoConfiguration> onBuilder) {
    TestCaseBuilderWorkflow.WithNoConfiguration builder = TestCaseBuilder
        .of(user1, user2, operationRepository, protocolInstanceRepository, voterRepository);
    onBuilder.accept(builder);
    return operationRepository.getOne(builder.getOperation().getId());
  }

  /**
   * Create an operation without 0 and 2 attachments
   */
  @Transactional
  public void withoutPrivilegedActionAnd2Attachments() {
    Operation operation = createOperationByClientId("1",
                                                    LocalDateTime.of(2017, 8, 20, 9, 0, 0));
    OperationConfiguration operationConfiguration =
        createOperationConfiguration("201710VP", VALIDATED,
                                     LocalDateTime.of(2017, 10, 23, 0, 0, 0),
                                     LocalDateTime.of(2017, 8, 20, 9, 0, 0),
                                     LocalDateTime.of(2017, 10, 20, 9, 0, 0),
                                     LocalDateTime.of(2017, 10, 20, 12, 0, 0));


    OperationReferenceFile attachment1 =
        createAttachment("Piece jointe 1", OPERATION_REPOSITORY_VOTATION,
                         ATTACHMENT_HASH,
                         LocalDateTime.of(2017, 2, 18, 12, 15, 0),
                         "aaa-bbb-101", LocalDateTime.of(2018, 1, 20, 13, 15, 0),
                         "Materiel de vote", "V3", "albert@etat-ge-ch",
                         LocalDateTime.of(2018, 2, 19, 12, 15, 0), OK);

    OperationReferenceFile attachment2 =
        createAttachment("Piece jointe 2", DOMAIN_OF_INFLUENCE, "11F453355B28E1158D4E516A2D3EDF96B3450406",
                         LocalDateTime.of(2017, 2, 19, 12, 15, 0),
                         "aaa-bbb-102",
                         LocalDateTime.of(2017, 11, 29, 7, 35, 0),
                         "Trucs divers", "V1", "bernadette@etat-ge-ch",
                         LocalDateTime.of(2018, 2, 18, 12, 15, 0), INVALID_SIGNATURE);

    operationConfiguration.setOperationReferenceFiles(Arrays.asList(attachment1, attachment2));


    PrinterConfiguration printerConfiguration =
        createPrinterConfiguration(PRINTER_1_BUSINESS_ID, PRINTER_1_NAME, false);
    printerConfiguration.setMunicipalities(Collections.singletonList(createMunicipalities(6621, GENEVE, false)));
    operationConfiguration.setTestPrinter(printerConfiguration);

    operation.setConfigurationInTest(operationConfiguration);
    operation = operationRepository.save(operation);

    createAndSaveProtocolInstance(operation, "1", RESULTS_AVAILABLE, 10L);
  }

  /**
   * Create an operation without privileged action and without attachments
   */
  @Transactional
  public void withoutPrivilegedActionNorAttachments() {
    Operation operation = createOperationByClientId("2", LocalDateTime.of(2017, 8, 21, 9, 0, 0));
    OperationConfiguration operationConfiguration =
        createOperationConfiguration("401910VP", VALIDATED,
                                     LocalDateTime.of(4019, 10, 1, 0, 0, 0),
                                     LocalDateTime.of(2017, 8, 20, 9, 0, 0),
                                     LocalDateTime.of(2017, 11, 2, 9, 0, 0),
                                     LocalDateTime.of(2017, 11, 2, 12, 0, 0));


    PrinterConfiguration printerConfiguration =
        createPrinterConfiguration(PRINTER_1_BUSINESS_ID, PRINTER_1_NAME, false);
    printerConfiguration.setMunicipalities(Collections.singletonList(createMunicipalities(6621, GENEVE, false)));
    operationConfiguration.setTestPrinter(printerConfiguration);

    operation.setConfigurationInTest(operationConfiguration);
    operation = operationRepository.save(operation);
    createAndSaveProtocolInstance(operation, "2", RESULTS_AVAILABLE, 10L);

  }

  /**
   * Create an operation with a PENDING privileged action and without attachments
   */
  @Transactional
  public void withPendingPrivilegedActionAndWithoutAttachments() {
    Operation operation = createOperationByClientId("3", LocalDateTime.of(2017, 8, 22, 9, 0, 0));

    OperationConfiguration operationConfiguration =
        createOperationConfiguration("201802VP", VALIDATED,
                                     LocalDateTime.of(2018, 2, 12, 0, 0, 0),
                                     LocalDateTime.of(2017, 8, 20, 9, 0, 0),
                                     LocalDateTime.of(2018, 2, 11, 9, 0, 0),
                                     LocalDateTime.of(2018, 2, 11, 12, 0, 0));

    PrinterConfiguration printerConfiguration =
        createPrinterConfiguration(PRINTER_1_BUSINESS_ID, PRINTER_1_NAME, false);
    printerConfiguration.setMunicipalities(Collections.singletonList(createMunicipalities(6621, GENEVE, false)));
    operationConfiguration.setTestPrinter(printerConfiguration);
    ConfigurationDeployment configurationDeployment =
        createConfigurationDeployment(operationConfiguration, PENDING,
                                      LocalDateTime.of(2018, 2, 3, 9, 21, 0), null);
    operationConfiguration.setAction(configurationDeployment);
    operation.setConfigurationInTest(operationConfiguration);

    operation = operationRepository.save(operation);
    createAndSaveProtocolInstance(operation, "3", RESULTS_AVAILABLE, 10L);
  }

  /**
   * Create an operation with a PENDING privileged action and one attachment
   */
  @Transactional
  public void withPendingPrivilegedActionAndOneAttachment() {
    Operation operation = createOperationByClientId("4", LocalDateTime.of(2017, 8, 24, 9, 0, 0));

    OperationConfiguration operationConfiguration =
        createOperationConfiguration("201805VP", VALIDATED,
                                     LocalDateTime.of(2017, 10, 23, 0, 0, 0),
                                     LocalDateTime.of(2018, 5, 6, 9, 0, 0),
                                     LocalDateTime.of(2018, 3, 17, 9, 0, 0),
                                     LocalDateTime.of(2018, 3, 17, 12, 0, 0));

    OperationReferenceFile attachment =
        createAttachment("Piece jointe 1", OPERATION_REPOSITORY_ELECTION, "2FD4E1C67A2D28FCED849EE1BB76E7391B93EB12",
                         LocalDateTime.of(2017, 2, 25, 13, 15, 0), ATTACHMENT_MESSAGE_ID,
                         LocalDateTime.of(2018, 1, 9, 21, 0, 23), "Materiel de vote",
                         "SOURCE_APPLICATION_PRODUCT_VERSION",
                         SIGNATURE_AUTHORITY, LocalDateTime.of(2018, 2, 25, 13, 15, 0), EXPIRED_CERTIFICATE);

    operationConfiguration.setOperationReferenceFiles(Collections.singletonList((attachment)));

    PrinterConfiguration printerConfiguration =
        createPrinterConfiguration(PRINTER_1_BUSINESS_ID, PRINTER_1_NAME, false);
    printerConfiguration.setMunicipalities(Collections.singletonList(createMunicipalities(6621, GENEVE, false)));
    operationConfiguration.setTestPrinter(printerConfiguration);
    ConfigurationDeployment configurationDeployment =
        createConfigurationDeployment(operationConfiguration, PENDING,
                                      LocalDateTime.of(2018, 3, 11, 15, 12, 0), null);

    operationConfiguration.setAction(configurationDeployment);
    operation.setConfigurationInTest(operationConfiguration);
    operation = operationRepository.save(operation);
    ProtocolInstance protocolInstance = createProtocolInstance("4", RESULTS_AVAILABLE);
    ProtocolStageProgress psp1 =
        createProtocolStageProgress(SEND_PUBLIC_PARAMETERS, -1, 1L, 1L);
    ProtocolStageProgress psp2 =
        createProtocolStageProgress(REQUEST_PRIVATE_CREDENTIALS, 0, 10L, 10L);
    ProtocolStageProgress psp3 =
        createProtocolStageProgress(REQUEST_PRIVATE_CREDENTIALS, 1, 10L, 10L);
    protocolInstance.setProgress(Arrays.asList(psp1, psp2, psp3));
    protocolInstance.setOperation(operation);
    protocolInstance.setConfiguration(operation.getConfigurationInTest().get());
    protocolInstanceRepository.save(protocolInstance);

  }

  /**
   * Create an operation with an APPROVED configuration deployment action and a PENDING voting materials creation action
   */
  @Transactional
  public void withApprovedConfigurationDeploymentAndPendingVotingMaterialsCreation() {
    Operation operation = createOperationByClientId("5", LocalDateTime.of(2017, 8, 25, 9, 0, 0));

    OperationConfiguration operationConfiguration =
        createOperationConfiguration("201812VP", VALIDATED,
                                     LocalDateTime.of(2018, 12, 12, 0, 0, 0),
                                     LocalDateTime.of(2017, 8, 20, 9, 0, 0),
                                     LocalDateTime.of(2018, 12, 19, 9, 0, 0),
                                     LocalDateTime.of(2018, 12, 19, 12, 0, 0));

    VotersRegisterFile register1Attachment =
        createRegisterAttachment("Registre 1",
                                 LocalDateTime.of(2017, 2, 18, 12, 15, 0),
                                 LocalDateTime.of(2017, 3, 4, 11, 5, 0),
                                 LocalDateTime.of(2018, 2, 25, 12, 15, 0),
                                 55678);

    operationConfiguration.setOperationReferenceFiles(Collections.singletonList(register1Attachment));

    PrinterConfiguration printerConfiguration1 =
        createPrinterConfiguration(PRINTER_10_BUSINESS_ID, PRINTER_10_NAME, false);
    printerConfiguration1.setMunicipalities(Collections.singletonList(createMunicipalities(6621, GENEVE, false)));

    PrinterConfiguration printerConfiguration2 =
        createPrinterConfiguration(PRINTER_1_BUSINESS_ID, PRINTER_1_NAME, false);

    List<Municipality> municipalities = new ArrayList<>();
    municipalities.add(createMunicipalities(6621, GENEVE, false));
    municipalities.add(createMunicipalities(6612, "Chêne-Bougeries", false));
    municipalities.add(createMunicipalities(6606, "Bellevue", false));
    municipalities.add(createMunicipalities(6601, "Aire-la-Ville", false));
    municipalities.add(createMunicipalities(6602, "Anières", false));
    municipalities.add(createMunicipalities(6603, "Avully", false));
    municipalities.add(createMunicipalities(6604, "Avusy", false));
    municipalities.add(createMunicipalities(6605, "Bardonnex", false));
    municipalities.add(createMunicipalities(6607, "Bernex", false));
    municipalities.add(createMunicipalities(6630, "Meyrin", false));
    municipalities.add(createMunicipalities(6628, "Lancy", false));
    municipalities.add(createMunicipalities(100, "Virtual", true));

    printerConfiguration2.setMunicipalities(municipalities);

    PrinterConfiguration printerConfiguration3 =
        createPrinterConfiguration("imprimeur-de-test", "Imprimeur de test", false);
    printerConfiguration3.setMunicipalities(Collections.singletonList(createMunicipalities(6620, "Dardagny", false)));

    PrinterConfiguration printerConfiguration4 = createPrinterConfiguration("imprimeur-se", "Imprimeur SE", true);

    operationConfiguration.setTestPrinter(printerConfiguration1);
    ConfigurationDeployment configurationDeployment =
        createConfigurationDeployment(operationConfiguration, APPROVED,
                                      LocalDateTime.of(2017, 12, 17, 16, 0, 39), null);
    operationConfiguration.setAction(configurationDeployment);


    VotingMaterialsConfiguration votingMaterialsConfiguration =
        createVotingMaterialsConfiguration(
            "Fred",
            LocalDateTime.of(2017, 11, 30, 16, 26, 49),
            "Votation du 26 Novembre 2017", DeploymentTarget.REAL,
            null);

    VotersRegisterFile register2Attachment =
        createRegisterAttachment("Registre 2",
                                 LocalDateTime.of(2017, 2, 18, 12, 15, 0),
                                 LocalDateTime.of(2017, 3, 4, 11, 5, 0),
                                 LocalDateTime.of(2018, 2, 25, 12, 15, 0), 342);

    votingMaterialsConfiguration.setRegisters(Arrays.asList(register1Attachment, register2Attachment));
    votingMaterialsConfiguration.setPrinters(
        Arrays.asList(printerConfiguration2, printerConfiguration3, printerConfiguration4));

    operation.setVotingMaterialsConfiguration(votingMaterialsConfiguration);
    operation.setDeployedConfiguration(operationConfiguration);
    operation = operationRepository.save(operation);

    createAndSaveVotingMaterialsCreation(operation, operationConfiguration);
  }

  /**
   * Create an operation with an invalidated configuration
   */
  @Transactional
  public void withAnInvalidatedConfiguration() {
    Operation operation = createOperationByClientId("6", LocalDateTime.of(2019, 8, 25, 9, 0, 0));

    OperationConfiguration operationConfiguration =
        getOperationConfiguration("201901VP", INVALIDATED, 2019, 30, 12, 19, PRINTER_10_BUSINESS_ID, PRINTER_10_NAME,
                                  APPROVED, 2017, null);
    operation.setConfigurationInTest(operationConfiguration);
    operation = operationRepository.save(operation);
    createAndSaveProtocolInstance(operation, "6", RESULTS_AVAILABLE, 0L);
  }

  /**
   * Create an operation with an in_validation configuration
   */
  @Transactional
  public void withIn_ValidationConfiguration() {
    Operation operation = createOperationByClientId("7", LocalDateTime.of(2019, 8, 25, 9, 0, 0));

    OperationConfiguration operationConfiguration =
        createOperationConfiguration("201901VP", NOT_YET_VALIDATED,
                                     LocalDateTime.of(2019, 1, 15, 0, 0, 0),
                                     LocalDateTime.of(2017, 8, 20, 9, 0, 0),
                                     LocalDateTime.of(2019, 12, 19, 9, 0, 0),
                                     LocalDateTime.of(2019, 12, 19, 12, 0, 0));

    PrinterConfiguration printerConfiguration =
        createPrinterConfiguration(PRINTER_10_BUSINESS_ID, PRINTER_10_NAME, false);
    printerConfiguration.setMunicipalities(Collections.singletonList(createMunicipalities(6621, GENEVE, false)));
    operationConfiguration.setTestPrinter(printerConfiguration);
    operation.setConfigurationInTest(operationConfiguration);
    operation = operationRepository.save(operation);
    createAndSaveProtocolInstance(operation, "7", READY_TO_RECEIVE_VOTES, 0L);
  }

  /**
   * Create a operation with a REJECTED privileged action
   */
  @Transactional
  public void withRejectedPrivilegedAction() {
    Operation operation = createOperationByClientId("8", LocalDateTime.of(2019, 8, 25, 9, 0, 0));

    OperationConfiguration operationConfiguration =
        getOperationConfiguration("202101VP", VALIDATED, 2021, 5, 12, 19, PRINTER_1_BUSINESS_ID, PRINTER_1_NAME,
                                  REJECTED,
                                  2017, "Bart Simpson is not a real person");
    operation.setConfigurationInTest(operationConfiguration);
    operation = operationRepository.save(operation);
    createAndSaveProtocolInstance(operation, "8", RESULTS_AVAILABLE, 0L);
  }

  /**
   * Create an operation with an APPROVED privileged action and voting materials without action
   */
  @Transactional
  public void withApprovedPrivilegedActionAndVotingMaterialsWithoutAction() {
    Operation operation = createOperationByClientId("9", LocalDateTime.of(2019, 8, 25, 9, 0, 0));

    OperationConfiguration operationConfiguration =
        getOperationConfiguration("201901VP", VALIDATED, 2019, 25, 1, 29, PRINTER_1_BUSINESS_ID, PRINTER_1_NAME,
                                  APPROVED,
                                  2017, null);


    VotingMaterialsConfiguration votingMaterialsConfiguration =
        createVotingMaterialsConfiguration("Betty", LocalDateTime.of(2017, 11, 30, 16, 26, 49),
                                           "Votation du 30 Janvier 2019",
                                           DeploymentTarget.SIMULATION, "Simulation 1");
    operation.setVotingMaterialsConfiguration(votingMaterialsConfiguration);

    operation.setDeployedConfiguration(operationConfiguration);
    operationRepository.save(operation);
  }

  /**
   * Create an operation with a deployed configuration with no action and with no voting materials configuration
   */
  @Transactional
  public void withDeployedConfigurationWithNoActionAndWithNoVotingMaterialsConfiguration() {
    Operation operation = createOperationByClientId("10", LocalDateTime.of(2018, 2, 18, 10, 0, 0));

    OperationConfiguration operationConfiguration =
        createOperationConfiguration("201810VP", VALIDATED,
                                     LocalDateTime.of(2018, 10, 1, 0, 0, 0),
                                     LocalDateTime.of(2017, 8, 20, 9, 0, 0),
                                     LocalDateTime.of(2018, 12, 19, 9, 0, 0),
                                     LocalDateTime.of(2018, 12, 19, 12, 0, 0));

    PrinterConfiguration printerConfiguration =
        createPrinterConfiguration(PRINTER_1_BUSINESS_ID, PRINTER_1_NAME, false);
    printerConfiguration.setMunicipalities(Collections.singletonList(createMunicipalities(6621, GENEVE, false)));
    operationConfiguration.setTestPrinter(printerConfiguration);
    operation.setDeployedConfiguration(operationConfiguration);

    operationRepository.save(operation);
  }

  /**
   * Create an operation with an APPROVED privileged action and voting period without action
   */
  @Transactional
  public void withApprovedPrivilegedActionAndVotingMaterialsAndInitialiseVotingPeriodWithoutAction() {
    OperationConfiguration operationConfiguration =
        getOperationConfiguration(OPERATION_NAME_201907VP, VALIDATED, 2019, 25, 1, 29, PRINTER_1_BUSINESS_ID,
                                  PRINTER_1_NAME,
                                  APPROVED, 2018, null);
    Operation operation =
        operationWithVotingPeriodConfiguration("11", OPERATION_NAME_201907VP, "Simulation 2", operationConfiguration);
    createAndSaveVotingPeriodCreation(operation, operationConfiguration);
    createAndSaveProtocolInstance(operation, "9", CREDENTIALS_GENERATED, 0L);

  }


  /**
   * Create an other operation with an APPROVED privileged action and voting period without action
   */
  @Transactional
  public void withOtherApprovedPrivilegedActionAndVotingMaterialsAndInitialiseVotingPeriodWithoutAction() {
    OperationConfiguration operationConfiguration =
        getOperationConfiguration(OPERATION_NAME_201908VP, VALIDATED, 2019, 25, 1, 29, PRINTER_1_BUSINESS_ID,
                                  PRINTER_1_NAME,
                                  APPROVED, 2018, null);
    Operation operation =
        operationWithVotingPeriodConfiguration("12", OPERATION_NAME_201908VP, "Simulation 1", operationConfiguration);
    createAndSaveProtocolInstance(operation, "10", CREDENTIALS_GENERATED, 0L);
  }

  private Operation operationWithVotingPeriodConfiguration(String clientID, String operationName, String simulationName,
                                                           OperationConfiguration operationConfiguration) {
    Operation operation = createOperationByClientId(clientID, LocalDateTime.of(2019, 7, 24, 9, 0, 0));

    VotingPeriodConfiguration votingPeriodConfiguration =
        createVotingPeriodConfiguration(USER_1, DeploymentTarget.SIMULATION, simulationName);

    operation.setVotingPeriodConfiguration(votingPeriodConfiguration);
    operation.setDeployedConfiguration(operationConfiguration);
    return operationRepository.save(operation);
  }

  private OperationConfiguration getOperationConfiguration(String operationName, ValidationStatus validated,
                                                           int i, int i2, int i3, int i4,
                                                           String printer1BusinessId, String printer1Name,
                                                           PrivilegedAction.Status approved, int i5, String o) {
    OperationConfiguration operationConfiguration =
        createOperationConfiguration(operationName, validated,
                                     LocalDateTime.of(i, 1, i2, 0, 0, 0),
                                     LocalDateTime.of(2017, 8, 20, 9, 0, 0),
                                     LocalDateTime.of(2019, i3, i4, 9, 0, 0),
                                     LocalDateTime.of(2019, i3, i4, 12, 0, 0));

    PrinterConfiguration printerConfiguration =
        createPrinterConfiguration(printer1BusinessId, printer1Name, false);
    printerConfiguration.setMunicipalities(Collections.singletonList(createMunicipalities(6621, GENEVE, false)));
    operationConfiguration.setTestPrinter(printerConfiguration);

    ConfigurationDeployment configurationDeployment =
        createConfigurationDeployment(operationConfiguration, approved,
                                      LocalDateTime.of(i5, 12, 17, 16, 0, 39), o);
    operationConfiguration.setAction(configurationDeployment);
    return operationConfiguration;
  }

  /**
   * Private methods for instantiating the objects needed to create previous operations
   */
  private void createAndSaveProtocolInstance(Operation operation, String protocolId, ProtocolInstanceStatus
      protocolInstanceStatus, long ccIndex) {
    ProtocolInstance protocolInstance = createProtocolInstance(protocolId, protocolInstanceStatus);
    ProtocolStageProgress psp1 =
        createProtocolStageProgress(SEND_PUBLIC_PARAMETERS, -1, 1L, 1L);
    ProtocolStageProgress psp2 =
        createProtocolStageProgress(REQUEST_PRIVATE_CREDENTIALS, 0, ccIndex, 10L);
    ProtocolStageProgress psp3 =
        createProtocolStageProgress(REQUEST_PRIVATE_CREDENTIALS, 1, 10L, 10L);

    protocolInstance.setProgress(Arrays.asList(psp1, psp2, psp3));

    if (operation.getConfigurationInTest().isPresent()) {
      protocolInstance.setConfiguration(operation.getConfigurationInTest().get());
    } else if (operation.getDeployedConfiguration().isPresent()) {
      protocolInstance.setConfiguration(operation.getDeployedConfiguration().get());
    }

    protocolInstance.setOperation(operation);
    protocolInstanceRepository.save(protocolInstance);
  }

  private void createAndSaveVotingMaterialsCreation(Operation operation, OperationConfiguration
      operationConfiguration) {
    VotingMaterialsCreation votingMaterialsCreation = new VotingMaterialsCreation();
    votingMaterialsCreation.setOperationName(operationConfiguration.getOperationName());
    votingMaterialsCreation.setOperationDate(operationConfiguration.getOperationDate());
    votingMaterialsCreation.setCreationDate(LocalDateTime.of(2017, 1, 12, 17, 36, 39));
    votingMaterialsCreation.setRequester(user1);
    votingMaterialsCreation.setStatus(PENDING);
    votingMaterialsCreation.setStatusDate(LocalDateTime.of(2017, 1, 12, 17, 36, 39));
    votingMaterialsCreation.setVotingMaterialsConfiguration(operation.getVotingMaterialsConfiguration().get());

    votingMaterialsCreationActionRepository.save(votingMaterialsCreation);
  }

  private VotingMaterialsConfiguration createVotingMaterialsConfiguration(String author, LocalDateTime subDate,
                                                                          String cardLabel,
                                                                          DeploymentTarget target,
                                                                          String simulationName) {
    VotingMaterialsConfiguration votingMaterialsConfiguration = new VotingMaterialsConfiguration();

    votingMaterialsConfiguration.setAuthor(author);
    votingMaterialsConfiguration.setSubmissionDate(subDate);
    votingMaterialsConfiguration.setVotingCardLabel(cardLabel);
    votingMaterialsConfiguration.setTarget(target);
    votingMaterialsConfiguration.setSimulationName(simulationName);
    return votingMaterialsConfiguration;
  }

  private void createAndSaveVotingPeriodCreation(Operation operation, OperationConfiguration
      operationConfiguration) {
    VotingPeriodInitialization votingMaterialsCreation = new VotingPeriodInitialization();
    votingMaterialsCreation.setOperationName(operationConfiguration.getOperationName());
    votingMaterialsCreation.setOperationDate(operationConfiguration.getOperationDate());
    votingMaterialsCreation.setCreationDate(LocalDateTime.of(2017, 1, 12, 17, 36, 39));
    votingMaterialsCreation.setRequester(user1);
    votingMaterialsCreation.setStatus(PENDING);
    votingMaterialsCreation.setStatusDate(LocalDateTime.of(2017, 1, 12, 17, 36, 39));
    votingMaterialsCreation.setVotingPeriodConfiguration(operation.getVotingPeriodConfiguration().get());

    votingPeriodInitializationActionRepository.save(votingMaterialsCreation);
  }

  private VotingPeriodConfiguration createVotingPeriodConfiguration(String author, DeploymentTarget target,
                                                                    String simulationName) {
    VotingPeriodConfiguration votingPeriodConfiguration = new VotingPeriodConfiguration();
    votingPeriodConfiguration.setSiteOpeningDate(LocalDateTime.of(2040, 5, 16, 13, 30, 0));
    votingPeriodConfiguration.setSiteClosingDate(LocalDateTime.of(2040, 5, 16, 17, 30, 0));
    votingPeriodConfiguration.setSubmissionDate(LocalDateTime.of(2018, 5, 16, 13, 30, 0));
    votingPeriodConfiguration.setAuthor(author);
    votingPeriodConfiguration.setTarget(target);
    votingPeriodConfiguration.setSimulationName(simulationName);
    votingPeriodConfiguration.setGracePeriod(200);
    final PublicKeyFile electoralAuthorityKey = new PublicKeyFile();
    electoralAuthorityKey.setType(AttachmentType.ELECTION_OFFICER_KEY);
    electoralAuthorityKey.setFileHash("A7ECDB122A5CFFA2D483F874C72DB962CD7693B318AD5259B9852498EB3CD191");
    votingPeriodConfiguration.setElectoralAuthorityKey(electoralAuthorityKey);
    return votingPeriodConfiguration;
  }

  private Operation createOperationByClientId(String clientId, LocalDateTime localDateTime) {
    logger.info("Start creating operation {} ...", clientId);
    Operation operation = new Operation();
    operation.setClientId(clientId);
    operation.setCreationDate(localDateTime);
    return operation;
  }

  private ConfigurationDeployment createConfigurationDeployment(OperationConfiguration operationConfiguration,
                                                                PrivilegedAction.Status status, LocalDateTime
                                                                    statusDate, String rejectionReason) {
    ConfigurationDeployment configurationDeployment = new ConfigurationDeployment();
    configurationDeployment.setOperationName(operationConfiguration.getOperationName());
    configurationDeployment.setOperationConfiguration(operationConfiguration);
    configurationDeployment.setOperationDate(operationConfiguration.getOperationDate());
    configurationDeployment.setRequester(user2);
    configurationDeployment.setValidator(user3);
    configurationDeployment.setStatus(status);
    configurationDeployment.setStatusDate(statusDate);
    configurationDeployment.setRejectionReason(rejectionReason);

    return configurationDeployment;
  }

  private PrinterConfiguration createPrinterConfiguration(String busId, String name, boolean withoutMunicipality) {
    PrinterConfiguration printerConfiguration = new PrinterConfiguration();
    printerConfiguration.setBusinessIdentifier(busId);
    printerConfiguration.setPrinterName(name);
    printerConfiguration.setSwissAbroadWithoutMunicipality(withoutMunicipality);
    printerConfiguration.setPublicKey(publicKey.toByteArray());

    return printerConfiguration;
  }

  private OperationConfiguration createOperationConfiguration(
      String opName, ValidationStatus status, LocalDateTime opDate, LocalDateTime lastChangeDate,
      LocalDateTime siteOpeningDate, LocalDateTime siteClosingDate) {

    OperationConfiguration operationConfiguration = new OperationConfiguration();

    operationConfiguration.setOperationName(opName);
    operationConfiguration.setOperationLabel(opName);
    operationConfiguration.setSiteTextHash(SITE_TEXT_HASH);
    operationConfiguration.setValidationStatus(status);
    operationConfiguration.setOperationDate(opDate);
    operationConfiguration.setLastChangeDate(lastChangeDate);
    operationConfiguration.setLastChangeUser(LAST_CHANGE_USER);
    operationConfiguration.setGracePeriod(200);
    operationConfiguration.setCanton(CantonAbbreviationType.GE.value());
    operationConfiguration.setGroupVotation(true);
    operationConfiguration.setSiteOpeningDate(siteOpeningDate);
    operationConfiguration.setSiteClosingDate(siteClosingDate);


    return operationConfiguration;
  }

  private Municipality createMunicipalities(Integer ofsId, String name, boolean isVirtual) {
    Municipality municipality = new Municipality();
    municipality.setName(name);
    municipality.setVirtual(isVirtual);
    municipality.setOfsId(ofsId);
    return municipality;
  }

  private OperationReferenceFile createAttachment(String name, AttachmentType attachmentType, String hash,
                                                  LocalDateTime importDate, String msgId,
                                                  LocalDateTime generationDate, String sap, String sapv,
                                                  String signAuthor, LocalDateTime expDate,
                                                  OperationReferenceFile.SignatureStatus signatureStatus) {

    OperationReferenceFile operationReferenceFile = new OperationReferenceFile();
    //Attachment
    operationReferenceFile.setName(name);
    operationReferenceFile.setType(attachmentType);
    operationReferenceFile.setFileHash(hash);
    operationReferenceFile.setImportDate(importDate);

    //ORF
    operationReferenceFile.setMessageId(msgId);
    operationReferenceFile.setGenerationDate(generationDate);
    operationReferenceFile.setSourceApplicationManufacturer(SOURCE_APPLICATION_MANUFACTURER);
    operationReferenceFile.setSourceApplicationProduct(sap);
    operationReferenceFile.setSourceApplicationProductVersion(sapv);
    operationReferenceFile.setSignatureAuthor(signAuthor);
    operationReferenceFile.setSignatureExpiryDate(expDate);
    operationReferenceFile.setSignatureStatus(signatureStatus);

    return operationReferenceFile;
  }

  private VotersRegisterFile createRegisterAttachment(String name, LocalDateTime importDate,
                                                      LocalDateTime generationDate,
                                                      LocalDateTime expDate, Integer nbOfVoters) {

    VotersRegisterFile votersRegisterFile = new VotersRegisterFile();
    //Attachment
    votersRegisterFile.setName(name);
    votersRegisterFile.setType(AttachmentType.OPERATION_REGISTER);
    votersRegisterFile.setFileHash(ATTACHMENT_HASH);
    votersRegisterFile.setImportDate(importDate);

    //ORF
    votersRegisterFile.setMessageId(ATTACHMENT_MESSAGE_ID);
    votersRegisterFile.setGenerationDate(generationDate);
    votersRegisterFile.setSourceApplicationManufacturer(SOURCE_APPLICATION_MANUFACTURER);
    votersRegisterFile.setSourceApplicationProduct(SOURCE_APPLICATION_PRODUCT);
    votersRegisterFile.setSourceApplicationProductVersion(SOURCE_APPLICATION_PRODUCT_VERSION);
    votersRegisterFile.setSignatureAuthor(SIGNATURE_AUTHORITY);
    votersRegisterFile.setSignatureExpiryDate(expDate);
    votersRegisterFile.setSignatureStatus(OperationReferenceFile.SignatureStatus.EXPIRED_CERTIFICATE);

    votersRegisterFile.setNbOfVoters(nbOfVoters);

    return votersRegisterFile;
  }

  private ProtocolInstance createProtocolInstance(String protocolId, ProtocolInstanceStatus protocolInstanceStatus) {
    ProtocolInstance protocolInstance = new ProtocolInstance();
    protocolInstance.setNodeRepartitionKey(StaticTestData.PINSTANCE_NODE_KEY);
    protocolInstance.setProtocolId(protocolId);
    protocolInstance.setEnvironment(ProtocolEnvironment.TEST);
    protocolInstance.setStatus(protocolInstanceStatus);
    protocolInstance.setPrinterArchiveCreationDate(LocalDateTime.now());

    return protocolInstance;
  }


  private User createUserIfNeeded(long id, String userName) {
    return userRepository.findByUsername(userName).orElseGet(() -> {
      User user = new User();
      user.setId(id);
      user.setUsername(userName);
      return userRepository.save(user);
    });
  }

}
