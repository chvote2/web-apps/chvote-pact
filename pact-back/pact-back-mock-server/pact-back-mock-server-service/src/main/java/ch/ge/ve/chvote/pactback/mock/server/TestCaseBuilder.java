/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.mock.server;

import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.BO_USER;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.CANTON;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.COUNTING_CIRCLE_ID_1;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.CREATION_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.DEPLOYED_CONFIGURATION_LAST_CHANGE_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.DEPLOYMENT_REQUEST_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.DEPLOYMENT_VALIDATION_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.GRACE_PERIOD;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.IN_TEST_CONFIGURATION_LAST_CHANGE_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.NB_VOTERS_FOR_CONTROLLER_TESTING_CARDS;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.NB_VOTERS_FOR_NOT_PRINTABLE_TESTING_CARDS;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.NB_VOTERS_FOR_PRINTABLE_TESTING_CARDS;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.NB_VOTERS_FOR_PRINTER_TESTING_CARDS;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.NB_VOTERS_ON_CC_1;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.NB_VOTERS_ON_CC_2_FOR_PRINTER_1;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.NB_VOTERS_ON_CC_2_FOR_PRINTER_2;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.OPERATION_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.OPERATION_LABEL;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.OPERATION_NAME;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.PRINTER_1;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.PRINTER_2;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.REAL_MUNICIPALITY;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.REJECTION_REASON;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.SIMULATION_NAME;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.SIMULATION_SITE_CLOSING_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.SIMULATION_SITE_OPENING_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.SITE_CLOSING_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.SITE_OPENING_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.SITE_TEXT_HASH;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.VIRTUAL_MUNICIPALITY;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.VOTING_CARD_LABEL;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.VOTING_MATERIAL_CREATION_REQUEST_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.VOTING_MATERIAL_CREATION_VALIDATION_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.VOTING_MATERIAL_INVALIDATION_APPROVATION_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.VOTING_MATERIAL_INVALIDATION_REQUEST_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.VOTING_MATERIAL_SENT_DATE;
import static ch.ge.ve.chvote.pactback.mock.server.StaticTestData.VOTING_MATERIAL_VALIDATION_DATE;
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.APPROVED;
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.PENDING;
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.REJECTED;

import ch.ge.ve.chvote.pactback.contract.operation.VirtualCountingCircle;
import ch.ge.ve.chvote.pactback.fixtures.PrintersPublicKeys;
import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeployment;
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreation;
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsInvalidation;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.BallotDocumentation;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.HighlightedQuestion;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.InformationAttachment;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.PublicKeyFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.Municipality;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.DateOfBirth;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterCountingCircle;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterEntity;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgress;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.springframework.util.StreamUtils;

public class TestCaseBuilder implements
    TestCaseBuilderWorkflow.OperationHolder,
    TestCaseBuilderWorkflow.WithNoConfiguration,
    TestCaseBuilderWorkflow.CanCreateDeployConfiguration,
    TestCaseBuilderWorkflow.WithDeployedConfiguration,
    TestCaseBuilderWorkflow.WithConfigurationInTest,
    TestCaseBuilderWorkflow.WithVotingMaterialConfiguration,
    TestCaseBuilderWorkflow.WithAcceptedVotingMaterialCreated,
    TestCaseBuilderWorkflow.WithVotingMaterialValidated,
    TestCaseBuilderWorkflow.NoMoreChangePossible,
    TestCaseBuilderWorkflow.WithVotingPeriodCreated {


  private static Long TEST_CASE_SEQUENCE = 1000L;


  private final User requester;
  private final User validator;

  private final Long                         testCaseId;
  private final OperationRepository          operationRepository;
  private final ProtocolInstanceRepository   protocolInstanceRepository;
  private final VoterRepository              voterRepository;
  private       OperationConfiguration       inTestConfiguration;
  private       OperationConfiguration       deployedConfiguration;
  private       VotingMaterialsConfiguration votingMaterialConfiguration;
  private       Municipality                 virtualMunicipality;
  private       Municipality                 realMunicipality1;
  private       PrinterConfiguration         printerConfiguration1;
  private       Municipality                 realMunicipality2;
  private       PrinterConfiguration         printerConfiguration2;
  private       ProtocolInstance             protocolInstance;
  private       Operation                    operation;
  private       VotingPeriodConfiguration    votingPeriodConfiguration;
  private       VoterEntity                  voterEntity;

  private TestCaseBuilder(
      Long testCaseId, User requester, User validator,
      OperationRepository operationRepository,
      ProtocolInstanceRepository protocolInstanceRepository,
      VoterRepository voterRepository
  ) {

    this.requester = requester;
    this.validator = validator;
    this.testCaseId = testCaseId;
    this.operationRepository = operationRepository;
    this.protocolInstanceRepository = protocolInstanceRepository;
    this.voterRepository = voterRepository;

    createPrinterConfig();
  }


  static TestCaseBuilderWorkflow.WithNoConfiguration of(
      User requester, User validator,
      OperationRepository operationRepository,
      ProtocolInstanceRepository protocolInstanceRepository,
      VoterRepository voterRepository
  ) {

    return new TestCaseBuilder(TEST_CASE_SEQUENCE++, requester, validator, operationRepository,
                               protocolInstanceRepository, voterRepository);
  }


  @Override
  public TestCaseBuilderWorkflow.WithConfigurationInTest withConfigurationInTest(ValidationStatus validationStatus) {
    inTestConfiguration = createBaseConfiguration();
    inTestConfiguration.setLastChangeDate(IN_TEST_CONFIGURATION_LAST_CHANGE_DATE);
    inTestConfiguration.setValidationStatus(ValidationStatus.VALIDATED);
    return this;
  }


  @Override
  public TestCaseBuilderWorkflow.CanCreateDeployConfiguration withPendingRequestToDeployConfiguration() {
    ConfigurationDeployment configurationDeployment = createBaseConfigurationDeployment();
    configurationDeployment.setValidator(null);
    configurationDeployment.setStatus(PrivilegedAction.Status.PENDING);
    configurationDeployment.setStatusDate(DEPLOYMENT_REQUEST_DATE);
    configurationDeployment.setRejectionReason(null);
    inTestConfiguration.setAction(configurationDeployment);
    return this;
  }

  @Override
  public TestCaseBuilderWorkflow.CanCreateDeployConfiguration withRefusedRequestToDeployConfiguration() {
    ConfigurationDeployment configurationDeployment =
        createBaseConfigurationDeployment();

    configurationDeployment.setValidator(validator);
    configurationDeployment.setStatus(PrivilegedAction.Status.REJECTED);
    configurationDeployment.setStatusDate(DEPLOYMENT_VALIDATION_DATE);
    configurationDeployment.setRejectionReason(REJECTION_REASON);
    inTestConfiguration.setAction(configurationDeployment);
    return this;
  }


  private ConfigurationDeployment createBaseConfigurationDeployment() {
    ConfigurationDeployment configurationDeployment = new ConfigurationDeployment();
    configurationDeployment.setOperationName(OPERATION_NAME);
    configurationDeployment.setOperationConfiguration(inTestConfiguration);
    configurationDeployment.setOperationDate(OPERATION_DATE);
    configurationDeployment.setRequester(requester);
    return configurationDeployment;
  }


  @Override
  public TestCaseBuilderWorkflow.WithDeployedConfiguration withConfigurationDeployed() {
    deployedConfiguration = createBaseConfiguration();
    deployedConfiguration.setLastChangeDate(DEPLOYED_CONFIGURATION_LAST_CHANGE_DATE);

    ConfigurationDeployment configurationDeployment = createBaseConfigurationDeployment();
    configurationDeployment.setValidator(validator);
    configurationDeployment.setStatus(PrivilegedAction.Status.APPROVED);
    configurationDeployment.setStatusDate(DEPLOYMENT_VALIDATION_DATE);
    deployedConfiguration.setAction(configurationDeployment);

    return this;
  }


  private OperationConfiguration createBaseConfiguration() {
    OperationConfiguration operationConfiguration = new OperationConfiguration();
    operationConfiguration.setOperationName(OPERATION_NAME);
    operationConfiguration.setOperationLabel(OPERATION_LABEL);
    operationConfiguration.setSiteTextHash(SITE_TEXT_HASH);
    operationConfiguration.setOperationDate(OPERATION_DATE);
    operationConfiguration.setLastChangeUser(BO_USER);
    operationConfiguration.setGracePeriod(GRACE_PERIOD);
    operationConfiguration.setCanton(CANTON);
    operationConfiguration.setGroupVotation(true);
    operationConfiguration.setSiteOpeningDate(SITE_OPENING_DATE);
    operationConfiguration.setSiteClosingDate(SITE_CLOSING_DATE);
    operationConfiguration.setOperationReferenceFiles(createReferencesFiles());
    operationConfiguration.setInformationAttachments(createInformationAttachments());
    operationConfiguration.setHighlightedQuestions(createHighlightedQuestions());
    operationConfiguration.setBallotDocumentations(createBallotDocumentations());
    operationConfiguration.setTestPrinter(printerConfiguration1);
    return operationConfiguration;
  }

  private List<InformationAttachment> createInformationAttachments() {
    try {
      InformationAttachment translations = new InformationAttachment();
      translations.setFile("{'translation':'t'}".getBytes(Charset.defaultCharset()));
      translations.setType(AttachmentType.OPERATION_TRANSLATION_FILE);
      translations.setName("translation.json");
      return list(translations,
                  createInformationAttachment(AttachmentType.OPERATION_FAQ),
                  createInformationAttachment(AttachmentType.OPERATION_TERMS_OF_USAGE),
                  createInformationAttachment(AttachmentType.OPERATION_CERTIFICATE_VERIFICATION_FILE));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private InformationAttachment createInformationAttachment(AttachmentType type) throws IOException {
    InformationAttachment attachment = new InformationAttachment();
    attachment.setLanguage("FR");
    attachment.setFile(StreamUtils.copyToByteArray(getClass().getResourceAsStream("/doc.pdf")));
    attachment.setType(type);
    attachment.setName(type.name().toLowerCase(Locale.getDefault()) + ".pdf");
    return attachment;
  }

  private List<HighlightedQuestion> createHighlightedQuestions() {
    try {
      HighlightedQuestion question = new HighlightedQuestion();
      question.setQuestion("Question");
      question.setLanguage("FR");
      question.setName("question.pdf");
      question.setFile(StreamUtils.copyToByteArray(getClass().getResourceAsStream("/doc.pdf")));
      question.setType(AttachmentType.HIGHLIGHTED_QUESTION);

      return list(question);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private List<BallotDocumentation> createBallotDocumentations() {
    try {
      BallotDocumentation votationBallotDoc = new BallotDocumentation();
      votationBallotDoc.setBallot("202012SGA-COM-6624");
      votationBallotDoc.setLabel("votation ballot doc");
      votationBallotDoc.setLanguage("FR");
      votationBallotDoc.setName("votation_ballot.pdf");
      votationBallotDoc.setFile(StreamUtils.copyToByteArray(getClass().getResourceAsStream("/doc.pdf")));
      votationBallotDoc.setType(AttachmentType.BALLOT_DOCUMENTATION);

      BallotDocumentation electionBallotDoc = new BallotDocumentation();
      electionBallotDoc.setBallot("27050");
      electionBallotDoc.setLanguage("FR");
      electionBallotDoc.setLabel("election ballot doc");
      electionBallotDoc.setName("election_ballot.pdf");
      electionBallotDoc.setFile(StreamUtils.copyToByteArray(getClass().getResourceAsStream("/doc.pdf")));
      electionBallotDoc.setType(AttachmentType.BALLOT_DOCUMENTATION);

      return list(votationBallotDoc, electionBallotDoc);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private List<OperationReferenceFile> createReferencesFiles() {
    try {
      OperationReferenceFile votation = new OperationReferenceFile();
      votation.setGenerationDate(LocalDateTime.of(2010, 1, 1, 0, 0));
      votation.setFile(StreamUtils.copyToByteArray(getClass().getResourceAsStream("/repositoryVotation.xml")));
      votation.setType(AttachmentType.OPERATION_REPOSITORY_VOTATION);


      OperationReferenceFile election = new OperationReferenceFile();
      election.setGenerationDate(LocalDateTime.of(2010, 1, 1, 0, 0));
      election.setFile(StreamUtils.copyToByteArray(getClass().getResourceAsStream("/repositoryElection.xml")));
      election.setType(AttachmentType.OPERATION_REPOSITORY_ELECTION);


      return list(votation, election);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void createPrinterConfig() {
    createVirtualMunicipality();
    createRealMunicipality();
    printerConfiguration1 = new PrinterConfiguration();
    printerConfiguration1.setMunicipalities(list(virtualMunicipality, realMunicipality1));
    printerConfiguration1.setPrinterName(PRINTER_1);
    printerConfiguration1.setPublicKey(PrintersPublicKeys.publicKeyPa0().toByteArray());
    printerConfiguration1.setBusinessIdentifier(PRINTER_1);
    printerConfiguration1.setSwissAbroadWithoutMunicipality(false);

    printerConfiguration2 = new PrinterConfiguration();
    printerConfiguration2.setMunicipalities(Collections.singletonList(realMunicipality2));
    printerConfiguration2.setPrinterName(PRINTER_2);
    printerConfiguration2.setPublicKey(PrintersPublicKeys.publicKeyPa1().toByteArray());
    printerConfiguration2.setBusinessIdentifier(PRINTER_2);
    printerConfiguration2.setSwissAbroadWithoutMunicipality(true);

  }


  private void createVirtualMunicipality() {
    virtualMunicipality = new Municipality();
    virtualMunicipality.setOfsId(9999);
    virtualMunicipality.setVirtual(true);
    virtualMunicipality.setName(VIRTUAL_MUNICIPALITY);
  }

  private void createRealMunicipality() {
    realMunicipality1 = new Municipality();
    realMunicipality1.setOfsId(1);
    realMunicipality1.setVirtual(false);
    realMunicipality1.setName(REAL_MUNICIPALITY + "_1");

    realMunicipality2 = new Municipality();
    realMunicipality2.setOfsId(2);
    realMunicipality2.setVirtual(false);
    realMunicipality2.setName(REAL_MUNICIPALITY + "_2");
  }

  @Override
  public TestCaseBuilderWorkflow.WithVotingMaterialConfiguration withVotingMaterialConfiguration
      (DeploymentTarget target) {
    votingMaterialConfiguration = new VotingMaterialsConfiguration();
    votingMaterialConfiguration.setAuthor(BO_USER);
    votingMaterialConfiguration.setSubmissionDate(VOTING_MATERIAL_SENT_DATE);
    votingMaterialConfiguration.setVotingCardLabel(VOTING_CARD_LABEL);
    votingMaterialConfiguration.setTarget(target);
    votingMaterialConfiguration.setValidationStatus(ValidationStatus.NOT_YET_VALIDATED);

    if (target == DeploymentTarget.SIMULATION) {
      votingMaterialConfiguration.setSimulationName(SIMULATION_NAME);
    }
    return this;
  }

  @Override
  public TestCaseBuilderWorkflow.NoMoreChangePossible requestVotingMaterialCreation() {
    VotingMaterialsCreation votingMaterialsCreation = createBaseVotingMaterialsCreation();
    votingMaterialsCreation.setStatusDate(VOTING_MATERIAL_CREATION_REQUEST_DATE);
    votingMaterialsCreation.setStatus(PENDING);
    votingMaterialConfiguration.setAction(votingMaterialsCreation);
    return this;
  }

  @Override
  public TestCaseBuilderWorkflow.NoMoreChangePossible rejectVotingMaterialCreation() {
    VotingMaterialsCreation votingMaterialsCreation = createBaseVotingMaterialsCreation();
    votingMaterialsCreation.setStatusDate(VOTING_MATERIAL_CREATION_VALIDATION_DATE);
    votingMaterialsCreation.setValidator(validator);
    votingMaterialsCreation.setRejectionReason(REJECTION_REASON);
    votingMaterialsCreation.setStatus(REJECTED);
    return this;
  }

  @Override
  public TestCaseBuilderWorkflow.WithAcceptedVotingMaterialCreated acceptVotingMaterialCreation
      (ProtocolInstanceStatus status) {
    VotingMaterialsCreation votingMaterialsCreation = createBaseVotingMaterialsCreation();
    votingMaterialsCreation.setStatusDate(VOTING_MATERIAL_CREATION_VALIDATION_DATE);
    votingMaterialsCreation.setValidator(validator);
    votingMaterialsCreation.setStatus(APPROVED);

    voterEntity = new VoterEntity();
    voterEntity.setAllowedDoiIds(Collections.singletonList("CH"));
    voterEntity
        .setVoterCountingCircle(new VoterCountingCircle(COUNTING_CIRCLE_ID_1, String.valueOf(COUNTING_CIRCLE_ID_1),
                                                        String.valueOf(COUNTING_CIRCLE_ID_1)));
    voterEntity.setDateOfBirth(new DateOfBirth(2001, 1, 1));
    voterEntity.setPrintingAuthorityName(PRINTER_1);
    voterEntity.setLocalPersonId("???");
    voterEntity.setProtocolVoterIndex(0);

    deployedConfiguration.setProtocolInstance(createProtocolInstanceForProduction(status));
    return this;
  }

  @Override
  public TestCaseBuilderWorkflow.WithVotingMaterialValidated validateVotingMaterial() {
    votingMaterialConfiguration.setValidationStatus(ValidationStatus.VALIDATED);
    votingMaterialConfiguration.setValidationDate(VOTING_MATERIAL_VALIDATION_DATE);
    votingMaterialConfiguration.setValidator(BO_USER);
    return this;
  }

  private ProtocolInstance createProtocolInstanceForProduction(ProtocolInstanceStatus status) {
    protocolInstance = new ProtocolInstance();
    protocolInstance.setNodeRepartitionKey(StaticTestData.PINSTANCE_NODE_KEY);
    protocolInstance.setProtocolId(String.valueOf(testCaseId));
    protocolInstance.setEnvironment(ProtocolEnvironment.PRODUCTION);
    protocolInstance.setStatus(status);


    if (status.ordinal() >= ProtocolInstanceStatus.CREDENTIALS_GENERATED.ordinal()) {
      protocolInstance.setStatistics(
          list(
              createVotersCreationStats(1, NB_VOTERS_ON_CC_1, PRINTER_1, "1"),
              createVotersCreationStats(2, NB_VOTERS_ON_CC_2_FOR_PRINTER_1, PRINTER_1, "2"),
              createVotersCreationStats(2, NB_VOTERS_ON_CC_2_FOR_PRINTER_2, PRINTER_2, "2"),
              createVotersCreationStats(3,
                                        NB_VOTERS_FOR_CONTROLLER_TESTING_CARDS, PRINTER_2,
                                        VirtualCountingCircle.CONTROLLER_TESTING_CARDS.id),
              createVotersCreationStats(4,
                                        NB_VOTERS_FOR_PRINTABLE_TESTING_CARDS, PRINTER_2,
                                        VirtualCountingCircle.PRINTABLE_TESTING_CARDS.id),
              createVotersCreationStats(5,
                                        NB_VOTERS_FOR_NOT_PRINTABLE_TESTING_CARDS, PRINTER_2,
                                        VirtualCountingCircle.NOT_PRINTABLE_TESTING_CARDS.id),
              createVotersCreationStats(6,
                                        NB_VOTERS_FOR_PRINTER_TESTING_CARDS, PRINTER_2,
                                        VirtualCountingCircle.PRINTER_TESTING_CARDS.id)
          ));
    }

    protocolInstance.setProgress(createProtocolStageProgress());
    return protocolInstance;
  }

  private List<ProtocolStageProgress> createProtocolStageProgress() {
    return list(
        createProtocolStageProgress(ProtocolStage.SEND_PUBLIC_PARAMETERS, -1, 100L, 100L),
        createProtocolStageProgress(ProtocolStage.PUBLISH_PUBLIC_KEY_PARTS, -1, 20L, 80L)
    );
  }


  @SafeVarargs
  private final <T> List<T> list(T... elements) {
    return new ArrayList<>(Arrays.asList(elements));
  }


  private static ProtocolStageProgress createProtocolStageProgress(
      ProtocolStage ps, Integer ccIndex, Long done, Long total) {
    ProtocolStageProgress protocolStageProgress = new ProtocolStageProgress();
    protocolStageProgress.setCcIndex(ccIndex);
    protocolStageProgress.setStage(ps);
    protocolStageProgress.setDone(done);
    protocolStageProgress.setTotal(total);
    protocolStageProgress.setStartDate(VOTING_MATERIAL_CREATION_VALIDATION_DATE);
    protocolStageProgress
        .setLastModificationDate(VOTING_MATERIAL_CREATION_VALIDATION_DATE.minusMinutes(5));
    return protocolStageProgress;
  }


  private VotersCreationStats createVotersCreationStats(int countingCircleId,
                                                        int numberOfVotersOnCc,
                                                        String printerAuthorityName,
                                                        String businessId) {
    VotersCreationStats votersCreationStats = new VotersCreationStats();
    votersCreationStats.setCountingCircleName("Counting-circle-" + countingCircleId);
    votersCreationStats.setCountingCircleId(countingCircleId);
    votersCreationStats.setNumberOfVoters(numberOfVotersOnCc);
    votersCreationStats.setPrinterAuthorityName(printerAuthorityName);
    votersCreationStats.setCountingCircleBusinessId(businessId);
    return votersCreationStats;
  }


  private VotingMaterialsCreation createBaseVotingMaterialsCreation() {
    VotingMaterialsCreation votingMaterialsCreation = new VotingMaterialsCreation();
    votingMaterialsCreation.setOperationName(OPERATION_NAME);
    votingMaterialsCreation.setOperationDate(OPERATION_DATE);
    votingMaterialsCreation.setCreationDate(VOTING_MATERIAL_CREATION_REQUEST_DATE);
    votingMaterialsCreation.setRequester(requester);
    votingMaterialsCreation.setVotingMaterialsConfiguration(votingMaterialConfiguration);
    votingMaterialConfiguration.setAction(votingMaterialsCreation);
    return votingMaterialsCreation;
  }


  private Operation buildAndSaveIfNeeded() {
    if (operation != null) {
      return operation;
    }
    operation = new Operation();
    operation.setClientId(String.valueOf(testCaseId));
    operation.setCreationDate(CREATION_DATE);
    operation.setConfigurationInTest(inTestConfiguration);
    operation.setDeployedConfiguration(deployedConfiguration);
    operation.setVotingMaterialsConfiguration(votingMaterialConfiguration);
    operation.setVotingPeriodConfiguration(votingPeriodConfiguration);
    operation = operationRepository.save(operation);
    if (protocolInstance != null) {
      protocolInstance.setOperation(operation);
      protocolInstance.setConfiguration(
          operation.getDeployedConfiguration()
                   .orElseThrow(() -> new IllegalStateException("should not occurs by construction")));

      protocolInstanceRepository.save(protocolInstance);
    }

    if (voterEntity != null) {
      voterEntity.setProtocolInstance(protocolInstance);
      voterRepository.save(voterEntity);
    }

    return operationRepository.getOne(operation.getId());
  }


  @Override
  public Operation getOperation() {
    return buildAndSaveIfNeeded();
  }


  @Override
  public TestCaseBuilderWorkflow.NoMoreChangePossible withPendingInvalidation() {
    VotingMaterialsInvalidation invalidationAction = createBaseVotingMaterialsInvalidation();
    invalidationAction.setStatus(PENDING);
    invalidationAction.setStatusDate(VOTING_MATERIAL_INVALIDATION_REQUEST_DATE);
    return this;
  }

  @Override
  public TestCaseBuilderWorkflow.NoMoreChangePossible withApprovedInvalidation() {
    VotingMaterialsInvalidation invalidationAction = createBaseVotingMaterialsInvalidation();
    invalidationAction.setStatus(APPROVED);
    invalidationAction.setValidator(validator);
    invalidationAction.setStatusDate(VOTING_MATERIAL_INVALIDATION_APPROVATION_DATE);
    return this;
  }


  @Override
  public TestCaseBuilderWorkflow.WithVotingPeriodCreated withVotingPeriodCreated() {
    votingPeriodConfiguration = new VotingPeriodConfiguration();
    votingPeriodConfiguration.setAuthor("MOCK-SERVER");
    votingPeriodConfiguration.setGracePeriod(10);
    votingPeriodConfiguration.setSiteClosingDate(SIMULATION_SITE_CLOSING_DATE);
    votingPeriodConfiguration.setSiteOpeningDate(SIMULATION_SITE_OPENING_DATE);

    final PublicKeyFile electoralAuthorityKey = new PublicKeyFile();
    electoralAuthorityKey.setType(AttachmentType.ELECTION_OFFICER_KEY);
    electoralAuthorityKey.setFileHash("A7ECDB122A5CFFA2D483F874C72DB962CD7693B318AD5259B9852498EB3CD191");
    votingPeriodConfiguration.setElectoralAuthorityKey(electoralAuthorityKey);
    votingPeriodConfiguration.setTarget(DeploymentTarget.SIMULATION);
    votingPeriodConfiguration.setSubmissionDate(LocalDateTime.of(2018, 5, 16, 13, 30, 0));

    return this;
  }

  private VotingMaterialsInvalidation createBaseVotingMaterialsInvalidation() {
    VotingMaterialsInvalidation invalidationAction = new VotingMaterialsInvalidation();
    invalidationAction.setCreationDate(VOTING_MATERIAL_INVALIDATION_REQUEST_DATE);
    invalidationAction.setRequester(requester);
    invalidationAction.setOperationDate(OPERATION_DATE);
    invalidationAction.setOperationName(OPERATION_NAME);
    invalidationAction.setVotingMaterialsConfiguration(votingMaterialConfiguration);
    votingMaterialConfiguration.setInvalidationAction(invalidationAction);
    return invalidationAction;
  }
}
