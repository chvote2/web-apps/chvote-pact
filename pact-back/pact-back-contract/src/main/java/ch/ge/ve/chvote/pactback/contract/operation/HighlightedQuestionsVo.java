/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import ch.ge.ve.chvote.pactback.contract.validation.AttachmentType;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * A DTO containing the metadata of a highlighted question PDF file.
 */
public class HighlightedQuestionsVo {
  /**
   * Pdf file containing the answer
   */
  @NotNull
  @AttachmentType(AttachmentFileEntryVo.AttachmentType.HIGHLIGHTED_QUESTION)
  private AttachmentFileEntryVo answerFile;

  /**
   * Localized Question
   */
  @NotNull
  @Pattern(regexp = "[\\p{L}\\d \\-',()/._?]+")
  @Size(min = 1, max = 500)
  private String question;

  public AttachmentFileEntryVo getAnswerFile() {
    return answerFile;
  }

  public void setAnswerFile(AttachmentFileEntryVo answerFile) {
    this.answerFile = answerFile;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HighlightedQuestionsVo that = (HighlightedQuestionsVo) o;
    return Objects.equals(answerFile.getZipFileName(), that.answerFile.getZipFileName()) &&
           Objects.equals(question, that.question);
  }

  @Override
  public int hashCode() {

    return Objects.hash(answerFile.getZipFileName(), question);
  }
}
