/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO that links a municipality to a printer.
 */
public class MunicipalityToPrinterLinkVo {

  @NotNull
  private int municipalityOfsId;

  @NotNull
  private String printerId;

  public int getMunicipalityOfsId() {
    return municipalityOfsId;
  }

  public void setMunicipalityOfsId(int municipalityOfsId) {
    this.municipalityOfsId = municipalityOfsId;
  }

  public String getPrinterId() {
    return printerId;
  }

  public void setPrinterId(String printerId) {
    this.printerId = printerId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MunicipalityToPrinterLinkVo that = (MunicipalityToPrinterLinkVo) o;
    return municipalityOfsId == that.municipalityOfsId &&
           Objects.equals(printerId, that.printerId);
  }

  @Override
  public int hashCode() {

    return Objects.hash(municipalityOfsId, printerId);
  }
}
