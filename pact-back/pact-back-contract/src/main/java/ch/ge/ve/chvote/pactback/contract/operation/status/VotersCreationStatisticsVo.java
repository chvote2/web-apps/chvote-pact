/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation.status;

import static java.util.Objects.requireNonNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

/**
 * A DTO representing the number of voters created by the protocol, per printer name and counting circle.
 */
public class VotersCreationStatisticsVo {

  private final String printerName;
  private final String countingCircleId;
  private final String countingCircleName;
  private final long   numberOfVoters;

  /**
   * Create a new voter creation statistics DTO.
   *
   * @param printerName        the name of the printer.
   * @param countingCircleId   the counting circle identifier.
   * @param countingCircleName the counting circle name.
   * @param numberOfVoters     the number of voters.
   */
  @JsonCreator
  public VotersCreationStatisticsVo(
      @JsonProperty(value = "printerName") String printerName,
      @JsonProperty(value = "countingCircleId") String countingCircleId,
      @JsonProperty(value = "countingCircleName") String countingCircleName,
      @JsonProperty(value = "numberOfVoters") long numberOfVoters) {
    this.printerName = requireNonNull(printerName, "A printer name must be specified");
    this.countingCircleId = countingCircleId;
    this.countingCircleName = countingCircleName;
    this.numberOfVoters = numberOfVoters;
  }

  public String getPrinterName() {
    return printerName;
  }

  public String getCountingCircleId() {
    return countingCircleId;
  }

  public String getCountingCircleName() {
    return countingCircleName;
  }

  public long getNumberOfVoters() {
    return numberOfVoters;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VotersCreationStatisticsVo that = (VotersCreationStatisticsVo) o;
    return Objects.equals(countingCircleId, that.countingCircleId) &&
           numberOfVoters == that.numberOfVoters &&
           Objects.equals(printerName, that.printerName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(printerName, countingCircleId, numberOfVoters);
  }
}
