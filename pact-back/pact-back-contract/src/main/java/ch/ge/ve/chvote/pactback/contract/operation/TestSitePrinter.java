/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import javax.validation.constraints.NotNull;

/**
 * A DTO containing the configuration of a test printer.
 */
public class TestSitePrinter {
  @NotNull
  private MunicipalityVo municipality;

  @NotNull
  private PrinterConfigurationVo printer;

  public MunicipalityVo getMunicipality() {
    return municipality;
  }

  public void setMunicipality(MunicipalityVo municipality) {
    this.municipality = municipality;
  }

  public PrinterConfigurationVo getPrinter() {
    return printer;
  }

  public void setPrinter(PrinterConfigurationVo printer) {
    this.printer = printer;
  }
}
