/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation.status;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;

/**
 * A DTO containing the state of the voting period for a deployed protocol instance.
 */
public class VotingPeriodStatusVo extends AbstractStatusVo {

  private final State  state;
  private final String configurationPageUrl;
  private final String rejectionReason;

  /**
   * Create a new voting period status DTO.
   *
   * @param lastChangeUser       the user that has modified the status for the last time.
   * @param lastChangeDate       the date of the last status modification.
   * @param state                State of the voting period.
   * @param rejectionReason      the reason of the rejection of the votign period.
   * @param configurationPageUrl PACT frontend URL of the deployment page for the current configuration.
   */
  @JsonCreator
  public VotingPeriodStatusVo(
      @JsonProperty("lastChangeUser") String lastChangeUser,
      @JsonProperty("lastChangeDate") LocalDateTime lastChangeDate,
      @JsonProperty("state") State state,
      @JsonProperty("rejectionReason") String rejectionReason,
      @JsonProperty("configurationPageUrl") String configurationPageUrl) {
    super(lastChangeUser, lastChangeDate);
    this.state = state;
    this.rejectionReason = rejectionReason;
    this.configurationPageUrl = configurationPageUrl;
  }

  public State getState() {
    return state;
  }

  public String getConfigurationPageUrl() {
    return configurationPageUrl;
  }

  public String getRejectionReason() {
    return rejectionReason;
  }

  /**
   * An enumeration with all the possible states of a voting period.
   */
  public enum State {
    /**
     * The voting period has been validated on the BO and sent to the PACT.
     */
    AVAILABLE_FOR_INITIALIZATION,
    /**
     * The initialization of the voting period has been requested on the PACT.
     * <p>
     * The voting period initialization action should exist and should be in state PENDING.
     * </p>
     */
    INITIALIZATION_REQUESTED,
    /**
     * The initialization of the voting period has been approved on the PACT. It is currently being initialized.
     * <p>
     * The voting period initialization action should exist and should be in state APPROVED.
     * </p>
     */
    INITIALIZATION_IN_PROGRESS,
    /**
     * The initialization of the voting period has been rejected on the PACT.
     * <p>
     * The voting period initialization action should exist and should be in state REJECTED.
     * </p>
     */
    INITIALIZATION_REJECTED,
    /**
     * The initialization of the voting period has been approved on the PACT, but later on it failed, typically for some
     * technical reason.
     */
    INITIALIZATION_FAILED,
    /**
     * The initialization of the voting period was successfully carried out on the PACT.
     */
    INITIALIZED,
    /**
     * The voting period is closed.
     */
    CLOSED
  }
}
