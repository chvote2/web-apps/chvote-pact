/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation.status;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A DTO containing the state of the tally archive creation for a deployed protocol instance.
 */
public class TallyArchiveStatusVo {
  private final State  state;
  private final String tallyArchiveLocation;

  /**
   * Initializes a tally archive status DTO.
   *
   * @param state                the state of the tally archive creation process.
   * @param tallyArchiveLocation the relative location of the generated tally archive or null if it's not yet available.
   */
  public TallyArchiveStatusVo(
      @JsonProperty("state") State state,
      @JsonProperty("tallyArchiveLocation") String tallyArchiveLocation) {
    this.state = state;
    this.tallyArchiveLocation = tallyArchiveLocation;
  }

  public State getState() {
    return state;
  }

  public String getTallyArchiveLocation() {
    return tallyArchiveLocation;
  }

  /**
   * The list of possible states of the tally archive creation process.
   */
  public enum State {
    /**
     * The creation of the tally archive has not been requested and the protocol is still in an earlier stage.
     */
    NOT_REQUESTED,
    /**
     * The tally archive can't be created because there are not enough votes cast to initialize the tally process.
     */
    NOT_ENOUGH_VOTES_CAST,
    /**
     * The creation of the tally archive has started and is currently in progress.
     */
    CREATION_IN_PROGRESS,
    /**
     * The creation of the tally archive has failed for some technical reason.
     */
    CREATION_FAILED,
    /**
     * The tally archive has been created.
     */
    CREATED
  }
}
