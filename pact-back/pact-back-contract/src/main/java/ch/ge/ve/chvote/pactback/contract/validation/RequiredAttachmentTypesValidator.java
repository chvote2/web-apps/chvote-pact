/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.validation;

import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * A validator implementation for {@link RequiredAttachmentTypes} constraint. Checks that the given list of
 * {@link AttachmentFileEntryVo} contains at least one element of each type set by
 * {@link RequiredAttachmentTypes#value()}}.
 */
public class RequiredAttachmentTypesValidator implements ConstraintValidator<RequiredAttachmentTypes,
    List<AttachmentFileEntryVo>> {
  private Set<AttachmentFileEntryVo.AttachmentType> mustHaveTypes;

  @Override
  public void initialize(RequiredAttachmentTypes constraintAnnotation) {
    mustHaveTypes = EnumSet.copyOf(Arrays.asList(constraintAnnotation.value()));
  }

  @Override
  public boolean isValid(List<AttachmentFileEntryVo> attachments, ConstraintValidatorContext
      constraintValidatorContext) {
    //null values are valid
    if (attachments == null) {
      return true;
    }

    return mustHaveTypes.stream()
                        .filter(type -> attachments.stream()
                                                   .map(AttachmentFileEntryVo::getAttachmentType)
                                                   .filter(type::equals).count() > 0)
                        .count() == mustHaveTypes.size();
  }
}