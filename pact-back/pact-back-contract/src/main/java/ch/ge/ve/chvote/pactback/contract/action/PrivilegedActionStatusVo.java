/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.action;

/**
 * A DTO for reporting privileged action status. {@link #getRejectionReason()}} will return null if the {@link
 * ActionStatus} is other than {@link ActionStatus#REJECTED}.
 */
public class PrivilegedActionStatusVo {
  private final ActionStatus status;
  private final String       rejectionReason;

  /**
   * Creates a new privileged action status value object for the given status and rejection reason.
   *
   * @param status          the {@link ActionStatus}.
   * @param rejectionReason the rejection reason if the status was {@link ActionStatus#REJECTED}.
   */
  public PrivilegedActionStatusVo(ActionStatus status, String rejectionReason) {
    this.status = status;
    this.rejectionReason = rejectionReason;
  }

  /**
   * Creates a new privileged action status value object without a rejection reason.
   *
   * @param status the {@link ActionStatus}.
   */
  public PrivilegedActionStatusVo(ActionStatus status) {
    this.status = status;
    this.rejectionReason = null;
  }

  public ActionStatus getStatus() {
    return status;
  }

  public String getRejectionReason() {
    return rejectionReason;
  }

  /**
   * An enum representing the different states of a privileged action.
   */
  public enum ActionStatus {
    /**
     * The action has not yet been created.
     */
    NOT_CREATED,
    /**
     * The actions is pending of approbation.
     */
    PENDING,
    /**
     * The action was approved.
     */
    APPROVED,
    /**
     * The action was rejected.
     */
    REJECTED
  }

}
