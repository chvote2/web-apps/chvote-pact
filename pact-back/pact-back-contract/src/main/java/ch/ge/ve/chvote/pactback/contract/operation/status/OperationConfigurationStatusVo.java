/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation.status;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A DTO containing the status of an operation in the test and production environments.
 */
public class OperationConfigurationStatusVo {

  private final InTestConfigurationStatusVo inTest;
  private final DeployedConfigurationStatusVo deployed;

  /**
   * Create a new operation configuration status DTO.
   *
   * @param inTest   the deployment configuration status in the Test environment
   * @param deployed the deployment configuration status in the Production environment
   */
  @JsonCreator
  public OperationConfigurationStatusVo(
      @JsonProperty("inTest") InTestConfigurationStatusVo inTest,
      @JsonProperty("deployed") DeployedConfigurationStatusVo deployed) {
    this.inTest = inTest;
    this.deployed = deployed;
  }

  public InTestConfigurationStatusVo getInTest() {
    return inTest;
  }

  public DeployedConfigurationStatusVo getDeployed() {
    return deployed;
  }

}
