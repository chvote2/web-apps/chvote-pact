/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import ch.ge.ve.chvote.pactback.contract.validation.AttachmentType;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * A DTO containing the metadata of a ballot documentation PDF file.
 */
public class BallotDocumentationVo {
  /**
   * Pdf file of the documentation
   */
  @NotNull
  @AttachmentType(AttachmentFileEntryVo.AttachmentType.BALLOT_DOCUMENTATION)
  private AttachmentFileEntryVo documentation;

  /**
   * Localized label
   */
  @NotNull
  @Pattern(regexp = "[\\p{L}\\d \\-',()/._]+")
  @Size(min = 1, max = 200)
  private String label;

  /**
   * Related Ballot
   */
  @NotNull
  @Pattern(regexp = "[\\p{L}\\d \\-',()/._]+")
  @Size(min = 1, max = 50)
  private String ballot;

  public AttachmentFileEntryVo getDocumentation() {
    return documentation;
  }

  public void setDocumentation(AttachmentFileEntryVo documentation) {
    this.documentation = documentation;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getBallot() {
    return ballot;
  }

  public void setBallot(String ballot) {
    this.ballot = ballot;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BallotDocumentationVo that = (BallotDocumentationVo) o;
    return Objects.equals(documentation.getZipFileName(), that.documentation.getZipFileName()) &&
           Objects.equals(label, that.label) &&
           Objects.equals(ballot, that.ballot);
  }

  @Override
  public int hashCode() {

    return Objects.hash(documentation.getZipFileName(), label, ballot);
  }
}
