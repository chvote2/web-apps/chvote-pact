/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation.status;

import java.time.LocalDateTime;

/**
 * A base DTO class for statuses.
 */
abstract class AbstractStatusVo {
  private final String        lastChangeUser;
  private final LocalDateTime lastChangeDate;

  /**
   * Default constructor for a base status object.
   *
   * @param lastChangeUser the user that has modified the status for the last time.
   * @param lastChangeDate the date of the last status modification.
   */
  AbstractStatusVo(String lastChangeUser, LocalDateTime lastChangeDate) {
    this.lastChangeUser = lastChangeUser;
    this.lastChangeDate = lastChangeDate;
  }

  /**
   * @return the user that has modified the status for the last time.
   */
  public String getLastChangeUser() {
    return lastChangeUser;
  }

  /**
   * @return the date of the last status modification.
   */
  public LocalDateTime getLastChangeDate() {
    return lastChangeDate;
  }

}
