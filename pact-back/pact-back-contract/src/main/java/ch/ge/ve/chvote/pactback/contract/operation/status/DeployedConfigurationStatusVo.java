/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation.status;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;

/**
 * A DTO containing the state of an operation configuration in Production.
 */
public class DeployedConfigurationStatusVo extends AbstractStatusVo {
  private final String voteReceiverUrl;

  /**
   * Create a new deployed configuration status DTO.
   *
   * @param lastChangeUser the user that has modified this configuration for the last time.
   * @param lastChangeDate the last status' date of this configuration.
   */
  @JsonCreator
  public DeployedConfigurationStatusVo(
      @JsonProperty("lastChangeUser") String lastChangeUser,
      @JsonProperty("lastChangeDate") LocalDateTime lastChangeDate,
      @JsonProperty("voteReceiverUrl") String voteReceiverUrl) {
    super(lastChangeUser, lastChangeDate);
    this.voteReceiverUrl = voteReceiverUrl;
  }

  public String getVoteReceiverUrl() {
    return voteReceiverUrl;
  }
}
