/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;

/**
 * A DTO containing the metadata of an incoming eCH-0045 voters register file.
 */
public class RegisterFileEntryVo {

  @NotNull
  private String zipFileName;

  private LocalDateTime importDateTime;

  private Long externalIdentifier;

  public String getZipFileName() {
    return zipFileName;
  }

  public void setZipFileName(String zipFileName) {
    this.zipFileName = zipFileName;
  }

  public LocalDateTime getImportDateTime() {
    return importDateTime;
  }

  public void setImportDateTime(LocalDateTime importDateTime) {
    this.importDateTime = importDateTime;
  }

  public Long getExternalIdentifier() {
    return externalIdentifier;
  }

  public void setExternalIdentifier(Long externalIdentifier) {
    this.externalIdentifier = externalIdentifier;
  }
}
