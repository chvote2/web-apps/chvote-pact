/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.validation;

import java.util.regex.Matcher;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Pattern;

/**
 * A validator implementation for {@link Pattern} constraint. Checks that all the elements in the given {@link Iterable}
 * match the specified regular expression in {@link Pattern#regexp()}.
 */
public class IterablePatternValidator implements ConstraintValidator<Pattern, Iterable<? extends CharSequence>> {
  private java.util.regex.Pattern pattern;

  @Override
  public void initialize(Pattern parameters) {
    int intFlag = Stream.of(parameters.flags())
                        .reduce(0,
                                (integer, flag) -> integer | flag.getValue(),
                                (integer, integer2) -> integer | integer2);

    pattern = java.util.regex.Pattern.compile(parameters.regexp(), intFlag);
  }

  @Override
  public boolean isValid(Iterable<? extends CharSequence> value,
                         ConstraintValidatorContext constraintValidatorContext) {
    if (value != null) {
      return StreamSupport.stream(value.spliterator(), false)
                          .map(pattern::matcher)
                          .allMatch(Matcher::matches);
    } else {
      return true;
    }
  }
}
