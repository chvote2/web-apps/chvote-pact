/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * A DTO containing metadata of an attachment.
 */
public class AttachmentFileEntryVo {
  @NotNull
  @Pattern(regexp = "[\\p{L}\\d \\-',()/._]+")
  @Size(min = 5, max = 100)
  private String zipFileName;

  @NotNull
  private AttachmentType attachmentType;

  @NotNull
  @Past
  private LocalDateTime importDateTime;

  /**
   * Allows to identify if two files correspond to the same original file.
   */
  @NotNull
  private Long externalIdentifier;

  public Long getExternalIdentifier() {
    return externalIdentifier;
  }

  public void setExternalIdentifier(Long externalIdentifier) {
    this.externalIdentifier = externalIdentifier;
  }

  private String language;

  public String getZipFileName() {
    return zipFileName;
  }

  public void setZipFileName(String zipFileName) {
    this.zipFileName = zipFileName;
  }

  public AttachmentType getAttachmentType() {
    return attachmentType;
  }

  public void setAttachmentType(AttachmentType attachmentType) {
    this.attachmentType = attachmentType;
  }

  public LocalDateTime getImportDateTime() {
    return importDateTime;
  }

  public void setImportDateTime(LocalDateTime importDateTime) {
    this.importDateTime = importDateTime;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  /**
   * An enumeration of all the accepted attachment types.
   */
  public enum AttachmentType {
    /**
     * An eCH-0159 attachment.
     */
    REPOSITORY_VOTATION,
    /**
     * An eCH-0157 attachment.
     */
    REPOSITORY_ELECTION,
    /**
     * An attachment that describes an area of influence.
     */
    DOMAIN_OF_INFLUENCE,
    /**
     * A PDF containing the frequently ask questions documentation.
     */
    DOCUMENT_FAQ,
    /**
     * A PDF containing the terms and conditions documentation.
     */
    DOCUMENT_TERMS,
    /**
     * Operation certificate file.
     */
    DOCUMENT_CERTIFICATE,
    /**
     * An eCH-0045 attachment.
     */
    REGISTER,
    /**
     * A PDF containing the documentation of a specific ballot.
     */
    BALLOT_DOCUMENTATION,
    /**
     * An attachment with the translations for the texts appearing in the voting site.
     */
    TRANSLATIONS,
    /**
     * A PDF containing the documentation of a highlighted question.
     */
    HIGHLIGHTED_QUESTION,
    /**
     * Election officer's public key.
     */
    ELECTION_OFFICER_KEY
  }

}
