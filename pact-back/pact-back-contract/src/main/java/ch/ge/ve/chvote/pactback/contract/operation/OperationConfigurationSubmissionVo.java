/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import ch.ge.ve.chvote.pactback.contract.validation.AtLeastOneAttachmentType;
import ch.ge.ve.chvote.pactback.contract.validation.RequiredAttachmentTypes;
import ch.ge.ve.chvote.pactback.contract.validation.RequiredMilestones;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * A DTO for incoming operation configuration requests.
 */
public class OperationConfigurationSubmissionVo {
  @NotNull
  @Pattern(regexp = "\\w+")
  @Size(min = 1, max = 50)
  private String user;

  @NotNull
  @Pattern(regexp = "\\d+")
  @Size(min = 1, max = 20)
  private String clientId;

  @NotNull
  @Pattern(regexp = "[\\p{L}\\d \\-',()/._]+")
  @Size(min = 1, max = 200)
  private String operationName;

  @NotNull
  @Pattern(regexp = "[\\p{L}\\d \\-',()/._]+")
  @Size(min = 1, max = 50)
  private String operationLabel;

  @NotNull
  @Future
  private LocalDateTime operationDate;

  @NotNull
  @Min(0)
  @Max(50)
  private Integer gracePeriod;

  @NotNull
  @Pattern(regexp = "[A-Z]{2}")
  @Size(max = 2, min = 2)
  private String canton;

  @NotNull
  private boolean groupVotation;

  @NotNull
  private TestSitePrinter testSitePrinter;

  @NotNull
  @Size(min = 3)
  @RequiredAttachmentTypes(
      {
          AttachmentFileEntryVo.AttachmentType.DOMAIN_OF_INFLUENCE,
          AttachmentFileEntryVo.AttachmentType.REGISTER,
          AttachmentFileEntryVo.AttachmentType.TRANSLATIONS
      })
  @AtLeastOneAttachmentType(
      {
          AttachmentFileEntryVo.AttachmentType.REPOSITORY_ELECTION,
          AttachmentFileEntryVo.AttachmentType.REPOSITORY_VOTATION
      }
  )
  private List<AttachmentFileEntryVo> attachments;

  /**
   * Election site configurations by ballot.
   */
  @NotNull
  @Valid
  private Set<ElectionSiteConfigurationVo> electionSiteConfigurations;

  /**
   * Questions to highlight on the web site.
   */
  @NotNull
  @Valid
  private Set<HighlightedQuestionsVo> highlightedQuestions;

  /**
   * Documentations relative to ballot
   */
  @NotNull
  @Valid
  private Set<BallotDocumentationVo> ballotDocumentations;

  @NotNull
  @Size(min = 2)
  @RequiredMilestones(
      {
          Milestone.SITE_OPEN,
          Milestone.SITE_CLOSE
      })
  private Map<Milestone, LocalDateTime> milestones;

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getOperationName() {
    return operationName;
  }

  public void setOperationName(String operationName) {
    this.operationName = operationName;
  }

  public String getOperationLabel() {
    return operationLabel;
  }

  public void setOperationLabel(String operationLabel) {
    this.operationLabel = operationLabel;
  }

  public LocalDateTime getOperationDate() {
    return operationDate;
  }

  public void setOperationDate(LocalDateTime operationDate) {
    this.operationDate = operationDate;
  }

  public Integer getGracePeriod() {
    return gracePeriod;
  }

  public void setGracePeriod(Integer gracePeriod) {
    this.gracePeriod = gracePeriod;
  }

  public String getCanton() {
    return canton;
  }

  public void setCanton(String canton) {
    this.canton = canton;
  }

  public boolean isGroupVotation() {
    return groupVotation;
  }

  public void setGroupVotation(boolean groupVotation) {
    this.groupVotation = groupVotation;
  }

  public Map<Milestone, LocalDateTime> getMilestones() {
    return milestones;
  }

  public void setMilestones(Map<Milestone, LocalDateTime> milestones) {
    this.milestones = milestones;
  }

  public TestSitePrinter getTestSitePrinter() {
    return testSitePrinter;
  }

  public void setTestSitePrinter(TestSitePrinter testSitePrinter) {
    this.testSitePrinter = testSitePrinter;
  }

  public List<AttachmentFileEntryVo> getAttachments() {
    return attachments;
  }

  public void setAttachments(List<AttachmentFileEntryVo> attachments) {
    this.attachments = attachments;
  }

  public Set<ElectionSiteConfigurationVo> getElectionSiteConfigurations() {
    return electionSiteConfigurations;
  }

  public void setElectionSiteConfigurations(Set<ElectionSiteConfigurationVo> electionSiteConfigurations) {
    this.electionSiteConfigurations = electionSiteConfigurations;
  }

  public Set<HighlightedQuestionsVo> getHighlightedQuestions() {
    return highlightedQuestions;
  }

  public void setHighlightedQuestions(Set<HighlightedQuestionsVo> highlightedQuestions) {
    this.highlightedQuestions = highlightedQuestions;
  }

  public Set<BallotDocumentationVo> getBallotDocumentations() {
    return ballotDocumentations;
  }

  public void setBallotDocumentations(Set<BallotDocumentationVo> ballotDocumentations) {
    this.ballotDocumentations = ballotDocumentations;
  }

  /**
   * An enumeration representing all the milestone of an operation.
   */
  public enum Milestone {
    /**
     * The site validation date.
     */
    SITE_VALIDATION,
    /**
     * The certification date.
     */
    CERTIFICATION,
    /**
     * The printer files date.
     */
    PRINTER_FILES,
    /**
     * The ballot box initialization date.
     */
    BALLOT_BOX_INIT,
    /**
     * The voting stie opening date.
     */
    SITE_OPEN,
    /**
     * The voting site closing date.
     */
    SITE_CLOSE,
    /**
     * The ballot box decryption date.
     */
    BALLOT_BOX_DECRYPT,
    /**
     * The result validation date.
     */
    RESULT_VALIDATION,
    /**
     * The data destruction date.
     */
    DATA_DESTRUCTION
  }

}
