/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation.status;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * A DTO containing the state of the voting materials for the deployed protocol instance.
 */
public class VotingMaterialsStatusVo extends AbstractStatusVo {

  private final State               state;
  private final String              rejectionReason;
  private final String              configurationPageUrl;
  private final String              invalidationPageUrl;
  private final Map<String, String> votingCardLocations;

  private final List<ProtocolStageProgressVo<VotingMaterialCreationStage>> protocolProgress;

  /**
   * Initializes a voting material status DTO.
   *
   * @param lastChangeUser       the user that has modified this configuration for the last time.
   * @param lastChangeDate       the last status' date of this configuration.
   * @param state                the state of the voting materials configuration
   * @param rejectionReason      a comment regarding the state, relevant only when the state is {@link
   *                             VotingMaterialsStatusVo.State#CREATION_REJECTED}.
   * @param configurationPageUrl The PACT frontend URL of the voting materials creation page for the current operation,
   *                             relative to the PACT application context URL
   * @param invalidationPageUrl  The PACT frontend URL of the voting materials invalidation page for the current
   *                             operation, relative to the PACT application context URL
   * @param votingCardLocations  A map containing the relative location of the printer archive in the shared volume for
   *                             each configured printer. The key of the map is the business id of the printer, the
   *                             value is the path to the printer archive.
   * @param protocolProgress     a list with the completion ratio of each stage of the protocol instance.
   */
  @JsonCreator
  public VotingMaterialsStatusVo(
      @JsonProperty("lastChangeUser") String lastChangeUser,
      @JsonProperty("lastChangeDate") LocalDateTime lastChangeDate,
      @JsonProperty("state") State state,
      @JsonProperty("rejectionReason") String rejectionReason,
      @JsonProperty("configurationPageUrl") String configurationPageUrl,
      @JsonProperty("invalidationPageUrl") String invalidationPageUrl,
      @JsonProperty("votingCardLocations") Map<String, String> votingCardLocations,
      @JsonProperty("protocolProgress") List<ProtocolStageProgressVo<VotingMaterialCreationStage>> protocolProgress) {
    super(lastChangeUser, lastChangeDate);
    this.state = state;
    this.rejectionReason = rejectionReason;
    this.configurationPageUrl = configurationPageUrl;
    this.invalidationPageUrl = invalidationPageUrl;
    this.votingCardLocations = votingCardLocations;
    this.protocolProgress = protocolProgress;
  }

  public State getState() {
    return state;
  }

  public String getRejectionReason() {
    return rejectionReason;
  }

  public String getConfigurationPageUrl() {
    return configurationPageUrl;
  }

  public String getInvalidationPageUrl() {
    return invalidationPageUrl;
  }

  public Map<String, String> getVotingCardLocations() {
    return votingCardLocations;
  }

  public List<ProtocolStageProgressVo<VotingMaterialCreationStage>> getProtocolProgress() {
    return protocolProgress;
  }

  /**
   * The list of possible states of the voting materials.
   * <p>
   * Reference: the UML state machine diagram in directory chvote-pact\docs\design. Note: the initial state DRAFT is not
   * listed here as it is not relevant to the PACT.
   */
  public enum State {

    /**
     * The configuration draft has been validated on the BO and sent to the PACT.
     */
    AVAILABLE_FOR_CREATION,

    /**
     * The creation of the voting materials has been requested on the PACT.
     * <p>
     * The voting materials creation action should exist and should be in state PENDING.
     */
    CREATION_REQUESTED,

    /**
     * The creation of the voting materials has been rejected on the PACT.
     * <p>
     * The voting materials creation action should exist and should be in state REJECTED.
     */
    CREATION_REJECTED,

    /**
     * The creation of the voting materials has been approved on the PACT. It is currently being created.
     * <p>
     * The voting materials creation action should exist and should be in state APPROVED.
     */
    CREATION_IN_PROGRESS,

    /**
     * The creation of the voting materials has been approved on the PACT, but later on it failed, typically for some
     * technical reason.
     */
    CREATION_FAILED,

    /**
     * The creation of the voting materials was successfully carried out on the PACT.
     */
    CREATED,

    /**
     * The creation of the voting materials was successfully carried out on the PACT and validated on BO.
     */
    VALIDATED,

    /**
     * An invalidation of the created voting materials has been requested on the PACT.
     */
    INVALIDATION_REQUESTED,

    /**
     * An invalidation of the created voting materials has been rejected on the PACT.
     */
    INVALIDATION_REJECTED,

    /**
     * The created voting materials have been invalidated
     */
    INVALIDATED
  }
}
