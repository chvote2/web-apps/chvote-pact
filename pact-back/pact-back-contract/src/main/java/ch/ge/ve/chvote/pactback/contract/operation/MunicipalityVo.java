/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import java.util.Objects;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * A DTO containing the necessary metadata of a municipality.
 */
public class MunicipalityVo {
  @NotNull
  @Min(1)
  @Max(9999)
  private int ofsId;

  @NotNull
  @Pattern(regexp = "[\\p{L} \\-',()/.]+")
  @Size(min = 1, max = 40)
  private String name;

  public int getOfsId() {
    return ofsId;
  }

  public void setOfsId(int ofsId) {
    this.ofsId = ofsId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if  (this == o){
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MunicipalityVo that = (MunicipalityVo) o;
    return ofsId == that.ofsId;
  }

  @Override
  public int hashCode() {

    return Objects.hash(ofsId);
  }
}
