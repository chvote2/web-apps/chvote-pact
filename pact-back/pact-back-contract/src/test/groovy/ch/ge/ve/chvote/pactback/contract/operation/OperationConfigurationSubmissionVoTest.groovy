/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation

import java.time.LocalDateTime
import javax.validation.Validation
import javax.validation.Validator
import spock.lang.Specification

class OperationConfigurationSubmissionVoTest extends Specification {
  private Validator validator

  def setup() {
    def factory = Validation.buildDefaultValidatorFactory()
    validator = factory.getValidator()
  }


  def "should not signal any violation for a valid OperationSubmissionVo"() {
    given: "A valid DOMAIN_OF_INFLUENCE attachment"
    def doiEntry = new AttachmentFileEntryVo()
    doiEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOMAIN_OF_INFLUENCE)
    doiEntry.setImportDateTime(LocalDateTime.now())
    doiEntry.setZipFileName("doi.zip")

    and: "a valid DOCUMENT_FAQ attachment in german"
    def faqDEEntry = new AttachmentFileEntryVo()
    faqDEEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOCUMENT_FAQ)
    faqDEEntry.setLanguage("DE")
    faqDEEntry.setImportDateTime(LocalDateTime.now())
    faqDEEntry.setZipFileName("op_doc_faq.de.zip")

    and: "a valid DOCUMENT_FAQ attachment in french"
    def faqFREntry = new AttachmentFileEntryVo()
    faqFREntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOCUMENT_FAQ)
    faqFREntry.setLanguage("FR")
    faqFREntry.setImportDateTime(LocalDateTime.now())
    faqFREntry.setZipFileName("op_doc_faq.fr.zip")

    and: "a valid REPOSITORY_VOTATION attachment"
    def orepEntry = new AttachmentFileEntryVo()
    orepEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REPOSITORY_VOTATION)
    orepEntry.setImportDateTime(LocalDateTime.now())
    orepEntry.setZipFileName("orep.zip")

    and: "a valid REPOSITORY_ELECTION attachment"
    def orepEntry2 = new AttachmentFileEntryVo()
    orepEntry2.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REPOSITORY_ELECTION)
    orepEntry2.setImportDateTime(LocalDateTime.now())
    orepEntry2.setZipFileName("orep2.zip")

    and: "a valid REGISTER attachment"
    def registerEntry = new AttachmentFileEntryVo()
    registerEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REGISTER)
    registerEntry.setImportDateTime(LocalDateTime.now())
    registerEntry.setZipFileName("register.zip")

    and: "a valid TRANSLATIONS attachment"
    def translationsEntry = new AttachmentFileEntryVo()
    translationsEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.TRANSLATIONS)
    translationsEntry.setImportDateTime(LocalDateTime.now())
    translationsEntry.setZipFileName("translations.zip")

    and: "a valid BALLOT_DOCUMENTATION attachment"
    def ballotDocEntry = new AttachmentFileEntryVo()
    ballotDocEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.BALLOT_DOCUMENTATION
    ballotDocEntry.importDateTime = LocalDateTime.now()
    ballotDocEntry.zipFileName = "ballotDoc.zip"
    def ballotDocumentation = new BallotDocumentationVo()
    ballotDocumentation.ballot = "ballot"
    ballotDocumentation.label = "label"
    ballotDocumentation.documentation = ballotDocEntry

    and: "a valid HIGHLIGHTED_QUESTION attachment"
    def highlightedQuestionEntry = new AttachmentFileEntryVo()
    highlightedQuestionEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.HIGHLIGHTED_QUESTION
    highlightedQuestionEntry.importDateTime = LocalDateTime.now()
    highlightedQuestionEntry.zipFileName = "highlightedQuestion.zip"
    def highlightedQuestion = new HighlightedQuestionsVo()
    highlightedQuestion.answerFile = highlightedQuestionEntry
    highlightedQuestion.question = "Question ?"

    and: "a valid election site configuration"
    def electionSiteConfiguration = new ElectionSiteConfigurationVo()
    electionSiteConfiguration.ballot = "ballot"
    electionSiteConfiguration.candidateInformationDisplayModel = "model"
    electionSiteConfiguration.displayedColumnsOnVerificationTable = ["col", "col"] as String[]
    electionSiteConfiguration.columnsOrderOnVerificationTable = ["col"] as String[]
    electionSiteConfiguration.displayEmptyPosition = true

    and: "a valid OperationConfigurationSubmissionVo"
    def submissionVo = new OperationConfigurationSubmissionVo()
    submissionVo.user = "Fred"
    submissionVo.gracePeriod = 50
    submissionVo.canton = "GE"
    submissionVo.groupVotation = true
    submissionVo.operationName = "201706VP"
    submissionVo.operationLabel = "201706VP"
    submissionVo.operationDate = LocalDateTime.MAX
    submissionVo.clientId = "1000"
    submissionVo.milestones = [:]
    submissionVo.testSitePrinter = new TestSitePrinter()
    submissionVo.testSitePrinter.municipality = new MunicipalityVo()
    submissionVo.testSitePrinter.municipality.ofsId = 6601
    submissionVo.testSitePrinter.municipality.name = "Municipality"
    submissionVo.testSitePrinter.printer = new PrinterConfigurationVo()
    submissionVo.testSitePrinter.printer.id = "printer-0"
    submissionVo.testSitePrinter.printer.name = "Printer 0"
    submissionVo.testSitePrinter.printer.publicKey = new BigInteger("1234567890123456789012345678901234567890abcdef", 16)
    submissionVo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_OPEN] = LocalDateTime.MAX
    submissionVo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_CLOSE] = LocalDateTime.MAX
    submissionVo.attachments = Arrays.asList(doiEntry, faqDEEntry, faqFREntry, orepEntry, registerEntry,
            highlightedQuestionEntry, ballotDocEntry, orepEntry2, translationsEntry)

    submissionVo.ballotDocumentations = [ballotDocumentation] as Set<BallotDocumentationVo>
    submissionVo.highlightedQuestions = [highlightedQuestion] as Set<HighlightedQuestionsVo>
    submissionVo.electionSiteConfigurations = [electionSiteConfiguration] as Set<ElectionSiteConfigurationVo>

    when:
    def violations = validator.validate(submissionVo)

    then:
    violations.empty
  }

  def "should not validate a OperationSubmissionVo without attachments"() {
    given: "a valid BALLOT_DOCUMENTATION attachment"
    def ballotDocEntry = new AttachmentFileEntryVo()
    ballotDocEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.BALLOT_DOCUMENTATION
    ballotDocEntry.importDateTime = LocalDateTime.now()
    ballotDocEntry.zipFileName = "ballotDoc.zip"
    def ballotDocumentation = new BallotDocumentationVo()
    ballotDocumentation.ballot = "ballot"
    ballotDocumentation.label = "label"
    ballotDocumentation.documentation = ballotDocEntry

    and: "a valid HIGHLIGHTED_QUESTION attachment"
    def highlightedQuestionEntry = new AttachmentFileEntryVo()
    highlightedQuestionEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.HIGHLIGHTED_QUESTION
    highlightedQuestionEntry.importDateTime = LocalDateTime.now()
    highlightedQuestionEntry.zipFileName = "highlightedQuestion.zip"
    def highlightedQuestion = new HighlightedQuestionsVo()
    highlightedQuestion.answerFile = highlightedQuestionEntry
    highlightedQuestion.question = "Question ?"

    and: "a valid election site configuration"
    def electionSiteConfiguration = new ElectionSiteConfigurationVo()
    electionSiteConfiguration.ballot = "ballot"
    electionSiteConfiguration.candidateInformationDisplayModel = "model"
    electionSiteConfiguration.displayedColumnsOnVerificationTable = ["col", "col"] as String[]
    electionSiteConfiguration.columnsOrderOnVerificationTable = ["col"] as String[]
    electionSiteConfiguration.displayEmptyPosition = true

    and: "a OperationConfigurationSubmissionVo missing all its attachments"
    def submissionVo = new OperationConfigurationSubmissionVo()
    submissionVo.user = "Fred"
    submissionVo.gracePeriod = 50
    submissionVo.canton = "GE"
    submissionVo.groupVotation = true
    submissionVo.operationName = "201706VP"
    submissionVo.operationLabel = "201706VP"
    submissionVo.operationDate = LocalDateTime.MAX
    submissionVo.clientId = "1000"
    submissionVo.milestones = [:]
    submissionVo.testSitePrinter = new TestSitePrinter()
    submissionVo.testSitePrinter.municipality = new MunicipalityVo()
    submissionVo.testSitePrinter.municipality.ofsId = 6601
    submissionVo.testSitePrinter.municipality.name = "Municipality"
    submissionVo.testSitePrinter.printer = new PrinterConfigurationVo()
    submissionVo.testSitePrinter.printer.id = "printer-0"
    submissionVo.testSitePrinter.printer.name = "Printer 0"
    submissionVo.testSitePrinter.printer.publicKey = new BigInteger("1234567890123456789012345678901234567890abcdef", 16)
    submissionVo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_OPEN] = LocalDateTime.MAX
    submissionVo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_CLOSE] = LocalDateTime.MAX

    submissionVo.ballotDocumentations = [ballotDocumentation] as Set<BallotDocumentationVo>
    submissionVo.highlightedQuestions = [highlightedQuestion] as Set<HighlightedQuestionsVo>
    submissionVo.electionSiteConfigurations = [electionSiteConfiguration] as Set<ElectionSiteConfigurationVo>

    when:
    def violations = validator.validate(submissionVo)

    then:
    violations.size() == 1
    violations[0].propertyPath.toString() == "attachments"
  }
}
