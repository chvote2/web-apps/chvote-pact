/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation

import java.time.LocalDateTime
import javax.validation.Validation
import javax.validation.Validator
import spock.lang.Specification

class BallotDocumentationVoTest extends Specification {
  private Validator validator

  def setup() {
    def factory = Validation.buildDefaultValidatorFactory()
    validator = factory.getValidator()
  }

  def "should not signal any violation for a valid BallotDocumentationVo"() {
    given:
    def ballotDocEntry = new AttachmentFileEntryVo()
    ballotDocEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.BALLOT_DOCUMENTATION
    ballotDocEntry.importDateTime = LocalDateTime.now()
    ballotDocEntry.zipFileName = "ballotDoc.zip"
    def ballotDocumentation = new BallotDocumentationVo()
    ballotDocumentation.ballot = "ballot"
    ballotDocumentation.label = "label"
    ballotDocumentation.documentation = ballotDocEntry

    when:
    def violations = validator.validate(ballotDocumentation)

    then:
    violations.empty
  }

  def "should not validate a BallotDocumentationVo with an invalid label"() {
    given:
    def ballotDocEntry = new AttachmentFileEntryVo()
    ballotDocEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.BALLOT_DOCUMENTATION
    ballotDocEntry.importDateTime = LocalDateTime.now()
    ballotDocEntry.zipFileName = "ballotDoc.zip"
    def ballotDocumentation = new BallotDocumentationVo()
    ballotDocumentation.ballot = "ballot"
    ballotDocumentation.label = "<></>"
    ballotDocumentation.documentation = ballotDocEntry

    when:
    def violations = validator.validate(ballotDocumentation)

    then:
    violations.size() == 1
    violations[0].propertyPath.toString() == "label"
  }

  def "should not validate a BallotDocumentationVo that's missing the attachment"() {
    given:
    def ballotDocumentation = new BallotDocumentationVo()
    ballotDocumentation.ballot = "ballot"
    ballotDocumentation.label = "label"

    when:
    def violations = validator.validate(ballotDocumentation)

    then:
    violations.size() == 1
    violations[0].propertyPath.toString() == "documentation"
  }

  def "should not validate a BallotDocumentationVo with the wrong attachment type"() {
    given:
    def ballotDocEntry = new AttachmentFileEntryVo()
    ballotDocEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.REPOSITORY_ELECTION
    ballotDocEntry.importDateTime = LocalDateTime.now()
    ballotDocEntry.zipFileName = "ballotDoc.zip"
    def ballotDocumentation = new BallotDocumentationVo()
    ballotDocumentation.ballot = "ballot"
    ballotDocumentation.label = "label"
    ballotDocumentation.documentation = ballotDocEntry

    when:
    def violations = validator.validate(ballotDocumentation)

    then:
    violations.size() == 1
    violations[0].propertyPath.toString() == "documentation"
  }
}