/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.user;

import ch.ge.ve.chvote.pactback.service.user.UserService;
import ch.ge.ve.chvote.pactback.service.user.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A controller for retrieving and modifying {@link UserVo} information.
 */
@RestController
@RequestMapping("/users")
@Api(
    value = "User",
    tags = "User")
public class UserController {
  private final UserService service;

  public UserController(UserService service) {
    this.service = service;
  }

  /**
   * Retrieve information for the {@link UserVo} of the current session.
   *
   * @param authentication the {@link Authentication} object for the current session.
   *
   * @return information for the current {@link UserVo}.
   */
  @GetMapping(value = "me", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Get current user information",
      notes = "Retrieve the current user's information.")
  @ApiResponses({
      @ApiResponse(code = 404, message = "User not found")
  })
  public UserVo getAuthenticatedUser(Authentication authentication) {
    return service.findByUsername(authentication.getName());
  }

}
