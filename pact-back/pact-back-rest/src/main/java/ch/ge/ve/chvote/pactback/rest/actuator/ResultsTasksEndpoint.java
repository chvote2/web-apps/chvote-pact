/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.actuator;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Actuator endpoint for listing the failed tally archive generation tasks.
 */
@Component
@RestControllerEndpoint(id = "results")
@Api(
    value = "Tally archive tasks",
    tags = "Tally archive tasks")
public class ResultsTasksEndpoint {

  private final ProtocolInstanceService protocolInstanceService;

  @Autowired
  public ResultsTasksEndpoint(ProtocolInstanceService protocolInstanceService) {
    this.protocolInstanceService = protocolInstanceService;
  }

  /**
   * Returns all the failed tally archive generation tasks.
   *
   * @return all the failed tally archive tasks.
   */
  @GetMapping("tasks/failed")
  @ApiOperation(
      value = "Get all failed tally archive tasks",
      notes = "Retrieve all failed tally archive tasks.")
  public List<TaskDTO> listFailedTasks() {
    return protocolInstanceService.getProtocolInstancesWhereTallyArchiveGenerationFailed().stream()
                                  .map(ResultsTasksEndpoint::mapToTaskDTO)
                                  .collect(Collectors.toList());
  }

  private static TaskDTO mapToTaskDTO(ProtocolInstance protocolInstance) {
    final String targetMode = protocolInstance.getOperation().getVotingPeriodConfiguration()
                                              .map(conf -> conf.getTarget().name())
                                              .orElse(null);

    return new TaskDTO(protocolInstance.getOperation().getClientId(),
                       protocolInstance.getEnvironment(),
                       targetMode,
                       protocolInstance.getStatus(),
                       protocolInstance.getStatusDate());
  }
}
