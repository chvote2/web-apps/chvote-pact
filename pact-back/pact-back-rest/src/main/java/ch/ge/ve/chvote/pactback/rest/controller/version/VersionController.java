/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.version;

import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A controller to retrieve the current version of the application.
 */
@RestController
@RequestMapping("/version")
@Api(
    value = "Version",
    tags = "Version")
public class VersionController {

  private final VersionVo currentVersion;

  /**
   * Creates a new {@link VersionController} with the given service.
   *
   * @param buildNumber    the current build number.
   * @param buildTimestamp the current build timestamp in UTC format.
   * @param properties     the configuration of the application.
   */
  @Autowired
  public VersionController(@Value("${chvote.pact.buildNumber}") String buildNumber,
                           @Value("${chvote.pact.buildTimestamp}") String buildTimestamp,
                           ChVoteConfigurationProperties properties) {
    this.currentVersion = new VersionVo(buildNumber, buildTimestamp, properties.getTimezone());
  }

  /**
   * Retrieve the current build number and timestamp of the application.
   *
   * @return the current build number and timestamp of te application.
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "The version of the PACT application.",
      notes = "Get the build number and timestamp.")
  public VersionVo getCurrentVersion() {
    return currentVersion;
  }
}
