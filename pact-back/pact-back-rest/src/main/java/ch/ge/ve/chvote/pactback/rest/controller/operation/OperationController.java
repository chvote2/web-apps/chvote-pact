/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.operation;

import ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.OperationConfigurationStatusVo;
import ch.ge.ve.chvote.pactback.rest.controller.operation.exception.InvalidOperationConfigurationException;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.operation.OperationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * A specialized controller for interacting with operations.
 */
@RestController
@RequestMapping("/operation")
@Api(value = "Operation", tags = "Operation")
public class OperationController {

  private final OperationService service;
  private final ObjectMapper     objectMapper;
  private final Validator        validator;

  /**
   * Creates a new operation controller.
   *
   * @param service      the operation service.
   * @param objectMapper the application default object mapper.
   * @param validator    the bean validator for incoming {@link OperationConfigurationSubmissionVo}.
   */
  @Autowired
  public OperationController(OperationService service, ObjectMapper objectMapper, Validator validator) {
    this.service = service;
    this.objectMapper = objectMapper;
    this.validator = validator;
  }

  /**
   * Retrieves the status of an operation.
   *
   * @param clientId the operation client identifier of the operation.
   *
   * @return an {@link OperationConfigurationStatusVo} for the matching operation.
   *
   * @throws EntityNotFoundException if no operation was found
   */
  @GetMapping(value = "{clientId}/configuration/status", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Get status by operation client id",
      notes = "Retrieve current operation status by operation client id.")
  @ApiResponses({
                    @ApiResponse(code = 404, message = "Operation not found")
                })
  public OperationConfigurationStatusVo getByClientId(
      @ApiParam(value = "Operation client identifier")
      @PathVariable("clientId") String clientId) {
    return service.getOperationStatusByClientId(clientId);
  }

  /**
   * Invalidates the configuration in test for the given client id, this will stop the protocol instance attached to the
   * configuration in test.
   *
   * @param clientId the operation client id that holds the configuration to invalidate.
   * @param user     a string identifying the user requesting the invalidation.
   */
  @PostMapping("{clientId}/configuration/invalidate")
  @ApiOperation(
      value = "Invalidate the test configuration by client id",
      notes = "Set the current test configuration for this operation client " +
              "id to invalidated, and remove the attached protocol instance.")
  @ApiResponses({
                    @ApiResponse(code = 404, message = "Operation not found"),
                    @ApiResponse(code = 409, message = "The configuration is in production or does not need validation")
                })
  public void invalidateConfigurationByClientId(
      @ApiParam("client ID of the operation")
      @PathVariable("clientId") String clientId,
      @RequestParam("user") String user) {
    service.invalidateConfigurationByClientId(clientId, user);
  }

  /**
   * Validates the configuration in test for the given client id. Once the configuration is validated a deployment
   * action will be available in the PACT frontend.
   *
   * @param clientId the operation client id that holds the configuration to validate.
   * @param user     a string identifying the user requesting the validation.
   */
  @PostMapping("{clientId}/configuration/validate")
  @ApiOperation(
      value = "Validate the test configuration by client id",
      notes = "Validate the test configuration for this operation client id to validated.\n" +
              "If this call is successful a production deployment action will be available in the PACT frontend.")
  @ApiResponses({
                    @ApiResponse(code = 404, message = "Operation not found"),
                    @ApiResponse(code = 409, message = "The configuration is in production or does not need validation")
                })
  public void validateConfigurationByClientId(
      @ApiParam("client ID of the operation")
      @PathVariable("clientId") String clientId,
      @RequestParam("user") String user) {
    service.validateConfigurationByClientId(clientId, user);
  }

  private OperationConfigurationSubmissionVo validateOperationSubmission(OperationConfigurationSubmissionVo vo) {
    DataBinder binder = new DataBinder(vo);
    binder.setValidator(validator);
    binder.addValidators();
    binder.validate();
    BindingResult results = binder.getBindingResult();
    if (results.hasErrors()) {
      throw new InvalidOperationConfigurationException(
          results.getAllErrors().stream().map(Object::toString).collect(Collectors.joining(", "))
      );
    }

    return vo;
  }

  /**
   * Create or update a new Operation for the given file and metadata or updates and existing one if it already exists.
   *
   * @param data      all the required attachments as ZIP files.
   * @param operation the operation metadata described by {@link OperationConfigurationSubmissionVo}.
   *
   * @throws IOException if there was a problem uploading the file.
   */
  @PostMapping(
      value = "{clientId}/configuration",
      consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Create or update an operation",
      notes = "Create or update an existing operation with the given parameters.\n" +
              "The incoming configuration will be deployed in a test environment. " +
              "In order to promote the configuration to production, it must be validated and a production " +
              "deployment request must be submitted to the PACT.\n" +
              "All the operation attachments should be sent as ZIP files in parts named data.")
  @ApiImplicitParams(
      {
          @ApiImplicitParam(
              required = true, name = "operation", value = "Operation metadata as a json string in a separate part",
              dataType = "ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo",
              paramType = "body"),
          @ApiImplicitParam(name = "clientId", value = "Operation's client id", paramType = "path", type = "string")
      })
  @ApiResponses({
                    @ApiResponse(code = 400, message = "The incoming operation data cannot be validated")
                })
  public void createOrUpdateOperation(
      @ApiParam(hidden = true)
      @RequestPart("data") MultipartFile[] data,
      @ApiParam(hidden = true)
      @RequestPart("operation") String operation)
      throws IOException {
    OperationConfigurationSubmissionVo vo = validateOperationSubmission(
        objectMapper.readValue(operation, OperationConfigurationSubmissionVo.class));
    service.createOrUpdateOperation(vo, Arrays.asList(data));
  }
}
