/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.chvote.pactback.rest.controller.operation;

import ch.ge.ve.chvote.pactback.contract.operation.VotingPeriodConfigurationSubmissionVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.operation.OperationService;
import ch.ge.ve.chvote.pactback.service.operation.period.VotingPeriodService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * A specialised controller for interacting with the Voting period configuration of an Operation.
 */
@RestController
@RequestMapping("/operation")
@Api(value = "Operation", tags = "Operation")
public class VotingPeriodConfigurationController {

  private final VotingPeriodService service;
  private final OperationService    operationService;
  private final ObjectMapper        objectMapper;

  /**
   * Creates a new {@link VotingPeriodConfigurationController} with the given services.
   *
   * @param operationService the {@link OperationService} service
   * @param service          the {@link VotingPeriodService} service
   */
  @Autowired
  public VotingPeriodConfigurationController(OperationService operationService, VotingPeriodService service,
                                             ObjectMapper objectMapper) {
    this.operationService = operationService;
    this.service = service;
    this.objectMapper = objectMapper;
  }

  /**
   * Creates the voting period configuration for an operation.
   *
   * @param clientId                 the identifier of the operation in the client's system.
   * @param electionOfficerPublicKey the election officer public key.
   * @param configuration            the voting materials metadata described by
   * {@link VotingPeriodConfigurationSubmissionVo}.
   *
   * @throws IOException If there was a problem retrieving the election officer public key.
   */
  @PostMapping(
      value = "{clientId}/voting-period/configuration",
      consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Creates the voting period configuration of an operation")
  @ApiImplicitParams(
      {
          @ApiImplicitParam(
              required = true, name = "configuration",
              value = "Voting period configuration metadata as a json string in a separate part",
              dataType = "ch.ge.ve.chvote.pactback.contract.operation.VotingPeriodConfigurationSubmissionVo",
              paramType = "body")
      })
  @ApiResponses({
      @ApiResponse(code = 404, message = "Operation not found"),
      @ApiResponse(code = 409, message = "The operation is not ready to receive the voting period")
  })
  public void submitVotingPeriodConfiguration(
      @PathVariable("clientId") String clientId,
      @ApiParam(hidden = true)
      @RequestPart("data") MultipartFile electionOfficerPublicKey,
      @ApiParam(hidden = true)
      @RequestPart("configuration") String configuration) throws IOException {
    VotingPeriodConfigurationSubmissionVo submissionVo =
        objectMapper.readValue(configuration, VotingPeriodConfigurationSubmissionVo.class);
    operationService.addVotingPeriodConfiguration(clientId, submissionVo, electionOfficerPublicKey);
  }

  /**
   * Retrieves the status of the voting period of the deployed configuration.
   *
   * @param clientId the operation identifier in the client's system
   *
   * @return an {@link VotingPeriodStatusVo} for the matching operation.
   *
   * @throws EntityNotFoundException if either the operation or the operation's voting period configuration was not
   *                                 found
   */
  @GetMapping(value = "{clientId}/voting-period/status", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Get voting period status by operation client id",
                notes = "Retrieve current voting period status by operation client id.")
  @ApiResponses({
      @ApiResponse(code = 404, message = "Voting period not found")
  })
  public VotingPeriodStatusVo getByClientId(@ApiParam(value = "Operation client identifier")
                                            @PathVariable("clientId") String clientId) {
    return service.getVotingPeriodStatusByClientId(clientId);
  }
}