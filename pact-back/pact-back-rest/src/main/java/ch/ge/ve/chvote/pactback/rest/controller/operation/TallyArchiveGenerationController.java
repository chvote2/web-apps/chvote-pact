/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.operation;

import ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.operation.tally.CannotPublishTallyArchiveException;
import ch.ge.ve.chvote.pactback.service.operation.tally.GenerationInProgressException;
import ch.ge.ve.chvote.pactback.service.operation.tally.TallyArchiveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST endpoints to demand the generation of the tally archive for an operation.
 */
@RestController
@RequestMapping(value = "/operation", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(
    value = "Tally archive",
    tags = "Tally archive")
public class TallyArchiveGenerationController {
  private static final Logger logger = LoggerFactory.getLogger(TallyArchiveGenerationController.class);

  private final TallyArchiveService tallyArchiveService;

  /**
   * Creates a new {@link TallyArchiveGenerationController} with the given service.
   *
   * @param tallyArchiveService the {@link TallyArchiveService} service
   */
  @Autowired
  public TallyArchiveGenerationController(TallyArchiveService tallyArchiveService) {
    this.tallyArchiveService = tallyArchiveService;
  }

  /**
   * Retrieves the status of the tally archive creation process of the deployed configuration.
   *
   * @param clientId the operation identifier in the client's system
   *
   * @return an {@link TallyArchiveStatusVo} for the matching operation.
   *
   * @throws EntityNotFoundException if the operation was not found
   */
  @GetMapping(value = "{clientId}/tally-archive/status", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Get tally creation archive status by operation client id",
      notes = "Retrieve the current tally creation archive status by operation client id.")
  @ApiResponses({
      @ApiResponse(code = 404, message = "Operation and protocol instance not found")
  })
  public TallyArchiveStatusVo getTallyArchiveStatusByClientId(
      @ApiParam(value = "Operation client identifier")
      @PathVariable("clientId") String clientId) {
    return tallyArchiveService.getTallyArchiveStatus(clientId);
  }

  /**
   * Triggers the tallying process.
   *
   * @param clientId the operation identifier in the client's system
   *
   * @deprecated use the PUT method {@link #demandEarlyArchiveGeneration(String, boolean)} instead
   */
  @Deprecated
  @PostMapping("{clientId}/tally-archive/trigger")
  @ApiOperation(
      value = "Start the tally archive generation process by client id",
      notes = "[DEPRECATED] Triggers the tally archive generation process for this operation client id")
  public void triggerTallyArchive(
      @ApiParam("client ID of the operation")
      @PathVariable("clientId") String clientId) {
    this.demandEarlyArchiveGeneration(clientId, false);
  }

  /**
   * Demand the generation of the tally archive as <em>early results generation</em>.
   * <p>
   *   This demand asks the Control Components to start shuffling and decrypting the cast ballots,
   *   and closes the operation. The caller is assured that the protocol has been notified when the endpoint returns.
   * </p>
   * <p>
   *   This endpoint can be called before the end of the voting period in order to "manually" close it.
   *   However its usage is reserved for SIMULATION environment.
   * </p>
   * <p>
   *   The call is idempotent : after the demand has been successfully created, any subsequent call will be no-op
   *   and return "200 - OK". Note that it will not check whether a task actually handles the demand. If you have
   *   good reasons to think a demand is not handled, you could set the "restart" argument to {@code true}
   * </p>
   *
   * @param clientId the operation identifier in the client's system
   * @param restartTask forces a task to start if the demand is not handled by one
   */
  @PutMapping("{clientId}/tally-archive/early-demand")
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation(
      value = "Demands early results (tally archive) generation",
      notes = "Triggers the tally archive generation process, even if the operation has not ended yet")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Demand being processed"),
      @ApiResponse(code = 201, message = "Demand created"),
      @ApiResponse(code = 403, message = "If the environment is not SIMULATION"),
      @ApiResponse(code = 404, message = "If the operation doesn't exist or is not deployed")
  })
  public void demandEarlyArchiveGeneration(
      @ApiParam("client ID of the operation") @PathVariable("clientId") String clientId,
      @ApiParam("restart the generation task")
      @RequestParam(name = "restart", defaultValue = "false") boolean restartTask) {
    logger.info("[early results generation] Demand TallyArchive generation for operation [{}] - restart flag : {}",
                clientId, restartTask);
    if (restartTask) {
      throw new UnsupportedOperationException("Not implemented yet");
    }
    tallyArchiveService.triggerGenerationManually(clientId);
  }

  /**
   * Demand the generation of the tally archive after the voting period of an operation has ended.
   * <p>
   *   This demand asks the Control Components to start shuffling and decrypting the cast ballots,
   *   and closes the operation. The caller is assured that the protocol has been notified when the endpoint returns.
   * </p>
   * <p>
   *   The call is idempotent : after the demand has been successfully created, any subsequent call will be no-op
   *   and return "200 - OK". Note that it will not check whether a task actually handles the demand. If you have
   *   good reasons to think a demand is not handled, you could set the "restart" argument to {@code true}
   * </p>
   *
   * @param clientId the operation identifier in the client's system
   * @param restartTask forces a task to start if the demand is not handled by one
   */
  @PutMapping("{clientId}/tally-archive/demand")
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation(
      value = "Demands results (tally archive) generation at the end of an operation",
      notes = "Triggers the tally archive generation process. The operation end date is verified to ensure it is over.")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Demand being processed"),
      @ApiResponse(code = 201, message = "Demand created"),
      @ApiResponse(code = 403, message = "If the operation has not ended yet"),
      @ApiResponse(code = 404, message = "If the operation doesn't exist or is not deployed")
  })
  public void demandArchiveGeneration(
      @ApiParam("client ID of the operation") @PathVariable("clientId") String clientId,
      @ApiParam("restart the generation task")
      @RequestParam(name = "restart", defaultValue = "false") boolean restartTask) {
    logger.info("Demand TallyArchive generation for operation [{}] - restart flag : {}", clientId, restartTask);
    if (restartTask) {
      throw new UnsupportedOperationException("Not implemented yet");
    }
    tallyArchiveService.triggerScheduledGeneration(clientId);
  }

  /**
   * When the generation cannot be requested because of the current (global) state of the operation.
   *
   * @param exc the intercepted exception
   */
  @ExceptionHandler(CannotPublishTallyArchiveException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  public void forbiddenCase(Exception exc) {
    logger.warn("TallyArchive generation forbidden by exception", exc);
  }

  /**
   * When the generation is already in progress.
   * <p>
   *   Maps to a status "200 - OK" because the call is expected to be idempotent.
   * </p>
   *
   * @param exc the intercepted exception
   */
  @ExceptionHandler(GenerationInProgressException.class)
  @ResponseStatus(HttpStatus.OK)
  public void alreadyTriggered(GenerationInProgressException exc) {
    logger.info("  > TallyArchive generation already in progress - request ignored", exc);
  }

}
