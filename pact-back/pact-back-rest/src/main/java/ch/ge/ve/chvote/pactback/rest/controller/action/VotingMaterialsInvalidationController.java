/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.action;

import ch.ge.ve.chvote.pactback.service.action.VotingMaterialsInvalidationService;
import ch.ge.ve.chvote.pactback.service.action.vo.VotingMaterialsInvalidationVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A specialised {@link ActionCreationController} for accessing {@link VotingMaterialsInvalidationVo}.
 */
@RestController
@RequestMapping("/privileged-actions/voting-materials-invalidation")
@Api(value = "Voting materials invalidation", tags = "Voting materials invalidation")
public class VotingMaterialsInvalidationController extends ActionCreationController<VotingMaterialsInvalidationVo> {

  /**
   * Creates a new {@link VotingMaterialsInvalidationController}s.
   *
   * @param service the {@link VotingMaterialsInvalidationVo} service.
   */
  @Autowired
  public VotingMaterialsInvalidationController(VotingMaterialsInvalidationService service) {
    super(service);
  }

}
