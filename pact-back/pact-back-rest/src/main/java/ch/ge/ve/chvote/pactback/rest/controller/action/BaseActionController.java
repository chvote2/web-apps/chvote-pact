/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.action;

import ch.ge.ve.chvote.pactback.service.action.BaseActionService;
import ch.ge.ve.chvote.pactback.service.action.vo.PrivilegedActionVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * A Base action controller for {@link PrivilegedActionVo} containing common functionality.
 *
 * @param <T> A subtype of {@link PrivilegedActionVo}
 */
public abstract class BaseActionController<T extends PrivilegedActionVo> {
  private final BaseActionService<T> service;

  protected BaseActionController(BaseActionService<T> service) {
    this.service = service;
  }

  /**
   * Retrieves a pending action of type {@link T} by id.
   *
   * @param id the id of the action to retrieve.
   *
   * @return The action of type {@link T}.
   */
  @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Retrieve an action by id",
      notes = "Retrieves a pending privileged action by id."
  )
  @ApiResponses({
      @ApiResponse(code = 404, message = "Privileged action not found"),
      @ApiResponse(code = 403, message = "The action cannot be displayed because its status is not PENDING")
  })
  public T getPendingAction(@ApiParam(value = "The action identifier") @PathVariable("id") long id) {
    return service.getPendingPrivilegedAction(id);
  }
}
