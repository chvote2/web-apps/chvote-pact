/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.action;

import ch.ge.ve.chvote.pactback.service.action.ActionCreationService;
import ch.ge.ve.chvote.pactback.service.action.vo.PrivilegedActionVo;
import ch.ge.ve.chvote.pactback.service.exception.IllegalTransitionException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A base controller that allows the creation, approval and rejection of privileged actions.
 *
 * @param <T> A subtype of {@link PrivilegedActionVo}.
 */
public abstract class ActionCreationController<T extends PrivilegedActionVo> extends BaseActionController<T> {
  private static final String APPROVED = "APPROVED";
  private static final String REJECTED = "REJECTED";

  private final ActionCreationService<T> service;

  protected ActionCreationController(ActionCreationService<T> service) {
    super(service);
    this.service = service;
  }

  /**
   * Retrieve a list of {@link T} that need approval or have not yet been requested for approval.
   *
   * @return a list of {@link T}.
   */
  @GetMapping(value = "new-or-pending", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Get new or pending",
      notes = "Retrieve all actions that need approval " +
              "or have not yet been requested for approval.")
  public List<T> getNewOrPendingActions() {
    return service.getNewOrPendingActions();
  }

  /**
   * Retrieve a {@link PrivilegedActionVo} of type {@link T} by the business id of its associated entity.
   *
   * @param businessId the business identifier.
   *
   * @return the matching {@link PrivilegedActionVo} of type {@link T}.
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Get by business id",
      notes = "Retrieve an action by the business id of the associated entity." +
              "If it doesn't exists, the returned action will have the status NOT_CREATED " +
              "and will not have any id associated to it.")
  @ApiResponses({
                    @ApiResponse(code = 404, message = "Entity not found"),
                    @ApiResponse(code = 403,
                                 message = "The action already exists but cannot be displayed because it is not " +
                                           "pending")
                })
  public T getByBusinessId(
      @ApiParam(value = "Business id")
      @RequestParam(value = "businessId") Long businessId) {
    return service.getByBusinessId(businessId);
  }

  /**
   * Create and persist a new action for the given business id where the requester is the user of the current session.
   *
   * @param businessId     the operation business identifier.
   * @param authentication the {@link Authentication} object for the current session.
   */
  @PutMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ApiOperation(
      value = "Create a new action",
      notes = "If there is no action associated to the given business " +
              "id, this call will create one. Otherwise an HTTP Status 409 Conflict will be returned.",
      consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  @ApiResponses({
                    @ApiResponse(code = 404, message = "Entity not found"),
                    @ApiResponse(code = 409, message = "An action already exists for the given business id")
                })
  public void createAction(
      @ApiParam(value = "Business id")
      @RequestParam(value = "businessId") Long businessId,
      Authentication authentication) {
    service.createAction(businessId, authentication.getName());
  }

  /**
   * Update a privileged action status for the given id. The user of the current session must be different than the
   * requester of the given privileged action id. The privileged action must be in PENDING at the time of updating it.
   *
   * @param actionId        the action id to approve.
   * @param status          the anew status for this action.
   * @param rejectionReason the rejection reason if the status is REJECTED.
   * @param authentication  the {@link Authentication} object for the current session.
   */
  @PutMapping(value = "{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ApiOperation(
      value = "Approve/Reject action",
      notes = "Update the status of the given action's identifier. The action can only be set " +
              "to APPROVED or REJECTED and must be in status PENDING at the time of updating it. " +
              "The current user must be different than the requester of the action.",
      consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  @ApiResponses({
                    @ApiResponse(code = 400, message = "The given status is not APPROVED or REJECTED"),
                    @ApiResponse(code = 404, message = "Privileged action not found"),
                    @ApiResponse(code = 403, message = "The action exists but its status is not PENDING")
                })
  public void updatePrivilegedActionStatus(
      @ApiParam(value = "The action identifier")
      @PathVariable("id") long actionId,
      @ApiParam(value = "The new status", allowableValues = APPROVED + "," + REJECTED)
      @RequestParam(value = "status") String status,
      @ApiParam(value = "The rejection reason")
      @RequestParam(value = "rejectionReason", required = false, defaultValue = "") String rejectionReason,
      Authentication authentication) {

    switch (status) {
      case APPROVED:
        service.approveAction(actionId, authentication.getName());
        break;
      case REJECTED:
        service.rejectAction(actionId, authentication.getName(), rejectionReason);
        break;
      default:
        throw new IllegalTransitionException("No possible transition of action " + actionId + " to status " + status);
    }
  }
}
