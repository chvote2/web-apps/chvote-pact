/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.chvote.pactback.rest.controller.action;

import ch.ge.ve.chvote.pactback.service.action.VotingPeriodInitializationService;
import ch.ge.ve.chvote.pactback.service.action.vo.VotingPeriodInitializationVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A specialised {@link ActionCreationController} for accessing {@link VotingPeriodInitializationVo}.
 */
@RestController
@RequestMapping("/privileged-actions/voting-period-initialization")
@Api(
    value = "Voting period initialization",
    tags = "Voting period initialization")
public class VotingPeriodInitializationController extends ActionCreationController<VotingPeriodInitializationVo> {

  /**
   * Creates a new voting period initialization controller.
   *
   * @param service the {@link VotingPeriodInitializationService} service.
   */
  @Autowired
  public VotingPeriodInitializationController(VotingPeriodInitializationService service) {
    super(service);
  }
}
