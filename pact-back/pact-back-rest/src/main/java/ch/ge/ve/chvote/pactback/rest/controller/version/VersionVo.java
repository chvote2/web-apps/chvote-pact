/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.version;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A DTO that represents the version of the application.
 */
public class VersionVo {
  private static final Logger logger = LoggerFactory.getLogger(VersionVo.class);

  private final String        buildNumber;
  private final LocalDateTime buildTimestamp;

  /**
   * Create a new version VO.
   *
   * @param buildNumber    a string representing the current build number of the application.
   * @param buildTimestamp the build timestamp of the application.
   * @param timezone       the target timezone for rendering the date.
   */
  public VersionVo(String buildNumber, String buildTimestamp, String timezone) {
    this.buildNumber = buildNumber.startsWith("@") ? "DEV" : buildNumber;
    this.buildTimestamp = safeParseDate(buildTimestamp, timezone);
  }

  private static LocalDateTime safeParseDate(String timestamp, String timezone) {
    try {
      return LocalDateTime.ofInstant(Instant.parse(timestamp),
                                     TimeZone.getTimeZone(timezone).toZoneId());
    } catch (DateTimeParseException exception) {
      logger.warn("Cannot parse date with timestamp = \"{}\" and timezone = \"{}\". Using startup date instead",
                  timestamp, timezone, exception);
      return LocalDateTime.now();
    }
  }

  public String getBuildNumber() {
    return buildNumber;
  }

  public LocalDateTime getBuildTimestamp() {
    return buildTimestamp;
  }
}
