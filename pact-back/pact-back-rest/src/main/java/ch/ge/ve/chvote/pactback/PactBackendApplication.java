/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback;

import ch.ge.ve.chvote.pactback.rest.config.CorsConfig;
import ch.ge.ve.chvote.pactback.rest.config.CustomHeaderFilter;
import ch.ge.ve.chvote.pactback.rest.config.Slf4jDataFilter;
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer;
import ch.ge.ve.jacksonserializer.JSDates;
import ch.ge.ve.model.convert.api.ElectionConverter;
import ch.ge.ve.model.convert.api.VotationConverter;
import ch.ge.ve.model.convert.api.VoterConverter;
import ch.ge.ve.model.convert.impl.DefaultElectionConverter;
import ch.ge.ve.model.convert.impl.DefaultVotationConverter;
import ch.ge.ve.model.convert.impl.DefaultVoterConverter;
import ch.ge.ve.model.convert.impl.VoterIdGenerator;
import ch.ge.ve.protocol.client.utils.RabbitUtilities;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import ch.ge.ve.service.AcknowledgementService;
import ch.ge.ve.service.AcknowledgementServiceInMemoryImpl;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.Signatories;
import ch.ge.ve.service.SignatureService;
import ch.ge.ve.service.SignatureServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.security.Security;
import java.sql.SQLException;
import java.util.Map;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.h2.tools.Server;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.web.embedded.EmbeddedWebServerFactoryCustomizerAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Entry point for the PACT SpringBoot application.
 */
@Configuration
@EnableAutoConfiguration(exclude = EmbeddedWebServerFactoryCustomizerAutoConfiguration.class)
@EnableAsync
@ComponentScan({
                   "ch.ge.ve.config", "ch.ge.ve.service", "ch.ge.ve.chvote.pactback.notification",
                   "ch.ge.ve.chvote.pactback.service", "ch.ge.ve.chvote.pactback.rest"})
@EntityScan({"ch.ge.ve.chvote.pactback.repository", "ch.ge.ve.event.acknowledgement"})
@EnableJpaRepositories({"ch.ge.ve.chvote.pactback.repository", "ch.ge.ve.event.acknowledgement"})
public class PactBackendApplication {

  /**
   * Initializes the PACT SpringBoot application.
   *
   * @param args arguments.
   */
  public static void main(String[] args) {
    new SpringApplicationBuilder()
        .parent(PactBackendApplication.class)
        .web(WebApplicationType.NONE)
        .child(B2bEndPointConfiguration.class)
        .profiles("b2b")
        .web(WebApplicationType.SERVLET)
        .sources(CorsConfig.class, CustomHeaderFilter.class, Slf4jDataFilter.class)
        .sibling(FrontendEndPointConfiguration.class)
        .profiles("frontend")
        .web(WebApplicationType.SERVLET)
        .sources(CorsConfig.class, CustomHeaderFilter.class, Slf4jDataFilter.class)
        .sibling(ManagementEndPointConfiguration.class)
        .profiles("management")
        .web(WebApplicationType.SERVLET)
        .sources(CorsConfig.class, Slf4jDataFilter.class)
        .run(args);
  }

  private final ChVoteConfigurationProperties chVoteConfigurationProperties;

  @Autowired
  public PactBackendApplication(ChVoteConfigurationProperties chVoteConfigurationProperties) {
    this.chVoteConfigurationProperties = chVoteConfigurationProperties;
  }

  @PostConstruct
  public void setDefaultTimezone() {
    TimeZone.setDefault(TimeZone.getTimeZone(chVoteConfigurationProperties.getTimezone()));
  }

  /**
   * FIXME In production should have a profile that exclude this server creation
   *
   * @return the created server
   *
   * @throws SQLException if we cannot connect
   */
  @Bean(initMethod = "start", destroyMethod = "stop")
  @Profile("!test")
  public Server h2Server() throws SQLException {
    return Server.createTcpServer("-web", "-tcp", "-tcpAllowOthers", "-tcpPort", "47645");
  }

  @Bean
  public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
    return jacksonObjectMapperBuilder -> {
      jacksonObjectMapperBuilder.serializers(JSDates.SERIALIZER);
      jacksonObjectMapperBuilder.deserializers(JSDates.DESERIALIZER);

      jacksonObjectMapperBuilder.serializers(new BigIntegerAsBase64Serializer());
      jacksonObjectMapperBuilder.deserializers(new BigIntegerAsBase64Deserializer());
    };
  }

  @Bean
  public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
    return new RabbitAdmin(connectionFactory);
  }

  @Bean
  public RabbitUtilities rabbitUtilities(EventBus eventBus, RabbitAdmin rabbitAdmin,
                                         MessageConverter messageConverter) {
    return new RabbitUtilities(eventBus, rabbitAdmin, messageConverter);
  }

  @Bean
  public RandomGenerator randomGenerator(@Value("${ch.ge.ve.rng.algorithm}") String rngAlgorithm,
                                         @Value("${ch.ge.ve.rng.provider}") String rngProvider) {
    return RandomGenerator.create(rngAlgorithm, rngProvider);
  }

  @Bean
  public SignatureService signatureService(IdentificationPrivateKey signingKey,
                                           Map<String, Map<String, BigInteger>> verificationKeys,
                                           AlgorithmsSpec algorithmsSpec, RandomGenerator randomGenerator,
                                           ObjectMapper objectMapper,
                                           PublicParameters publicParameters) {
    return new SignatureServiceImpl(Signatories.CHVOTE, signingKey,
                                    verificationKeys, algorithmsSpec, randomGenerator, objectMapper,
                                    id -> publicParameters);
  }

  @Bean
  PublicParameters publicParameters(@Value("${ch.ge.ve.security-level}") int securityLevel,
                                    @Value("${chvote.protocol-core.control-components-count}") int ccCount) {
    return PublicParametersFactory.forLevel(securityLevel).createPublicParameters(ccCount);
  }

  @Bean
  public IdentificationPrivateKey signingKey(
      @Value("${chvote.protocol-core.signing-key.url}") URL signingKeyUrl,
      @Value("${chvote.protocol-core.signing-key.password}") char[] signingKeyPassword,
      ObjectMapper objectMapper) {
    // FIXME: protect key with a password
    try {
      return objectMapper.readValue(signingKeyUrl, IdentificationPrivateKey.class);
    } catch (IOException e) {
      throw new IllegalStateException("Initialization failure: can't read signing key", e);
    }
  }

  @PostConstruct
  public void addBouncyCastleProvider() {
    Security.addProvider(new BouncyCastleProvider());
  }


  @Bean
  public VotationConverter votationConverter() {
    return new DefaultVotationConverter();
  }

  @Bean
  public ElectionConverter electionConverter() {
    return new DefaultElectionConverter();
  }

  @Bean
  public VoterConverter voterConverter() {
    return new DefaultVoterConverter(new VoterIdGenerator());
  }

  @Bean
  public AcknowledgementService acknowledgementService() {
    return new AcknowledgementServiceInMemoryImpl();
  }
}
