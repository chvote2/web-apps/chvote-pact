/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.actuator

import static ch.ge.ve.chvote.pactback.fixtures.repository.Operations.failedOnResultsGenerationBuilder
import static org.hamcrest.Matchers.hasSize
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.ManagementEndPointConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfigurationBuilder
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstanceBuilder
import ch.ge.ve.chvote.pactback.rest.H2DatabaseCleaner
import ch.ge.ve.chvote.pactback.rest.MockedRabbitConfiguration
import ch.ge.ve.chvote.pactback.rest.RestEndpointTest
import ch.ge.ve.chvote.pactback.rest.spring.AfterLastTest
import ch.ge.ve.chvote.pactback.rest.spring.ContextConfigTestExecutionListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification

@Transactional
@RestEndpointTest(context = [MockedRabbitConfiguration, ManagementEndPointConfiguration])
@TestExecutionListeners(listeners = ContextConfigTestExecutionListener, mergeMode = MERGE_WITH_DEFAULTS)
class ResultsTasksEndpointTest extends Specification {

  @Autowired
  private OperationRepository repository

  @Autowired
  private MockMvc mvc

  @AfterLastTest
  static void restoreDatabase(H2DatabaseCleaner cleaner) {
    cleaner.reinitSequences()
  }

  List<Operation> createDataset() {
    def failedOnResultsGeneration_op = failedOnResultsGenerationBuilder().withClientId("operation 1").build()

    def instance2 = ProtocolInstanceBuilder.copyWithoutBidirectional(failedOnResultsGeneration_op.fetchDeployedConfiguration().fetchProtocolInstance())
            .withStatus(ProtocolInstanceStatus.RESULTS_AVAILABLE)
            .withProtocolId("0202-20200202-11")
            .build()

    def resultsAvailable_op = failedOnResultsGenerationBuilder().withClientId("operation 2")
            .withDeployedConfiguration(OperationConfigurationBuilder.copyWithoutBidirectionnal(failedOnResultsGeneration_op.fetchDeployedConfiguration())
            .withProtocolInstance(instance2).build())
            .build()

    return [failedOnResultsGeneration_op, resultsAvailable_op]
  }

  def "get should list all tasks found by the service"() {
    given:
    repository.saveAll(createDataset())

    expect:
    mvc.perform(get("/results/tasks/failed")
            .with(user("user1").password("password"))).andExpect(status().isOk())
            .andExpect(jsonPath('$', hasSize(1)))
            .andExpect(jsonPath('$.[0].operationId').value("operation 1"))
            .andExpect(jsonPath('$.[0].protocolEnvironment').value('PRODUCTION'))
            .andExpect(jsonPath('$.[0].targetMode').value('SIMULATION'))
            .andExpect(jsonPath('$.[0].status.value').value('TALLY_ARCHIVE_GENERATION_FAILURE'))
            .andExpect(jsonPath('$.[0].status.since').value([2018,6,18,9,18]))
    // Note : everything's OK but the date format. The format is correct if the PACT is entirely deployed though.
  }
}
