/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.action

import static ch.ge.ve.chvote.pactback.rest.TestUtils.XML_HTTP_REQUEST
import static ch.ge.ve.chvote.pactback.rest.TestUtils.X_REQUESTED_WITH
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.FrontendEndPointConfiguration
import ch.ge.ve.chvote.pactback.mock.server.OperationsCreationService
import ch.ge.ve.chvote.pactback.rest.H2DatabaseCleaner
import ch.ge.ve.chvote.pactback.rest.MockedRabbitConfiguration
import ch.ge.ve.chvote.pactback.rest.RestEndpointTest
import ch.ge.ve.chvote.pactback.rest.spring.AfterLastTest
import ch.ge.ve.chvote.pactback.rest.spring.BeforeFirstTest
import ch.ge.ve.chvote.pactback.rest.spring.ContextConfigTestExecutionListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

@RestEndpointTest(context = [MockedRabbitConfiguration, FrontendEndPointConfiguration])
@TestExecutionListeners(listeners = ContextConfigTestExecutionListener, mergeMode = MERGE_WITH_DEFAULTS)
class PrivilegedActionsControllerTest extends Specification {
  @Autowired
  MockMvc mvc

  @BeforeFirstTest
  static void prepareOperations(OperationsCreationService service) {
    service.createAllOperations()
  }

  @AfterLastTest
  static void restoreDatabase(H2DatabaseCleaner cleaner) {
    cleaner.cleanupDatabase()
  }

  def "retrieve the count of pending actions"() {
    when:
    def resultActions = mvc.perform(get("/privileged-actions/pending-count")
            .with(user("user1")).header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(content().string("2"))
  }

  def "retrieve the list of pending actions"() {
    when:
    def resultActions = mvc.perform(get("/privileged-actions/pending-actions")
            .with(user("user1")).header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.length()').value(4))
            .andExpect(jsonPath('$[0].id').value('1'))
            .andExpect(jsonPath('$[0].ownerName').value('user2'))
            .andExpect(jsonPath('$[0].type').value('CONFIGURATION_DEPLOYMENT'))
  }

}
