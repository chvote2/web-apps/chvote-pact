/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.runner

import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.INITIALIZING

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInitializationService
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService
import org.springframework.boot.ApplicationArguments
import spock.lang.Specification

class ProtocolInstancesRecoveryRunnerTest extends Specification {

  def protocolInstanceService = Mock(ProtocolInstanceService)
  def initializationService = Mock(ProtocolInitializationService)

  ProtocolInstancesRecoveryRunner runner

  def applicationArgs = Mock(ApplicationArguments)

  void setup() {
    runner = new ProtocolInstancesRecoveryRunner(protocolInstanceService, initializationService)
  }

  def "Run - do nothing when no suspicious ProtocolInstances found"() {
    given:
    protocolInstanceService.getProtocolInstancesStuckInGeneration() >> Collections.emptyList()

    when:
    runner.run(applicationArgs)
    then:
    0 * initializationService._
  }

  def "Run - all protocol instances stuck before CREDENTIAL_GENERATED should be passed to the initializationService"() {
    given:
    def instances = [new ProtocolInstance(id: 25L, status: INITIALIZING), new ProtocolInstance(id: 5L, status: INITIALIZING)]
    protocolInstanceService.getProtocolInstancesStuckInGeneration() >> instances

    when:
    runner.run(applicationArgs)
    then:
    1 * initializationService.continueProtocol(instances[0])
    1 * initializationService.continueProtocol(instances[1])
  }
}
