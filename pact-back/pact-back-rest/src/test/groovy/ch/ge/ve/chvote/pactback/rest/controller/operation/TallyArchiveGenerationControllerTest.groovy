/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.operation

import static ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo.State.NOT_REQUESTED
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo
import ch.ge.ve.chvote.pactback.rest.config.DefaultSecurityConfiguration
import ch.ge.ve.chvote.pactback.rest.controller.ExceptionHandlerController
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException
import ch.ge.ve.chvote.pactback.service.operation.tally.CannotPublishTallyArchiveException
import ch.ge.ve.chvote.pactback.service.operation.tally.GenerationInProgressException
import ch.ge.ve.chvote.pactback.service.operation.tally.TallyArchiveService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import spock.lang.Specification
import spock.mock.DetachedMockFactory

/**
 * Those are simple tests to check pure REST behaviour, ie which response code is returned depending on
 * the service's own behaviour.
 */
@SpringBootTest
@ContextConfiguration(classes = MinimalRestContext)
@AutoConfigureMockMvc
class TallyArchiveGenerationControllerTest extends Specification {

  @Configuration
  @EnableWebMvc
  @Import([DefaultSecurityConfiguration, ExceptionHandlerController, TallyArchiveGenerationController])
  static class MinimalRestContext {

    private final mockFactory = new DetachedMockFactory()

    @Bean
    TallyArchiveService tallyArchiveService() {
      return mockFactory.Mock(TallyArchiveService.class)
    }

  }

  @Autowired
  private MockMvc mvc

  @Autowired
  private TallyArchiveService mockedService

  private String clientId = "tested-operation"

  def "GetTallyArchiveStatusByClientId"() {
    given:
    mockedService.getTallyArchiveStatus(clientId) >> new TallyArchiveStatusVo(NOT_REQUESTED, null)

    when:
    def response = mvc.perform(get("/operation/$clientId/tally-archive/status")
            .with(user("user1").password("password")))

    then:
    response.andExpect(status().isOk())
            .andExpect(content().json('{"state":"NOT_REQUESTED","tallyArchiveLocation":null}'))
  }

  def "early-demand should return 201 when generation is requested"() {
    when:
    def response = mvc.perform(put("/operation/$clientId/tally-archive/early-demand")
            .with(user("user1").password("password")))

    then:
    response.andExpect(status().isCreated())
    1 * mockedService.triggerGenerationManually(clientId)
  }

  def "demand should return 201 when generation is requested"() {
    when:
    def response = mvc.perform(put("/operation/$clientId/tally-archive/demand")
            .with(user("user1").password("password")))

    then:
    response.andExpect(status().isCreated())
    1 * mockedService.triggerScheduledGeneration(clientId)
  }

  def "early-demand should return 200 when generation is already in progress"() {
    given:
    mockedService.triggerGenerationManually(clientId) >> {
      throw new GenerationInProgressException("Exception created for test purposes")
    }

    expect:
    mvc.perform(put("/operation/$clientId/tally-archive/early-demand")
            .with(user("user1").password("password")))
            .andExpect(status().isOk())
  }

  def "demand should return 200 when generation is already in progress"() {
    given:
    mockedService.triggerScheduledGeneration(clientId) >> {
      throw new GenerationInProgressException("Exception created for test purposes")
    }

    expect:
    mvc.perform(put("/operation/$clientId/tally-archive/demand")
            .with(user("user1").password("password")))
            .andExpect(status().isOk())
  }

  def "early-demand should return 404 when some data cannot be found"() {
    given:
    mockedService.triggerGenerationManually(clientId) >> {
      throw new EntityNotFoundException("Exception created for test purposes")
    }

    expect:
    mvc.perform(put("/operation/$clientId/tally-archive/early-demand")
            .with(user("user1").password("password")))
            .andExpect(status().isNotFound())
  }

  def "demand should return 404 when some data cannot be found"() {
    given:
    mockedService.triggerScheduledGeneration(clientId) >> {
      throw new EntityNotFoundException("Exception created for test purposes")
    }

    expect:
    mvc.perform(put("/operation/$clientId/tally-archive/demand")
            .with(user("user1").password("password")))
            .andExpect(status().isNotFound())
  }

  def "early-demand should return 403 when the tally archive cannot be published"() {
    given:
    mockedService.triggerGenerationManually(clientId) >> {
      throw new CannotPublishTallyArchiveException("Exception created for test purposes")
    }

    expect:
    mvc.perform(put("/operation/$clientId/tally-archive/early-demand")
            .with(user("user1").password("password")))
            .andExpect(status().isForbidden())
  }

  def "demand should return 403 when the tally archive cannot be published"() {
    given:
    mockedService.triggerScheduledGeneration(clientId) >> {
      throw new CannotPublishTallyArchiveException("Exception created for test purposes")
    }

    expect:
    mvc.perform(put("/operation/$clientId/tally-archive/demand")
            .with(user("user1").password("password")))
            .andExpect(status().isForbidden())
  }
}
