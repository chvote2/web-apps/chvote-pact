/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.action

import static ch.ge.ve.chvote.pactback.rest.TestUtils.XML_HTTP_REQUEST
import static ch.ge.ve.chvote.pactback.rest.TestUtils.X_REQUESTED_WITH
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.FrontendEndPointConfiguration
import ch.ge.ve.chvote.pactback.mock.server.OperationsCreationService
import ch.ge.ve.chvote.pactback.rest.H2DatabaseCleaner
import ch.ge.ve.chvote.pactback.rest.MockedRabbitConfiguration
import ch.ge.ve.chvote.pactback.rest.RestEndpointTest
import ch.ge.ve.chvote.pactback.rest.spring.AfterLastTest
import ch.ge.ve.chvote.pactback.rest.spring.BeforeFirstTest
import ch.ge.ve.chvote.pactback.rest.spring.ContextConfigTestExecutionListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.lang.Stepwise

@RestEndpointTest(context = [MockedRabbitConfiguration, FrontendEndPointConfiguration])
@TestExecutionListeners(listeners = ContextConfigTestExecutionListener, mergeMode = MERGE_WITH_DEFAULTS)
@Stepwise
class ConfigurationDeploymentControllerTest extends Specification {

  @Autowired
  MockMvc mvc

  @BeforeFirstTest
  static void prepareOperations(OperationsCreationService service) {
    service.createAllOperations()
  }

  @AfterLastTest
  static void restoreDatabase(H2DatabaseCleaner cleaner) {
    cleaner.cleanupDatabase()
  }

  def "should retrieve all pending or new configuration deployments"() {
    when:
    def resultActions = mvc.perform(get("/privileged-actions/configuration-deployment/new-or-pending/")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.length()').value('5'))
            .andExpect(jsonPath('$[0].operationConfiguration.id').value('1'))
            .andExpect(jsonPath('$[0].type').value('CONFIGURATION_DEPLOYMENT'))
            .andExpect(jsonPath('$[1].operationConfiguration.id').value('2'))
            .andExpect(jsonPath('$[1].type').value('CONFIGURATION_DEPLOYMENT'))
            .andExpect(jsonPath('$[2].operationConfiguration.id').value('3'))
            .andExpect(jsonPath('$[2].type').value('CONFIGURATION_DEPLOYMENT'))
            .andExpect(jsonPath('$[3].operationConfiguration.id').value('4'))
            .andExpect(jsonPath('$[3].type').value('CONFIGURATION_DEPLOYMENT'))
            .andExpect(jsonPath('$[4].operationConfiguration.id').value('10'))
            .andExpect(jsonPath('$[4].type').value('CONFIGURATION_DEPLOYMENT'))
  }

  def "should retrieve pending configuration deployment by id"() {
    when:
    def resultActions = mvc.perform(get("/privileged-actions/configuration-deployment/2")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.id').value('2'))
            .andExpect(jsonPath('$.type').value('CONFIGURATION_DEPLOYMENT'))
            .andExpect(jsonPath('$.operationConfiguration.id').value('4'))
            .andExpect(jsonPath('$.operationConfiguration.deploymentFiles.length()').value('1'))
            .andExpect(jsonPath('$.operationConfiguration.deploymentFiles[0].id').value('3'))
  }

  def "should retrieve pending configuration deployment by operation configuration ID"() {
    when:
    def resultActions = mvc.perform(get("/privileged-actions/configuration-deployment?businessId=4")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.id').value('2'))
            .andExpect(jsonPath('$.type').value('CONFIGURATION_DEPLOYMENT'))
            .andExpect(jsonPath('$.operationConfiguration.id').value('4'))
  }

  def "should fail to retrieve a non pending configuration deployment by ID"() {
    when:
    def resultActions = mvc.perform(get("/privileged-actions/configuration-deployment/5")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isForbidden())
  }

  def "should create a new deployment action for the given operation configuration id"() {
    when:
    def response = mvc.perform(put("/privileged-actions/configuration-deployment/")
            .param("businessId", "1")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    response
            .andExpect(status().isNoContent())
  }

  def "should fail to create a new deployment action if an action already exists for the given operationConfigurationId"() {
    when:
    def response = mvc.perform(put("/privileged-actions/configuration-deployment/")
            .param("businessId", "1")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    response
            .andExpect(status().isConflict())
  }

  def "approve a PENDING privileged action"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/configuration-deployment/1")
            .param("status", "APPROVED").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isNoContent())
  }

  def "bad status for approving a PENDING privileged action"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/configuration-deployment/1")
            .param("status", "PENDING").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isBadRequest())
  }

  def "user doesn't have the right to approve request"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/configuration-deployment/2")
            .param("status", "APPROVED").with(user("user2"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isForbidden())
  }

  def "approve and already approved action"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/configuration-deployment/1")
            .param("status", "APPROVED").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isForbidden())
  }

  def "action to approve doesn't exists"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/configuration-deployment/27")
            .param("status", "APPROVED").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isNotFound())
  }

  def "reject a PENDING privileged action"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/configuration-deployment/2")
            .param("status", "REJECTED")
            .param("rejectionReason", "Not good").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isNoContent())
  }

}
