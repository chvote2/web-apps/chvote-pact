/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.operation

import static ch.ge.ve.chvote.pactback.rest.TestUtils.XML_HTTP_REQUEST
import static ch.ge.ve.chvote.pactback.rest.TestUtils.X_REQUESTED_WITH
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.B2bEndPointConfiguration
import ch.ge.ve.chvote.pactback.contract.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.contract.operation.PrinterConfigurationVo
import ch.ge.ve.chvote.pactback.contract.operation.VotingMaterialsConfigurationSubmissionVo
import ch.ge.ve.chvote.pactback.fixtures.PrintersPublicKeys
import ch.ge.ve.chvote.pactback.mock.server.OperationsCreationService
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.rest.H2DatabaseCleaner
import ch.ge.ve.chvote.pactback.rest.MockedRabbitConfiguration
import ch.ge.ve.chvote.pactback.rest.RestEndpointTest
import ch.ge.ve.chvote.pactback.rest.spring.AfterLastTest
import ch.ge.ve.chvote.pactback.rest.spring.BeforeFirstTest
import ch.ge.ve.chvote.pactback.rest.spring.ContextConfigTestExecutionListener
import com.fasterxml.jackson.databind.ObjectMapper
import javax.transaction.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.lang.Stepwise

@RestEndpointTest(context = [MockedRabbitConfiguration, B2bEndPointConfiguration])
@TestExecutionListeners(listeners = ContextConfigTestExecutionListener, mergeMode = MERGE_WITH_DEFAULTS)
@Stepwise
class VotingMaterialsConfigurationControllerTest extends Specification {

  @Autowired
  MockMvc mvc

  @Autowired
  OperationsCreationService operationsCreationService

  @Autowired
  ObjectMapper objectMapper

  @BeforeFirstTest
  static void prepareOperations(OperationsCreationService service) {
    service.createAllOperations()
  }

  @AfterLastTest
  static void restoreDatabase(H2DatabaseCleaner cleaner) {
    cleaner.cleanupDatabase()
  }

  def "should create a voting materials configuration"() {
    given:
    def vo = new VotingMaterialsConfigurationSubmissionVo()
    vo.user = "some user"
    vo.votingCardLabel = "some label"
    vo.target = DeploymentTarget.SIMULATION
    vo.simulationName = "simulation-1"
    def swissVo = new PrinterConfigurationVo()
    swissVo.id = "some ID"
    swissVo.name = "some name"
    swissVo.publicKey = PrintersPublicKeys.publicKeyPa0()
    vo.swissAbroadWithoutMunicipalityPrinter = swissVo

    def configuration = new MockMultipartFile("configuration", "", "application/json",
            objectMapper.writeValueAsBytes(vo))

    when:
    def resultActions = mvc.perform(multipart("/operation/10/voting-materials/configuration")
            .file(configuration)
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isOk())
  }

  
  def "should retrieve a voting materials status VO for deployed operations with a voting material configuration"() {
    when:
    def operationStatus = mvc.perform(
            get("/operation/9/voting-materials/status")
                    .with(httpBasic("user1", "password"))
                    .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    operationStatus
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.state').value("AVAILABLE_FOR_CREATION"))
            .andExpect(jsonPath('$.lastChangeUser').value("Betty"))
            .andExpect(jsonPath('$.votingCardLocations.length()').value("0"))
  }

  def "should not be able to update a PENDING voting material configuration"() {
    given:
    def vo = new VotingMaterialsConfigurationSubmissionVo()
    vo.user = "some user"
    vo.votingCardLabel = "some label"
    def swissVo = new PrinterConfigurationVo()
    swissVo.id = "some ID"
    swissVo.name = "some name"
    swissVo.publicKey = PrintersPublicKeys.publicKeyPa0()
    vo.swissAbroadWithoutMunicipalityPrinter = swissVo

    def configuration = new MockMultipartFile("configuration", "", "application/json",
            objectMapper.writeValueAsBytes(vo))

    when:
    def resultActions = mvc.perform(multipart("/operation/5/voting-materials/configuration")
            .file(configuration)
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isConflict())

  }

  @Transactional
  def "should be able to update a REJECTED voting material configuration"() {
    given:
    mvc.perform(put("/privileged-actions/voting-materials-creation/4")
            .param("status", "REJECTED").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST)).andExpect(status().isNoContent())
    def vo = new VotingMaterialsConfigurationSubmissionVo()
    vo.user = "some user"
    vo.votingCardLabel = "some label"
    vo.target = DeploymentTarget.REAL
    def swissVo = new PrinterConfigurationVo()
    swissVo.id = "some ID"
    swissVo.name = "some name"
    swissVo.publicKey = PrintersPublicKeys.publicKeyPa0()
    vo.swissAbroadWithoutMunicipalityPrinter = swissVo

    def configuration = new MockMultipartFile("configuration", "", "application/json",
            objectMapper.writeValueAsBytes(vo))

    when:
    def resultActions = mvc.perform(multipart("/operation/5/voting-materials/configuration")
            .file(configuration)
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isOk())
  }

  @Transactional
  def "should validate a created voting material"() {
    when:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget.REAL)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
            }

    def resultActions = mvc.perform(post("/operation/" + id.clientId + "/voting-materials/validate")
            .param("user", "johndoe")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then: "the call is successful"
    resultActions.andExpect(status().isOk())

    and: "the status is modified as expected"
    mvc.perform(get("/operation/" + id.clientId + "/voting-materials/status")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))
            .andExpect(jsonPath('$.state').value("VALIDATED"))
  }

}
