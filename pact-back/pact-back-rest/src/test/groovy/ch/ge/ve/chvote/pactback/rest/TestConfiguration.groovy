/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest

import ch.ge.ve.chvote.pactback.mock.server.OperationsCreationService
import ch.ge.ve.chvote.pactback.repository.action.materials.VotingMaterialsCreationActionRepository
import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.VotingPeriodInitializationActionRepository
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository
import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository
import ch.ge.ve.chvote.pactback.repository.user.UserRepository
import javax.annotation.PostConstruct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean

@org.springframework.boot.test.context.TestConfiguration
class TestConfiguration {

  @Autowired
  OperationsCreationService operationsCreationService

  @PostConstruct
  void prepareDatabaseWithUsers() {
    // Actually they should have been created by script - but they need to be referenced in the service object
    operationsCreationService.createUsersIfAbsent()
  }

  @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
  @Bean
  OperationsCreationService operationsCreationService(OperationRepository operationRepository,
                                                      ProtocolInstanceRepository protocolInstanceRepository,
                                                      UserRepository userRepository,
                                                      VotingMaterialsCreationActionRepository votingMaterialsCreationActionRepository,
                                                      VotingPeriodInitializationActionRepository votingPeriodInitializationActionRepository,
                                                      VoterRepository voterRepository) {
    return new OperationsCreationService(operationRepository, protocolInstanceRepository, userRepository, votingMaterialsCreationActionRepository, votingPeriodInitializationActionRepository, voterRepository)
  }
}
