/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.action

import static ch.ge.ve.chvote.pactback.rest.TestUtils.XML_HTTP_REQUEST
import static ch.ge.ve.chvote.pactback.rest.TestUtils.X_REQUESTED_WITH
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.FrontendEndPointConfiguration
import ch.ge.ve.chvote.pactback.mock.server.OperationsCreationService
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.rest.H2DatabaseCleaner
import ch.ge.ve.chvote.pactback.rest.MockedRabbitConfiguration
import ch.ge.ve.chvote.pactback.rest.RestEndpointTest
import ch.ge.ve.chvote.pactback.rest.spring.AfterLastTest
import ch.ge.ve.chvote.pactback.rest.spring.BeforeFirstTest
import ch.ge.ve.chvote.pactback.rest.spring.ContextConfigTestExecutionListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.lang.Stepwise

@RestEndpointTest(context = [MockedRabbitConfiguration, FrontendEndPointConfiguration])
@TestExecutionListeners(listeners = ContextConfigTestExecutionListener, mergeMode = MERGE_WITH_DEFAULTS)
@Stepwise
class VotingMaterialsInvalidationControllerTest extends Specification {

  @Autowired
  MockMvc mvc

  @Autowired
  OperationsCreationService operationsCreationService

  @BeforeFirstTest
  static void prepareOperations(OperationsCreationService service) {
    service.createAllOperations()
  }

  @AfterLastTest
  static void restoreDatabase(H2DatabaseCleaner cleaner) {
    cleaner.cleanupDatabase()
  }

  def "should retrieve all pending invalidation actions"() {
    given:
    operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                      .withPendingInvalidation()
            }


    when:
    def resultActions = mvc.perform(get("/privileged-actions/voting-materials-invalidation/new-or-pending/")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.length()').value('1'))
            .andExpect(jsonPath('$[0].type').value('VOTING_MATERIALS_INVALIDATION'))
  }

  def "should retrieve a materials invalidation action by id"() {
    given:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                      .withPendingInvalidation()
            }

    when:
    def resultActions = mvc.perform(get("/privileged-actions/voting-materials-invalidation/" + id.votingMaterialInvalidationId)
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.id').value(id.votingMaterialInvalidationId))
            .andExpect(jsonPath('$.type').value('VOTING_MATERIALS_INVALIDATION'))
  }

  def "should retrieve pending materials invalidation actions by voting materials configuration ID"() {
    given:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                      .withPendingInvalidation()
            }

    when:
    def resultActions = mvc.perform(get("/privileged-actions/voting-materials-invalidation?businessId=" + id.votingMaterialConfigurationId)
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.votingMaterialsConfigurationId').value(id.votingMaterialConfigurationId))
            .andExpect(jsonPath('$.type').value('VOTING_MATERIALS_INVALIDATION'))
  }


  def "should retrieve possible new materials invalidation actions by voting materials configuration ID"() {
    given:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
            }

    when:
    def resultActions = mvc.perform(get("/privileged-actions/voting-materials-invalidation?businessId=" + id.votingMaterialConfigurationId)
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.id').isEmpty())
            .andExpect(jsonPath('$.votingMaterialsConfigurationId').value(id.votingMaterialConfigurationId))
            .andExpect(jsonPath('$.type').value('VOTING_MATERIALS_INVALIDATION'))
  }


  def "should fail to retrieve a non pending materials creation invalidation by ID"() {
    given:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                      .withApprovedInvalidation()
            }

    when:
    def resultActions = mvc.perform(get("/privileged-actions/voting-materials-invalidation/" + id.votingMaterialInvalidationId)
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isForbidden())
  }

  def "should create a new voting material invalidation action for the given voting materials configuration id"() {
    given:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
            }

    when:
    def response = mvc.perform(put("/privileged-actions/voting-materials-invalidation/")
            .param("businessId", String.valueOf(id.votingMaterialConfigurationId))
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    response
            .andExpect(status().isNoContent())
  }

  def "should fail to create a new action if an action already exists for the given voting materials configuration id"() {
    given:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                      .withPendingInvalidation()
            }

    when:
    def response = mvc.perform(put("/privileged-actions/voting-materials-invalidation/")
            .param("businessId", String.valueOf(id.votingMaterialConfigurationId))
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    response
            .andExpect(status().isConflict())
  }


  def "bad status for approving a PENDING privileged action"() {
    given:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                      .withPendingInvalidation()
            }

    when:
    def resultActions = mvc.perform(put("/privileged-actions/voting-materials-invalidation/" + id.getVotingMaterialInvalidationId())
            .param("status", "PENDING").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isBadRequest())
  }

  def "user doesn't have the right to approve request"() {
    given:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                      .withPendingInvalidation()
            }

    when:
    def resultActions = mvc.perform(put("/privileged-actions/voting-materials-invalidation/" + id.getVotingMaterialInvalidationId())
            .param("status", "APPROVED").with(user("user1"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isForbidden())
  }

  def "approve a PENDING privileged action"() {
    given:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                      .withPendingInvalidation()
            }

    when:
    def resultActions = mvc.perform(put("/privileged-actions/voting-materials-invalidation/" + id.getVotingMaterialInvalidationId())
            .param("status", "APPROVED").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isNoContent())
  }

  def "approve and already approved action"() {
    given:
    operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                      .withApprovedInvalidation()
            }


    when:
    def resultActions = mvc.perform(put("/privileged-actions/voting-materials-invalidation/2")
            .param("status", "APPROVED").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isNotFound())
  }

  def "action to approve doesn't exists"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/voting-materials-invalidation/-1")
            .param("status", "APPROVED").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isNotFound())
  }
}
