/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest

import ch.ge.ve.chvote.pactback.repository.user.entity.User
import groovy.sql.BatchingStatementWrapper
import groovy.sql.Sql
import groovy.transform.CompileStatic
import javax.persistence.Table
import javax.sql.DataSource
import org.springframework.stereotype.Component

/**
 * A test-dedicated component to clear our H2 database.
 * <p>
 *   Our context in this module assumes a database where the schema has been initialized, the "basic 3 users"
 *   inserted, and all sequences starting at their lowest index.
 *   <br>
 *   Some tests might change the database data, so this component should help us restoring this expected state.
 */
@Component
@CompileStatic
class H2DatabaseCleaner {

  private static final String USER_TABLE = User.getAnnotation(Table).name()

  private final Sql sql

  H2DatabaseCleaner(DataSource dataSource) {
    this.sql = new Sql(dataSource)
  }

  private List<String> defineSequencesReinitSql() {
    def rows = sql.rows("select * from information_schema.sequences where SEQUENCE_SCHEMA = 'PUBLIC'")
    return rows.collect {
      "ALTER SEQUENCE ${it.SEQUENCE_NAME} RESTART WITH ${it.MIN_VALUE}".toString()
    }
  }

  /**
   * Truncate all tables except the User's, and reset all sequences to their lowest value.
   */
  void cleanupDatabase() {
    sql.execute("SET REFERENTIAL_INTEGRITY FALSE")

    def tables = sql.rows("SHOW TABLES").collect { it.TABLE_NAME }.toSet()
    tables.remove(USER_TABLE)

    def sequencesSql = defineSequencesReinitSql()

    sql.withBatch { BatchingStatementWrapper stmt ->
      tables.each {
        stmt.addBatch("TRUNCATE TABLE $it")
      }

      reinitSequencesInBatch(stmt, sequencesSql)
    }

    sql.execute("SET REFERENTIAL_INTEGRITY TRUE")
  }

  /**
   * Reset all sequences to their lowest value.
   */
  void reinitSequences() {
    def sequencesSql = defineSequencesReinitSql()
    sql.withBatch( H2DatabaseCleaner.&reinitSequencesInBatch.rcurry(sequencesSql) )
  }

  private static void reinitSequencesInBatch(BatchingStatementWrapper stmt, List<String> sequencesSql) {
    sequencesSql.each( stmt.&addBatch )
  }

}
