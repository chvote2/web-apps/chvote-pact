/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.AliasFor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

/**
 * Meta-annotation Spring to declare an integration test for REST endpoints.
 * <p>
 *   This starts a {@link SpringBootTest} with auto-configured {@code MockMvc}, and active profiles determined
 *   by our {@link TestProfileResolver}.
 * </p>
 * <p>
 *   The whole Pact server is started by SpringBoot, with the Rabbit part excluded from SpringBoot auto-configuration
 *   and with mocked EventBus and rabbit-oriented beans (see {@link MockedRabbitConfiguration}).
 * </p>
 * <p>
 *   The {@link ContextConfiguration} can be "overridden" via the {@link #context()} attribute. When you do it,
 *   you should include {@link MockedRabbitConfiguration} within the declared classes.
 * </p>
 */
// Note : declared as a Java class, as groovyc does not allow the usage of Spring type-targeting annotations in an annotation
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootTest(
    properties = {
        "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration",
        // Allows overriding of already defined beans by mocks
        "spring.main.allow-bean-definition-overriding=true"
    })
@ContextConfiguration
@AutoConfigureMockMvc
@ActiveProfiles(resolver = TestProfileResolver.class)
public @interface RestEndpointTest {

  /**
   * The @Configuration classes to be used as the test context.
   * Should include {@link MockedRabbitConfiguration} (default) to be coherent with the disabled RabbitAutoConfiguration
   */
  @AliasFor(annotation = ContextConfiguration.class, attribute = "classes")
  Class<?>[] context() default { MockedRabbitConfiguration.class };

}
