/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest;

import ch.ge.ve.chvote.pactback.PactBackendApplication;
import ch.ge.ve.service.EventBus;
import org.mockito.Mockito;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

/**
 * Overrides the protocol-defined Rabbit-oriented beans, as they are often not part of the test scope
 *
 * @see RestEndpointTest
 */
@org.springframework.boot.test.context.TestConfiguration
@Import(PactBackendApplication.class)
public class MockedRabbitConfiguration {

  @Bean
  public EventBus eventBus() {
    return Mockito.mock(EventBus.class);
  }

  @Bean
  public RabbitAdmin rabbitAdmin() {
    RabbitAdmin rabbitAdmin = Mockito.mock(RabbitAdmin.class, Mockito.RETURNS_DEEP_STUBS);
    Mockito.when(rabbitAdmin.declareQueue()).thenReturn(new Queue("test_queue"));
    return rabbitAdmin;
  }

  @Bean
  public ConnectionFactory connectionFactory() {
    return Mockito.mock(ConnectionFactory.class, Mockito.RETURNS_DEEP_STUBS);
  }

  @Bean
  public RabbitTemplate rabbitTemplate() {
    return Mockito.mock(RabbitTemplate.class, Mockito.RETURNS_DEEP_STUBS);
  }

}
