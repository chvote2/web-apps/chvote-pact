/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, protractor } from 'protractor';
import { RequestOptions } from 'http';
import { log } from 'util';
import { promise } from 'selenium-webdriver';

const http = require('http');

function request(path: string) {
  let deferred = protractor.promise.defer();
  let options: RequestOptions = {
    hostname: browser.params.mockServer.hostname,
    port: browser.params.mockServer.port,
    path: path,
    headers: {},
    method: 'GET',
  };

  let callback = function (res) {
    let data = "";
    res.on('data', (chunk) => {
      data += chunk;
    });
    res.on('end', () => {
      deferred.fulfill(data);
    });
  };

  http.request(options, callback).end();

  return deferred.promise;
}

function requestWithCallBack(path: string, onResultCallBack: (any) => any) {
  browser.wait(() => request(path)
    .then(data => {
      try {
        onResultCallBack(data);
      }
      catch (e) {
        return false;
      }
      return true;
    }), 30000);
}

export function resetDB() {
  log('resetting database');
  return request('/mock/database/reset');
}

export function createOperations() {
  log('Create operations');
  return request('/mock/operations/create');
}


export interface UseCaseId {
  deployedConfigurationId: number
  testConfigurationId: number,
  votingMaterialConfigurationId: number,
  votingMaterialInvalidationId: number,
  clientId: string,
  operationId: number,
  protocolId: string
}

export function initForUseCase(useCase: string): promise.Promise<UseCaseId> {
  browser.controlFlow().execute(() => resetDB());
  browser.controlFlow().execute(() => log('Create usecase ' + useCase));
  return browser.controlFlow()
    .execute(() => request('/mock/operations/test-case/' + useCase))
    .then(value => JSON.parse(<string>value));
}



