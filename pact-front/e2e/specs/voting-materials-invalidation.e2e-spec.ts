/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { PACTController } from '../controller/pact.po';
import { LoginTestHelper } from '../helper/LoginTestHelper';
import { PrivilegedActionsController } from '../controller/privileged-actions.po';
import { initForUseCase } from '../helper/mock-server';
import { log } from 'util';
import { ActionDetailController } from '../controller/action-detail.po';
import { VotingMaterialsInvalidationDetailController } from '../controller/voting-materials-invalidation-detail.po';
import { ActionTableController } from '../controller/action-table.po';


describe('Voting materials invalidation', () => {
  beforeAll((done) => {
    PACTController.open().then(done);
  });

  LoginTestHelper.testValidLogin('user2', 'password');

  describe('Creation', () => {
    beforeAll((done) => {
      initForUseCase("voting-material-created")
        .then(value =>
          PACTController.openPage(`/voting-materials-invalidation/${value.votingMaterialConfigurationId}`)
        ).then(done);

    });

    it('should allow user to create a new request', (done) => {
      ActionDetailController.createAction();
      expect(ActionTableController.numberOfRows).toBe(1);
      done();
    });
  });


  describe('Rejection', () => {
    beforeAll((done) => {
      initForUseCase("voting-material-with-pending-invalidation")
        .then(value => log(JSON.stringify(value)))
        .then(() => PACTController.open())
        .then(done);
    });

    it('Invalidation action should be available in privileged action grid page', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(1);
        expect(PrivilegedActionsController.numberOfColumns).toBe(6);
        done();
      });
    });

    it('should navigate to privileged action detail page', (done) => {
      PrivilegedActionsController.openPrivilegedActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle)
          .toBe('Operation Name - Demander l\'invalidation du matériel de vote');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user1');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toBe('18.10.2040 00:00');
        expect(ActionDetailController.getValueByFieldName('operationDate')).toBe('10.10.2041');

        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeTruthy();
        expect(ActionDetailController.hasRejectButton).toBeTruthy();
        done();
      });
    });


    it('should provide voting material statistics', (done) => {
      expect(VotingMaterialsInvalidationDetailController.nbrOfPrinter).toBe(2);
      expect(VotingMaterialsInvalidationDetailController.totalRealCardCount(1)).toBe(30);
      expect(VotingMaterialsInvalidationDetailController.totalTestingCardCount(1)).toBe(0);
      VotingMaterialsInvalidationDetailController.openExpansionPanel(1);
      expect(VotingMaterialsInvalidationDetailController.hasRealCardStatistics(1)).toBeTruthy();
      expect(VotingMaterialsInvalidationDetailController.getRealCardCount(1, "Counting-circle-1")).toBe(10);
      expect(VotingMaterialsInvalidationDetailController.getRealCardCount(1, "Counting-circle-2")).toBe(20);
      expect(VotingMaterialsInvalidationDetailController.hasTestCardStatistics(1)).toBeFalsy();

      expect(VotingMaterialsInvalidationDetailController.totalRealCardCount(2)).toBe(15);
      expect(VotingMaterialsInvalidationDetailController.totalTestingCardCount(2)).toBe(26);
      VotingMaterialsInvalidationDetailController.openExpansionPanel(2);
      expect(VotingMaterialsInvalidationDetailController.hasRealCardStatistics(2)).toBeTruthy();
      expect(VotingMaterialsInvalidationDetailController.getRealCardCount(2, "Counting-circle-2")).toBe(15);
      expect(VotingMaterialsInvalidationDetailController.hasTestCardStatistics(2)).toBeTruthy();
      expect(VotingMaterialsInvalidationDetailController.controllerTestingCardCount(2)).toBe(5);
      expect(VotingMaterialsInvalidationDetailController.nonPrintableTestingCardCount(2)).toBe(7);
      expect(VotingMaterialsInvalidationDetailController.printableTestingCardCount(2)).toBe(6);
      expect(VotingMaterialsInvalidationDetailController.printerTestingCardCount(2)).toBe(8);
      done();
    });

    it("should allow user to reject voting material invalidation", (done) => {
      ActionDetailController.rejectAction();
      expect(ActionTableController.numberOfRows).toBe(0);
      expect(ActionTableController.emptyTableIsDisplayed).toBeTruthy();
      done();
    });

  });

  describe('Validation', () => {
    beforeAll((done) => {
      initForUseCase("voting-material-with-pending-invalidation")
        .then(value => log(JSON.stringify(value)))
        .then(() => PACTController.open())
        .then(done);
    });

    it('should allow user to validate', (done) => {
      PrivilegedActionsController.openPrivilegedActionByPosition(0).then(() => {
        ActionDetailController.approveAction();
        expect(ActionTableController.numberOfRows).toBe(0);
        expect(ActionTableController.emptyTableIsDisplayed).toBeTruthy();
        done();
      });
    });

  });

  LoginTestHelper.testLogout();
});



