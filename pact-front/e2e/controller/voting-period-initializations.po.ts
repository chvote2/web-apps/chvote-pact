/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions } from 'protractor';
import { SidenavController } from './sidenav.po';

export class VotingPeriodInitializationsController {

  static get votingPeriodConfigurationList() {
    return element(by.css('#voting-period-initializations-list'));
  }

  static get isVotingPeriodConfigurationsPage() {
    return VotingPeriodInitializationsController.votingPeriodConfigurationList.isPresent();
  }

  private static get sidenavLink() {
    return element(by.css('#pact-voting-period-initialization-menu-link'));
  }

  static open() {
    return SidenavController.openSideNav()
      .then(
        () => browser.wait(ExpectedConditions.elementToBeClickable(VotingPeriodInitializationsController.sidenavLink)))
      .then(() => VotingPeriodInitializationsController.sidenavLink.click())
      .then(() => browser.wait(
        ExpectedConditions.presenceOf(VotingPeriodInitializationsController.votingPeriodConfigurationList)));
  }
}
