/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions } from 'protractor';
import { SidenavController } from './sidenav.po';

export class PrivilegedActionsController {

  private static get privilegedActionsTable() {
    return element(by.css('#privilegedActionsTable'));
  }

  static get isPrivilegedActionsPage() {
    return this.privilegedActionsTable.isPresent();
  }

  private static get privilegedActionsRows() {
    return element.all(by.css('#privilegedActionsTable > mat-row'));
  }

  static get numberOfRows() {
    return this.privilegedActionsRows.count();
  }

  static get numberOfColumns() {
    return element.all(by.css('#privilegedActionsTable > mat-header-row  > mat-header-cell')).count();
  }

  private static getPrivilegedActionByPosition(pos: number) {
    return element.all(by.css('#privilegedActionsTable > mat-row button')).get(pos);
  }

  private static getPrivilegedActionTab(index: number) {
    return element.all(by.className('mat-tab-label')).get(index);
  }

  static openPrivilegedActionByPosition(pos: number) {
    return browser.wait(
      ExpectedConditions.elementToBeClickable(PrivilegedActionsController.getPrivilegedActionByPosition(pos))
    ).then(() => PrivilegedActionsController.getPrivilegedActionByPosition(pos).click());
  }

  static splitPrivilegedActions(index: number) {
    return browser.wait(
      ExpectedConditions.elementToBeClickable(PrivilegedActionsController.getPrivilegedActionTab(index))
    ).then(() => PrivilegedActionsController.getPrivilegedActionTab(index).click());
  }

  private static get sidenavLink() {
    return element(by.css('#pact-privileged-actions-menu-link'));
  }

  static open() {
    return SidenavController.openSideNav()
      .then(() => browser.wait(ExpectedConditions.elementToBeClickable(PrivilegedActionsController.sidenavLink)))
      .then(() => PrivilegedActionsController.sidenavLink.click())
      .then(() => browser.wait(ExpectedConditions.presenceOf(PrivilegedActionsController.privilegedActionsTable)))
      .then(() => PrivilegedActionsController.splitPrivilegedActions(0));
  }
}
