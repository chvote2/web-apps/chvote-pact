/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions } from 'protractor';
import { ActionTableController } from './action-table.po';

export class ActionDetailController {
  static get cardTitleElmt() {
    return element(by.css('mat-card-title'));
  }

  static get cardTitle() {
    return this.cardTitleElmt.getText();
  }

  static getValueByFieldName(fieldName: string) {
    return element(by.className('action-cell-' + fieldName)).getText();
  }

  static getElementByFieldName(fieldName: string) {
    return element(by.className(fieldName)).getText();
  }

  static get createBtn() {
    return element(by.id('createActionBtn'));
  }

  static get hasCreateButton() {
    return this.createBtn.isPresent();
  }

  static get approveBtn() {
    return element(by.id('approveActionBtn'));
  }

  static createAction() {
    return browser.wait(ExpectedConditions.elementToBeClickable(this.createBtn))
      .then(() => this.createBtn.click())
      .then(() => this.confirmDialog());
  }

  static get hasApproveButton() {
    return this.approveBtn.isPresent();
  }

  static approveAction() {
    return browser.wait(ExpectedConditions.elementToBeClickable(this.approveBtn))
      .then(() => this.approveBtn.click().then(this.confirmDialog.bind(this)));
  }

  static get rejectBtn() {
    return element(by.id('rejectActionBtn'));
  }

  static get hasRejectButton() {
    return this.rejectBtn.isPresent();
  }

  static rejectAction() {
    return browser.wait(ExpectedConditions.elementToBeClickable(this.rejectBtn))
      .then(() => this.rejectBtn.click().then(this.confirmDialog.bind(this)));
  }

  static get confirmationBtn() {
    return element(by.id('confirmation-dialog-confirm'));
  }

  static confirmDialog() {
    return browser.wait(ExpectedConditions.elementToBeClickable(this.confirmationBtn))
      .then(() => this.confirmationBtn.click())
      .then(() => browser.wait(ExpectedConditions.presenceOf(ActionTableController.actionTable)));
  }
}
