/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts
var path = require('path');

const {SpecReporter} = require('jasmine-spec-reporter');
var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');

var htmlReporter = new HtmlScreenshotReporter({
  dest: 'test_reports/protractor',
  filename: 'index.html',
  reportOnlyFailedSpecs: false,
  captureOnlyFailedSpecs: true,
  reportTitle: 'CHVote PACT: End to End Tests'
});

exports.config = {
  allScriptsTimeout: 120000,
  getPageTimeout: 120000,
  params: {
    mockServer: {
      hostname: "127.0.0.1",
      port: 47646
    }
  },
  multiCapabilities: [{
    'browserName': 'chrome',
    'screenWidth': '1600',
    'chromeOptions': {
      'args': ['--window-size=1600,900', '--no-sandbox', '--headless', '--disable-gpu'],
      // Set download path and avoid prompting for download
      prefs: {
        'download': {
          'prompt_for_download': false,
          'directory_upgrade': true,
          'default_directory': path.join(__dirname, '/test_reports/protractor/1600')
        },
        'safebrowsing': {
          'enabled': true
        }
      }
    },
    specs: [
      './e2e/**/*.e2e-spec.ts'
    ],
  }, {
    'browserName': 'chrome',
    'screenWidth': '1024',
    'chromeOptions': {
      'args': ['--window-size=1024,780', '--no-sandbox', '--headless', '--disable-gpu'],
      // Set download path and avoid prompting for download
      prefs: {
        'download': {
          'prompt_for_download': false,
          'directory_upgrade': true,
          'default_directory': path.join(__dirname, '/test_reports/protractor/1024')
        },
        'safebrowsing': {
          'enabled': true
        }
      }
    },
    specs: ['./e2e/**/*.e2e-1024-spec.ts']
  }],
  directConnect: true,
  maxSessions: 1,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 60000,
    print: function () {
    }
  },
  beforeLaunch: function () {
    return new Promise(function (resolve) {
      htmlReporter.beforeLaunch(resolve);
    });
  },
  onPrepare() {
    require('ts-node').register({project: 'e2e/tsconfig.e2e.json'});
    jasmine.getEnv().addReporter(new SpecReporter({spec: {displayStacktrace: true}}));
    jasmine.getEnv().addReporter(htmlReporter);
  },
  afterLaunch: function (exitCode) {
    return new Promise(function (resolve) {
      htmlReporter.afterLaunch(resolve.bind(this, exitCode));
    });
  }
};
