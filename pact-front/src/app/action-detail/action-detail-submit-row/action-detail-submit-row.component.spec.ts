/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { ActionDetailSubmitRowComponent } from './action-detail-submit-row.component';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from '../../core/confirmation-dialog/confirmation-dialog.component';
import { of } from 'rxjs';
import { AuthenticationService } from '../../authentication/authentication.service';
import { PrivilegedAction } from '../../privileged-actions/model/privileged-action';

describe('ActionDetailSubmitRowComponent', () => {
  let component: ActionDetailSubmitRowComponent;
  const dialog: MatDialog = <any> {
    open: jasmine.createSpy('open').and.callFake((componentRef, config) => {
      expect(componentRef).toBe(ConfirmationDialogComponent);
      expect(config.data.title).toBeDefined();
      expect(config.data.content).toBeDefined();
      return {
        afterClosed: jasmine.createSpy('afterClosed').and.returnValue(of({
          reason: 'reason',
          accepted: true
        }))
      };
    })
  };
  const authService: AuthenticationService = <any> {
    currentUser: 'Fred'
  };


  it('should retrieve and approve a pending action', () => {
    const action: PrivilegedAction = {
      id: 1,
      ownerName: 'user1',
      operationDate: new Date(Date.now()),
      creationDate: new Date(Date.now()),
      statusDate: new Date(Date.now()),
      operationName: '201709VP',
      status: 'PENDING',
      type: 'CONFIGURATION_DEPLOYMENT'
    };

    component = new ActionDetailSubmitRowComponent(dialog, authService);
    component.onApprove = <any> {
      emit: jasmine.createSpy('emit')
    };

    component.action = action;
    component.ngOnInit();
    component.ngOnChanges();

    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('PENDING');
    expect(component.canApprove).toBeTruthy();

    component.openApproveActionDialog();

    expect(component.onApprove.emit).toHaveBeenCalled();
  });

  it('should retrieve and reject a pending action', () => {
    const action: PrivilegedAction = {
      id: 1,
      ownerName: 'user1',
      operationDate: new Date(Date.now()),
      creationDate: new Date(Date.now()),
      statusDate: new Date(Date.now()),
      operationName: '201709VP',
      status: 'PENDING',
      type: 'CONFIGURATION_DEPLOYMENT'
    };

    component = new ActionDetailSubmitRowComponent(dialog, authService);
    component.onReject = <any> {
      emit: jasmine.createSpy('emit')
    };

    component.action = action;
    component.ngOnInit();
    component.ngOnChanges();

    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('PENDING');
    expect(component.canApprove).toBeTruthy();

    component.openRejectActionDialog();

    expect(component.onReject.emit).toHaveBeenCalledWith('reason');
  });

  it('should retrieve and create a pending action', () => {
    const action: PrivilegedAction = {
      id: null,
      ownerName: 'Fred',
      operationDate: new Date(Date.now()),
      creationDate: new Date(Date.now()),
      statusDate: new Date(Date.now()),
      operationName: '201709VP',
      status: 'NOT_CREATED',
      type: 'CONFIGURATION_DEPLOYMENT'
    };

    component = new ActionDetailSubmitRowComponent(dialog, authService);
    component.onCreateAction = <any> {
      emit: jasmine.createSpy('emit')
    };

    component.action = action;
    component.ngOnInit();
    component.ngOnChanges();

    expect(component.action.id).toBe(null);
    expect(component.action.status).toBe('NOT_CREATED');
    expect(component.canApprove).toBeFalsy();

    component.openCreateActionDialog();

    expect(component.onCreateAction.emit).toHaveBeenCalled();
  });

});
