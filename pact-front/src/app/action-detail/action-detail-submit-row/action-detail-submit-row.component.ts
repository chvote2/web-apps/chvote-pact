/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { AuthenticationService } from '../../authentication/authentication.service';
import { PrivilegedAction } from '../../privileged-actions/model/privileged-action';
import { MatDialog } from '@angular/material';
import { isNullOrUndefined } from 'util';
import { ConfirmationDialogComponent } from '../../core/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'pact-action-detail-submit-row',
  templateUrl: './action-detail-submit-row.component.html',
  styleUrls: ['./action-detail-submit-row.component.scss']
})
export class ActionDetailSubmitRowComponent implements OnInit, OnChanges {
  @Input() i18nLabel: string;
  @Input() action: PrivilegedAction;
  @Output() onCreateAction: EventEmitter<any> = new EventEmitter();
  @Output() onApprove: EventEmitter<any> = new EventEmitter();
  @Output() onReject: EventEmitter<any> = new EventEmitter();

  private _canApprove: boolean;

  constructor(private dialog: MatDialog,
              private authService: AuthenticationService) {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    const currentUser = this.authService.currentUser;
    this._canApprove = currentUser !== this.action.ownerName &&
                                       this.action.status !== 'APPROVED' &&
                                       this.action.status !== 'REJECTED';
  }

  get canApprove(): boolean {
    return this._canApprove;
  }

  private openDialog(title: string, content: string, contentParams: { [key: string]: string },
                     success: (reason: string) => void, inputPlaceHolder?: string) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: {
        title: title,
        content: content,
        contentParams: contentParams,
        hasInput: !isNullOrUndefined(inputPlaceHolder),
        inputPlaceHolder: inputPlaceHolder
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.accepted) {
        success.bind(this)(result.reason);
      }
    });
  }

  openCreateActionDialog() {
    this.openDialog(
      this.i18nLabel + '.detail.dialog.create.title',
      this.i18nLabel + '.detail.dialog.create.content',
      {'operationName': this.action.operationName},
      this.createAction);
  }

  openApproveActionDialog() {
    this.openDialog(
      this.i18nLabel + '.detail.dialog.approve.title',
      this.i18nLabel + '.detail.dialog.approve.content',
      {'operationName': this.action.operationName},
      this.approveAction);
  }

  openRejectActionDialog() {
    this.openDialog(
      this.i18nLabel + '.detail.dialog.reject.title',
      this.i18nLabel + '.detail.dialog.reject.content',
      {'operationName': this.action.operationName},
      this.rejectAction,
      this.i18nLabel + '.detail.dialog.reject.input');
  }

  private createAction() {
    this.onCreateAction.emit();
  }

  private approveAction() {
    this.onApprove.emit();
  }

  private rejectAction(reason: string) {
    this.onReject.emit(reason);
  }
}
