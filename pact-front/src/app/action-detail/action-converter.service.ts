/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Injectable } from '@angular/core';
import { ConfigurationDeployment } from '../configuration-deployment/model/configuration-deployment';
import { ActionMetadata } from './model/action-metadata';
import { Attachment } from '../configuration-deployment/model/attachment';
import { VotingMaterialsCreation } from '../voting-materials-creation/model/voting-materials-creation';
import { PrinterConfiguration } from '../voting-materials-creation/model/printer-configuration';
import { VotersRegisterFile } from '../voting-materials-creation/model/voters-register-file';
import { VotingMaterialsInvalidation } from '../voting-materials-invalidation/model/voting-materials-invalidation';
import { VotingPeriodInitialization } from '../voting-period-initialization/model/voting-period-initialization';

@Injectable()
export class ActionConverterService {

  constructor() {
  }

  convertConfigurationDeployment(action: ConfigurationDeployment): ActionMetadata {
    return <ActionMetadata>{
      operationName: {value: action.operationName},
      version: {value: action.operationConfiguration.version},
      gracePeriod: {value: action.operationConfiguration.gracePeriod},
      siteTextHash: {value: action.operationConfiguration.siteTextHash, fake: true},
      operationDate: {value: action.operationDate, type: 'DATE'},
      statusDate: {value: action.statusDate, type: 'DATETIME'},
      siteOpeningDate: {value: action.operationConfiguration.siteOpeningDate, type: 'DATETIME'},
      siteClosingDate: {value: action.operationConfiguration.siteClosingDate, type: 'DATETIME'},
      ownerName: {value: action.ownerName},
      status: {value: `configuration-deployments.status.${action.status}`, type: 'TRANSLATION_LABEL'},
      action: {
        value: action.status === 'PENDING' ?
          `/pending-actions/configuration-deployments/${action.id}` :
          `/configuration-deployments/${action.operationConfiguration.id}`,
        type: 'DETAILS_LINK'
      }
    };
  }

  convertAttachment(attachment: Attachment): ActionMetadata {
    return <ActionMetadata>{
      name: {value: attachment.name},
      type: {
        value: `configuration-deployments.attachments.type.${attachment.type}`,
        type: 'TRANSLATION_LABEL'
      },
      signatureAuthor: {value: attachment.signatureAuthor, fake: true},
      signatureStatus: {
        value: `configuration-deployments.attachments.signatureStatus.${attachment.signatureStatus}`,
        type: 'TRANSLATION_LABEL', fake: true
      },
      importDate: {value: attachment.importDate, type: 'DATETIME'},
      generationDate: {value: attachment.generationDate, type: 'DATETIME'},
      messageId: {value: attachment.messageId},
      sourceApplication: {
        value: attachment.sourceApplicationManufacturer + ' ' +
               attachment.sourceApplicationProduct + ' ' +
               attachment.sourceApplicationProductVersion
      },
      signatureExpiryDate: {value: attachment.signatureExpiryDate, type: 'DATETIME', fake: true},
      fileHash: {value: attachment.fileHash, fake: true}
    };
  }

  convertVotingMaterialsCreation(action: VotingMaterialsCreation): ActionMetadata {
    return <ActionMetadata>{
      operationName: {value: action.operationName},
      operationDate: {value: action.operationDate, type: 'DATE'},
      statusDate: {value: action.statusDate, type: 'DATETIME'},
      siteOpeningDate: {value: action.votingMaterialsConfiguration.configuration.siteOpeningDate, type: 'DATETIME'},
      siteClosingDate: {value: action.votingMaterialsConfiguration.configuration.siteClosingDate, type: 'DATETIME'},
      status: {value: `voting-materials-creation.status.${action.status}`, type: 'TRANSLATION_LABEL'},
      ownerName: {value: action.ownerName},
      votingCardLabel: {value: action.votingMaterialsConfiguration.votingCardLabel},
      target: {
        value: `voting-materials-creation.target.${action.votingMaterialsConfiguration.target}`,
        type: 'TRANSLATION_LABEL'
      },
      simulationName: {value: action.votingMaterialsConfiguration.simulationName},
      digitalHash1: {value: action.votingMaterialsConfiguration.digitalHash1, fake: true},
      digitalHash2: {value: action.votingMaterialsConfiguration.digitalHash2, fake: true},
      votingSiteUrl: {value: action.votingMaterialsConfiguration.votingSiteUrl, fake: true},
      action: {
        value: action.status === 'PENDING' ?
          `/pending-actions/voting-materials-creation/${action.id}` :
          `/voting-materials-creation/${action.votingMaterialsConfiguration.id}`,
        type: 'DETAILS_LINK'
      }
    };
  }

  static convertPrinterConfiguration(printer: PrinterConfiguration): ActionMetadata {
    return {
      printerName: {value: printer.printerName},
      votingCardTypes: {
        value: `voting-materials-creation.printers.cardTypes.${printer.votingCardType}`,
        type: 'TRANSLATION_LABEL'
      },
      showMunicipalities: {value: printer.votingCardType === 'REAL' && !printer.swissAbroadWithoutMunicipality},
      swissAbroadWithoutMunicipality: {value: printer.swissAbroadWithoutMunicipality},
      printerHash: {value: printer.publicKeyHash, fake: true},
      municipalities: {value: printer.municipalities.map(elem => elem.name).sort()}
    } as ActionMetadata;
  }

  convertVotersRegisterFile(register: VotersRegisterFile): ActionMetadata {
    const metadata = this.convertAttachment(register);

    metadata['nbOfVoters'] = {value: register.nbOfVoters};

    return metadata;
  }


  convertVotingPeriodCreation(action: VotingPeriodInitialization): ActionMetadata {
    return {
      operationName: {value: action.operationName},
      operationDate: {value: action.operationDate, type: 'DATE'},
      statusDate: {value: action.statusDate, type: 'DATETIME'},
      siteOpeningDate: {value: action.votingPeriodConfiguration.siteOpeningDate, type: 'DATETIME'},
      siteClosingDate: {value: action.votingPeriodConfiguration.siteClosingDate, type: 'DATETIME'},
      status: {value: `voting-period-initialization.status.${action.status}`, type: 'TRANSLATION_LABEL'},
      ownerName: {value: action.ownerName},
      target: {
        value: `voting-period-initialization.target.${action.votingPeriodConfiguration.target}`,
        type: 'TRANSLATION_LABEL'
      },
      simulationName: {value: action.votingPeriodConfiguration.simulationName},
      action: {
        value: action.status === 'PENDING' ?
          `/pending-actions/voting-period-initialization/${action.id}` :
          `/voting-period-initialization/${action.votingPeriodConfiguration.id}`,
        type: 'DETAILS_LINK'
      },
      electoralAuthorityKey: {value: action.votingPeriodConfiguration.electoralAuthorityKey, fake: true}
    } as ActionMetadata;
  }


  convertVotingMaterialsInvalidation(action: VotingMaterialsInvalidation) {
    return {
      operationName: {value: action.operationName},
      operationDate: {value: action.operationDate, type: 'DATE'},
      statusDate: {value: action.statusDate, type: 'DATETIME'},
      status: {value: `voting-materials-invalidation.status.${action.status}`, type: 'TRANSLATION_LABEL'},
      ownerName: {value: action.ownerName},
      action: {
        value: action.status === 'PENDING' ?
          `/pending-actions/voting-materials-invalidation/${action.id}` :
          `/voting-materials-invalidation/${action.votingMaterialsConfigurationId}`,
        type: 'DETAILS_LINK'
      }
    } as ActionMetadata;
  }
}
