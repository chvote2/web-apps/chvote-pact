/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { URLSearchParams } from '@angular/http';
import { PollingService } from '../polling/polling.service';
import { Observable } from 'rxjs';
import { PrivilegedAction } from '../privileged-actions/model/privileged-action';

export class ActionDetailService<T extends PrivilegedAction> {

  constructor(private httpClient: HttpClient,
              private pollingService: PollingService,
              private baseUrl: string) {
  }

  get newOrPending(): Observable<T[]> {
    return this.pollingService.poll<T[]>(`${this.baseUrl}/new-or-pending/`);
  }

  getByBusinessId(businessId: number): Observable<T> {
    const params = new HttpParams().set('businessId', businessId.toString());
    return this.httpClient.get<T>(this.baseUrl, {params: params});
  }

  getById(actionId: number): Observable<T> {
    return this.httpClient.get<T>(`${this.baseUrl}/${actionId}`);
  }

  createAction(id: number): Observable<any> {
    const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    const urlSearchParams = new URLSearchParams();
    urlSearchParams.append('businessId', id.toString());

    return Observable.create((observer) => {
      return this.httpClient.put(this.baseUrl, urlSearchParams.toString(), {headers: headers})
        .subscribe((result) => {
          this.pollingService.broadcast();
          observer.next(result);
        });
    });
  }

  private updateStatus(id: number, status: string, rejectionReason: string): Observable<any> {
    const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    const urlSearchParams = new URLSearchParams();
    urlSearchParams.append('status', status);
    urlSearchParams.append('rejectionReason', rejectionReason);

    return Observable.create((observer) => {
      return this.httpClient.put(`${this.baseUrl}/${id}`,
        urlSearchParams.toString(), {headers: headers}).subscribe((responseBody) => {
        this.pollingService.broadcast();
        observer.next(responseBody);
      });
    });
  }

  approve(id: number): Observable<any> {
    return this.updateStatus(id, 'APPROVED', null);
  }

  reject(id: number, rejectionReason: string): Observable<any> {
    return this.updateStatus(id, 'REJECTED', rejectionReason);
  }

}
