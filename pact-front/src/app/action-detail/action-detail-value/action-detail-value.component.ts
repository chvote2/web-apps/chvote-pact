/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Component, Input, OnInit } from '@angular/core';
import { ActionValue } from '../model/action-metadata';
import { Router } from '@angular/router';

@Component({
  selector: 'pact-action-detail-value',
  templateUrl: './action-detail-value.component.html',
  styleUrls: ['./action-detail-value.component.scss']
})
export class ActionDetailValueComponent implements OnInit {
  @Input() action: ActionValue;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  viewDetails(url: string) {
    this.router.navigate([url]);
  }
}
