/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { ActionTableComponent } from './action-table.component';

describe('ActionTableComponent', () => {
  let component: ActionTableComponent;

  it('should populate data source with incoming actions', (done) => {
    component = new ActionTableComponent();

    component.ngOnInit();
    expect(component.dataSource.empty).toBeTruthy();

    component.i18nLabel = 'configuration-deployments';
    component.actions = [{
      operationName: {value: '201710VP'},
      siteOpeningDate: {value: new Date(), type: 'DATE'},
      status: {value: 'PENDING', type: 'TRANSLATION_LABEL'},
      action: {value: '/pending-actions', type: 'DETAILS_LINK'}
    }];
    component.columns = ['operationName', 'siteOpeningDate', 'status', 'action'];

    component.ngOnChanges();

    component.dataSource.connect().subscribe(actions => {
      expect(actions.length).toBe(1);
      expect(component.dataSource.empty).toBeFalsy();
      done();
    });
  });
});
