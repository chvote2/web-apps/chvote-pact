/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { ConfigurationDeploymentTableComponent } from './configuration-deployment-table.component';
import { ConfigurationDeploymentService } from '../configuration-deployment.service';
import { of } from 'rxjs';
import { ConfigurationDeployment } from '../model/configuration-deployment';
import { ActionConverterService } from '../../action-detail/action-converter.service';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';

describe('ConfigurationDeploymentTableComponent', () => {

  let component: ConfigurationDeploymentTableComponent;
  let service: ConfigurationDeploymentService;

  it('should populate data source with incoming actions', () => {
    service = <any> {
      newOrPending: of(
        <ConfigurationDeployment[]> [
          {
            id: 1,
            ownerName: 'user1',
            creationDate: new Date(Date.now()),
            statusDate: new Date(Date.now()),
            operationName: '201709VP',
            status: 'PENDING',
            type: 'CONFIGURATION_DEPLOYMENT',
            operationConfiguration: {}
          }, {
            id: 2,
            ownerName: 'user2',
            creationDate: new Date(Date.now()),
            statusDate: new Date(Date.now()),
            operationName: '201709VP',
            status: 'PENDING',
            type: 'CONFIGURATION_DEPLOYMENT',
            operationConfiguration: {}
          }, {
            id: null,
            ownerName: 'user1',
            creationDate: null,
            statusDate: null,
            operationName: '201709VP',
            status: 'NOT_CREATED',
            type: 'CONFIGURATION_DEPLOYMENT',
            operationConfiguration: {}
          }
        ]
      )
    };

    component = new ConfigurationDeploymentTableComponent(service, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(component.rows.length).toBe(3);
  });
});
