/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ConfigurationDeploymentService } from '../configuration-deployment.service';
import { Subscription } from 'rxjs';
import { ActionMetadata } from '../../action-detail/model/action-metadata';
import { ActionConverterService } from '../../action-detail/action-converter.service';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';

@Component({
  templateUrl: './configuration-deployment-table.component.html',
  styleUrls: ['./configuration-deployment-table.component.scss']
})
export class ConfigurationDeploymentTableComponent implements OnInit, OnDestroy {

  constructor(private configurationDeploymentService: ConfigurationDeploymentService,
              private loadingSpinnerService: LoadingSpinnerService,
              private actionConverter: ActionConverterService) {
  }

  private _newOrPendingSubscription: Subscription;
  private _rows: ActionMetadata[] = [];

  get rows(): ActionMetadata[] {
    return this._rows;
  }

  ngOnInit(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this._newOrPendingSubscription = this.configurationDeploymentService.newOrPending.subscribe(
      actions => {
        this._rows = [];
        actions.forEach(action => {
          this._rows.push(this.actionConverter.convertConfigurationDeployment(action));
        });
        this.loadingSpinnerService.broadcastLoadingStatus(false);
      });
  }

  ngOnDestroy() {
    this._newOrPendingSubscription.unsubscribe();
  }
}
