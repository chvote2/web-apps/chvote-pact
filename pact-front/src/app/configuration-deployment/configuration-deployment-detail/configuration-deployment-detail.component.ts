/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/


import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ConfigurationDeploymentService } from '../configuration-deployment.service';
import { ConfigurationDeployment } from '../model/configuration-deployment';
import { Observable } from 'rxjs';

import { ActionMetadata } from '../../action-detail/model/action-metadata';
import { MatSnackBar } from '@angular/material';
import { SnackBarAlertComponent } from '../../core/snack-bar-alert/snack-bar-alert.component';
import { ActionConverterService } from '../../action-detail/action-converter.service';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';

@Component({
  templateUrl: './configuration-deployment-detail.component.html',
  styleUrls: ['./configuration-deployment-detail.component.scss']
})
export class ConfigurationDeploymentDetailComponent implements OnInit {
  action: ConfigurationDeployment;
  actionMetadata: ActionMetadata;
  attachmentMetadata: ActionMetadata[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private configurationDeploymentService: ConfigurationDeploymentService,
              private loadingSpinnerService: LoadingSpinnerService,
              private actionConverter: ActionConverterService) {
  }

  ngOnInit() {
    const actionObservable: Observable<ConfigurationDeployment> = this.route.paramMap.pipe(switchMap(
      (params: ParamMap) => {
        const id = parseInt(params.get('id'));

        if (this.router.url.startsWith('/pending-actions')) {
          return this.configurationDeploymentService.getById(id);
        } else {
          return this.configurationDeploymentService.getByBusinessId(id);
        }
      }));

    this.loadingSpinnerService.broadcastLoadingStatus(true);
    actionObservable.subscribe(action => {
      this.action = action;
      this.actionMetadata = this.actionConverter.convertConfigurationDeployment(action);
      this.attachmentMetadata = [];
      this.loadingSpinnerService.broadcastLoadingStatus(false);
      action.operationConfiguration.deploymentFiles.forEach(file => {
        this.attachmentMetadata.push(this.actionConverter.convertAttachment(file));
      });
    });
  }

  private onSuccess(messageLabel: string): void {
    this.loadingSpinnerService.broadcastLoadingStatus(false);
    this.router.navigate(['/configuration-deployments']);
    this.snackBar.openFromComponent(SnackBarAlertComponent, {
      duration: 2500,
      data: {
        message: 'configuration-deployments.detail.snack-bar.' + messageLabel,
        messageParams: {operationName: this.action.operationName}
      }
    });
  }

  private onError(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(false);
    this.snackBar.openFromComponent(SnackBarAlertComponent, {
      duration: 2500,
      data: {
        message: 'Error while saving.'
      }
    });
  }

  public onCreateAction(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this.configurationDeploymentService.createAction(this.action.operationConfiguration.id).subscribe(
      this.onSuccess.bind(this, 'created'),
      this.onError.bind(this));
  }

  public onApproveAction(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this.configurationDeploymentService.approve(this.action.id).subscribe(
      this.onSuccess.bind(this, 'approved'),
      this.onError.bind(this));
  }

  public onRejectAction(reason: string): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this.configurationDeploymentService.reject(this.action.id, reason).subscribe(
      this.onSuccess.bind(this, 'rejected'),
      this.onError.bind(this));
  }

}
