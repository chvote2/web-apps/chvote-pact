/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/


import { debounceTime } from 'rxjs/operators';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable()
export class LoadingSpinnerService {

  private _loadingEvents: EventEmitter<boolean> = new EventEmitter();

  constructor() {
  }

  get loadingEvents(): Observable<boolean> {
    return this._loadingEvents.pipe(debounceTime(500));
  }

  broadcastLoadingStatus(loading: boolean) {
    this._loadingEvents.emit(loading);
  }
}
