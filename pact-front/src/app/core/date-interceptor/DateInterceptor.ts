/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class DateInterceptor implements HttpInterceptor {
  static reJSDate: RegExp = /^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\.(\d{3})\]$/;

  intercept(req: HttpRequest<any>,
            next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({responseType: 'text'});
    return next.handle(req).pipe(map((evt) => {
      if (evt instanceof HttpResponse) {
        // use JSON parser with reviver to correctly parse Date objects
        evt = evt.clone({body: JSON.parse(evt.body, DateInterceptor.parseDateFields)});
        return evt;
      }
    }));
  }

  /**
   * JSON reviver function used to transform any date field (in JSDate format) into a Date object.
   *
   * @param key field's name
   * @param value field's value
   * @returns the field's value as Date object of its a date or the original value if not
   */
  static parseDateFields(key: string, value: any): any {
    if (typeof value === 'string') {
      let dateField = DateInterceptor.reJSDate.exec(value);
      if (dateField) {
        let day = dateField[1];
        let month = dateField[2];
        let year = dateField[3];
        let hours = dateField[4];
        let minutes = dateField[5];
        let seconds = dateField[6];

        return new Date(
          parseInt(year), parseInt(month) - 1, parseInt(day),
          parseInt(hours), parseInt(minutes), parseInt(seconds)
        );
      }
    }
    return value;
  }
}
