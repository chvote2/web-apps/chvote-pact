/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Component, OnDestroy, OnInit } from '@angular/core';
import { VotingPeriodInitializationService } from '../voting-period-initialization.service';
import { Subscription } from 'rxjs';
import { ActionMetadata } from '../../action-detail/model/action-metadata';
import { ActionConverterService } from '../../action-detail/action-converter.service';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';

@Component({
  templateUrl: './voting-period-initialization-table.component.html',
  styleUrls: ['./voting-period-initialization-table.component.scss']
})
export class VotingPeriodInitializationTableComponent implements OnInit, OnDestroy {

  constructor(private votingPeriodCreationService: VotingPeriodInitializationService,
              private loadingSpinnerService: LoadingSpinnerService,
              private actionConverter: ActionConverterService) {

  }

  private _newOrPendingSubscription: Subscription;
  private _rows: ActionMetadata[] = [];

  get rows(): ActionMetadata[] {
    return this._rows;
  }

  ngOnInit(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this._newOrPendingSubscription = this.votingPeriodCreationService.newOrPending
      .subscribe(
        actions => {
          this._rows = [];
          actions.forEach(action => {
            this._rows.push(this.actionConverter.convertVotingPeriodCreation(action));
          });
          this.loadingSpinnerService.broadcastLoadingStatus(false);
        });
  }

  ngOnDestroy() {
    this._newOrPendingSubscription.unsubscribe();
  }
}
