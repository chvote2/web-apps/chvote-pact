/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/


import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { VotingPeriodInitialization } from '../model/voting-period-initialization';
import { VotingPeriodInitializationService } from '../voting-period-initialization.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { SnackBarAlertComponent } from '../../core/snack-bar-alert/snack-bar-alert.component';
import { MatSnackBar } from '@angular/material';
import { ActionMetadata } from '../../action-detail/model/action-metadata';
import { ActionConverterService } from '../../action-detail/action-converter.service';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';

@Component({
  templateUrl: './voting-period-initialization-detail.component.html',
  styleUrls: ['./voting-period-initialization-detail.component.scss']
})
export class VotingPeriodInitializationDetailComponent implements OnInit {
  action: VotingPeriodInitialization;
  actionMetadata: ActionMetadata;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private votingPeriodCreationService: VotingPeriodInitializationService,
              private loadingSpinnerService: LoadingSpinnerService,
              private actionConverter: ActionConverterService) {
  }

  ngOnInit() {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    const actionObservable: Observable<VotingPeriodInitialization> = this.route.paramMap.pipe(switchMap(
      (params: ParamMap) => {
        const id = parseInt(params.get('id'));

        if (this.router.url.startsWith('/pending-actions')) {
          return this.votingPeriodCreationService.getById(id);
        } else {
          return this.votingPeriodCreationService.getByBusinessId(id);
        }
      }));

    actionObservable.subscribe(action => {
      this.action = action;
      this.actionMetadata = this.actionConverter.convertVotingPeriodCreation(action);
      this.loadingSpinnerService.broadcastLoadingStatus(false);
    });
  }

  public onCreateAction(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this.votingPeriodCreationService.createAction(this.action.votingPeriodConfiguration.id).subscribe(
      this.onSuccess.bind(this, 'created'),
      this.onError.bind(this));
  }

  public onApproveAction(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this.votingPeriodCreationService.approve(this.action.id).subscribe(
      this.onSuccess.bind(this, 'approved'),
      this.onError.bind(this));
  }

  public onRejectAction(reason: string): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this.votingPeriodCreationService.reject(this.action.id, reason).subscribe(
      this.onSuccess.bind(this, 'rejected'),
      this.onError.bind(this));
  }

  private onSuccess(messageLabel: string): void {
    this.loadingSpinnerService.broadcastLoadingStatus(false);
    this.router.navigate(['/voting-period-initialization']);
    this.snackBar.openFromComponent(SnackBarAlertComponent, {
      duration: 2500,
      data: {
        message: 'voting-period-initialization.detail.snack-bar.' + messageLabel,
        messageParams: {operationName: this.action.operationName}
      }
    });
  }

  private onError(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(false);
    this.snackBar.openFromComponent(SnackBarAlertComponent, {
      duration: 2500,
      data: {
        message: 'Error while saving.'
      }
    });
  }
}
