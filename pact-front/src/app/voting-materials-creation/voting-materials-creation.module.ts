/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { VotingMaterialsCreationRoutingModule } from './voting-materials-creation-routing.module';
import { VotingMaterialsCreationService } from './voting-materials-creation.service';
import { CoreModule } from '../core/core.module';
import { PollingModule } from '../polling/polling.module';
import { AuthenticationModule } from '../authentication/authentication.module';
import { MaterialModule } from '../material/material.module';
import { LayoutModule } from '../layout/layout.module';
import { VotingMaterialsCreationDetailComponent } from './voting-materials-creation-detail/voting-materials-creation-detail.component';
import { VotingMaterialsCreationTableComponent } from './voting-materials-creation-table/voting-materials-creation-table.component';
import { ActionDetailModule } from '../action-detail/action-detail.module';

@NgModule({
  imports: [
    CoreModule,
    PollingModule,
    MaterialModule,
    AuthenticationModule,
    LayoutModule,
    ActionDetailModule,
    VotingMaterialsCreationRoutingModule,
    FlexLayoutModule
  ],
  providers: [VotingMaterialsCreationService],
  declarations: [VotingMaterialsCreationDetailComponent, VotingMaterialsCreationTableComponent]
})
export class VotingMaterialsCreationModule {
}
