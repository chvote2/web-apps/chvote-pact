/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { OperationConfiguration } from '../../configuration-deployment/model/operation-configuration';
import { VotersRegisterFile } from './voters-register-file';
import { PrinterConfiguration } from './printer-configuration';

export interface VotingMaterialsConfiguration {
  id: number;
  author: string;
  submissionDate: Date;
  votingCardLabel: string;
  target: 'SIMULATION' | 'REAL';
  simulationName: string;
  digitalHash1: string;
  digitalHash2: string;
  votingSiteUrl: string;
  configuration: OperationConfiguration;
  registers: VotersRegisterFile[];
  printers: PrinterConfiguration[];
}
