/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { VotingMaterialsCreationDetailComponent } from './voting-materials-creation-detail.component';
import { ActivatedRoute, convertToParamMap, Params, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of } from 'rxjs';
import { VotingMaterialsCreationService } from '../voting-materials-creation.service';
import { ActionConverterService } from '../../action-detail/action-converter.service';
import { VotingMaterialsCreation } from '../model/voting-materials-creation';
import { VotingMaterialsConfiguration } from '../model/voting-materials-configuration';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';
import Spy = jasmine.Spy;

describe('VotingMaterialsCreationDetailComponent', () => {
  let component: VotingMaterialsCreationDetailComponent;

  const action1: VotingMaterialsCreation = {
    id: null,
    ownerName: 'user1',
    operationDate: new Date(Date.now()),
    creationDate: null,
    statusDate: null,
    operationName: '201709VP',
    status: 'NOT_CREATED',
    type: 'MATERIALS_CREATION',
    votingMaterialsConfiguration: <VotingMaterialsConfiguration>{
      id: 1,
      registers: [],
      printers: [],
      configuration: {}
    }
  };
  const action2: VotingMaterialsCreation = {
    id: 1,
    ownerName: 'user1',
    operationDate: new Date(Date.now()),
    creationDate: new Date(Date.now()),
    statusDate: new Date(Date.now()),
    operationName: '201709VP',
    status: 'PENDING',
    type: 'MATERIALS_CREATION',
    votingMaterialsConfiguration: <VotingMaterialsConfiguration>{
      id: 1,
      registers: [],
      printers: [],
      configuration: {}
    }
  };

  const route: ActivatedRoute = <any> {
    paramMap: of(convertToParamMap(<Params> {id: '1'}))
  };
  const snackBar: MatSnackBar = <any> {
    openFromComponent: jasmine.createSpy('openFromComponent')
  };
  const votingMaterialsCreationService: VotingMaterialsCreationService = <any> {
    getById: jasmine.createSpy('getById').and.returnValue(of(action2)),
    getByBusinessId: jasmine.createSpy('getByBusinessId').and
      .returnValue(of(action1)),
    createAction: jasmine.createSpy('createAction').and.returnValue(of(true)),
    approve: jasmine.createSpy('approve').and.returnValue(of(true)),
    reject: jasmine.createSpy('reject').and.returnValue(of(true))
  };

  beforeEach(() => {
    (votingMaterialsCreationService.getById as Spy).calls.reset();
    (votingMaterialsCreationService.getByBusinessId as Spy).calls.reset();
    (votingMaterialsCreationService.createAction as Spy).calls.reset();
    (votingMaterialsCreationService.approve as Spy).calls.reset();
    (votingMaterialsCreationService.reject as Spy).calls.reset();
  });

  it('should retrieve and approve a pending action', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/pending-actions/1'
    };

    component = new VotingMaterialsCreationDetailComponent(router, route, snackBar,
      votingMaterialsCreationService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(votingMaterialsCreationService.getById).toHaveBeenCalledTimes(1);
    expect(votingMaterialsCreationService.getById).toHaveBeenCalledWith(1);
    expect(votingMaterialsCreationService.getByBusinessId).not.toHaveBeenCalled();
    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('PENDING');

    component.onApproveAction();

    expect(votingMaterialsCreationService.approve).toHaveBeenCalledWith(1);
    expect(votingMaterialsCreationService.approve).toHaveBeenCalledTimes(1);
    expect(votingMaterialsCreationService.reject).not.toHaveBeenCalled();
    expect(votingMaterialsCreationService.createAction).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/voting-materials-creation']);
  });

  it('should retrieve and reject a pending action', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/pending-actions/1'
    };

    component = new VotingMaterialsCreationDetailComponent(router, route, snackBar,
      votingMaterialsCreationService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(votingMaterialsCreationService.getById).toHaveBeenCalledTimes(1);
    expect(votingMaterialsCreationService.getById).toHaveBeenCalledWith(1);
    expect(votingMaterialsCreationService.getByBusinessId).not.toHaveBeenCalled();
    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('PENDING');

    component.onRejectAction('reason');

    expect(votingMaterialsCreationService.reject).toHaveBeenCalledWith(1, 'reason');
    expect(votingMaterialsCreationService.reject).toHaveBeenCalledTimes(1);
    expect(votingMaterialsCreationService.approve).not.toHaveBeenCalled();
    expect(votingMaterialsCreationService.createAction).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/voting-materials-creation']);
  });

  it('should retrieve and create an voting material configuration', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/voting-materials-creation/1'
    };

    component = new VotingMaterialsCreationDetailComponent(router, route, snackBar,
      votingMaterialsCreationService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(votingMaterialsCreationService.getByBusinessId).toHaveBeenCalledTimes(1);
    expect(votingMaterialsCreationService.getByBusinessId).toHaveBeenCalledWith(1);
    expect(votingMaterialsCreationService.getById).not.toHaveBeenCalled();
    expect(component.action.id).toBeNull();
    expect(component.action.status).toBe('NOT_CREATED');

    component.onCreateAction();

    expect(votingMaterialsCreationService.createAction).toHaveBeenCalledWith(1);
    expect(votingMaterialsCreationService.createAction).toHaveBeenCalledTimes(1);
    expect(votingMaterialsCreationService.reject).not.toHaveBeenCalled();
    expect(votingMaterialsCreationService.approve).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/voting-materials-creation']);
  });
});
