/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { AuthenticationService } from './authentication.service';
import { UserService } from '../user/user.service';
import { of } from 'rxjs';

import { UserInfo } from '../user/user-info';

describe('AuthenticationService', () => {
  let authenticationService: AuthenticationService;
  const userService: UserService = <any> {
    currentUser: of<UserInfo>({username: 'test'})
  };

  beforeEach(() => {
    authenticationService = new AuthenticationService(userService);
  });

  it('should return authorization', (done) => {
    authenticationService.loginEvents.subscribe(() => {
      expect(authenticationService.currentUser).toBe('test');
      done();
    });
    expect(AuthenticationService.getAuthorization()).toBeNull();
    authenticationService.login('test', '123');
    expect(AuthenticationService.getAuthorization()).toBe(btoa('test:123'));
  });

  it('should remove authorization on logout', (done) => {
    authenticationService.logoutEvents.subscribe(() => {
      expect(authenticationService.currentUser).toBeUndefined();
      done();
    });
    authenticationService.login('test', '123');
    expect(AuthenticationService.getAuthorization()).toBe(btoa('test:123'));
    authenticationService.logout();
    expect(AuthenticationService.getAuthorization()).toBeNull();
  });
});
