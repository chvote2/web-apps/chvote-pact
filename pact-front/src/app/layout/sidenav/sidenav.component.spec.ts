/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { SidenavComponent } from './sidenav.component';
import { AuthenticationService } from '../../authentication/authentication.service';
import { PrivilegedActionsService } from '../../privileged-actions/privileged-actions.service';
import { Observable, of } from 'rxjs';

import { BaseUrlService } from '../../core/base-url-service/base-url.service';
import { HttpClient } from '@angular/common/http';
import { PollingService } from '../../polling/polling.service';
import { MatSidenav } from '@angular/material';
import { WindowRefService } from '../../core/window-ref-service/window-ref.service';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';
import { VersionService } from '../../version/version.service';

describe('SidenavComponent', () => {
  let sidenav: SidenavComponent;

  const matSideNavMock: MatSidenav = <any> {};
  const defaultWindowRefServiceMock: WindowRefService = <any> {
    nativeWindow: <any> {innerWidth: 1600}
  };
  const loadingSpinnerService: LoadingSpinnerService = <any> {
    loadingEvents: of(false)
  };
  const versionService: VersionService = <any> {
    currentVersion: of({buildNumber: 'DEV', buildTimestamp: new Date()})
  };

  it('should retrieve the number of pending actions when already logged in', () => {
    // given
    const authenticationService = <AuthenticationService> {
      currentUser: 'user',
      loginEvents: Observable.create(() => {
      }),
      logoutEvents: Observable.create(() => {
      })
    };
    const privilegedActionsService = <PrivilegedActionsService> {
      pendingCount: of(3)
    };
    sidenav = new SidenavComponent(authenticationService, privilegedActionsService,
      loadingSpinnerService, versionService, defaultWindowRefServiceMock);
    sidenav.sidenav = matSideNavMock;

    // when
    sidenav.ngOnInit();

    // then
    expect(sidenav.pendingCount).toBe(3);
    expect(sidenav.version.buildNumber).toBe('DEV');
  });

  it('should update the number of pending actions on login', () => {
    // given
    const authenticationService = <AuthenticationService> {
      currentUser: null,
      loginEvents: of({}),
      logoutEvents: Observable.create(() => {
      })
    };
    const privilegedActionsService = new PrivilegedActionsService(<HttpClient>{}, <PollingService>{},
      <BaseUrlService>{});
    sidenav = new SidenavComponent(authenticationService, privilegedActionsService,
      loadingSpinnerService, versionService, defaultWindowRefServiceMock);
    sidenav.sidenav = matSideNavMock;

    const pendingCountSpy = spyOnProperty(privilegedActionsService, 'pendingCount', 'get');
    pendingCountSpy.and.returnValue(of(42));

    // when
    sidenav.ngOnInit();

    // then
    expect(sidenav.pendingCount).toBe(42);
    expect(pendingCountSpy).toHaveBeenCalledTimes(1);
    expect(sidenav.version.buildNumber).toBe('DEV');
  });

  it('should fix the side nav for screen width equal to 1281px and above', () => {
    // given
    const authenticationService = <AuthenticationService> {
      currentUser: 'user',
      loginEvents: Observable.create(() => {
      }),
      logoutEvents: Observable.create(() => {
      })
    };
    const privilegedActionsService = <PrivilegedActionsService> {
      pendingCount: of(3)
    };

    const windowRefServiceMock: WindowRefService = <any> {
      nativeWindow: <any> {innerWidth: 1281}
    };

    sidenav = new SidenavComponent(authenticationService, privilegedActionsService,
      loadingSpinnerService, versionService, windowRefServiceMock);
    sidenav.sidenav = matSideNavMock;

    // when
    sidenav.configureSideNav();

    // then
    expect(sidenav.sidenav.mode).toBe('side');
    expect(sidenav.sidenav.opened).toBe(true);
  });

  it('should not fix the side nav for screen width up to 1280px', () => {
    // given
    const authenticationService = <AuthenticationService> {
      currentUser: 'user',
      loginEvents: Observable.create(() => {
      }),
      logoutEvents: Observable.create(() => {
      })
    };
    const privilegedActionsService = <PrivilegedActionsService> {
      pendingCount: of(3)
    };

    const windowRefServiceMock: WindowRefService = <any> {
      nativeWindow: <any> {innerWidth: 1280}
    };

    sidenav = new SidenavComponent(authenticationService, privilegedActionsService,
      loadingSpinnerService, versionService, windowRefServiceMock);
    sidenav.sidenav = matSideNavMock;

    // when
    sidenav.configureSideNav();

    // then
    expect(sidenav.sidenav.mode).toBe('over');
    expect(sidenav.sidenav.opened).toBe(false);
  });
});
