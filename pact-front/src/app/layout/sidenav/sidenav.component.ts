/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { AuthenticationService } from '../../authentication/authentication.service';
import { PrivilegedActionsService } from '../../privileged-actions/privileged-actions.service';
import { isNullOrUndefined } from 'util';
import { Subscription } from 'rxjs';
import { WindowRefService } from '../../core/window-ref-service/window-ref.service';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';
import { VersionInfo } from '../../version/version-info';
import { VersionService } from '../../version/version.service';

@Component({
  selector: 'pact-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  @ViewChild(MatSidenav) sidenav: MatSidenav;

  private _window: Window;
  private _version: VersionInfo = null;

  constructor(private authenticationService: AuthenticationService,
              private privilegedActionsService: PrivilegedActionsService,
              private loadingSpinnerService: LoadingSpinnerService,
              private versionService: VersionService,
              private windowRef: WindowRefService) {
    this._window = windowRef.nativeWindow;
  }

  private FIXED_SIDE_NAV_SCREEN_WIDTH = 1281;
  private _isLoading = false;
  private _pendingCount = 0;
  private _pendingCountSubscription: Subscription;
  private _loadingSpinnerSubscription: Subscription;

  @HostListener('window:resize', ['$event'])
  onResize(event: UIEvent) {
    this.configureSideNav();
  }

  configureSideNav() {
    if (this.shouldSideNavBeFixed()) {
      this.sidenav.mode = 'side';
      this.sidenav.opened = !isNullOrUndefined(this.authenticationService.currentUser);
    } else {
      this.sidenav.mode = 'over';
      this.sidenav.opened = false;
    }
  }

  get pendingCount(): number {
    return this._pendingCount;
  }

  get isLoading(): boolean {
    return this._isLoading;
  }

  ngOnInit(): void {
    if (isNullOrUndefined(this.authenticationService.currentUser)) {
      this.onLogout();
    } else {
      this.onLogin();
    }

    this.configureSideNav();

    this.authenticationService.loginEvents.subscribe(_ => this.onLogin());
    this.authenticationService.logoutEvents.subscribe(_ => this.onLogout());
  }

  toggle() {
    if (!this.shouldSideNavBeFixed()) {
      this.sidenav.toggle();
    }
  }

  closeSideNav() {
    if (!this.shouldSideNavBeFixed()) {
      this.sidenav.close();
    }
  }

  shouldSideNavBeFixed() {
    return isNullOrUndefined(this.authenticationService.currentUser) ||
           this._window.innerWidth >= this.FIXED_SIDE_NAV_SCREEN_WIDTH;
  }

  get version(): VersionInfo | undefined {
    return this._version;
  }

  private pendingCountUnsubscribe() {
    if (this._pendingCountSubscription) {
      this._pendingCountSubscription.unsubscribe();
      this._pendingCountSubscription = null;
    }
    this._pendingCount = 0;
  }

  private loadingSpinnerUnsubscribe() {

    if (this._loadingSpinnerSubscription) {
      this._loadingSpinnerSubscription.unsubscribe();
      this._loadingSpinnerSubscription = null;
    }
    this._isLoading = false;
  }


  private onLogout() {
    this.pendingCountUnsubscribe();
    this.loadingSpinnerUnsubscribe();
    this.configureSideNav();
    this._version = undefined;
  }

  private onLogin() {
    this.pendingCountUnsubscribe();
    this.loadingSpinnerUnsubscribe();
    this.subscribeToPendingCountUpdates();
    this.subscribeToLoadingSpinnerUpdates();
    this.configureSideNav();
    this.versionService.currentVersion.subscribe(version => this._version = version);
  }

  private subscribeToLoadingSpinnerUpdates() {
    this._loadingSpinnerSubscription =
      this.loadingSpinnerService.loadingEvents.subscribe(loading => this._isLoading = loading);
  }

  private subscribeToPendingCountUpdates() {
    this._pendingCountSubscription =
      this.privilegedActionsService.pendingCount.subscribe(pendingCount => this._pendingCount = pendingCount || 0);
  }
}
