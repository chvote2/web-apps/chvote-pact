/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { VotingMaterialsInvalidationRoutingModule } from './voting-materials-invalidation-routing.module';
import { VotingMaterialsInvalidationService } from './voting-materials-invalidation.service';
import { CoreModule } from '../core/core.module';
import { PollingModule } from '../polling/polling.module';
import { AuthenticationModule } from '../authentication/authentication.module';
import { MaterialModule } from '../material/material.module';
import { LayoutModule } from '../layout/layout.module';
import { VotingMaterialsInvalidationDetailComponent } from './voting-materials-invalidation-detail/voting-materials-invalidation-detail.component';
import { VotingMaterialsInvalidationTableComponent } from './voting-materials-invalidation-table/voting-materials-invalidation-table.component';
import { ActionDetailModule } from '../action-detail/action-detail.module';
import { PrinterStatsComponent } from './printer-stats/printer-stats.component';

@NgModule({
  imports: [
    CoreModule,
    PollingModule,
    MaterialModule,
    AuthenticationModule,
    LayoutModule,
    ActionDetailModule,
    VotingMaterialsInvalidationRoutingModule,
    FlexLayoutModule
  ],
  providers: [VotingMaterialsInvalidationService],
  declarations: [
    VotingMaterialsInvalidationDetailComponent,
    VotingMaterialsInvalidationTableComponent,
    PrinterStatsComponent]
})
export class VotingMaterialsInvalidationModule {
}
