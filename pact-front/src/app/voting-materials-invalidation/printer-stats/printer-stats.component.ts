/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Component, Input, OnInit } from '@angular/core';
import { VoterCreationStats } from '../model/voting-materials-invalidation';
import { ActionMetadata } from '../../action-detail/model/action-metadata';

@Component({
  selector: 'pact-printer-stats',
  templateUrl: './printer-stats.component.html',
  styleUrls: ['./printer-stats.component.scss']
})
export class PrinterStatsComponent implements OnInit {


  @Input()
  stats: VoterCreationStats;
  testingCardsMetadata: ActionMetadata;

  constructor() {
  }

  ngOnInit() {

    this.testingCardsMetadata = {
      controllerTestingCards: {value: this.stats.controllerTestingCards},
      nonPrintableTestingCards: {value: this.stats.nonPrintableTestingCards},
      printableTestingCards: {value: this.stats.printableTestingCards},
      printerTestingCards: {value: this.stats.printerTestingCards},
    }

  }

  get hasRealCards() {
    return this.stats.statByCountingCircle.length > 0;
  }

  get hasTestingCards() {
    return this.stats.controllerTestingCards +
           this.stats.printableTestingCards +
           this.stats.nonPrintableTestingCards +
           this.stats.printerTestingCards > 0
  }

}
