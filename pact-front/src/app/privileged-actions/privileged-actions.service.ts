/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/


import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseUrlService } from '../core/base-url-service/base-url.service';
import { Observable } from 'rxjs';
import { PrivilegedAction } from './model/privileged-action';
import { PollingService } from '../polling/polling.service';

@Injectable()
export class PrivilegedActionsService {

  constructor(private httpClient: HttpClient,
              private pollingService: PollingService,
              private baseUrlService: BaseUrlService) {
  }

  get pendingCount(): Observable<number> {
    return this.pollingService.poll<number>(
      `${this.baseUrlService.backendBaseUrl}/privileged-actions/pending-count`);
  }

  get pendingActions(): Observable<Array<PrivilegedAction>> {
    return this.pollingService.poll<Array<PrivilegedAction>>(
      `${this.baseUrlService.backendBaseUrl}/privileged-actions/pending-actions`);
  }

  getPendingAction(id: number): Observable<PrivilegedAction> {
    return this.httpClient.get<any>(`${this.baseUrlService.backendBaseUrl}/privileged-actions/${id}`).pipe(
      map(action => {
        action.creationDate = new Date(action.creationDate * 1000);
        action.statusDate = new Date(action.statusDate * 1000);
        return action;
      }));
  }
}
