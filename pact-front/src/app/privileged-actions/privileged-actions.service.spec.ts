/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { PrivilegedActionsService } from './privileged-actions.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { BaseUrlService } from '../core/base-url-service/base-url.service';
import { PrivilegedAction } from './model/privileged-action';
import { PollingService } from '../polling/polling.service';

describe('PrivilegedActionsService', () => {
  let privilegedActionsService: PrivilegedActionsService;
  const pollingService: PollingService = <any> {
    poll: jasmine.createSpy('poll').and.returnValue(of()),
    broadcast: jasmine.createSpy('broadcast')
  };
  const baseUrlServiceMock: BaseUrlService = <any> {
    backendBaseUrl: '/api',
  };

  it('should call the service to retrieve the number of pending actions', () => {
    const httpClientMock: HttpClient = <any> {};
    pollingService.poll = jasmine.createSpy('poll').and.returnValue(of(3));

    privilegedActionsService = new PrivilegedActionsService(httpClientMock, pollingService, baseUrlServiceMock);
    privilegedActionsService.pendingCount.subscribe(pendingCount => expect(pendingCount).toBe(3));
    expect(pollingService.poll).toHaveBeenCalledWith('/api/privileged-actions/pending-count');
  });

  it('should call the service to retrieve the list of pending actions', () => {
    const action1: PrivilegedAction = {
      id: 1,
      ownerName: 'toto',
      operationDate: new Date(Date.now()),
      creationDate: new Date(Date.now()),
      statusDate: new Date(Date.now()),
      operationName: '201709VP',
      status: 'PENDING',
      type: 'CONFIGURATION_DEPLOYMENT'
    };
    const action2: PrivilegedAction = {
      id: 2,
      ownerName: 'titi',
      operationDate: new Date(Date.now()),
      creationDate: new Date(Date.now()),
      statusDate: new Date(Date.now()),
      operationName: '201709VP',
      status: 'PENDING',
      type: 'VOTER_REGISTRY_PUBLICATION'
    };
    const httpClientMock: HttpClient = <any> {};
    pollingService.poll = jasmine.createSpy('poll').and.returnValue(of([action1, action2]));

    privilegedActionsService = new PrivilegedActionsService(httpClientMock, pollingService, baseUrlServiceMock);
    privilegedActionsService.pendingActions.subscribe(pendingActions => expect(pendingActions.length).toBe(2));
    expect(pollingService.poll).toHaveBeenCalledWith('/api/privileged-actions/pending-actions');
  });

  it('should call the service to retrieve a pending action by id', () => {
    const action: PrivilegedAction = {
      id: 1,
      ownerName: 'toto',
      operationDate: new Date(Date.now()),
      creationDate: new Date(Date.now()),
      statusDate: new Date(Date.now()),
      operationName: '201709VP',
      status: 'PENDING',
      type: 'CONFIGURATION_DEPLOYMENT'
    };
    const httpClientMock: HttpClient = <any> {
      get: jasmine.createSpy('get').and.returnValue(of(action))
    };

    privilegedActionsService = new PrivilegedActionsService(httpClientMock, pollingService, baseUrlServiceMock);
    privilegedActionsService.getPendingAction(1).subscribe(pendingAction => expect(pendingAction.id).toBe(1));
    expect(httpClientMock.get).toHaveBeenCalledWith('/api/privileged-actions/1');
  });
});
